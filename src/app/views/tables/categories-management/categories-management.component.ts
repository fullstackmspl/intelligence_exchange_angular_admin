import { Component, OnInit } from '@angular/core';
  import { TablesService } from '../tables.service';
  import { Router } from '@angular/router';
  import { DataService } from 'app/shared/services/dataservice.service';
  import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
  import { ApiService } from 'app/shared/services/api.service';
  import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
  import { MatSnackBar } from '@angular/material/snack-bar';
  import { ToastrService } from 'ngx-toastr'
  
  import { User } from 'app/shared/models/user.model';
  @Component({
    selector: 'app-categories-management',
    templateUrl: './categories-management.component.html',
    styleUrls: ['./categories-management.component.scss']
  })
  export class CategoriesManagementComponent implements OnInit {
  
    rows: any = [];
    columns: any = [];
    temp: any = [];
  
    isLoading: boolean;
    usersData: any = {};
    user = new User
    pageSize: number;
    pageNo = 1;
    submitted: any;
    userResponse: any;
    message: string = 'User Deleted Successfully!';
    list: any = [];
    adminList: any = [];
    admin: User;
    role: any;
    formData = new FormData();
  
    constructor(
      public route: Router,
      private dataservice: DataService,
      private confirmService: AppConfirmService,
      private apiservice: ApiService,
      private toast: ToastrService
  
    ) {
  
      // this.admin = this.auth.currentUser()
      // this.role = this.admin.role
  
    }
  
    ngOnInit() {
      // sessionStorage.clear();
      // this.getUsers();
      // this.getCategories();
    }
  
  //   getCategories() {
  //     this.apiservice.getCategories().subscribe((res: any) => {
  //       console.log('getCategories', res)
  //       this.list = res.data
  //       this.temp = this.list
  //       this.rows = this.temp;
  // console.log('temp1',this.temp)
  //     });
  //   }
    upload(event: any){
    //   var fileData: File;
    //   fileData = event.target.files[0];
    //   this.formData.append("excel",fileData);
    //  this.onSubmit(fileData)
    let formData = new FormData();
    var fileData: File;
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      fileData = event.target.files[0];
      formData.append("file", fileData);
      // this.onSubmit(formData);
      console.log(fileData)
    }
    }
  
    // onSubmit(fileData:any) {
    //     this.apiservice.uploadCagegory(fileData).subscribe((res: any) => { 
    //       console.log("res",res)  
    //     });
    //   }
    
  
  
    updateFilter(event) {
      const val = event.target.value.toLowerCase();
      var columns = Object.keys(this.temp[0]);
      columns.splice(columns.length - 1);
  
      if (!columns.length)
        return;
  
      const rows = this.temp.filter(function (d) {
        for (let i = 0; i <= columns.length; i++) {
          let column = columns[i];
          if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
            return true;
          }
        }
      });
      this.rows = rows;
    }
  
  
    // deletePlan(data) {
    //   this.confirmService.confirm({ message: `Delete ${data.category}?` }).subscribe(res => {
    //     if (res) {
    //       this.apiservice.deleteCategory(data.id).subscribe(res => {
    //         this.userResponse = res;
    //         this.getCategories();
  
    //         // this.route.navigateByUrl('/tables', { skipLocationChange: true }).then(() => {
    //         //   this.route.navigate(['tables/']);
    //         // })
  
  
    //       })
    //     }
    //   })
    // }
  
  
    add() {
      let data: any = {};
      data.isPLan = false;
      this.dataservice.setOption(data);
  
      this.route.navigate(['forms/add-category']);
    }
    update(category) {
      let data: any = category;
      data.isPLan = true;
      this.dataservice.setOption(data);
      this.route.navigate(['forms/add-category']);
    }
  
  
  }
  
  