import { Component, OnInit } from '@angular/core';

import { TablesService } from '../tables.service';
import { Router } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { ApiService } from 'app/shared/services/api.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ToastrService } from 'ngx-toastr'

import { AuthService } from 'app/shared/services/auth.service';
import { User } from 'app/shared/models/user.model';

@Component({
  selector: 'app-filter-table',
  templateUrl: './filter-table.component.html',
  styleUrls: ['./filter-table.component.css'],
  providers: [TablesService]
})
export class FilterTableComponent implements OnInit {
  rows: any = [];
  columns: any = [];
  temp: any = [];

  isLoading: boolean;
  usersData: any = {};
  user = new User
  pageSize: number;
  pageNo = 1;
  submitted: any;
  userResponse: any;
  message: string = 'User Deleted Successfully!';
  list: any = [];
  adminList: any = [];
  admin: User;
  role: any;
  users: any;
  adminData: any
  constructor(
    public route: Router,
    private dataservice: DataService,
    private confirmService: AppConfirmService,
    private apiservice: ApiService,
    private auth: AuthService,
    private toast: ToastrService

  ) {
  }

  ngOnInit() {
    sessionStorage.clear();
    this.getUsers();
    // this.getUsers();
    // this.getUsers();
  }
  getUsers() {
    console.log('add function calling')
    this.apiservice.getAllUsers().subscribe((res: any) => {
      console.log('resssssss', res)
      this.users = res.data
      this.rows = this.users.reverse();
    });
  }



  // getUsers() {
  //   this.apiservice.getUsers().subscribe((res: any) => {
  //     this.list = res.data
  //     this.temp = this.list
  //     // this.temp.forEach(element => {
  //     //   if (element.status == '1') {
  //     //     element.status = 'active'
  //     //   }
  //     //   if (element.status == '0') {
  //     //     element.status = 'inactive'
  //     //   }
  //     // })
  //     this.rows = this.temp;

  //   });
  // }


  // changeStatus(user) {
  //   if (user.verified === '1') {
  //     user.verified = '0'
  //     this.toast.warning('not verified')
  //   } else {
  //     user.verified = '1'
  //     this.toast.success('verified')
  //   }
  //   this.apiservice.updateUser(user.id,user).subscribe((res: any) => {
  //   })

  // }



  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    var columns = Object.keys(this.temp[0]);
    columns.splice(columns.length - 1);

    if (!columns.length)
      return;

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        let column = columns[i];
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });
    this.rows = rows;
  }


  deleteUser(data: any) {
    console.log(data)
    this.confirmService.confirm({ message: `Delete ${data.firstName} ${data.lastName}?` }).subscribe(res => {
      console.log('user idddd', data._id)
      if (data._id) {
        this.apiservice.deleteUserr(data._id).subscribe(res => {
          this.userResponse = res;
          this.route.navigateByUrl('/tables', { skipLocationChange: true }).then(() => {
            this.route.navigate(['tables/filter']);
          })


        })
      }
    })

  }

  userProfile(data) {
    data.isSelect = false;
    this.dataservice.setOption(data);
    localStorage.setItem('editUser', data.id);
    this.route.navigate(['profile/admin', data.id]);
  }

  add() {
    let data: any = {};
    this.dataservice.setOption(data);
    this.route.navigate(['forms/user']);
  }
  update(data) {
    // data.isSelect = true
    this.dataservice.setOption(data);
    this.route.navigate(['forms/user']);
  }
  Profile(data) {
    data.isSelect = false;
    this.dataservice.setOption(data);
    this.route.navigate(['profile/admin', data.id]);
  }
}
