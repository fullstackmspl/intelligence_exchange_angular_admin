import { Routes } from '@angular/router';

import { FullscreenTableComponent } from './fullscreen-table/fullscreen-table.component';
import { PagingTableComponent } from './paging-table/paging-table.component';
import { FilterTableComponent } from './filter-table/filter-table.component';
import { MaterialTableComponent } from './material-table/material-table.component';
import { UserReportsComponent } from './user-reports/user-reports.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { UserTableComponent } from './user-table/user-table.component';
import { PlansTableComponent } from './plans-table/plans-table.component';
import { HostUsersComponent } from './host-users/host-users.component';
import { LeaderBoardWeeklyComponent } from './leader-board-weekly/leader-board-weekly.component';
import { LeaderBoardMonthlyComponent } from './leader-board-monthly/leader-board-monthly.component';
import { GiftsTableComponent } from './gifts-table/gifts-table.component';
import { CategoriesManagementComponent } from './categories-management/categories-management.component';
import { RegionTypesComponent } from './region-types/region-types.component';
import { RegionsComponent } from './regions/regions.component';
import { StoreComponent } from './store/store.component';

export const TablesRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'fullscreen',
      component: FullscreenTableComponent,
      data: { title: 'Fullscreen', breadcrumb: 'FULLSCREEN' }
    }, {
      path: 'paging',
      component: PagingTableComponent,
      data: { title: 'Paging', breadcrumb: 'PAGING' }
    }, {
      path: 'userReposts',
      component: UserReportsComponent,
      data: { title: 'Reports', breadcrumb: 'Reports' }
    }, {
      path: 'permissions',
      component: PermissionsComponent,
      data: { title: 'Roles', breadcrumb: 'Roles' }
    }, {
      path: 'filter',
      component: FilterTableComponent,
      data: { title: 'Admin', breadcrumb: 'Admin' }
    },
    {
      path: 'regionTypes',
      component: RegionTypesComponent,
      data: { title: 'Region Types', breadcrumb: 'Region Types' }
    },
    {
      path: 'regions',
      component: RegionsComponent,
      data: { title: 'Regions', breadcrumb: 'Regions' }
    },
    {
      path: 'host-users',
      component: HostUsersComponent,
      data: { title: 'host-users', breadcrumb: 'host-users' }
    },
    {
      path: 'user',
      component: FilterTableComponent,
      data: { title: 'User', breadcrumb: 'User' }
    },
    {
      path: 'plan',
      component: PlansTableComponent,
      data: { title: 'Plan', breadcrumb: 'Plan' }
    },
    {
      path: 'gift',
      component: GiftsTableComponent,
      data: { title: 'Gift', breadcrumb: 'Gift' }
    },
    {
      path: 'categories',
      component: CategoriesManagementComponent,
      data: { title: 'Categories', breadcrumb: 'Categories' }
    },
    {
      path: 'weekly',
      component: LeaderBoardWeeklyComponent,
      data: { title: 'weekly', breadcrumb: 'weekly' }
    },
    {
      path: 'monthly',
      component: LeaderBoardMonthlyComponent,
      data: { title: 'monthly', breadcrumb: 'monthly' }
    }, {
      path: 'mat-table',
      component: MaterialTableComponent,
      data: { title: 'Material TAble', breadcrumb: 'Material Table' }
    },
    {
      path: 'store',
      component: StoreComponent,
      data: { title: 'store', breadcrumb: 'store' }
    }, 
  ]
  }
];
