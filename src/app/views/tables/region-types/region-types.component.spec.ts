import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionTypesComponent } from './region-types.component';

describe('RegionTypesComponent', () => {
  let component: RegionTypesComponent;
  let fixture: ComponentFixture<RegionTypesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegionTypesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
