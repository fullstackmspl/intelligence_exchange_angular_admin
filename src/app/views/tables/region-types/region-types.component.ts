


  import { Component, OnInit } from '@angular/core';

  import { TablesService } from '../tables.service';
  import { Router } from '@angular/router';
  import { DataService } from 'app/shared/services/dataservice.service';
  import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
  import { ApiService } from 'app/shared/services/api.service';
  import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
  import { MatSnackBar } from '@angular/material/snack-bar';
  import { ToastrService } from 'ngx-toastr'
  
  import { AuthService } from 'app/shared/services/auth.service';
  import { User } from 'app/shared/models/user.model';
  
  @Component({
    selector: 'app-region-types',
    templateUrl: './region-types.component.html',
    styleUrls: ['./region-types.component.scss']
  })
  export class RegionTypesComponent implements OnInit {
    rows: any = [];
    columns: any = [];
    temp: any = [];
  
    isLoading: boolean;
    usersData: any = {};
    user = new User
    pageSize: number;
    pageNo = 1;
    submitted: any;
    userResponse: any;
    message: string = 'User Deleted Successfully!';
    list: any = [];
    adminList: any = [];
    admin: User;
    role: any;
  
    constructor(
      public route: Router,
      private dataservice: DataService,
      private confirmService: AppConfirmService,
      private apiservice: ApiService,
      private auth: AuthService,
      private toast: ToastrService
  
    ) {
  
      this.admin = this.auth.currentUser()
    }
  
    ngOnInit() {
      sessionStorage.clear();
      // this.getRegiontypes();
  
    }
  
    // getRegiontypes() {
    //   this.apiservice.getRegiontypes().subscribe((res: any) => {
    //     this.list = res.data
    //     this.temp = this.list
    //     console.log('region types', this.list)
  
    //     // this.temp.forEach(element => {
    //     //   if (element.status == '1') {
    //     //     element.status = 'active'
    //     //   }
    //     //   if (element.status == '0') {
    //     //     element.status = 'inactive'
    //     //   }
    //     // })
    //     this.rows = this.temp;
  
    //   });
    // }
  
  
    // changeStatus(user) {
    //   if (user.status === '1') {
    //     user.status = '0'
    //     this.toast.warning('Status Deactivated')
    //   } else {
    //     user.status = '1'
    //     this.toast.success('Status Activated')
    //   }
    //   this.apiservice.updateUser(user).subscribe((res: any) => {
    //   })
  
    // }
  
  
  
    updateFilter(event) {
      const val = event.target.value.toLowerCase();
      var columns = Object.keys(this.temp[0]);
      columns.splice(columns.length - 1);
  
      if (!columns.length)
        return;
  
      const rows = this.temp.filter(function (d) {
        for (let i = 0; i <= columns.length; i++) {
          let column = columns[i];
          if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
            return true;
          }
        }
      });
      this.rows = rows;
    }
  
  
    // deleteUser(data) {
    //   this.confirmService.confirm({ message: `Delete ${data.name}?` }).subscribe(res => {
    //     if (res) {
    //       this.apiservice.deleteUser(data.id).subscribe(res => {
    //         this.userResponse = res;
    //         this.route.navigateByUrl('/tables', { skipLocationChange: true }).then(() => {
    //           this.route.navigate(['tables/filter']);
    //         })
  
  
    //       })
    //     }
    //   })
    // }
  
    userProfile(data) {
      data.isSelect = false;
      this.dataservice.setOption(data);
      this.route.navigate(['profile/admin', data.id]);
    }
  
    add() {
      let data: any = {};
      this.dataservice.setOption(data);
      this.route.navigate(['forms/add-regiontype']);
    }
    update(data) {
      this.dataservice.setOption(data);
      this.route.navigate(['forms/add-regiontype']);
    }
    Profile(data) {
      data.isSelect = false;
      this.dataservice.setOption(data);
      this.route.navigate(['forms/view-profile', data.id]);
    }
  }
  