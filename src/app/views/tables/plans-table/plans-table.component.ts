import { Component, OnInit } from '@angular/core';

import { TablesService } from '../tables.service';
import { Router } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { ApiService } from 'app/shared/services/api.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ToastrService } from 'ngx-toastr'

import { AuthService } from 'app/shared/services/auth.service';
import { User } from 'app/shared/models/user.model';

@Component({
  selector: 'app-plans-table',
  templateUrl: './plans-table.component.html',
  styleUrls: ['./plans-table.component.css'],
  providers: [TablesService]
})
export class PlansTableComponent implements OnInit {
  rows: any = [];
  columns: any = [];
  temp: any = [];

  isLoading: boolean;
  usersData: any = {};
  user = new User
  pageSize: number;
  pageNo = 1;
  submitted: any;
  userResponse: any;
  message: string = 'User Deleted Successfully!';
  list: any = [];
  adminList: any = [];
  admin: User;
  role: any;


  constructor(
    public route: Router,
    private dataservice: DataService,
    private confirmService: AppConfirmService,
    private apiservice: ApiService,
    private auth: AuthService,
    private toast: ToastrService

  ) {

    this.admin = this.auth.currentUser()
    this.role = this.admin.role

  }

  ngOnInit() {
    // sessionStorage.clear();
    // this.getUsers();
    // this.getPlans();
  }

  // getPlans() {
  //   this.apiservice.getPlans().subscribe((res: any) => {
  //     console.log('plans', res)
  //     this.list = res.plans
  //     this.temp = this.list
  //     this.rows = this.temp;

  //   });
  // }



  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    var columns = Object.keys(this.temp[0]);
    columns.splice(columns.length - 1);

    if (!columns.length)
      return;

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        let column = columns[i];
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });
    this.rows = rows;
  }


  // deletePlan(data) {
  //   this.confirmService.confirm({ message: `Delete ${data.plan}?` }).subscribe(res => {
  //     if (res) {
  //       this.apiservice.deletePlan(data.id).subscribe(res => {
  //         this.userResponse = res;

  //         this.route.navigateByUrl('/tables', { skipLocationChange: true }).then(() => {
  //           this.route.navigate(['tables/plan']);
  //         })


  //       })
  //     }
  //   })
  // }


  add() {
    let data: any = {};
    data.isPLan = false;
    this.dataservice.setOption(data);

    this.route.navigate(['forms/add-plan']);
  }
  update(plan) {
    let data: any = plan;
    data.isPLan = true;
    this.dataservice.setOption(data);
    this.route.navigate(['forms/add-plan']);
  }


}
