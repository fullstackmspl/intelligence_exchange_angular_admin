import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { ApiService } from 'app/shared/services/api.service';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { AuthService } from 'app/shared/services/auth.service';
import { DataService } from 'app/shared/services/dataservice.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {
  rows: any = [];
  columns: any = [];
  temp: any = [];

  isLoading: boolean;
  usersData: any = {};
  pageSize: number;
  pageNo = 1;
  submitted: any;
  userResponse: any;
  message: string = 'User Deleted Successfully!';
  list: any = [];
  adminList: any = [];
  role: any;
  stores: any;
  userId: any;
  constructor(
    public route: Router,
    private dataservice: DataService,
    private confirmService: AppConfirmService,
    private apiservice: ApiService,
    private auth: AuthService,
    private toast: ToastrService,
    private activatedRoute: ActivatedRoute
  ) {
    this.userId = this.activatedRoute.snapshot.params['id'];
    console.log('user id ==>', this.userId)
  }

  ngOnInit(): void {
    this.getStores();
  }
  getStores() {
    console.log('add function calling')
    this.apiservice.getAllStore().subscribe((res: any) => {
      console.log('resssssss', res)
      this.stores = res.data
      this.rows = this.stores.reverse()
    });
  }
  add() {
    let data: any = {};
    this.dataservice.setOption(data);
    this.route.navigate(['forms/add-store']);
  }
  update(data) {
    this.dataservice.setOption(data);
    this.route.navigate(['forms/add-store']);
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    var columns = Object.keys(this.temp[0]);
    columns.splice(columns.length - 1);

    if (!columns.length)
      return;

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        let column = columns[i];
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });
    this.rows = rows;
  }


}
