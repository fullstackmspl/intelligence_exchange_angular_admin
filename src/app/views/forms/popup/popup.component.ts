import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DataService } from 'app/shared/services/dataservice.service';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {
  Competitores: FormGroup
  constructor(private dialogRef: MatDialogRef<PopupComponent>,
    private dataservice: DataService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {
    this.Competitores = new FormGroup({
      companyName: new FormControl('',),
      fulfilledCombinedDemand: new FormControl('',),
      fulfilledBrandDemand: new FormControl('',),
      fulfilledDemandforBrand: new FormControl('',),
    })
  }
  close() {
    this.dialogRef.close()
  }
  add() {
    console.log(this.Competitores.value)
    this.dataservice.setOption(this.Competitores.value)
    this.close()
  }
}
