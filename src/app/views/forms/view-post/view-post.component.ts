import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ApiService } from 'app/shared/services/api.service';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { DataService } from 'app/shared/services/dataservice.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.scss']
})
export class ViewPostComponent implements OnInit {
  post: any;
  likesList: any;
  lastLike: any;
  totalLikes: any;
  commentList: any;
  lastComment: any;
  totalComments: any;

  userObj: any = [];

  constructor(
    private apiservice: ApiService,
    private toast: ToastrService,
    private confirmService: AppConfirmService,
    public route: Router,
    private dataservice: DataService,
    public dialogRef: MatDialogRef<ViewPostComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.post = this.data
  }

  ngOnInit(): void {
    // this.getLikes();
    // this.getComments();
  }

  // deleteComment(comment) {
  //   this.confirmService.confirm({ message: `Delete Comment?` }).subscribe(res => {
  //     if (res) {
  //       this.apiservice.deleteComment(comment._id).subscribe((res: any) => {
  //         this.toast.success('Post Deleted Successfully');
  //         this.dialogRef.close(true);
  //       });
  //     }
  //   });
  // }

  // deletePost() {
  //   this.confirmService.confirm({ message: `Delete Post?` }).subscribe(res => {
  //     if (res) {
  //       this.apiservice.deletePost(this.post._id).subscribe((res: any) => {
  //         this.toast.success('Post Deleted Successfully');
  //         this.dialogRef.close(true);
  //       });
  //     }
  //   });
  // }

  // getComments() {
  //   this.apiservice.getComments(this.post._id).subscribe((res: any) => {
  //     this.commentList = res.reverse();
  //   });
  // }

  // getLikes() {
  //   this.apiservice.getLikes(this.post._id).subscribe((res: any) => {
  //     this.likesList = res.reverse();
  //     if (this.likesList && this.likesList.length) {
  //       for (let item of this.likesList) {
  //         this.lastLike = item.userId.userName;
  //         // this.userObj.push({userName: item.userId.userName, userAvatar:item.userId.avatar})
  //       }
  //       this.totalLikes = this.likesList.length - 1;
  //     }
  //   });
  // }

  selectUser(data) {
    this.dataservice.setOption(data);
    this.route.navigate(['forms/view-profile']);
    this.dialogRef.close(true);
  }

  back(): void {
    this.dialogRef.close();
  }

}
