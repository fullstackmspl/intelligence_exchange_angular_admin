import { I } from "@angular/cdk/keycodes";
import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import {
  MatSnackBar,
  MatSnackBarConfig,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from "app/shared/services/api.service";
import { AppLoaderService } from "app/shared/services/app-loader/app-loader.service";
import { DataService } from "app/shared/services/dataservice.service";
import { TablesService } from "app/views/tables/tables.service";
import { ToastrService } from "ngx-toastr";
import { NgxUiLoaderService } from "ngx-ui-loader";

@Component({
  selector: "app-add-regions",
  templateUrl: "./add-regions.component.html",
  styleUrls: ["./add-regions.component.scss"],
})
export class AddRegionsComponent implements OnInit {
  formData = {};
  rows: any = [];
  columns: any = [];
  temp: any = [];
  regionForm: FormGroup;
  region: any;

  rolesList: any = [];
  FormData: any;
  submitted: boolean;
  usersData: any = {};
  responseData: any;
  res: any[];
  show: boolean = true;
  isLoading: boolean;
  message: string = "User Added Successfully!";
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = "center";
  verticalPosition: MatSnackBarVerticalPosition = "bottom";
  userResponse: any;
  formName: string;
  regiontypes: any;
  selectedRegiontype: any;
  selectedContinental: any;
  selectedNational: any;
  selectedRegional: any;
  parentId: any;
  regions: any;
  constructor(
    private service: TablesService,
    public dataRoute: ActivatedRoute,
    private dataservice: DataService,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private ngxLoader: NgxUiLoaderService,
    private route: Router,
    private toast: ToastrService,
    private loader: AppLoaderService
  ) {
    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;

    let data = dataservice.getOption();

    if (Object.keys(data).length === 0 && data.constructor === Object) {
      this.formName = "Add Region";
    } else {
      this.region = data;
      this.formName = "Edit Region";
    }
  }
  updateRegion() {
    switch (this.selectedRegiontype) {
      case 1:
        this.parentId = 0;
        console.log("1");
        console.log(this.selectedRegiontype, "value");
        break;
      case 2:
        this.parentId = 1;
        console.log("2");
        console.log(this.selectedRegiontype, "value");
        break;

      case 3:
        this.parentId = this.selectedContinental;
        console.log("3");
        console.log(this.selectedRegiontype, "value");
        break;

      case 4:
        this.parentId = this.selectedNational;
        console.log("4");
        console.log(this.selectedRegiontype, "value");
        break;

      case 5:
        this.parentId = this.selectedRegional;
        console.log("5");
        console.log(this.selectedRegiontype, "value");
        break;

      default:
        console.log(this.selectedRegiontype, "value");
        //  this.parentId= 0  ;
        break;
    }
    let body = {
      parentId: this.parentId,
      level: this.selectedRegiontype,
      name: this.regionForm.controls.name.value,
      regiontypeId: this.selectedRegiontype,
    };
    console.log("body", body);
    // this.apiservice.updateRegion(this.region.id,body).subscribe((res: any) => {
    //   console.log("resssssss", res);
    // });
  }
  back() {
    this.route.navigate(["tables/regions"]);
  }

  ngOnInit() {
    // this.region = this.dataservice.getOption();
    // console.log(this.region);
    // this.regionForm = new FormGroup({
    //   name: new FormControl("", Validators.required),
    // });
    // this.getRegiontypes();
    // this.getRegions();
    // if (this.region.id) {
    //   this.selectedRegiontype = this.region?.level;
    //   switch (this.selectedRegiontype) {
    //     case 1:
    //       this.parentId = 0;
    //       console.log("1");
    //       console.log(this.selectedRegiontype, "value");
    //       break;
    //     case 2:
    //       this.apiservice
    //         .getRegionById(this.region.parentId)
    //         .subscribe((res: any) => {});
    //       break;

    //     case 3:
    //       this.apiservice
    //         .getRegionById(this.region.parentId)
    //         .subscribe((res: any) => {
    //           this.selectedContinental = res?.data?.id;

    //         });
    //       break;

    //     case 4:
    //       this.apiservice
    //         .getRegionById(this.region.parentId)
    //         .subscribe((res: any) => {
    //           this.selectedNational = res?.data?.id;
    //           this.apiservice
    //           .getRegionById(res?.data.parentId)
    //           .subscribe((response: any) => {
    //             this.selectedContinental = response?.data?.id;
    //           });
    //         });
    //       break;

    //     case 5:
    //       this.apiservice
    //         .getRegionById(this.region.parentId)
    //         .subscribe((res: any) => {
    //           this.selectedRegional = res?.data?.id;
    //           this.apiservice
    //             .getRegionById(res?.data.parentId)
    //             .subscribe((resp: any) => {
    //               this.selectedNational = resp?.data?.id;
    //               this.apiservice
    //                 .getRegionById(resp?.data.parentId)
    //                 .subscribe((response: any) => {
    //                   this.selectedContinental = response?.data?.id;
    //                 });
    //             });
    //         });
    //       break;

    //     default:
    //       console.log(this.selectedRegiontype, "value");
    //       //  this.parentId= 0  ;
    //       break;
    //   }
    //   if (this.selectedRegiontype != 0 && this.selectedRegiontype != 1) {
    //   }
    // }
  }
  // getRegiontypes() {
  //   this.apiservice.getRegiontypes().subscribe((res: any) => {
  //     this.regiontypes = res.data;
  //     this.temp = this.regiontypes;
  //     console.log("region types", this.regiontypes);

  //     // this.temp.forEach(element => {
  //     //   if (element.status == '1') {
  //     //     element.status = 'active'
  //     //   }
  //     //   if (element.status == '0') {
  //     //     element.status = 'inactive'
  //     //   }
  //     // })
  //     this.rows = this.temp;
  //   });
  // }
  // getRegions() {
  //   this.apiservice.getRegions().subscribe((res: any) => {
  //     this.regions = res.data;

  //     console.log("regions", this.regions);
  //   });
  // }
  // addRegion() {
  //   switch (this.selectedRegiontype) {
  //     case 1:
  //       this.parentId = 0;
  //       console.log("1");
  //       console.log(this.selectedRegiontype, "value");
  //       break;
  //     case 2:
  //       this.parentId = 1;
  //       console.log("2");
  //       console.log(this.selectedRegiontype, "value");
  //       break;

  //     case 3:
  //       this.parentId = this.selectedContinental;
  //       console.log("3");
  //       console.log(this.selectedRegiontype, "value");
  //       break;

  //     case 4:
  //       this.parentId = this.selectedNational;
  //       console.log("4");
  //       console.log(this.selectedRegiontype, "value");
  //       break;

  //     case 5:
  //       this.parentId = this.selectedRegional;
  //       console.log("5");
  //       console.log(this.selectedRegiontype, "value");
  //       break;

  //     default:
  //       console.log(this.selectedRegiontype, "value");
  //       //  this.parentId= 0  ;
  //       break;
  //   }
  //   let body = {
  //     parentId: this.parentId,
  //     level: this.selectedRegiontype,
  //     name: this.regionForm.controls.name.value,
  //     regiontypeId: this.selectedRegiontype,
  //   };
  //   console.log("body", body);
  //   this.apiservice.addRegion(body).subscribe((res: any) => {
  //     console.log("resssssss", res);
  //   });
  // }

  onSubmit() {
    this.submitted = true;
    if (!this.region.id) {
      // return this.addRegion();
    } else {
      this.updateRegion()
    }
  }
}
