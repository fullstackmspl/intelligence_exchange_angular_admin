import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatStepperModule } from '@angular/material/stepper';
import { FlexLayoutModule } from '@angular/flex-layout';
import { QuillModule } from 'ngx-quill';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FileUploadModule } from 'ng2-file-upload';

import { BasicFormComponent } from './basic-form/basic-form.component';
import { RichTextEditorComponent } from './rich-text-editor/rich-text-editor.component';
import { FileUploadComponent } from './file-upload/file-upload.component';

import { FormsRoutes } from "./forms.routing";
import { WizardComponent } from './wizard/wizard.component';
import { UserManageComponent } from './user-manage/user-manage.component';
import { MatSelectModule } from '@angular/material/select';
import { UpdateUserComponent } from './update-user/update-user.component';
import { AddPlanComponent } from './add-plan/add-plan.component';
import { ViewprofileComponent } from './viewprofile/viewprofile.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { ViewPostComponent } from './view-post/view-post.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FollowUserComponent } from './follow-user/follow-user.component';
import { MatMenuModule } from '@angular/material/menu';
import { AddGiftComponent } from './add-gift/add-gift.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { AddRegionsComponent } from './add-regions/add-regions.component';
import { AddRegionTypeComponent } from './add-region-type/add-region-type.component';
import { AddStoreComponent } from './add-store/add-store.component';
import { AgmCoreModule } from '@agm/core';
import { PopupComponent } from './popup/popup.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatListModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatRadioModule,
    MatCheckboxModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    MatStepperModule,
    FlexLayoutModule,
    MatTooltipModule,
    MatGridListModule,
    MatMenuModule,
    QuillModule,
    NgxDatatableModule,
    FileUploadModule,
    RouterModule.forChild(FormsRoutes),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyBhVQKXquGT9zTznHXq1A6YJCcrrC5HWv8",
      libraries: ['places']
    }),
  ],
  declarations: [BasicFormComponent,
    RichTextEditorComponent,
    FileUploadComponent,
    WizardComponent,
    UserManageComponent,
    UpdateUserComponent,
    ViewprofileComponent,
    ViewPostComponent,
    FollowUserComponent,
    AddPlanComponent,
    AddGiftComponent,
    AddCategoryComponent,
    AddRegionsComponent,
    AddRegionTypeComponent,
    AddStoreComponent,
    PopupComponent,
  ]
})
export class AppFormsModule { }