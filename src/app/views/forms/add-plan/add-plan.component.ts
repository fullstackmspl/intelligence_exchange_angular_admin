import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'app/shared/services/api.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { DataService } from 'app/shared/services/dataservice.service';
import { TablesService } from 'app/views/tables/tables.service';
import { CustomValidators } from 'ngx-custom-validators';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-add-plan',
  templateUrl: './add-plan.component.html',
  styleUrls: ['./add-plan.component.css']
})
export class AddPlanComponent implements OnInit {
  formData = {};
  rows: any = [];
  columns: any = [];
  temp: any = [];
  planForm: FormGroup;
  plan: any = {
    name: '',
    plan: '',
    amount: '',
    coins: '',
    beforediscount: '',
    afterdiscount: '',
    discountpercent: '',
    planId: ''
  };
  permissions: {}


  rolesList: any = [];
  FormData: any;
  submitted: boolean;
  usersData: any = {};
  responseData: any;
  res: any[];
  show: boolean = true;
  isLoading: boolean;
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  userResponse: any;
  formName: string


  constructor(private service: TablesService,
    public dataRoute: ActivatedRoute,
    private dataservice: DataService,
    private apiservice: ApiService,
    private route: Router,
    private toast: ToastrService,
    private loader: AppLoaderService) {
    this.plan = dataservice.getOption()
    console.log('plan data ', this.plan)
    this.permissions = {
      view: false,
      add: false,
      edit: false,
      all: false
    }
  }



  back() {
    this.route.navigate(['tables/plan']);
  }



  ngOnInit() {

    this.planForm = new FormGroup({
      name: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
      coins: new FormControl('', Validators.required),
      beforediscount: new FormControl('', Validators.required),
      afterdiscount: new FormControl('', Validators.required),
      discountpercent: new FormControl('', Validators.required),
    });
  }

  onChange(value) {
    let permissions = Object.keys(this.permissions);
    // this.permissions[value] = true
    for (let permission of permissions) {
      if (permission == value) {
        this.permissions[permission] = true
      } else {
        this.permissions[permission] = false
      }

    }
  }


  // addPlan() {
  //   console.log('add function calling')
  //   this.apiservice.addPlans(this.plan).subscribe((res: any) => {
  //     console.log('resssssss', res)
  //   });
  // }
  // updatelan() {
  //   console.log('update function calling')
  //   this.plan.planId = this.plan.id;
  //   this.apiservice.updatePlan(this.plan).subscribe((res: any) => {
  //     console.log('resssssss', res)
  //   });
  // }

  onSubmit() {
    this.submitted = true;
    console.log(this.planForm.value)
    if (!this.plan.id) {
      // return this.addPlan();
    }
    else {
      // return this.updatelan();
    }
  }
}


