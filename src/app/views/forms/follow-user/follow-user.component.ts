import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ApiService } from 'app/shared/services/api.service';
import { DataService } from 'app/shared/services/dataservice.service';

@Component({
  selector: 'app-follow-user',
  templateUrl: './follow-user.component.html',
  styleUrls: ['./follow-user.component.scss']
})
export class FollowUserComponent implements OnInit {
  user: any;
  followerList: any;
  followingList: any;
  userImg: any;

  constructor(
    private apiservice: ApiService,
    public route: Router,
    private dataservice: DataService,
    public dialogRef: MatDialogRef<FollowUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

  ngOnInit(): void {
    if (this.data.status === 'follower') {
      // this.getFollowers();
    } else {
      // this.getFollowing();
    }
  }



  // getFollowers() {
  //   this.apiservice.getFollowers(this.data.userId).subscribe((res: any) => {
  //     this.followerList = res
  //     // this.userImg = this.followerList.avatar
  //   });
  // };

  // getFollowing() {
  //   this.apiservice.getFollowing(this.data.userId).subscribe((res: any) => {
  //     this.followingList = res
  //     // this.userImg = this.followingList.avatar
  //   });
  // }

  profile(data) {
    data.isSelect = false;
    this.dataservice.setOption(data);
    this.route.navigate(['forms/view-profile']);
    this.dialogRef.close(true);
  }


  back(): void {
    this.dialogRef.close();
  }
}
