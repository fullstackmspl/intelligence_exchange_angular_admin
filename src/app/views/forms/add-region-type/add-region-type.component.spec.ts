import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRegionTypeComponent } from './add-region-type.component';

describe('AddRegionTypeComponent', () => {
  let component: AddRegionTypeComponent;
  let fixture: ComponentFixture<AddRegionTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddRegionTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRegionTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
