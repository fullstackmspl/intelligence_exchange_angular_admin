


  import { Component, OnInit } from '@angular/core';
  import { FormGroup, FormControl, Validators } from '@angular/forms';
  
  import { ActivatedRoute, Router } from '@angular/router';
  import { ApiService } from 'app/shared/services/api.service';
  import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
  import { DataService } from 'app/shared/services/dataservice.service';
  import { TablesService } from 'app/views/tables/tables.service';
  import { CustomValidators } from 'ngx-custom-validators';
  import { ToastrService } from 'ngx-toastr';
  
  
  @Component({
    selector: 'app-add-region-type',
    templateUrl: './add-region-type.component.html',
    styleUrls: ['./add-region-type.component.scss']
  })
  export class AddRegionTypeComponent implements OnInit {
    formData = {};
    rows: any = [];
    columns: any = [];
    temp: any = [];
    regionTypeForm: FormGroup;
    regionTypes: any 
    permissions: {}
  
  
    rolesList: any = [];
    FormData: any;
    submitted: boolean;
    usersData: any = {};
    responseData: any;
    res: any[];
    show: boolean = true;
    isLoading: boolean;
    action: boolean = true;
    setAutoHide: boolean = true;
    autoHide: number = 4000;
    userResponse: any;
    formName: string
    formData1 = new FormData();
    fileData:any= File;
    constructor(private service: TablesService,
      public dataRoute: ActivatedRoute,
      private dataservice: DataService,
      private apiservice: ApiService,
      private route: Router,
      private toast: ToastrService,
      private loader: AppLoaderService) {
      this.regionTypes = dataservice.getOption()
      console.log('plan data ', this.regionTypes)

      this.permissions = {
        view: false,
        add: false,
        edit: false,
        all: false
      }
    }
  
  
  
    back() {
      this.route.navigate(['tables/regionTypes']);
    }
  
  
  
    ngOnInit() {
  if(this.regionTypes.isPLan===true){
    this.formName="Update region type"
  }
  else{
    this.formName="Create region type"
  }
      this.regionTypeForm = new FormGroup({
        name: new FormControl('', Validators.required),
      });
    }
  
    onChange(value) {
      let permissions = Object.keys(this.permissions);
      // this.permissions[value] = true
      for (let permission of permissions) {
        if (permission == value) {
          this.permissions[permission] = true
        } else {
          this.permissions[permission] = false
        }
  
      }
    }
  
  
    // addCategory() {
    //   console.log('add function calling')
    //   this.apiservice.addRegiontype(this.regionTypeForm.value).subscribe((res: any) => {
    //     console.log('resssssss', res)
    //   });
    // }
    // updatCategory() {
    //   console.log('update function calling')
    //   // this.category.planId = this.category.id;
    //   this.apiservice.updateRegiontype(this.regionTypes.id,this.regionTypes).subscribe((res: any) => {
    //     console.log('resssssss', res)
    //   });
    // }
  
    onSubmit() {
  
      this.submitted = true;
      console.log(this.regionTypeForm.value)
      if (!this.regionTypes.id) {
        // return this.addCategory();
      }
      else {
        // return this.updatCategory();
      }
    }
  }
  
  
  