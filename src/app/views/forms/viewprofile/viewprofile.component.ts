import { Component, OnInit } from '@angular/core';
import { DataService } from 'app/shared/services/dataservice.service';
import { ApiService } from 'app/shared/services/api.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { MatDialog } from '@angular/material/dialog';
import { ViewPostComponent } from '../view-post/view-post.component';
import { FollowUserComponent } from '../follow-user/follow-user.component';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.component.html',
  styleUrls: ['./viewprofile.component.scss']
})
export class ViewprofileComponent implements OnInit {

  user: any;
  isLoading: boolean;
  postList: any;
  userImg: any;
  userId: any = {
    userId: ''
  }
  constructor(
    private apiservice: ApiService,
    private loader: AppLoaderService,
    public dialog: MatDialog,
    public dataservice: DataService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.activatedRoute.params.subscribe(params => {
      this.userId.userId = params['id'];
      // this.getMedia();
      // this.getCurrentUser()
    });
  }

  ngOnInit(): void {
    // this.currentUser = this.dataservice.getOption();
  }

  // getMedia() {
  //   this.apiservice.getMedia(this.userId.userId).subscribe((res: any) => {
  //     this.postList = res

  //   });
  // }

  // getCurrentUser() {
  //   this.apiservice.getCurrentUser(this.userId).subscribe((res: any) => {
  //     this.user = res
  //     this.userImg = this.user.avatar
  //   });
  // }

  // selectPost(post): void {
  //   const dialogRef = this.dialog.open(ViewPostComponent, {
  //     panelClass: 'selectPost',
  //     width: '1000px',
  //     data: post
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       this.currentUser = this.dataservice.getOption();
  //       this.getCurrentUser();
  //       this.getMedia();
  //     }
  //   });
  // }

  // getFollowers(user) {
  //   const dialogRef = this.dialog.open(FollowUserComponent, {
  //     panelClass: 'selectPost',
  //     width: '500px',
  //     height: '250px',
  //     data: { userId: user._id, status: 'follower' }
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       this.currentUser = this.dataservice.getOption();
  //       this.getCurrentUser();
  //       this.getMedia();
  //     }
  //   });
  // }

  // getFollowing(user) {
  //   const dialogRef = this.dialog.open(FollowUserComponent, {
  //     panelClass: 'selectPost',
  //     width: '500px',
  //     height: '250px',
  //     data: { userId: user._id, status: 'following' }
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     if (result) {
  //       this.currentUser = this.dataservice.getOption();
  //       this.getCurrentUser();
  //       this.getMedia();
  //     }
  //   });
  // }
}
