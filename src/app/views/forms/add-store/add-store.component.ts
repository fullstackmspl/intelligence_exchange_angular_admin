import { MapsAPILoader } from '@agm/core';
import { HttpClient } from '@angular/common/http';
// import { google } from '@agm/core/services/google-maps-types';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'app/shared/services/api.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { DataService } from 'app/shared/services/dataservice.service';
import { TablesService } from 'app/views/tables/tables.service';
import { ScrollableDialogOverviewComponent } from 'assets/examples/material/scrollable-dialog/scrollable-dialog-overview/scrollable-dialog-overview.component';
import { ToastrService } from 'ngx-toastr';
import { PopupComponent } from '../popup/popup.component';
interface marker {
  lat: number;
  lng: number;
  draggable: boolean;
  zoom: number;
}
@Component({
  selector: 'app-add-store',
  templateUrl: './add-store.component.html',
  styleUrls: ['./add-store.component.scss']
})
export class AddStoreComponent implements OnInit {
  storeForm: FormGroup;
  store: any = {
    storeId: '',
    address: '',
    city: '',
    zipCode: '',
    staff: '',
    hoursOpen: '',
    demandForServiceStaff: '',
    servicesFulfilled: '',
    incomingCall: '',
    answeredCalls: '',
    capacityForAppointments: '',
    scheduledAppointments: '',
    actualCustomers: '',
    possibleCustomers: '',
    location: '',
    name: '',
    covidNewCaseCount: '',
    covidHospitalizationRate: '',
    localInfectedRatePercent: '',
    locaHospitalizationRate: '',
    nationalUnemploymentPercent: '',
    nationalPerCapitaDisposableIncome: '',
    nationalConsumerConfidenceIndex: '',
    localPerCapitaDisposableIncome: '',
    localConsumerConfidenceIndex: '',
    nationalFavorability: '',
    localFavorability: '',
    localDailyFootTraffic: '',
    amountSpentOnCallCenterInDollars: '',
    amountSpentOnAffiliateMarketingInDollars: '',
    directSpendingOnMarketingChannelInDollars: '',
    directSpendingOnMailMarketingInDollars: '',
    amountSpentOnDisplayMarketingInDollars: '',
    amountSpentOnEmailMarketingInDollars: '',
    amountSpentOnOnlineVideosMarketingInDollars: '',
    amountSpentOnOrganicSearchMarketingInDollars: '',
    amountSpentOnOrganicSocialMediaMarketingInDollars: '',
    amountPaidToLocalMarketingChannelInDollars: '',
    amountPaidToSearchMarketingInDollars: '',
    amountSpentOnRadioMarketingInDollars: '',
    rawDemand: '',
    // percentFulfilled: '',
    capacityToService: '',
    answerfulfilledDemand: '',
    competitores: ''
  };
  location: any = {
    longitude: '',
    latitude: ''
  }
  competitores: any = [];
  storeData: any;
  submitted: boolean;
  address: string;
  latitude: number;
  longitude: number;
  zoom: number;
  private geoCoder;
  locationData: any = {
    address: '',
    lat: '',
    lng: ''
  };
  lat: any;
  lng: any;
  latlng: any
  searchControl: FormControl;
  @ViewChild("search")
  public searchElementRef: ElementRef;
  marker: marker;
  constructor(
    private service: TablesService,
    public dataRoute: ActivatedRoute,
    private dataservice: DataService,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private toast: ToastrService,
    private route: Router,
    private loader: AppLoaderService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    public dialog: MatDialog,
    private http: HttpClient
  ) {
    this.store = this.dataservice.getOption()
    console.log('store====>>', this.store)
    this.locationAutoComplete();
  }


  ngOnInit(): void {
    this.storeForm = new FormGroup({
      storeID: new FormControl('', Validators.required),
      storeManager: new FormControl(''),
      address: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      zipCode: new FormControl('', Validators.required),
      staff: new FormControl('',),
      hoursOpen: new FormControl(''),
      demandForServiceStaff: new FormControl('', Validators.required),
      servicesFulfilled: new FormControl('', Validators.required),
      incomingCall: new FormControl('', Validators.required),
      answeredCalls: new FormControl('', Validators.required),
      capacityForAppointments: new FormControl('',),
      scheduledAppointments: new FormControl(''),
      actualCustomers: new FormControl('', Validators.required),
      possibleCustomers: new FormControl('', Validators.required),
      softdelete: new FormControl(),
      // location: this.location.coordinates,
      covidNewCaseCount: new FormControl(''),
      covidHospitalizationRate: new FormControl(''),
      localInfectedRatePercent: new FormControl(''),
      locaHospitalizationRate: new FormControl(''),
      nationalUnemploymentPercent: new FormControl(''),
      localUnemploymentPercent: new FormControl(''),
      nationalPerCapitaDisposableIncome: new FormControl(''),
      nationalConsumerConfidenceIndex: new FormControl(''),
      localPerCapitaDisposableIncome: new FormControl(''),
      localConsumerConfidenceIndex: new FormControl(''),
      nationalFavorability: new FormControl(''),
      localFavorability: new FormControl(''),
      localDailyFootTraffic: new FormControl(''),
      amountSpentOnCallCenterInDollars: new FormControl(''),
      amountSpentOnAffiliateMarketingInDollars: new FormControl(''),
      directSpendingOnMarketingChannelInDollars: new FormControl(''),
      directSpendingOnMailMarketingInDollars: new FormControl(''),
      amountSpentOnDisplayMarketingInDollars: new FormControl(''),
      amountSpentOnEmailMarketingInDollars: new FormControl(''),
      amountSpentOnOnlineVideosMarketingInDollars: new FormControl(''),
      amountSpentOnOrganicSearchMarketingInDollars: new FormControl(''),
      amountSpentOnReferralsInDollars: new FormControl(''),
      amountSpentOnTVMarketingInDollars: new FormControl(''),
      amountSpentOnOrganicSocialMediaMarketingInDollars: new FormControl(''),
      amountPaidToLocalMarketingChannelInDollars: new FormControl(''),
      amountPaidToSearchMarketingInDollars: new FormControl(''),
      amountSpentOnRadioMarketingInDollars: new FormControl(''),
      rawDemand: new FormControl(''),
      percentFulfilled: new FormControl(''),
      capacityToService: new FormControl(''),
      answerfulfilledDemand: new FormControl(''),
    });
  }
  locationAutoComplete() {
    this.searchControl = new FormControl();
    this.setCurrentLocation();
    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef?.nativeElement);
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          this.address = place.formatted_address;
          console.log('address', this.address)
          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          // set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });
  }
  // getCurrentLocation(lat, lng) {
  //   this.lat = lat;
  //   this.lng = lng
  //   let key = "AIzaSyBhVQKXquGT9zTznHXq1A6YJCcrrC5HWv8"
  //   const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${key}`;
  //   return this.http.get(url)
  //     .subscribe(response => console.log('res ===', response))
  // }

  back() {
    this.route.navigate(['tables/filter']);
  }
  addStore() {
    let body = {
      storeID: this.storeForm.value.storeID,
      address: this.storeForm.value.address,
      storeManager: this.storeForm.value.storeManager,
      city: this.storeForm.value.city,
      zipCode: this.storeForm.value.zipCode,
      staff: this.storeForm.value.staff,
      hoursOpen: this.storeForm.value.hoursOpen,
      demandForServiceStaff: this.storeForm.value.demandForServiceStaff,
      servicesFulfilled: this.storeForm.value.servicesFulfilled,
      incomingCall: this.storeForm.value.incomingCall,
      answeredCalls: this.storeForm.value.answeredCalls,
      capacityForAppointments: this.storeForm.value.capacityForAppointments,
      scheduledAppointments: this.storeForm.value.scheduledAppointments,
      actualCustomers: this.storeForm.value.actualCustomers,
      possibleCustomers: this.storeForm.value.possibleCustomers,
      competitors: this.competitores,
      location: this.location,
      covidNewCaseCount: this.storeForm.value.covidNewCaseCount,
      covidHospitalizationRate: this.storeForm.value.covidHospitalizationRate,
      localInfectedRatePercent: this.storeForm.value.localInfectedRatePercent,
      locaHospitalizationRate: this.storeForm.value.locaHospitalizationRate,
      nationalUnemploymentPercent: this.storeForm.value.nationalUnemploymentPercent,
      localUnemploymentPercent: this.storeForm.value.localUnemploymentPercent,
      nationalPerCapitaDisposableIncome: this.storeForm.value.nationalPerCapitaDisposableIncome,
      nationalConsumerConfidenceIndex: this.storeForm.value.nationalConsumerConfidenceIndex,
      localPerCapitaDisposableIncome: this.storeForm.value.localPerCapitaDisposableIncome,
      localConsumerConfidenceIndex: this.storeForm.value.localConsumerConfidenceIndex,
      nationalFavorability: this.storeForm.value.nationalFavorability,
      localFavorability: this.storeForm.value.localFavorability,
      localDailyFootTraffic: this.storeForm.value.localDailyFootTraffic,
      amountSpentOnCallCenterInDollars: this.storeForm.value.amountSpentOnCallCenterInDollars,
      amountSpentOnAffiliateMarketingInDollars: this.storeForm.value.amountSpentOnAffiliateMarketingInDollars,
      directSpendingOnMarketingChannelInDollars: this.storeForm.value.directSpendingOnMarketingChannelInDollars,
      directSpendingOnMailMarketingInDollars: this.storeForm.value.directSpendingOnMailMarketingInDollars,
      amountSpentOnDisplayMarketingInDollars: this.storeForm.value.amountSpentOnDisplayMarketingInDollars,
      amountSpentOnEmailMarketingInDollars: this.storeForm.value.amountSpentOnEmailMarketingInDollars,
      amountSpentOnOnlineVideosMarketingInDollars: this.storeForm.value.amountSpentOnOnlineVideosMarketingInDollars,
      amountSpentOnOrganicSearchMarketingInDollars: this.storeForm.value.amountSpentOnOrganicSearchMarketingInDollars,
      amountSpentOnOrganicSocialMediaMarketingInDollars: this.storeForm.value.amountSpentOnOrganicSocialMediaMarketingInDollars,
      amountSpentOnReferralsInDollars: this.storeForm.value.amountSpentOnReferralsInDollars,
      amountSpentOnTVMarketingInDollars: this.storeForm.value.amountSpentOnTVMarketingInDollars,
      amountPaidToLocalMarketingChannelInDollars: this.storeForm.value.amountPaidToLocalMarketingChannelInDollars,
      amountPaidToSearchMarketingInDollars: this.storeForm.value.amountPaidToSearchMarketingInDollars,
      amountSpentOnRadioMarketingInDollars: this.storeForm.value.amountSpentOnRadioMarketingInDollars,
      rawDemand: this.storeForm.value.rawDemand,
      percentFulfilled: this.storeForm.value.percentFulfilled,
      capacityToService: this.storeForm.value.capacityToService,
      answerfulfilledDemand: this.storeForm.value.answerfulfilledDemand,

    }
    console.log('before res ==>', body)

    this.apiservice.createStore(body).subscribe((res: any) => {
      console.log('add user res ==>', res)
      this.route.navigate(['tables/store']);
    });
  }
  storeUpdate() {
    let id = this.store._id
    console.log('storeee iddd', id)
    if (id) {
      this.apiservice.storeUpdate(id, this.store).subscribe((res: any) => {
        console.log('update res', res)
        this.route.navigate(['tables/store']);
      })
    }

  }
  onSubmit() {
    this.submitted = true;
    // this.addUser();
    if (!this.store._id) {
      return this.addStore();
    } else {
      this.storeUpdate();
    }
  }
  // getUsers() {
  //   console.log('add function calling')
  //   this.apiservice.getAllStore().subscribe((res: any) => {
  //     console.log('resssssss', res)
  //     this.stores = res.data

  //     console.log('get users ===>>', this.stores)
  //   });
  // }




  setData() {
    this.location = {
      longitude: this.longitude,
      latitude: this.latitude
    }

    console.log('location', this.location)
  }

  // Get Current Location Coordinates
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }


  getAddress(latitude, longitude) {
    console.log('lat lng', latitude, longitude)
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log('results', results);
      console.log('status', status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }
  openData(data: any) {

    const dialogRef = this.dialog.open(PopupComponent, {
      width: '650px',
      height: '500px',
      data: {
        dataKey: data,
      }
    });
    dialogRef.afterClosed().subscribe(res => {
      console.log(this.dataservice.getOption())
      this.competitores.push(this.dataservice.getOption())
      console.log('===', this.competitores)
    });
  }


}
