import { Routes } from '@angular/router';

import { BasicFormComponent } from './basic-form/basic-form.component';
import { RichTextEditorComponent } from './rich-text-editor/rich-text-editor.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { WizardComponent } from './wizard/wizard.component';
import { UserManageComponent } from './user-manage/user-manage.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { ViewprofileComponent } from './viewprofile/viewprofile.component';
import { AddPlanComponent } from './add-plan/add-plan.component';
import { AddGiftComponent } from './add-gift/add-gift.component';
import { AddCategoryComponent } from './add-category/add-category.component';
import { AddRegionTypeComponent } from './add-region-type/add-region-type.component';
import { AddRegionsComponent } from './add-regions/add-regions.component';
import { AddStoreComponent } from './add-store/add-store.component';

export const FormsRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'basic',
      component: BasicFormComponent,
      data: { title: 'Roles', breadcrumb: 'ROLES' }
    },
    {
      path: 'add-store',
      component: AddStoreComponent,
      data: { title: 'Add-Store', breadcrumb: 'Add-Store' }
    },
    {
      path: 'editor',
      component: RichTextEditorComponent,
      data: { title: 'Editor', breadcrumb: 'EDITOR' }
    }, {
      path: 'upload',
      component: FileUploadComponent,
      data: { title: 'Upload', breadcrumb: 'UPLOAD' }
    }, {
      path: 'wizard',
      component: WizardComponent,
      data: { title: 'Wizard', breadcrumb: 'WIZARD' }
    }, {
      path: 'user',
      component: UserManageComponent,
      data: { title: 'User', breadcrumb: 'USER' }
    }, {
      path: 'view-profile/:id',
      component: ViewprofileComponent,
      data: { title: 'view-profile', breadcrumb: 'view-profile' }
    },
    {
      path: 'add-plan',
      component: AddPlanComponent,
      data: { title: 'add-plan', breadcrumb: 'add-plan' }
    },
    {
      path: 'add-gift',
      component: AddGiftComponent,
      data: { title: 'add-gift', breadcrumb: 'add-gift' }
    },
    {
      path: 'add-category',
      component: AddCategoryComponent,
      data: { title: 'add-category', breadcrumb: 'add-category' }
    },
    {
      path: 'add-regiontype',
      component: AddRegionTypeComponent,
      data: { title: 'add-regiontype', breadcrumb: 'add-regiontype' }
    },
    {
      path: 'add-regions',
      component: AddRegionsComponent,
      data: { title: 'add-regions', breadcrumb: 'add-regions' }
    },
    {
      path: 'update',
      component: UpdateUserComponent,
      data: { title: 'User', breadcrumb: 'USER' }
    }]
  }
];