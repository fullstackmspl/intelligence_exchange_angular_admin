import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { TablesService } from 'app/views/tables/tables.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';
import { ApiService } from 'app/shared/services/api.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-user-manage',
  templateUrl: './user-manage.component.html',
  styleUrls: ['./user-manage.component.scss']
})
export class UserManageComponent implements OnInit {
  formData = {};
  adminForm: FormGroup;
  admin: any = {
    storeID: '',
    username: '',
    firstName: '',
    lastName: '',
    email: '',
    phoneNumber: '',
    profilePictureUrl: '',
    password: '',
  };

  FormData: any;
  submitted: boolean;
  usersData: any = {};
  responseData: any;
  res: any[];
  show: boolean = true;

  message: string = 'User Added Successfully!';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  userResponse: any;
  isLoading: boolean;
  stores: any;
  store_id: any
  users: any;
  userId: any;
  constructor(private service: TablesService,
    public dataRoute: ActivatedRoute,
    private dataservice: DataService,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private toast: ToastrService,
    private route: Router,
    private loader: AppLoaderService) {
    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
    this.admin = this.dataservice.getOption()
    console.log("admin", this.admin)
  }

  back() {
    this.route.navigate(['tables/filter']);
  }
  ngOnInit() {
    this.adminForm = new FormGroup({
      storeId: new FormControl('', Validators.required),
      username: new FormControl('', Validators.required),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('',),
      phoneNumber: new FormControl(''),
      password: new FormControl('', Validators.required),
    });
    this.getStore()
  }

  addUser() {
    console.log(this.adminForm.value)
    this.apiservice.addUser(this.adminForm.value).subscribe((res: any) => {
      console.log('add user res ==>', res)
      this.route.navigate(['tables/filter']);
    });
  }
  getCurrentUser(id: any) {

    this.apiservice.getUserByIdd(id).subscribe((res: any) => {
      console.log('res ===>', res)
    })
  }
  userUpdate() {
    let id = this.admin._id
    if (id) {
      this.apiservice.userUpdate(id, this.admin).subscribe((res: any) => {
        console.log('update res', res)
        this.route.navigate(['tables/filter']);
      })
    }

  }
  onSubmit() {
    this.submitted = true;
    // this.addUser();
    if (!this.admin._id) {
      return this.addUser();
    } else {
      this.userUpdate();
    }
  }
  getStore() {
    console.log('add function calling')
    this.apiservice.getAllStore().subscribe((res: any) => {
      console.log('resssssss', res)
      this.stores = res.data

      console.log('get users ===>>', this.stores)
    });
  }
}

