import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatButton } from '@angular/material/button';
import { MatProgressBar } from '@angular/material/progress-bar';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';

import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'app/shared/services/api.service';
import { AuthService } from 'app/shared/services/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;

  signinForm: FormGroup;
  errorMsg = '';
  // return: string;
  credentials = {
    email: '',
    password: '',

  };
  isLoading: boolean = false;
  userData: any = {};
  show: boolean = true;
  token: any;
  private _unsubscribeAll: Subject<any>;

  constructor(
    private egretLoader: AppLoaderService,
    private route: ActivatedRoute,
    private router: Router,
    private toast: ToastrService,
    private apiservice: ApiService,
    private authservice: AuthService,
    private loader: AppLoaderService,
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.signinForm = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }
  login() {
    this.apiservice.loginn(this.signinForm.value.email, this.signinForm.value.password).subscribe((res: any) => {
      console.log('res', res)
      this.router.navigate(['dashboard/analytics'])
    })
  }

  //   // this.route.queryParams
  //   //   .pipe(takeUntil(this._unsubscribeAll))
  //   //   .subscribe(params => this.return = params['return'] || '/');
  // }
  logIn() {


    // localStorage.removeItem('token');
    this.loader.open();
    this.authservice.login(this.signinForm.value.email, this.signinForm.value.password).subscribe((res: any) => {
      this.userData = res;
      this.loader.close();
      console.log('resssssss', res)
      if (this.userData.token) {
        this.loader.close();
        this.toast.success(this.userData.message)
        this.router.navigate(['dashboard/analytics']);
      }
      else {
        this.loader.close();
        this.toast.error(res.error)
        return this.isLoading = false;
      };
      this.loader.close();
    });
    this.loader.close();
  }


  // ngAfterViewInit() {
  //   this.autoSignIn();
  // }

  // ngOnDestroy() {
  //   this._unsubscribeAll.next();
  //   this._unsubscribeAll.complete();
  // }

  // signin() {
  //   const signinData = this.signinForm.value

  //   this.submitButton.disabled = true;
  //   this.progressBar.mode = 'indeterminate';

  //   this.jwtAuth.signin(signinData.username, signinData.password)
  //   .subscribe(response => {
  //     this.router.navigateByUrl(this.jwtAuth.return);
  //   }, err => {
  //     this.submitButton.disabled = false;
  //     this.progressBar.mode = 'determinate';
  //     this.errorMsg = err.message;
  //     // console.log(err);
  //   })
  // }

  // autoSignIn() {    
  //   if(this.jwtAuth.return === '/') {
  //     return
  //   }
  //   this.egretLoader.open(`Automatically Signing you in! \n Return url: ${this.jwtAuth.return.substring(0, 20)}...`, {width: '320px'});
  //   setTimeout(() => {
  //     this.signin();
  //     console.log('autoSignIn');
  //     this.egretLoader.close()
  //   }, 2000);
  // }

}
