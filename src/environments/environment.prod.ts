// import { config } from "config";

// export const environment = {
//   production: true,
//   apiURL: config.apiUrl
// };
export const environment = {
  production: true,
  apiUrl: {   
    master: 'http://93.188.167.68:9898/api',
  },
  socketUrl: 'http://93.188.167.68:4500',
  name: 'prod'
};