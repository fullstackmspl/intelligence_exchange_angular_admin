"use strict";
(self["webpackChunkegret"] = self["webpackChunkegret"] || []).push([["src_app_views_forms_forms_module_ts"],{

/***/ 52426:
/*!********************************************************************!*\
  !*** ./src/app/views/forms/add-category/add-category.component.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddCategoryComponent": () => (/* binding */ AddCategoryComponent)
/* harmony export */ });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 90587);
/* harmony import */ var app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/shared/services/api.service */ 94761);
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ 77009);
/* harmony import */ var app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/dataservice.service */ 43396);
/* harmony import */ var app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/views/tables/tables.service */ 27582);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ 7100);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/card */ 4005);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/divider */ 51996);
/* harmony import */ var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/flex-layout/flex */ 23081);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/form-field */ 3403);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/input */ 23852);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/button */ 2955);






















function AddCategoryComponent_input_11_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "input", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddCategoryComponent_input_11_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r3); const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r2.category.sic = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx_r0.category.sic);
} }
function AddCategoryComponent_input_12_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "input", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddCategoryComponent_input_12_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r5); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r4.category.sic = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx_r1.category.sic);
} }
class AddCategoryComponent {
    constructor(service, dataRoute, dataservice, apiservice, route, toast, loader) {
        this.service = service;
        this.dataRoute = dataRoute;
        this.dataservice = dataservice;
        this.apiservice = apiservice;
        this.route = route;
        this.toast = toast;
        this.loader = loader;
        this.formData = {};
        this.rows = [];
        this.columns = [];
        this.temp = [];
        this.rolesList = [];
        this.usersData = {};
        this.show = true;
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.formData1 = new FormData();
        this.fileData = File;
        this.category = dataservice.getOption();
        console.log('plan data ', this.category);
        this.permissions = {
            view: false,
            add: false,
            edit: false,
            all: false
        };
    }
    back() {
        this.route.navigate(['tables/categories']);
    }
    ngOnInit() {
        if (this.category.isPLan === true) {
            this.formName = "Update Category";
        }
        else {
            this.formName = "Create Category";
        }
        this.categoryForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormGroup({
            sic: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required),
            parentsic: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required),
            category: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required),
            level: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required),
        });
    }
    onChange(value) {
        let permissions = Object.keys(this.permissions);
        // this.permissions[value] = true
        for (let permission of permissions) {
            if (permission == value) {
                this.permissions[permission] = true;
            }
            else {
                this.permissions[permission] = false;
            }
        }
    }
    // addCategory() {
    //   console.log('add function calling')
    //   this.apiservice.addCagegory(this.categoryForm.value).subscribe((res: any) => {
    //     console.log('resssssss', res)
    //   });
    // }
    // updatCategory() {
    //   console.log('update function calling')
    //   // this.category.planId = this.category.id;
    //   this.apiservice.updateCategory(this.category.id,this.category).subscribe((res: any) => {
    //     console.log('resssssss', res)
    //   });
    // }
    onSubmit() {
        this.submitted = true;
        console.log(this.categoryForm.value);
        if (!this.category.id) {
            // return this.addCategory();
        }
        else {
            // return this.updatCategory();
        }
    }
}
AddCategoryComponent.ɵfac = function AddCategoryComponent_Factory(t) { return new (t || AddCategoryComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__.TablesService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__.DataService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__.ApiService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_7__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__.AppLoaderService)); };
AddCategoryComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({ type: AddCategoryComponent, selectors: [["app-add-category"]], decls: 29, vars: 7, consts: [[1, "p-0"], [1, "title"], [1, "card-title-text"], [3, "formGroup"], ["fxLayout", "row wrap"], ["fxFlex", "100", "fxFlex.gt-xs", "75", 1, "pr-1"], [1, "pb-1"], [1, "full-width"], ["matInput", "", "name", "sic", "placeholder", "Sic", "formControlName", "sic", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["matInput", "", "name", "parentsic", "placeholder", "Parent sic", "formControlName", "parentsic", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "category", "placeholder", "Category", "formControlName", "category", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "level", "placeholder", "level", "formControlName", "level", 3, "ngModel", "ngModelChange"], ["mat-raised-button", "", "color", "warn", 3, "click"], ["mat-raised-button", "", "color", "primary", 1, "submitBtn", 3, "click"], ["matInput", "", "name", "sic", "placeholder", "Sic", "formControlName", "sic", 3, "ngModel", "ngModelChange"]], template: function AddCategoryComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-card-title", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](4, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "mat-card-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](6, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](10, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](11, AddCategoryComponent_input_11_Template, 1, 1, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](12, AddCategoryComponent_input_12_Template, 1, 1, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](13, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](14, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](15, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](16, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddCategoryComponent_Template_input_ngModelChange_16_listener($event) { return ctx.category.parentsic = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](17, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](18, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](19, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](20, "input", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddCategoryComponent_Template_input_ngModelChange_20_listener($event) { return ctx.category.category = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](21, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](22, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](23, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](24, "input", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddCategoryComponent_Template_input_ngModelChange_24_listener($event) { return ctx.category.level = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](25, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function AddCategoryComponent_Template_button_click_25_listener() { return ctx.back(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](26, "Back");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](27, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function AddCategoryComponent_Template_button_click_27_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](28, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](ctx.formName);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formGroup", ctx.categoryForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.category.id);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", !ctx.category.id);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.category.parentsic);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.category.category);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.category.level);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCard, _angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCardTitle, _angular_material_divider__WEBPACK_IMPORTED_MODULE_9__.MatDivider, _angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCardContent, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormGroupDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__.DefaultLayoutDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__.DefaultFlexDirective, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__.MatFormField, _angular_common__WEBPACK_IMPORTED_MODULE_12__.NgIf, _angular_material_input__WEBPACK_IMPORTED_MODULE_13__.MatInput, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControlName, _angular_material_button__WEBPACK_IMPORTED_MODULE_14__.MatButton], styles: [".mat-card[_ngcontent-%COMP%] {\n  box-shadow: none !important;\n}\n\n.title[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\n.submitBtn[_ngcontent-%COMP%] {\n  align-items: flex-end;\n  margin-left: 15px;\n}\n\nmat-icon[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZC1jYXRlZ29yeS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDJCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtBQUNKIiwiZmlsZSI6ImFkZC1jYXRlZ29yeS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtY2FyZCB7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50aXRsZSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5zdWJtaXRCdG4ge1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7XHJcbn1cclxuXHJcbm1hdC1pY29uIHtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufSJdfQ== */"] });


/***/ }),

/***/ 56501:
/*!************************************************************!*\
  !*** ./src/app/views/forms/add-gift/add-gift.component.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddGiftComponent": () => (/* binding */ AddGiftComponent)
/* harmony export */ });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 90587);
/* harmony import */ var app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/shared/services/api.service */ 94761);
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ 77009);
/* harmony import */ var app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/dataservice.service */ 43396);
/* harmony import */ var app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/views/tables/tables.service */ 27582);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ 7100);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/card */ 4005);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/divider */ 51996);
/* harmony import */ var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/flex-layout/flex */ 23081);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/form-field */ 3403);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/input */ 23852);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/button */ 2955);






















function AddGiftComponent_input_11_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "input", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddGiftComponent_input_11_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r3); const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r2.plan.plan = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx_r0.plan.plan);
} }
function AddGiftComponent_input_12_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "input", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddGiftComponent_input_12_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r5); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r4.plan.name = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx_r1.plan.name);
} }
class AddGiftComponent {
    constructor(service, dataRoute, dataservice, apiservice, route, toast, loader) {
        this.service = service;
        this.dataRoute = dataRoute;
        this.dataservice = dataservice;
        this.apiservice = apiservice;
        this.route = route;
        this.toast = toast;
        this.loader = loader;
        this.formData = {};
        this.rows = [];
        this.columns = [];
        this.temp = [];
        this.plan = {
            name: '',
            plan: '',
            coins: '',
            planId: ''
        };
        this.rolesList = [];
        this.usersData = {};
        this.show = true;
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.formData1 = new FormData();
        this.fileData = File;
        this.plan = dataservice.getOption();
        console.log('plan data ', this.plan);
        this.permissions = {
            view: false,
            add: false,
            edit: false,
            all: false
        };
    }
    back() {
        this.route.navigate(['tables/gift']);
    }
    ngOnInit() {
        this.planForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormGroup({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required),
            coins: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required),
        });
    }
    onChange(value) {
        let permissions = Object.keys(this.permissions);
        // this.permissions[value] = true
        for (let permission of permissions) {
            if (permission == value) {
                this.permissions[permission] = true;
            }
            else {
                this.permissions[permission] = false;
            }
        }
    }
    // addPlan() {
    //   console.log('add function calling')
    //   this.apiservice.addGifts(this.formData1).subscribe((res: any) => {
    //     console.log('resssssss', res)
    //   });
    // }
    // updatelan() {
    //   console.log('update function calling')
    //   this.plan.planId = this.plan.id;
    //   this.apiservice.updatePlan(this.plan).subscribe((res: any) => {
    //     console.log('resssssss', res)
    //   });
    // }
    onSelectFile(event) {
        var fileData;
        // if (event.target.files && event.target.files[0]) {
        //   var reader = new FileReader();
        this.fileData = event.target.files[0];
        // reader.readAsDataURL(event.target.files[0]);
        // reader.onload = (event: any) => {
        //   this.avatar = event.target.result;
        // };
    }
    onSubmit() {
        this.formData1.append("name", this.planForm.value.name);
        this.formData1.append("coins", this.planForm.value.coin);
        this.formData1.append("image", this.fileData);
        this.submitted = true;
        console.log(this.planForm.value);
        if (!this.plan.id) {
            // return this.addPlan();
        }
        else {
            // return this.updatelan();
        }
    }
}
AddGiftComponent.ɵfac = function AddGiftComponent_Factory(t) { return new (t || AddGiftComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__.TablesService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__.DataService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__.ApiService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_7__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__.AppLoaderService)); };
AddGiftComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({ type: AddGiftComponent, selectors: [["app-add-gift"]], decls: 26, vars: 5, consts: [[1, "p-0"], [1, "title"], [1, "card-title-text"], [3, "formGroup", "ngSubmit"], ["fxLayout", "row wrap"], ["fxFlex", "100", "fxFlex.gt-xs", "75", 1, "pr-1"], [1, "pb-1"], [1, "full-width"], ["matInput", "", "name", "name", "placeholder", "Name", "formControlName", "name", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["matInput", "", "name", "coins", "placeholder", "Coins", "formControlName", "coins", 3, "ngModel", "ngModelChange"], ["id", "fileInput", "type", "file", "accept", "image/jpg,image/jpeg,image/svg, image/gif", 3, "change"], ["mat-raised-button", "", "color", "warn", 3, "click"], ["mat-raised-button", "", "color", "primary", 1, "submitBtn"], ["matInput", "", "name", "name", "placeholder", "Name", "formControlName", "name", 3, "ngModel", "ngModelChange"]], template: function AddGiftComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-card-title", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](4, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "mat-card-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](6, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngSubmit", function AddGiftComponent_Template_form_ngSubmit_6_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](10, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](11, AddGiftComponent_input_11_Template, 1, 1, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](12, AddGiftComponent_input_12_Template, 1, 1, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](13, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](14, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](15, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](16, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddGiftComponent_Template_input_ngModelChange_16_listener($event) { return ctx.plan.coins = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](17, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](18, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](19, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](20, "input", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("change", function AddGiftComponent_Template_input_change_20_listener($event) { return ctx.onSelectFile($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](21, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](22, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function AddGiftComponent_Template_button_click_22_listener() { return ctx.back(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](23, "Back");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](24, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](25, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](ctx.formName);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formGroup", ctx.planForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.plan.id);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", !ctx.plan.id);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.plan.coins);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCard, _angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCardTitle, _angular_material_divider__WEBPACK_IMPORTED_MODULE_9__.MatDivider, _angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCardContent, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormGroupDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__.DefaultLayoutDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__.DefaultFlexDirective, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__.MatFormField, _angular_common__WEBPACK_IMPORTED_MODULE_12__.NgIf, _angular_material_input__WEBPACK_IMPORTED_MODULE_13__.MatInput, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControlName, _angular_material_button__WEBPACK_IMPORTED_MODULE_14__.MatButton], styles: [".mat-card[_ngcontent-%COMP%] {\n  box-shadow: none !important;\n}\n\n.title[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\n.submitBtn[_ngcontent-%COMP%] {\n  align-items: flex-end;\n  margin-left: 15px;\n}\n\nmat-icon[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZC1naWZ0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksMkJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQkFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0FBQ0oiLCJmaWxlIjoiYWRkLWdpZnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWF0LWNhcmQge1xyXG4gICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGl0bGUge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uc3VibWl0QnRuIHtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG59XHJcblxyXG5tYXQtaWNvbiB7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn0iXX0= */"] });


/***/ }),

/***/ 41623:
/*!************************************************************!*\
  !*** ./src/app/views/forms/add-plan/add-plan.component.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddPlanComponent": () => (/* binding */ AddPlanComponent)
/* harmony export */ });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 90587);
/* harmony import */ var app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/shared/services/api.service */ 94761);
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ 77009);
/* harmony import */ var app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/dataservice.service */ 43396);
/* harmony import */ var app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/views/tables/tables.service */ 27582);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ 7100);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/card */ 4005);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/divider */ 51996);
/* harmony import */ var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/flex-layout/flex */ 23081);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/form-field */ 3403);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/input */ 23852);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/button */ 2955);






















function AddPlanComponent_input_11_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "input", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddPlanComponent_input_11_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r3); const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r2.plan.plan = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx_r0.plan.plan);
} }
function AddPlanComponent_input_12_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "input", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddPlanComponent_input_12_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r5); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r4.plan.name = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx_r1.plan.name);
} }
class AddPlanComponent {
    constructor(service, dataRoute, dataservice, apiservice, route, toast, loader) {
        this.service = service;
        this.dataRoute = dataRoute;
        this.dataservice = dataservice;
        this.apiservice = apiservice;
        this.route = route;
        this.toast = toast;
        this.loader = loader;
        this.formData = {};
        this.rows = [];
        this.columns = [];
        this.temp = [];
        this.plan = {
            name: '',
            plan: '',
            amount: '',
            coins: '',
            beforediscount: '',
            afterdiscount: '',
            discountpercent: '',
            planId: ''
        };
        this.rolesList = [];
        this.usersData = {};
        this.show = true;
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.plan = dataservice.getOption();
        console.log('plan data ', this.plan);
        this.permissions = {
            view: false,
            add: false,
            edit: false,
            all: false
        };
    }
    back() {
        this.route.navigate(['tables/plan']);
    }
    ngOnInit() {
        this.planForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormGroup({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required),
            amount: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required),
            coins: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required),
            beforediscount: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required),
            afterdiscount: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required),
            discountpercent: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required),
        });
    }
    onChange(value) {
        let permissions = Object.keys(this.permissions);
        // this.permissions[value] = true
        for (let permission of permissions) {
            if (permission == value) {
                this.permissions[permission] = true;
            }
            else {
                this.permissions[permission] = false;
            }
        }
    }
    // addPlan() {
    //   console.log('add function calling')
    //   this.apiservice.addPlans(this.plan).subscribe((res: any) => {
    //     console.log('resssssss', res)
    //   });
    // }
    // updatelan() {
    //   console.log('update function calling')
    //   this.plan.planId = this.plan.id;
    //   this.apiservice.updatePlan(this.plan).subscribe((res: any) => {
    //     console.log('resssssss', res)
    //   });
    // }
    onSubmit() {
        this.submitted = true;
        console.log(this.planForm.value);
        if (!this.plan.id) {
            // return this.addPlan();
        }
        else {
            // return this.updatelan();
        }
    }
}
AddPlanComponent.ɵfac = function AddPlanComponent_Factory(t) { return new (t || AddPlanComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__.TablesService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__.DataService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__.ApiService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_7__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__.AppLoaderService)); };
AddPlanComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({ type: AddPlanComponent, selectors: [["app-add-plan"]], decls: 34, vars: 8, consts: [[1, "p-0"], [1, "title"], [1, "card-title-text"], [3, "formGroup", "ngSubmit"], ["fxLayout", "row wrap"], ["fxFlex", "100", "fxFlex.gt-xs", "75", 1, "pr-1"], [1, "pb-1"], [1, "full-width"], ["matInput", "", "name", "name", "placeholder", "Name", "formControlName", "name", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["matInput", "", "name", "coins", "placeholder", "Coins", "formControlName", "coins", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "costbeforeDISCOUNT", "placeholder", "Cost Before Discount", "formControlName", "beforediscount", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "afterdiscount", "placeholder", "Cost After Discount", "formControlName", "afterdiscount", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "discountpercentage", "placeholder", "Discount %", "formControlName", "discountpercent", 3, "ngModel", "ngModelChange"], ["mat-raised-button", "", "color", "warn", 3, "click"], ["mat-raised-button", "", "color", "primary", 1, "submitBtn"], ["matInput", "", "name", "name", "placeholder", "Name", "formControlName", "name", 3, "ngModel", "ngModelChange"]], template: function AddPlanComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-card-title", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](4, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "mat-card-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](6, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngSubmit", function AddPlanComponent_Template_form_ngSubmit_6_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](10, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](11, AddPlanComponent_input_11_Template, 1, 1, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](12, AddPlanComponent_input_12_Template, 1, 1, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](13, " \\ ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](14, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](16, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](17, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddPlanComponent_Template_input_ngModelChange_17_listener($event) { return ctx.plan.coins = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](18, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](19, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](20, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](21, "input", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddPlanComponent_Template_input_ngModelChange_21_listener($event) { return ctx.plan.beforediscount = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](22, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](23, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](24, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](25, "input", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddPlanComponent_Template_input_ngModelChange_25_listener($event) { return ctx.plan.afterdiscount = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](26, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](27, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](28, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](29, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddPlanComponent_Template_input_ngModelChange_29_listener($event) { return ctx.plan.discountpercent = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](30, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function AddPlanComponent_Template_button_click_30_listener() { return ctx.back(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](31, "Back");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](32, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](33, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](ctx.formName);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formGroup", ctx.planForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.plan.id);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", !ctx.plan.id);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.plan.coins);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.plan.beforediscount);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.plan.afterdiscount);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.plan.discountpercent);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCard, _angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCardTitle, _angular_material_divider__WEBPACK_IMPORTED_MODULE_9__.MatDivider, _angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCardContent, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormGroupDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__.DefaultLayoutDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__.DefaultFlexDirective, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__.MatFormField, _angular_common__WEBPACK_IMPORTED_MODULE_12__.NgIf, _angular_material_input__WEBPACK_IMPORTED_MODULE_13__.MatInput, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControlName, _angular_material_button__WEBPACK_IMPORTED_MODULE_14__.MatButton], styles: [".mat-card[_ngcontent-%COMP%] {\r\n    box-shadow: none !important;\r\n}\r\n\r\n.title[_ngcontent-%COMP%] {\r\n    text-align: center;\r\n}\r\n\r\n.submitBtn[_ngcontent-%COMP%] {\r\n    align-items: flex-end;\r\n    margin-left: 15px;\r\n}\r\n\r\nmat-icon[_ngcontent-%COMP%] {\r\n    cursor: pointer;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZC1wbGFuLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSwyQkFBMkI7QUFDL0I7O0FBRUE7SUFDSSxrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSxxQkFBcUI7SUFDckIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksZUFBZTtBQUNuQiIsImZpbGUiOiJhZGQtcGxhbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hdC1jYXJkIHtcclxuICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRpdGxlIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnN1Ym1pdEJ0biB7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgICBtYXJnaW4tbGVmdDogMTVweDtcclxufVxyXG5cclxubWF0LWljb24ge1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59Il19 */"] });


/***/ }),

/***/ 19916:
/*!**************************************************************************!*\
  !*** ./src/app/views/forms/add-region-type/add-region-type.component.ts ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddRegionTypeComponent": () => (/* binding */ AddRegionTypeComponent)
/* harmony export */ });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 90587);
/* harmony import */ var app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/shared/services/api.service */ 94761);
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ 77009);
/* harmony import */ var app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/dataservice.service */ 43396);
/* harmony import */ var app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/views/tables/tables.service */ 27582);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ 7100);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/card */ 4005);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/divider */ 51996);
/* harmony import */ var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/flex-layout/flex */ 23081);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/form-field */ 3403);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/button */ 2955);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/input */ 23852);






















function AddRegionTypeComponent_input_11_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "input", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddRegionTypeComponent_input_11_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r3); const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r2.regionTypes.name = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx_r0.regionTypes.name);
} }
function AddRegionTypeComponent_input_12_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "input", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddRegionTypeComponent_input_12_Template_input_ngModelChange_0_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r5); const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r4.regionTypes.name = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx_r1.regionTypes.name);
} }
class AddRegionTypeComponent {
    constructor(service, dataRoute, dataservice, apiservice, route, toast, loader) {
        this.service = service;
        this.dataRoute = dataRoute;
        this.dataservice = dataservice;
        this.apiservice = apiservice;
        this.route = route;
        this.toast = toast;
        this.loader = loader;
        this.formData = {};
        this.rows = [];
        this.columns = [];
        this.temp = [];
        this.rolesList = [];
        this.usersData = {};
        this.show = true;
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.formData1 = new FormData();
        this.fileData = File;
        this.regionTypes = dataservice.getOption();
        console.log('plan data ', this.regionTypes);
        this.permissions = {
            view: false,
            add: false,
            edit: false,
            all: false
        };
    }
    back() {
        this.route.navigate(['tables/regionTypes']);
    }
    ngOnInit() {
        if (this.regionTypes.isPLan === true) {
            this.formName = "Update region type";
        }
        else {
            this.formName = "Create region type";
        }
        this.regionTypeForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormGroup({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required),
        });
    }
    onChange(value) {
        let permissions = Object.keys(this.permissions);
        // this.permissions[value] = true
        for (let permission of permissions) {
            if (permission == value) {
                this.permissions[permission] = true;
            }
            else {
                this.permissions[permission] = false;
            }
        }
    }
    // addCategory() {
    //   console.log('add function calling')
    //   this.apiservice.addRegiontype(this.regionTypeForm.value).subscribe((res: any) => {
    //     console.log('resssssss', res)
    //   });
    // }
    // updatCategory() {
    //   console.log('update function calling')
    //   // this.category.planId = this.category.id;
    //   this.apiservice.updateRegiontype(this.regionTypes.id,this.regionTypes).subscribe((res: any) => {
    //     console.log('resssssss', res)
    //   });
    // }
    onSubmit() {
        this.submitted = true;
        console.log(this.regionTypeForm.value);
        if (!this.regionTypes.id) {
            // return this.addCategory();
        }
        else {
            // return this.updatCategory();
        }
    }
}
AddRegionTypeComponent.ɵfac = function AddRegionTypeComponent_Factory(t) { return new (t || AddRegionTypeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__.TablesService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__.DataService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__.ApiService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_7__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__.AppLoaderService)); };
AddRegionTypeComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({ type: AddRegionTypeComponent, selectors: [["app-add-region-type"]], decls: 17, vars: 4, consts: [[1, "p-0"], [1, "title"], [1, "card-title-text"], [3, "formGroup"], ["fxLayout", "row wrap"], ["fxFlex", "100", "fxFlex.gt-xs", "75", 1, "pr-1"], [1, "pb-1"], [1, "full-width"], ["matInput", "", "name", "Name", "placeholder", "name", "formControlName", "name", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["matInput", "", "name", "name", "placeholder", "name", "formControlName", "name", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["mat-raised-button", "", "color", "warn", 3, "click"], ["mat-raised-button", "", "color", "primary", 1, "submitBtn", 3, "click"], ["matInput", "", "name", "Name", "placeholder", "name", "formControlName", "name", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "name", "placeholder", "name", "formControlName", "name", 3, "ngModel", "ngModelChange"]], template: function AddRegionTypeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-card-title", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](4, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "mat-card-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](6, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](10, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](11, AddRegionTypeComponent_input_11_Template, 1, 1, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](12, AddRegionTypeComponent_input_12_Template, 1, 1, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](13, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function AddRegionTypeComponent_Template_button_click_13_listener() { return ctx.back(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](14, "Back");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](15, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function AddRegionTypeComponent_Template_button_click_15_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](16, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](ctx.formName);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formGroup", ctx.regionTypeForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.regionTypes.id);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", !ctx.regionTypes.id);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCard, _angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCardTitle, _angular_material_divider__WEBPACK_IMPORTED_MODULE_9__.MatDivider, _angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCardContent, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormGroupDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__.DefaultLayoutDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__.DefaultFlexDirective, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__.MatFormField, _angular_common__WEBPACK_IMPORTED_MODULE_12__.NgIf, _angular_material_button__WEBPACK_IMPORTED_MODULE_13__.MatButton, _angular_material_input__WEBPACK_IMPORTED_MODULE_14__.MatInput, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormControlName], styles: [".mat-card[_ngcontent-%COMP%] {\n  box-shadow: none !important;\n}\n\n.title[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\n.submitBtn[_ngcontent-%COMP%] {\n  align-items: flex-end;\n  margin-left: 15px;\n}\n\nmat-icon[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZC1yZWdpb24tdHlwZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDJCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtBQUNKIiwiZmlsZSI6ImFkZC1yZWdpb24tdHlwZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtY2FyZCB7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50aXRsZSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5zdWJtaXRCdG4ge1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7XHJcbn1cclxuXHJcbm1hdC1pY29uIHtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufSJdfQ== */"] });


/***/ }),

/***/ 79394:
/*!******************************************************************!*\
  !*** ./src/app/views/forms/add-regions/add-regions.component.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddRegionsComponent": () => (/* binding */ AddRegionsComponent)
/* harmony export */ });
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/snack-bar */ 89737);
/* harmony import */ var app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/shared/services/api.service */ 94761);
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ 77009);
/* harmony import */ var app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/dataservice.service */ 43396);
/* harmony import */ var app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/views/tables/tables.service */ 27582);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-ui-loader */ 21175);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-toastr */ 7100);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/card */ 4005);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/divider */ 51996);
/* harmony import */ var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/flex-layout/flex */ 23081);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ 90587);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/form-field */ 3403);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/input */ 23852);
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/select */ 5451);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/button */ 2955);
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/core */ 2106);




























function AddRegionsComponent_div_17_mat_option_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-option", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const regiontype_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("value", regiontype_r4.id);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", regiontype_r4.name, " ");
} }
function AddRegionsComponent_div_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, AddRegionsComponent_div_17_mat_option_1_Template, 2, 2, "mat-option", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const regiontype_r4 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", regiontype_r4.id != "1");
} }
function AddRegionsComponent_div_18_div_5_mat_option_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-option", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const region_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("value", region_r8.id);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", region_r8.name, " ");
} }
function AddRegionsComponent_div_18_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, AddRegionsComponent_div_18_div_5_mat_option_1_Template, 2, 2, "mat-option", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const region_r8 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", region_r8.parentId === 1);
} }
function AddRegionsComponent_div_18_Template(rf, ctx) { if (rf & 1) {
    const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-form-field", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "mat-label");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](3, "Select Continental");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "mat-select", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddRegionsComponent_div_18_Template_mat_select_ngModelChange_4_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r12); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r11.selectedContinental = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](5, AddRegionsComponent_div_18_div_5_Template, 2, 1, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("disabled", ctx_r1.region.id && ctx_r1.selectedRegiontype != 5)("ngModel", ctx_r1.selectedContinental);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngForOf", ctx_r1.regions);
} }
function AddRegionsComponent_div_19_div_5_mat_option_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-option", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const region_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("value", region_r14.id);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", region_r14.name, " ");
} }
function AddRegionsComponent_div_19_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, AddRegionsComponent_div_19_div_5_mat_option_1_Template, 2, 2, "mat-option", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const region_r14 = ctx.$implicit;
    const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", region_r14.parentId === ctx_r13.selectedContinental);
} }
function AddRegionsComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    const _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-form-field", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "mat-label");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](3, "Select National");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "mat-select", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddRegionsComponent_div_19_Template_mat_select_ngModelChange_4_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r18); const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r17.selectedNational = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](5, AddRegionsComponent_div_19_div_5_Template, 2, 1, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("disabled", ctx_r2.region.id && ctx_r2.selectedRegiontype != 5)("ngModel", ctx_r2.selectedNational);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngForOf", ctx_r2.regions);
} }
function AddRegionsComponent_div_20_div_5_mat_option_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-option", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const region_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("value", region_r20.id);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", region_r20.name, " ");
} }
function AddRegionsComponent_div_20_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, AddRegionsComponent_div_20_div_5_mat_option_1_Template, 2, 2, "mat-option", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const region_r20 = ctx.$implicit;
    const ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", region_r20.parentId === ctx_r19.selectedNational);
} }
function AddRegionsComponent_div_20_Template(rf, ctx) { if (rf & 1) {
    const _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-form-field", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "mat-label");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](3, "Select Regional");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "mat-select", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddRegionsComponent_div_20_Template_mat_select_ngModelChange_4_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r24); const ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r23.selectedRegional = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](5, AddRegionsComponent_div_20_div_5_Template, 2, 1, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("disabled", ctx_r3.region.id && ctx_r3.selectedRegiontype != 5)("ngModel", ctx_r3.selectedRegional);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngForOf", ctx_r3.regions);
} }
class AddRegionsComponent {
    constructor(service, dataRoute, dataservice, apiservice, snack, ngxLoader, route, toast, loader) {
        this.service = service;
        this.dataRoute = dataRoute;
        this.dataservice = dataservice;
        this.apiservice = apiservice;
        this.snack = snack;
        this.ngxLoader = ngxLoader;
        this.route = route;
        this.toast = toast;
        this.loader = loader;
        this.formData = {};
        this.rows = [];
        this.columns = [];
        this.temp = [];
        this.rolesList = [];
        this.usersData = {};
        this.show = true;
        this.message = "User Added Successfully!";
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.horizontalPosition = "center";
        this.verticalPosition = "bottom";
        let config = new _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__.MatSnackBarConfig();
        config.verticalPosition = this.verticalPosition;
        config.horizontalPosition = this.horizontalPosition;
        config.duration = this.setAutoHide ? this.autoHide : 0;
        let data = dataservice.getOption();
        if (Object.keys(data).length === 0 && data.constructor === Object) {
            this.formName = "Add Region";
        }
        else {
            this.region = data;
            this.formName = "Edit Region";
        }
    }
    updateRegion() {
        switch (this.selectedRegiontype) {
            case 1:
                this.parentId = 0;
                console.log("1");
                console.log(this.selectedRegiontype, "value");
                break;
            case 2:
                this.parentId = 1;
                console.log("2");
                console.log(this.selectedRegiontype, "value");
                break;
            case 3:
                this.parentId = this.selectedContinental;
                console.log("3");
                console.log(this.selectedRegiontype, "value");
                break;
            case 4:
                this.parentId = this.selectedNational;
                console.log("4");
                console.log(this.selectedRegiontype, "value");
                break;
            case 5:
                this.parentId = this.selectedRegional;
                console.log("5");
                console.log(this.selectedRegiontype, "value");
                break;
            default:
                console.log(this.selectedRegiontype, "value");
                //  this.parentId= 0  ;
                break;
        }
        let body = {
            parentId: this.parentId,
            level: this.selectedRegiontype,
            name: this.regionForm.controls.name.value,
            regiontypeId: this.selectedRegiontype,
        };
        console.log("body", body);
        // this.apiservice.updateRegion(this.region.id,body).subscribe((res: any) => {
        //   console.log("resssssss", res);
        // });
    }
    back() {
        this.route.navigate(["tables/regions"]);
    }
    ngOnInit() {
        // this.region = this.dataservice.getOption();
        // console.log(this.region);
        // this.regionForm = new FormGroup({
        //   name: new FormControl("", Validators.required),
        // });
        // this.getRegiontypes();
        // this.getRegions();
        // if (this.region.id) {
        //   this.selectedRegiontype = this.region?.level;
        //   switch (this.selectedRegiontype) {
        //     case 1:
        //       this.parentId = 0;
        //       console.log("1");
        //       console.log(this.selectedRegiontype, "value");
        //       break;
        //     case 2:
        //       this.apiservice
        //         .getRegionById(this.region.parentId)
        //         .subscribe((res: any) => {});
        //       break;
        //     case 3:
        //       this.apiservice
        //         .getRegionById(this.region.parentId)
        //         .subscribe((res: any) => {
        //           this.selectedContinental = res?.data?.id;
        //         });
        //       break;
        //     case 4:
        //       this.apiservice
        //         .getRegionById(this.region.parentId)
        //         .subscribe((res: any) => {
        //           this.selectedNational = res?.data?.id;
        //           this.apiservice
        //           .getRegionById(res?.data.parentId)
        //           .subscribe((response: any) => {
        //             this.selectedContinental = response?.data?.id;
        //           });
        //         });
        //       break;
        //     case 5:
        //       this.apiservice
        //         .getRegionById(this.region.parentId)
        //         .subscribe((res: any) => {
        //           this.selectedRegional = res?.data?.id;
        //           this.apiservice
        //             .getRegionById(res?.data.parentId)
        //             .subscribe((resp: any) => {
        //               this.selectedNational = resp?.data?.id;
        //               this.apiservice
        //                 .getRegionById(resp?.data.parentId)
        //                 .subscribe((response: any) => {
        //                   this.selectedContinental = response?.data?.id;
        //                 });
        //             });
        //         });
        //       break;
        //     default:
        //       console.log(this.selectedRegiontype, "value");
        //       //  this.parentId= 0  ;
        //       break;
        //   }
        //   if (this.selectedRegiontype != 0 && this.selectedRegiontype != 1) {
        //   }
        // }
    }
    // getRegiontypes() {
    //   this.apiservice.getRegiontypes().subscribe((res: any) => {
    //     this.regiontypes = res.data;
    //     this.temp = this.regiontypes;
    //     console.log("region types", this.regiontypes);
    //     // this.temp.forEach(element => {
    //     //   if (element.status == '1') {
    //     //     element.status = 'active'
    //     //   }
    //     //   if (element.status == '0') {
    //     //     element.status = 'inactive'
    //     //   }
    //     // })
    //     this.rows = this.temp;
    //   });
    // }
    // getRegions() {
    //   this.apiservice.getRegions().subscribe((res: any) => {
    //     this.regions = res.data;
    //     console.log("regions", this.regions);
    //   });
    // }
    // addRegion() {
    //   switch (this.selectedRegiontype) {
    //     case 1:
    //       this.parentId = 0;
    //       console.log("1");
    //       console.log(this.selectedRegiontype, "value");
    //       break;
    //     case 2:
    //       this.parentId = 1;
    //       console.log("2");
    //       console.log(this.selectedRegiontype, "value");
    //       break;
    //     case 3:
    //       this.parentId = this.selectedContinental;
    //       console.log("3");
    //       console.log(this.selectedRegiontype, "value");
    //       break;
    //     case 4:
    //       this.parentId = this.selectedNational;
    //       console.log("4");
    //       console.log(this.selectedRegiontype, "value");
    //       break;
    //     case 5:
    //       this.parentId = this.selectedRegional;
    //       console.log("5");
    //       console.log(this.selectedRegiontype, "value");
    //       break;
    //     default:
    //       console.log(this.selectedRegiontype, "value");
    //       //  this.parentId= 0  ;
    //       break;
    //   }
    //   let body = {
    //     parentId: this.parentId,
    //     level: this.selectedRegiontype,
    //     name: this.regionForm.controls.name.value,
    //     regiontypeId: this.selectedRegiontype,
    //   };
    //   console.log("body", body);
    //   this.apiservice.addRegion(body).subscribe((res: any) => {
    //     console.log("resssssss", res);
    //   });
    // }
    onSubmit() {
        this.submitted = true;
        if (!this.region.id) {
            // return this.addRegion();
        }
        else {
            this.updateRegion();
        }
    }
}
AddRegionsComponent.ɵfac = function AddRegionsComponent_Factory(t) { return new (t || AddRegionsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__.TablesService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__.DataService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__.ApiService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__.MatSnackBar), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__.NgxUiLoaderService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_8__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__.AppLoaderService)); };
AddRegionsComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({ type: AddRegionsComponent, selectors: [["app-add-regions"]], decls: 25, vars: 10, consts: [[1, "p-0"], [1, "title"], [1, "card-title-text"], ["fxLayout", "row wrap"], ["fxFlex", "100", "fxFlex.gt-xs", "50", 1, "pr-1"], [3, "formGroup", "ngSubmit"], [1, "pb-1"], [1, "full-width"], ["matInput", "", "name", "firstName", "placeholder", "Name", "formControlName", "name", 3, "ngModel", "ngModelChange"], [3, "disabled", "ngModel", "ngModelChange"], [4, "ngFor", "ngForOf"], ["class", "pb-1", 4, "ngIf"], ["mat-raised-button", "", "color", "warn", 3, "click"], ["mat-raised-button", "", "color", "primary", 1, "submitBtn", 3, "disabled", "click"], [3, "value", 4, "ngIf"], [3, "value"]], template: function AddRegionsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-card-title", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](4, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "mat-card-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](6, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](8, "form", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngSubmit", function AddRegionsComponent_Template_form_ngSubmit_8_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](10, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](11, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddRegionsComponent_Template_input_ngModelChange_11_listener($event) { return ctx.region.name = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](12, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](13, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](14, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](15, "Region Type");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](16, "mat-select", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function AddRegionsComponent_Template_mat_select_ngModelChange_16_listener($event) { return ctx.selectedRegiontype = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](17, AddRegionsComponent_div_17_Template, 2, 1, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](18, AddRegionsComponent_div_18_Template, 6, 3, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](19, AddRegionsComponent_div_19_Template, 6, 3, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](20, AddRegionsComponent_div_20_Template, 6, 3, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](21, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function AddRegionsComponent_Template_button_click_21_listener() { return ctx.back(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](22, "Back");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](23, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function AddRegionsComponent_Template_button_click_23_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](24, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](ctx.formName);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formGroup", ctx.regionForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.region.name);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("disabled", ctx.region.id && ctx.selectedRegiontype != 5)("ngModel", ctx.selectedRegiontype);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngForOf", ctx.regiontypes);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.selectedRegiontype === 3 || ctx.selectedRegiontype === 4 || ctx.selectedRegiontype === 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.selectedRegiontype === 4 || ctx.selectedRegiontype === 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.selectedRegiontype === 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("disabled", ctx.regionForm.invalid);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_9__.MatCard, _angular_material_card__WEBPACK_IMPORTED_MODULE_9__.MatCardTitle, _angular_material_divider__WEBPACK_IMPORTED_MODULE_10__.MatDivider, _angular_material_card__WEBPACK_IMPORTED_MODULE_9__.MatCardContent, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_11__.DefaultLayoutDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_11__.DefaultFlexDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_12__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_12__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_12__.FormGroupDirective, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_13__.MatFormField, _angular_material_input__WEBPACK_IMPORTED_MODULE_14__.MatInput, _angular_forms__WEBPACK_IMPORTED_MODULE_12__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_12__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_12__.FormControlName, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_13__.MatLabel, _angular_material_select__WEBPACK_IMPORTED_MODULE_15__.MatSelect, _angular_forms__WEBPACK_IMPORTED_MODULE_12__.NgModel, _angular_common__WEBPACK_IMPORTED_MODULE_16__.NgForOf, _angular_common__WEBPACK_IMPORTED_MODULE_16__.NgIf, _angular_material_button__WEBPACK_IMPORTED_MODULE_17__.MatButton, _angular_material_core__WEBPACK_IMPORTED_MODULE_18__.MatOption], styles: [".mat-card[_ngcontent-%COMP%] {\n  box-shadow: none !important;\n}\n\n.title[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\n.submitBtn[_ngcontent-%COMP%] {\n  align-items: flex-end;\n  margin-left: 15px;\n}\n\nmat-icon[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n\n  .egret-navy .mat-select-disabled .mat-select-value {\n  color: rgba(0, 0, 0, 0.925);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZC1yZWdpb25zLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksMkJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQkFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0FBQ0o7O0FBQ0E7RUFDSSwyQkFBQTtBQUVKIiwiZmlsZSI6ImFkZC1yZWdpb25zLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hdC1jYXJkIHtcclxuICAgIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRpdGxlIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnN1Ym1pdEJ0biB7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgICBtYXJnaW4tbGVmdDogMTVweDtcclxufVxyXG5cclxubWF0LWljb24ge1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbjo6bmctZGVlcCAuZWdyZXQtbmF2eSAubWF0LXNlbGVjdC1kaXNhYmxlZCAubWF0LXNlbGVjdC12YWx1ZSB7XHJcbiAgICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjkyNSlcclxufSJdfQ== */"] });


/***/ }),

/***/ 15263:
/*!**************************************************************!*\
  !*** ./src/app/views/forms/add-store/add-store.component.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddStoreComponent": () => (/* binding */ AddStoreComponent)
/* harmony export */ });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 90587);
/* harmony import */ var app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/shared/services/api.service */ 94761);
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ 77009);
/* harmony import */ var app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/dataservice.service */ 43396);
/* harmony import */ var app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/views/tables/tables.service */ 27582);
/* harmony import */ var _popup_popup_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../popup/popup.component */ 65101);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/snack-bar */ 89737);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ 7100);
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @agm/core */ 93333);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/dialog */ 86290);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common/http */ 28784);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/card */ 4005);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/divider */ 51996);
/* harmony import */ var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/flex-layout/flex */ 23081);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/form-field */ 3403);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/input */ 23852);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/button */ 2955);


// import { google } from '@agm/core/services/google-maps-types';






























const _c0 = ["search"];
function AddStoreComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](1, "Add Store");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
} }
function AddStoreComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](1, "Edit Store");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
} }
function AddStoreComponent_div_24_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](1, "mat-form-field", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](2, "input", 58, 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("keydown.enter", function AddStoreComponent_div_24_Template_input_keydown_enter_2_listener($event) { return $event.preventDefault(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](4, "button", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function AddStoreComponent_div_24_Template_button_click_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵnextContext"](); return ctx_r5.setData(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](5, "Set Location");
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
} }
class AddStoreComponent {
    constructor(service, dataRoute, dataservice, apiservice, snack, toast, route, loader, mapsAPILoader, ngZone, dialog, http) {
        this.service = service;
        this.dataRoute = dataRoute;
        this.dataservice = dataservice;
        this.apiservice = apiservice;
        this.snack = snack;
        this.toast = toast;
        this.route = route;
        this.loader = loader;
        this.mapsAPILoader = mapsAPILoader;
        this.ngZone = ngZone;
        this.dialog = dialog;
        this.http = http;
        this.store = {
            storeId: '',
            address: '',
            city: '',
            zipCode: '',
            staff: '',
            hoursOpen: '',
            demandForServiceStaff: '',
            servicesFulfilled: '',
            incomingCall: '',
            answeredCalls: '',
            capacityForAppointments: '',
            scheduledAppointments: '',
            actualCustomers: '',
            possibleCustomers: '',
            location: '',
            name: '',
            covidNewCaseCount: '',
            covidHospitalizationRate: '',
            localInfectedRatePercent: '',
            locaHospitalizationRate: '',
            nationalUnemploymentPercent: '',
            nationalPerCapitaDisposableIncome: '',
            nationalConsumerConfidenceIndex: '',
            localPerCapitaDisposableIncome: '',
            localConsumerConfidenceIndex: '',
            nationalFavorability: '',
            localFavorability: '',
            localDailyFootTraffic: '',
            amountSpentOnCallCenterInDollars: '',
            amountSpentOnAffiliateMarketingInDollars: '',
            directSpendingOnMarketingChannelInDollars: '',
            directSpendingOnMailMarketingInDollars: '',
            amountSpentOnDisplayMarketingInDollars: '',
            amountSpentOnEmailMarketingInDollars: '',
            amountSpentOnOnlineVideosMarketingInDollars: '',
            amountSpentOnOrganicSearchMarketingInDollars: '',
            amountSpentOnOrganicSocialMediaMarketingInDollars: '',
            amountPaidToLocalMarketingChannelInDollars: '',
            amountPaidToSearchMarketingInDollars: '',
            amountSpentOnRadioMarketingInDollars: '',
            rawDemand: '',
            // percentFulfilled: '',
            capacityToService: '',
            answerfulfilledDemand: '',
            competitores: ''
        };
        this.location = {
            longitude: '',
            latitude: ''
        };
        this.competitores = [];
        this.locationData = {
            address: '',
            lat: '',
            lng: ''
        };
        this.store = this.dataservice.getOption();
        console.log('store====>>', this.store);
        this.locationAutoComplete();
    }
    ngOnInit() {
        this.storeForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormGroup({
            storeID: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            storeManager: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            address: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            city: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            zipCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            staff: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            hoursOpen: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            demandForServiceStaff: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            servicesFulfilled: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            incomingCall: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            answeredCalls: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            capacityForAppointments: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            scheduledAppointments: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            actualCustomers: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            possibleCustomers: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            softdelete: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(),
            // location: this.location.coordinates,
            covidNewCaseCount: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            covidHospitalizationRate: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            localInfectedRatePercent: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            locaHospitalizationRate: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            nationalUnemploymentPercent: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            localUnemploymentPercent: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            nationalPerCapitaDisposableIncome: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            nationalConsumerConfidenceIndex: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            localPerCapitaDisposableIncome: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            localConsumerConfidenceIndex: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            nationalFavorability: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            localFavorability: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            localDailyFootTraffic: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            amountSpentOnCallCenterInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            amountSpentOnAffiliateMarketingInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            directSpendingOnMarketingChannelInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            directSpendingOnMailMarketingInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            amountSpentOnDisplayMarketingInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            amountSpentOnEmailMarketingInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            amountSpentOnOnlineVideosMarketingInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            amountSpentOnOrganicSearchMarketingInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            amountSpentOnReferralsInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            amountSpentOnTVMarketingInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            amountSpentOnOrganicSocialMediaMarketingInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            amountPaidToLocalMarketingChannelInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            amountPaidToSearchMarketingInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            amountSpentOnRadioMarketingInDollars: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            rawDemand: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            percentFulfilled: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            capacityToService: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            answerfulfilledDemand: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
        });
    }
    locationAutoComplete() {
        this.searchControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl();
        this.setCurrentLocation();
        this.mapsAPILoader.load().then(() => {
            var _a;
            this.geoCoder = new google.maps.Geocoder;
            let autocomplete = new google.maps.places.Autocomplete((_a = this.searchElementRef) === null || _a === void 0 ? void 0 : _a.nativeElement);
            autocomplete.addListener('place_changed', () => {
                this.ngZone.run(() => {
                    let place = autocomplete.getPlace();
                    this.address = place.formatted_address;
                    console.log('address', this.address);
                    // verify result
                    if (place.geometry === undefined || place.geometry === null) {
                        return;
                    }
                    // set latitude, longitude and zoom
                    this.latitude = place.geometry.location.lat();
                    this.longitude = place.geometry.location.lng();
                    this.zoom = 12;
                });
            });
        });
    }
    // getCurrentLocation(lat, lng) {
    //   this.lat = lat;
    //   this.lng = lng
    //   let key = "AIzaSyBhVQKXquGT9zTznHXq1A6YJCcrrC5HWv8"
    //   const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${key}`;
    //   return this.http.get(url)
    //     .subscribe(response => console.log('res ===', response))
    // }
    back() {
        this.route.navigate(['tables/filter']);
    }
    addStore() {
        let body = {
            storeID: this.storeForm.value.storeID,
            address: this.storeForm.value.address,
            storeManager: this.storeForm.value.storeManager,
            city: this.storeForm.value.city,
            zipCode: this.storeForm.value.zipCode,
            staff: this.storeForm.value.staff,
            hoursOpen: this.storeForm.value.hoursOpen,
            demandForServiceStaff: this.storeForm.value.demandForServiceStaff,
            servicesFulfilled: this.storeForm.value.servicesFulfilled,
            incomingCall: this.storeForm.value.incomingCall,
            answeredCalls: this.storeForm.value.answeredCalls,
            capacityForAppointments: this.storeForm.value.capacityForAppointments,
            scheduledAppointments: this.storeForm.value.scheduledAppointments,
            actualCustomers: this.storeForm.value.actualCustomers,
            possibleCustomers: this.storeForm.value.possibleCustomers,
            competitors: this.competitores,
            location: this.location,
            covidNewCaseCount: this.storeForm.value.covidNewCaseCount,
            covidHospitalizationRate: this.storeForm.value.covidHospitalizationRate,
            localInfectedRatePercent: this.storeForm.value.localInfectedRatePercent,
            locaHospitalizationRate: this.storeForm.value.locaHospitalizationRate,
            nationalUnemploymentPercent: this.storeForm.value.nationalUnemploymentPercent,
            localUnemploymentPercent: this.storeForm.value.localUnemploymentPercent,
            nationalPerCapitaDisposableIncome: this.storeForm.value.nationalPerCapitaDisposableIncome,
            nationalConsumerConfidenceIndex: this.storeForm.value.nationalConsumerConfidenceIndex,
            localPerCapitaDisposableIncome: this.storeForm.value.localPerCapitaDisposableIncome,
            localConsumerConfidenceIndex: this.storeForm.value.localConsumerConfidenceIndex,
            nationalFavorability: this.storeForm.value.nationalFavorability,
            localFavorability: this.storeForm.value.localFavorability,
            localDailyFootTraffic: this.storeForm.value.localDailyFootTraffic,
            amountSpentOnCallCenterInDollars: this.storeForm.value.amountSpentOnCallCenterInDollars,
            amountSpentOnAffiliateMarketingInDollars: this.storeForm.value.amountSpentOnAffiliateMarketingInDollars,
            directSpendingOnMarketingChannelInDollars: this.storeForm.value.directSpendingOnMarketingChannelInDollars,
            directSpendingOnMailMarketingInDollars: this.storeForm.value.directSpendingOnMailMarketingInDollars,
            amountSpentOnDisplayMarketingInDollars: this.storeForm.value.amountSpentOnDisplayMarketingInDollars,
            amountSpentOnEmailMarketingInDollars: this.storeForm.value.amountSpentOnEmailMarketingInDollars,
            amountSpentOnOnlineVideosMarketingInDollars: this.storeForm.value.amountSpentOnOnlineVideosMarketingInDollars,
            amountSpentOnOrganicSearchMarketingInDollars: this.storeForm.value.amountSpentOnOrganicSearchMarketingInDollars,
            amountSpentOnOrganicSocialMediaMarketingInDollars: this.storeForm.value.amountSpentOnOrganicSocialMediaMarketingInDollars,
            amountSpentOnReferralsInDollars: this.storeForm.value.amountSpentOnReferralsInDollars,
            amountSpentOnTVMarketingInDollars: this.storeForm.value.amountSpentOnTVMarketingInDollars,
            amountPaidToLocalMarketingChannelInDollars: this.storeForm.value.amountPaidToLocalMarketingChannelInDollars,
            amountPaidToSearchMarketingInDollars: this.storeForm.value.amountPaidToSearchMarketingInDollars,
            amountSpentOnRadioMarketingInDollars: this.storeForm.value.amountSpentOnRadioMarketingInDollars,
            rawDemand: this.storeForm.value.rawDemand,
            percentFulfilled: this.storeForm.value.percentFulfilled,
            capacityToService: this.storeForm.value.capacityToService,
            answerfulfilledDemand: this.storeForm.value.answerfulfilledDemand,
        };
        console.log('before res ==>', body);
        this.apiservice.createStore(body).subscribe((res) => {
            console.log('add user res ==>', res);
            this.route.navigate(['tables/store']);
        });
    }
    storeUpdate() {
        let id = this.store._id;
        console.log('storeee iddd', id);
        if (id) {
            this.apiservice.storeUpdate(id, this.store).subscribe((res) => {
                console.log('update res', res);
                this.route.navigate(['tables/store']);
            });
        }
    }
    onSubmit() {
        this.submitted = true;
        // this.addUser();
        if (!this.store._id) {
            return this.addStore();
        }
        else {
            this.storeUpdate();
        }
    }
    // getUsers() {
    //   console.log('add function calling')
    //   this.apiservice.getAllStore().subscribe((res: any) => {
    //     console.log('resssssss', res)
    //     this.stores = res.data
    //     console.log('get users ===>>', this.stores)
    //   });
    // }
    setData() {
        this.location = {
            longitude: this.longitude,
            latitude: this.latitude
        };
        console.log('location', this.location);
    }
    // Get Current Location Coordinates
    setCurrentLocation() {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
                this.zoom = 8;
                this.getAddress(this.latitude, this.longitude);
            });
        }
    }
    getAddress(latitude, longitude) {
        console.log('lat lng', latitude, longitude);
        this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
            console.log('results', results);
            console.log('status', status);
            if (status === 'OK') {
                if (results[0]) {
                    this.zoom = 12;
                    this.address = results[0].formatted_address;
                }
                else {
                    window.alert('No results found');
                }
            }
            else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }
    openData(data) {
        const dialogRef = this.dialog.open(_popup_popup_component__WEBPACK_IMPORTED_MODULE_4__.PopupComponent, {
            width: '650px',
            height: '500px',
            data: {
                dataKey: data,
            }
        });
        dialogRef.afterClosed().subscribe(res => {
            console.log(this.dataservice.getOption());
            this.competitores.push(this.dataservice.getOption());
            console.log('===', this.competitores);
        });
    }
}
AddStoreComponent.ɵfac = function AddStoreComponent_Factory(t) { return new (t || AddStoreComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__.TablesService), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_7__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__.DataService), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__.ApiService), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_8__.MatSnackBar), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_9__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_7__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__.AppLoaderService), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_agm_core__WEBPACK_IMPORTED_MODULE_10__.MapsAPILoader), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgZone), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_11__.MatDialog), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_12__.HttpClient)); };
AddStoreComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({ type: AddStoreComponent, selectors: [["app-add-store"]], viewQuery: function AddStoreComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵviewQuery"](_c0, 5);
    } if (rf & 2) {
        let _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵloadQuery"]()) && (ctx.searchElementRef = _t.first);
    } }, decls: 155, vars: 49, consts: [[1, "p-0"], [1, "title"], ["class", "card-title-text", 4, "ngIf"], [3, "formGroup"], ["fxLayout", "row wrap"], ["fxFlex", "100", "fxFlex.gt-xs", "50", 1, "pr-1"], [1, "pb-1"], [1, "full-width"], ["matInput", "", "name", "storeId", "placeholder", "storeID", "formControlName", "storeID", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "storeManager", "placeholder", "storeManager", "formControlName", "storeManager", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "address", "placeholder", "address", "formControlName", "address", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "city", "placeholder", "city", "formControlName", "city", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "zipCode", "placeholder", "zipCode", "formControlName", "zipCode", 3, "ngModel", "ngModelChange"], ["class", "pb-1", 4, "ngIf"], ["matInput", "", "name", "staff", "placeholder", "staff", "formControlName", "staff", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "hoursOpen", "placeholder", "hoursOpen", "formControlName", "hoursOpen", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "demandForServiceStaff", "placeholder", "demandForServiceStaff", "formControlName", "demandForServiceStaff", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "capacityToService", "formControlName", "capacityToService", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "servicesFulfilled", "placeholder", "servicesFulfilled", "formControlName", "servicesFulfilled", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "incomingCall", "placeholder", "incomingCall", "formControlName", "incomingCall", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "answeredCalls", "placeholder", "answeredCalls", "formControlName", "answeredCalls", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "capacityForAppointments", "placeholder", "capacityForAppointments", "formControlName", "capacityForAppointments", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "scheduledAppointments", "placeholder", "scheduledAppointments", "formControlName", "scheduledAppointments", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "answerfulfilledDemand", "formControlName", "answerfulfilledDemand", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "rawDemand", "formControlName", "rawDemand", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "actualCustomers", "placeholder", "actualCustomers", "formControlName", "actualCustomers", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "possibleCustomers", "placeholder", "possibleCustomers", "formControlName", "possibleCustomers", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "covidNewCaseCount", "formControlName", "covidNewCaseCount", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "covidHospitalizationRate", "formControlName", "covidHospitalizationRate", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "localInfectedRatePercent", "formControlName", "localInfectedRatePercent", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "locaHospitalizationRate", "formControlName", "locaHospitalizationRate", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "nationalUnemploymentPercent", "formControlName", "nationalUnemploymentPercent", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "localUnemploymentPercent", "formControlName", "localUnemploymentPercent", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "nationalPerCapitaDisposableIncome", "formControlName", "nationalPerCapitaDisposableIncome", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "nationalConsumerConfidenceIndex", "formControlName", "nationalConsumerConfidenceIndex", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "localPerCapitaDisposableIncome", "formControlName", "localPerCapitaDisposableIncome", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "localConsumerConfidenceIndex", "formControlName", "localConsumerConfidenceIndex", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "nationalFavorability", "formControlName", "nationalFavorability", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "localFavorability", "formControlName", "localFavorability", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "localDailyFootTraffic", "formControlName", "localDailyFootTraffic", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "amountSpentOnCallCenterInDollars", "formControlName", "amountSpentOnCallCenterInDollars", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "amountSpentOnAffiliateMarketingInDollars", "formControlName", "amountSpentOnAffiliateMarketingInDollars", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "directSpendingOnMarketingChannelInDollars", "formControlName", "directSpendingOnMarketingChannelInDollars", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "directSpendingOnMailMarketingInDollars", "formControlName", "directSpendingOnMailMarketingInDollars", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "amountSpentOnDisplayMarketingInDollars", "formControlName", "amountSpentOnDisplayMarketingInDollars", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "amountSpentOnEmailMarketingInDollars", "formControlName", "amountSpentOnEmailMarketingInDollars", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "amountSpentOnOnlineVideosMarketingInDollars", "formControlName", "amountSpentOnOnlineVideosMarketingInDollars", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "amountSpentOnReferralsInDollars", "formControlName", "amountSpentOnReferralsInDollars", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "amountSpentOnTVMarketingInDollars", "formControlName", "amountSpentOnTVMarketingInDollars", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "amountSpentOnOrganicSearchMarketingInDollars", "formControlName", "amountSpentOnOrganicSearchMarketingInDollars", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "amountSpentOnOrganicSocialMediaMarketingInDollars", "formControlName", "amountSpentOnOrganicSocialMediaMarketingInDollars", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "amountPaidToLocalMarketingChannelInDollars", "formControlName", "amountPaidToLocalMarketingChannelInDollars", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "amountPaidToSearchMarketingInDollars", "formControlName", "amountPaidToSearchMarketingInDollars", 3, "ngModel", "ngModelChange"], ["matInput", "", "placeholder", "amountSpentOnRadioMarketingInDollars", "formControlName", "amountSpentOnRadioMarketingInDollars", 3, "ngModel", "ngModelChange"], ["mat-raised-button", "", "color", "primary", 3, "click"], ["mat-raised-button", "", "color", "warn", 3, "click"], ["mat-raised-button", "", "color", "primary", 1, "submitBtn", 3, "click"], [1, "card-title-text"], ["matInput", "", "type", "text", "formControlName", "location", "placeholder", "Enter Location", "autocorrect", "off", "autocapitalize", "off", "spellcheck", "off", "type", "text", 1, "form-control", 3, "keydown.enter"], ["search", ""]], template: function AddStoreComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](1, "mat-card-title", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](2, AddStoreComponent_div_2_Template, 2, 0, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](3, AddStoreComponent_div_3_Template, 2, 0, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](4, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](5, "mat-card-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](6, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](10, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](11, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_11_listener($event) { return ctx.store.storeID = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](12, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](13, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](14, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_14_listener($event) { return ctx.store.storeManager = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](16, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](17, "input", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_17_listener($event) { return ctx.store.address = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](18, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](19, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](20, "input", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_20_listener($event) { return ctx.store.city = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](21, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](22, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](23, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_23_listener($event) { return ctx.store.zipCode = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtemplate"](24, AddStoreComponent_div_24_Template, 6, 0, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](25, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](26, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](27, "input", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_27_listener($event) { return ctx.store.staff = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](28, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](29, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](30, "input", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_30_listener($event) { return ctx.store.hoursOpen = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](31, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](32, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](33, "input", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_33_listener($event) { return ctx.store.demandForServiceStaff = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](34, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](35, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](36, "input", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_36_listener($event) { return ctx.store.capacityToService = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](37, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](38, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](39, "input", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_39_listener($event) { return ctx.store.servicesFulfilled = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](40, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](41, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](42, "input", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_42_listener($event) { return ctx.store.incomingCall = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](43, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](44, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](45, "input", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_45_listener($event) { return ctx.store.answeredCalls = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](46, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](47, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](48, "input", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_48_listener($event) { return ctx.store.capacityForAppointments = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](49, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](50, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](51, "input", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_51_listener($event) { return ctx.store.scheduledAppointments = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](52, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](53, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](54, "input", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_54_listener($event) { return ctx.store.answerfulfilledDemand = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](55, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](56, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](57, "input", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_57_listener($event) { return ctx.store.rawDemand = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](58, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](59, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](60, "input", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_60_listener($event) { return ctx.store.actualCustomers = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](61, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](62, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](63, "input", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_63_listener($event) { return ctx.store.possibleCustomers = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](64, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](65, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](66, "input", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_66_listener($event) { return ctx.store.covidNewCaseCount = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](67, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](68, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](69, "input", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_69_listener($event) { return ctx.store.covidHospitalizationRate = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](70, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](71, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](72, "input", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_72_listener($event) { return ctx.store.localInfectedRatePercent = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](73, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](74, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](75, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](76, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](77, "input", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_77_listener($event) { return ctx.store.locaHospitalizationRate = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](78, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](79, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](80, "input", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_80_listener($event) { return ctx.store.nationalUnemploymentPercent = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](81, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](82, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](83, "input", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_83_listener($event) { return ctx.store.localUnemploymentPercent = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](84, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](85, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](86, "input", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_86_listener($event) { return ctx.store.nationalPerCapitaDisposableIncome = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](87, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](88, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](89, "input", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_89_listener($event) { return ctx.store.nationalConsumerConfidenceIndex = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](90, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](91, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](92, "input", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_92_listener($event) { return ctx.store.localPerCapitaDisposableIncome = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](93, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](94, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](95, "input", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_95_listener($event) { return ctx.store.localConsumerConfidenceIndex = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](96, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](97, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](98, "input", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_98_listener($event) { return ctx.store.nationalFavorability = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](99, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](100, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](101, "input", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_101_listener($event) { return ctx.store.localFavorability = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](102, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](103, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](104, "input", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_104_listener($event) { return ctx.store.localDailyFootTraffic = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](105, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](106, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](107, "input", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_107_listener($event) { return ctx.store.amountSpentOnCallCenterInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](108, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](109, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](110, "input", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_110_listener($event) { return ctx.store.amountSpentOnAffiliateMarketingInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](111, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](112, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](113, "input", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_113_listener($event) { return ctx.store.directSpendingOnMarketingChannelInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](114, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](115, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](116, "input", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_116_listener($event) { return ctx.store.directSpendingOnMailMarketingInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](117, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](118, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](119, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](120, "input", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_120_listener($event) { return ctx.store.amountSpentOnDisplayMarketingInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](121, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](122, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](123, "input", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_123_listener($event) { return ctx.store.amountSpentOnEmailMarketingInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](124, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](125, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](126, "input", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_126_listener($event) { return ctx.store.amountSpentOnOnlineVideosMarketingInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](127, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](128, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](129, "input", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_129_listener($event) { return ctx.store.amountSpentOnReferralsInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](130, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](131, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](132, "input", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_132_listener($event) { return ctx.store.amountSpentOnTVMarketingInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](133, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](134, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](135, "input", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_135_listener($event) { return ctx.store.amountSpentOnOrganicSearchMarketingInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](136, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](137, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](138, "input", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_138_listener($event) { return ctx.store.amountSpentOnOrganicSocialMediaMarketingInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](139, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](140, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](141, "input", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_141_listener($event) { return ctx.store.amountPaidToLocalMarketingChannelInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](142, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](143, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](144, "input", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_144_listener($event) { return ctx.store.amountPaidToSearchMarketingInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](145, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](146, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](147, "input", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function AddStoreComponent_Template_input_ngModelChange_147_listener($event) { return ctx.store.amountSpentOnRadioMarketingInDollars = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](148, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](149, "button", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function AddStoreComponent_Template_button_click_149_listener() { return ctx.openData(""); });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](150, "Add Competitores");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](151, "button", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function AddStoreComponent_Template_button_click_151_listener() { return ctx.back(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](152, "Back");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](153, "button", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function AddStoreComponent_Template_button_click_153_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](154, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", !ctx.store._id);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", ctx.store._id);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("formGroup", ctx.storeForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.storeID);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.storeManager);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.address);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.city);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.zipCode);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngIf", !ctx.store._id);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.staff);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.hoursOpen);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.demandForServiceStaff);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.capacityToService);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.servicesFulfilled);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.incomingCall);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.answeredCalls);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.capacityForAppointments);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.scheduledAppointments);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.answerfulfilledDemand);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.rawDemand);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.actualCustomers);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.possibleCustomers);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.covidNewCaseCount);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.covidHospitalizationRate);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.localInfectedRatePercent);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.locaHospitalizationRate);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.nationalUnemploymentPercent);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.localUnemploymentPercent);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.nationalPerCapitaDisposableIncome);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.nationalConsumerConfidenceIndex);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.localPerCapitaDisposableIncome);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.localConsumerConfidenceIndex);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.nationalFavorability);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.localFavorability);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.localDailyFootTraffic);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.amountSpentOnCallCenterInDollars);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.amountSpentOnAffiliateMarketingInDollars);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.directSpendingOnMarketingChannelInDollars);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.directSpendingOnMailMarketingInDollars);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.amountSpentOnDisplayMarketingInDollars);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.amountSpentOnEmailMarketingInDollars);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.amountSpentOnOnlineVideosMarketingInDollars);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.amountSpentOnReferralsInDollars);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.amountSpentOnTVMarketingInDollars);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.amountSpentOnOrganicSearchMarketingInDollars);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.amountSpentOnOrganicSocialMediaMarketingInDollars);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.amountPaidToLocalMarketingChannelInDollars);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.amountPaidToSearchMarketingInDollars);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.store.amountSpentOnRadioMarketingInDollars);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_13__.MatCard, _angular_material_card__WEBPACK_IMPORTED_MODULE_13__.MatCardTitle, _angular_common__WEBPACK_IMPORTED_MODULE_14__.NgIf, _angular_material_divider__WEBPACK_IMPORTED_MODULE_15__.MatDivider, _angular_material_card__WEBPACK_IMPORTED_MODULE_13__.MatCardContent, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormGroupDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_16__.DefaultLayoutDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_16__.DefaultFlexDirective, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_17__.MatFormField, _angular_material_input__WEBPACK_IMPORTED_MODULE_18__.MatInput, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControlName, _angular_material_button__WEBPACK_IMPORTED_MODULE_19__.MatButton], styles: [".mat-card[_ngcontent-%COMP%] {\n  box-shadow: none !important;\n}\n\n.title[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\n.submitBtn[_ngcontent-%COMP%] {\n  align-items: flex-end;\n  margin-left: 15px;\n}\n\nmat-icon[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZC1zdG9yZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDJCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtBQUNKIiwiZmlsZSI6ImFkZC1zdG9yZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtY2FyZCB7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50aXRsZSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5zdWJtaXRCdG4ge1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7XHJcbn1cclxuXHJcbm1hdC1pY29uIHtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufSJdfQ== */"] });


/***/ }),

/***/ 27553:
/*!****************************************************************!*\
  !*** ./src/app/views/forms/basic-form/basic-form.component.ts ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BasicFormComponent": () => (/* binding */ BasicFormComponent)
/* harmony export */ });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 90587);
/* harmony import */ var app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/shared/services/api.service */ 94761);
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ 77009);
/* harmony import */ var app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/dataservice.service */ 43396);
/* harmony import */ var app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/views/tables/tables.service */ 27582);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ 7100);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/card */ 4005);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/divider */ 51996);
/* harmony import */ var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/flex-layout/flex */ 23081);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/form-field */ 3403);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/input */ 23852);
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/select */ 5451);
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/core */ 2106);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/button */ 2955);























class BasicFormComponent {
    constructor(service, dataRoute, dataservice, apiservice, route, toast, loader) {
        this.service = service;
        this.dataRoute = dataRoute;
        this.dataservice = dataservice;
        this.apiservice = apiservice;
        this.route = route;
        this.toast = toast;
        this.loader = loader;
        this.formData = {};
        this.rows = [];
        this.columns = [];
        this.temp = [];
        this.role = {
            type: '',
            permissions: {}
        };
        this.rolesList = [];
        this.usersData = {};
        this.show = true;
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.permissions = { view: false,
            add: false,
            edit: false,
            all: false };
    }
    back() {
        this.route.navigate(['tables/roles']);
    }
    ngOnInit() {
        this.roleForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormGroup({
            type: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required),
            permissions: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(''),
        });
    }
    onChange(value) {
        let permissions = Object.keys(this.permissions);
        // this.permissions[value] = true
        for (let permission of permissions) {
            if (permission == value) {
                this.permissions[permission] = true;
            }
            else {
                this.permissions[permission] = false;
            }
        }
    }
    // addRoles() {
    //   this.role.permissions = this.permissions;
    //     this.apiservice.addRoles(this.role).subscribe((res: any) => {
    //       console.log('resssssss', res)
    //        });
    //   }
    onSubmit() {
        this.submitted = true;
        console.log(this.roleForm.value);
        //  this.addRoles();
    }
}
BasicFormComponent.ɵfac = function BasicFormComponent_Factory(t) { return new (t || BasicFormComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__.TablesService), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__.DataService), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__.ApiService), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_7__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdirectiveInject"](app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__.AppLoaderService)); };
BasicFormComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵdefineComponent"]({ type: BasicFormComponent, selectors: [["app-basic-form"]], decls: 28, vars: 3, consts: [[1, "p-0"], [1, "title"], [1, "card-title-text"], [3, "formGroup", "ngSubmit"], ["fxLayout", "row wrap"], ["fxFlex", "100", "fxFlex.gt-xs", "75", 1, "pr-1"], [1, "pb-1"], [1, "full-width"], ["matInput", "", "name", "type", "placeholder", "Role type", "formControlName", "type", 3, "ngModel", "ngModelChange"], ["name", "permissions", "placeholder", "Permissions", "formControlName", "permissions", 3, "ngModelChange"], ["value", "view"], ["value", "add"], ["value", "edit"], ["value", "all"], ["mat-raised-button", "", "color", "warn", 3, "click"], ["mat-raised-button", "", "color", "primary", 1, "submitBtn"]], template: function BasicFormComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](1, "mat-card-title", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelement"](4, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](5, "mat-card-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](6, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngSubmit", function BasicFormComponent_Template_form_ngSubmit_6_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](10, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](11, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function BasicFormComponent_Template_input_ngModelChange_11_listener($event) { return ctx.role.type = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](12, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](13, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](14, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](15, "mat-select", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("ngModelChange", function BasicFormComponent_Template_mat_select_ngModelChange_15_listener($event) { return ctx.onChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](16, "mat-option", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](17, "view");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](18, "mat-option", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](19, "add");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](20, "mat-option", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](21, "edit");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](22, "mat-option", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](23, "all");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](24, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵlistener"]("click", function BasicFormComponent_Template_button_click_24_listener() { return ctx.back(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](25, "Back");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementStart"](26, "button", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtext"](27, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵtextInterpolate"](ctx.formName);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("formGroup", ctx.roleForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_5__["ɵɵproperty"]("ngModel", ctx.role.type);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCard, _angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCardTitle, _angular_material_divider__WEBPACK_IMPORTED_MODULE_9__.MatDivider, _angular_material_card__WEBPACK_IMPORTED_MODULE_8__.MatCardContent, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormGroupDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__.DefaultLayoutDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_10__.DefaultFlexDirective, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_11__.MatFormField, _angular_material_input__WEBPACK_IMPORTED_MODULE_12__.MatInput, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControlName, _angular_material_select__WEBPACK_IMPORTED_MODULE_13__.MatSelect, _angular_material_core__WEBPACK_IMPORTED_MODULE_14__.MatOption, _angular_material_button__WEBPACK_IMPORTED_MODULE_15__.MatButton], styles: [".mat-card[_ngcontent-%COMP%] {\r\n    box-shadow: none !important;\r\n}\r\n\r\n.title[_ngcontent-%COMP%] {\r\n    text-align: center;\r\n}\r\n\r\n.submitBtn[_ngcontent-%COMP%] {\r\n    align-items: flex-end;\r\n    margin-left: 15px;\r\n}\r\n\r\nmat-icon[_ngcontent-%COMP%] {\r\n    cursor: pointer;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJhc2ljLWZvcm0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLDJCQUEyQjtBQUMvQjs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxlQUFlO0FBQ25CIiwiZmlsZSI6ImJhc2ljLWZvcm0uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXQtY2FyZCB7XHJcbiAgICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50aXRsZSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5zdWJtaXRCdG4ge1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7XHJcbn1cclxuXHJcbm1hdC1pY29uIHtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufSJdfQ== */"] });


/***/ }),

/***/ 91814:
/*!******************************************************************!*\
  !*** ./src/app/views/forms/file-upload/file-upload.component.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FileUploadComponent": () => (/* binding */ FileUploadComponent)
/* harmony export */ });
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-file-upload */ 19403);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/card */ 4005);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/divider */ 51996);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_flex_layout_extended__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/flex-layout/extended */ 27448);
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/progress-bar */ 58682);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button */ 2955);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/icon */ 68159);










function FileUploadComponent_tbody_30_tr_1_mat_icon_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-icon", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "check");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function FileUploadComponent_tbody_30_tr_1_mat_icon_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-icon", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function FileUploadComponent_tbody_30_tr_1_mat_icon_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-icon", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "error");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function (a0) { return { "width": a0 }; };
function FileUploadComponent_tbody_30_tr_1_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](5, "number");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "td");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "mat-progress-bar", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "td", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, FileUploadComponent_tbody_30_tr_1_mat_icon_11_Template, 2, 0, "mat-icon", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, FileUploadComponent_tbody_30_tr_1_mat_icon_12_Template, 2, 0, "mat-icon", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, FileUploadComponent_tbody_30_tr_1_mat_icon_13_Template, 2, 0, "mat-icon", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "td", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileUploadComponent_tbody_30_tr_1_Template_button_click_15_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const item_r4 = restoredCtx.$implicit; return item_r4.upload(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Upload");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileUploadComponent_tbody_30_tr_1_Template_button_click_17_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const item_r4 = restoredCtx.$implicit; return item_r4.cancel(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Cancel");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "button", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileUploadComponent_tbody_30_tr_1_Template_button_click_19_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const item_r4 = restoredCtx.$implicit; return item_r4.remove(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Remove");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r4 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r4 == null ? null : item_r4.file == null ? null : item_r4.file.name);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](5, 9, (item_r4 == null ? null : item_r4.file == null ? null : item_r4.file.size) / 1024 / 1024, ".2"), " MB");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](12, _c0, item_r4.progress + "%"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", item_r4.progress);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", item_r4.isSuccess);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", item_r4.isCancel);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", item_r4.isError);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", item_r4.isReady || item_r4.isUploading || item_r4.isSuccess);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !item_r4.isUploading);
} }
function FileUploadComponent_tbody_30_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tbody");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, FileUploadComponent_tbody_30_tr_1_Template, 21, 14, "tr", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.uploader.queue);
} }
const _c1 = function () { return { padding: "0 1.2rem" }; };
function FileUploadComponent_ng_template_31_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Queue is empty");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](1, _c1));
} }
const _c2 = function (a0) { return { "dz-file-over": a0 }; };
class FileUploadComponent {
    constructor() {
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_1__.FileUploader({ url: 'https://evening-anchorage-315.herokuapp.com/api/' });
        this.hasBaseDropZoneOver = false;
        this.console = console;
    }
    ngOnInit() {
    }
    fileOverBase(e) {
        this.hasBaseDropZoneOver = e;
    }
}
FileUploadComponent.ɵfac = function FileUploadComponent_Factory(t) { return new (t || FileUploadComponent)(); };
FileUploadComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: FileUploadComponent, selectors: [["app-file-upload"]], decls: 44, vars: 12, consts: [[1, "p-0"], [1, ""], [1, "card-title-text"], [1, "mb-1"], ["type", "file", "ng2FileSelect", "", "multiple", "", 3, "uploader"], ["type", "file", "ng2FileSelect", "", 3, "uploader"], ["ng2FileDrop", "", 1, "fileupload-drop-zone", "mb-24", 3, "ngClass", "uploader", "fileOver"], [1, "default-table", "mat-box-shadow", "mb-24", 2, "width", "100%"], ["width", "30%"], [4, "ngIf", "ngIfElse"], ["tableNoData", ""], [1, "progress", "mb-1"], ["color", "primary", "mode", "determinate", 1, "", 3, "value"], ["mat-raised-button", "", "color", "primary", 1, "mx-4", 3, "disabled", "click"], ["mat-raised-button", "", "color", "accent", 1, "mx-4", 3, "disabled", "click"], ["mat-raised-button", "", "color", "warn", 1, "mx-4", 3, "disabled", "click"], [4, "ngFor", "ngForOf"], ["nowrap", ""], [1, "progress", 2, "margin-bottom", "0"], ["role", "progressbar", 1, "progress-bar", 3, "ngStyle"], ["color", "primary", 4, "ngIf"], ["color", "accent", 4, "ngIf"], ["color", "warn", 4, "ngIf"], ["mat-raised-button", "", "color", "warn", 1, "mx-4", 3, "click"], ["color", "primary"], ["color", "accent"], ["color", "warn"], [3, "ngStyle"]], template: function FileUploadComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card-title", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "File upload");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-card-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Multiple");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Single");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "input", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("fileOver", function FileUploadComponent_Template_div_fileOver_15_listener($event) { return ctx.fileOverBase($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, " Drop file here ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "table", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "thead");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "tr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "th", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Name");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Size");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Progress");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Status");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "th");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Actions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](30, FileUploadComponent_tbody_30_Template, 2, 1, "tbody", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](31, FileUploadComponent_ng_template_31_Template, 2, 2, "ng-template", null, 10, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Queue progress:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "mat-progress-bar", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileUploadComponent_Template_button_click_38_listener() { return ctx.uploader.uploadAll(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Upload all");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileUploadComponent_Template_button_click_40_listener() { return ctx.uploader.cancelAll(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Cancel all");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "button", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileUploadComponent_Template_button_click_42_listener() { return ctx.uploader.clearQueue(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Remove all");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("uploader", ctx.uploader);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("uploader", ctx.uploader);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](10, _c2, ctx.hasBaseDropZoneOver))("uploader", ctx.uploader);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.uploader.queue.length)("ngIfElse", _r1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.uploader.progress);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.uploader.getNotUploadedItems().length);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.uploader.isUploading);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx.uploader.queue.length);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_2__.MatCard, _angular_material_card__WEBPACK_IMPORTED_MODULE_2__.MatCardTitle, _angular_material_divider__WEBPACK_IMPORTED_MODULE_3__.MatDivider, _angular_material_card__WEBPACK_IMPORTED_MODULE_2__.MatCardContent, ng2_file_upload__WEBPACK_IMPORTED_MODULE_1__.FileSelectDirective, ng2_file_upload__WEBPACK_IMPORTED_MODULE_1__.FileDropDirective, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgClass, _angular_flex_layout_extended__WEBPACK_IMPORTED_MODULE_5__.DefaultClassDirective, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgIf, _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_6__.MatProgressBar, _angular_material_button__WEBPACK_IMPORTED_MODULE_7__.MatButton, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgForOf, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgStyle, _angular_flex_layout_extended__WEBPACK_IMPORTED_MODULE_5__.DefaultStyleDirective, _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__.MatIcon], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.DecimalPipe], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmaWxlLXVwbG9hZC5jb21wb25lbnQuY3NzIn0= */"] });


/***/ }),

/***/ 72148:
/*!******************************************************************!*\
  !*** ./src/app/views/forms/follow-user/follow-user.component.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FollowUserComponent": () => (/* binding */ FollowUserComponent)
/* harmony export */ });
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ 86290);
/* harmony import */ var app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/shared/services/api.service */ 94761);
/* harmony import */ var app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/services/dataservice.service */ 43396);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/icon */ 68159);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/list */ 42873);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/divider */ 51996);













function FollowUserComponent_mat_list_7_mat_list_item_1_img_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "img", 13);
} if (rf & 2) {
    const user_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("src", user_r4.followingId.avatar, _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsanitizeUrl"]);
} }
function FollowUserComponent_mat_list_7_mat_list_item_1_img_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "img", 14);
} }
function FollowUserComponent_mat_list_7_mat_list_item_1_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "mat-list-item", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function FollowUserComponent_mat_list_7_mat_list_item_1_Template_div_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r9); const user_r4 = restoredCtx.$implicit; const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2); return ctx_r8.profile(user_r4.followingId); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](2, FollowUserComponent_mat_list_7_mat_list_item_1_img_2_Template, 1, 1, "img", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](3, FollowUserComponent_mat_list_7_mat_list_item_1_img_3_Template, 1, 0, "img", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "h5", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const user_r4 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", user_r4.followingId.avatar);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", !user_r4.followingId.avatar);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](user_r4.followingId.userName);
} }
function FollowUserComponent_mat_list_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "mat-list", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, FollowUserComponent_mat_list_7_mat_list_item_1_Template, 7, 3, "mat-list-item", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngForOf", ctx_r0.followerList);
} }
function FollowUserComponent_mat_list_8_mat_list_item_1_img_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "img", 13);
} if (rf & 2) {
    const user_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("src", user_r11.userId.avatar, _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsanitizeUrl"]);
} }
function FollowUserComponent_mat_list_8_mat_list_item_1_img_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "img", 14);
} }
function FollowUserComponent_mat_list_8_mat_list_item_1_Template(rf, ctx) { if (rf & 1) {
    const _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "mat-list-item", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function FollowUserComponent_mat_list_8_mat_list_item_1_Template_div_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r16); const user_r11 = restoredCtx.$implicit; const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2); return ctx_r15.profile(user_r11.userId); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](2, FollowUserComponent_mat_list_8_mat_list_item_1_img_2_Template, 1, 1, "img", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](3, FollowUserComponent_mat_list_8_mat_list_item_1_img_3_Template, 1, 0, "img", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "h5", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](7, "mat-divider");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const user_r11 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", user_r11.userId.avatar);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", !user_r11.userId.avatar);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](user_r11.userId.userName);
} }
function FollowUserComponent_mat_list_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "mat-list", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, FollowUserComponent_mat_list_8_mat_list_item_1_Template, 8, 3, "mat-list-item", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngForOf", ctx_r1.followingList);
} }
function FollowUserComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "h4", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate1"]("NO ", ctx_r2.data.status, " !!!");
} }
class FollowUserComponent {
    constructor(apiservice, route, dataservice, dialogRef, data) {
        this.apiservice = apiservice;
        this.route = route;
        this.dataservice = dataservice;
        this.dialogRef = dialogRef;
        this.data = data;
    }
    ngOnInit() {
        if (this.data.status === 'follower') {
            // this.getFollowers();
        }
        else {
            // this.getFollowing();
        }
    }
    // getFollowers() {
    //   this.apiservice.getFollowers(this.data.userId).subscribe((res: any) => {
    //     this.followerList = res
    //     // this.userImg = this.followerList.avatar
    //   });
    // };
    // getFollowing() {
    //   this.apiservice.getFollowing(this.data.userId).subscribe((res: any) => {
    //     this.followingList = res
    //     // this.userImg = this.followingList.avatar
    //   });
    // }
    profile(data) {
        data.isSelect = false;
        this.dataservice.setOption(data);
        this.route.navigate(['forms/view-profile']);
        this.dialogRef.close(true);
    }
    back() {
        this.dialogRef.close();
    }
}
FollowUserComponent.ɵfac = function FollowUserComponent_Factory(t) { return new (t || FollowUserComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__.ApiService), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_1__.DataService), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__.MatDialogRef), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__.MAT_DIALOG_DATA)); };
FollowUserComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({ type: FollowUserComponent, selectors: [["app-follow-user"]], decls: 10, vars: 4, consts: [[1, "main"], [1, "text-uppercase", "text-center"], [1, "float-right", "cursor-pointer", 3, "click"], [1, "content"], ["role", "list", 4, "ngIf"], [4, "ngIf"], ["role", "list"], ["role", "listitem", 4, "ngFor", "ngForOf"], ["role", "listitem"], [1, "d-flex", "flex-row", "cursor-pointer", 3, "click"], ["width", "30", "class", "user-img rounded-circle mr-2", 3, "src", 4, "ngIf"], ["src", "../../../../assets/images/no-user.jpg", "width", "30", "class", "user-img rounded-circle mr-2", 4, "ngIf"], [1, "font-weight-bold", "mt-3", "text-capitalize", "text-primary"], ["width", "30", 1, "user-img", "rounded-circle", "mr-2", 3, "src"], ["src", "../../../../assets/images/no-user.jpg", "width", "30", 1, "user-img", "rounded-circle", "mr-2"]], template: function FollowUserComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "header");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "h4", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "mat-icon", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function FollowUserComponent_Template_mat_icon_click_4_listener() { return ctx.back(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](5, "close");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](7, FollowUserComponent_mat_list_7_Template, 2, 1, "mat-list", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](8, FollowUserComponent_mat_list_8_Template, 2, 1, "mat-list", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](9, FollowUserComponent_div_9_Template, 3, 1, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate1"](" ", ctx.data.status, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.followerList && ctx.followerList.length);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.followingList && ctx.followingList.length);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.followingList && ctx.followingList.length === 0 || ctx.followerList && ctx.followerList.length === 0);
    } }, directives: [_angular_material_icon__WEBPACK_IMPORTED_MODULE_5__.MatIcon, _angular_common__WEBPACK_IMPORTED_MODULE_6__.NgIf, _angular_material_list__WEBPACK_IMPORTED_MODULE_7__.MatList, _angular_common__WEBPACK_IMPORTED_MODULE_6__.NgForOf, _angular_material_list__WEBPACK_IMPORTED_MODULE_7__.MatListItem, _angular_material_divider__WEBPACK_IMPORTED_MODULE_8__.MatDivider], styles: [".main[_ngcontent-%COMP%]   header[_ngcontent-%COMP%] {\n  background-color: rgba(34, 42, 69, 0.96);\n  color: white;\n}\n\nheader[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%] {\n  padding: 15px;\n}\n\n.user-img[_ngcontent-%COMP%] {\n  margin-top: 4px;\n  width: 45px;\n  height: 45px;\n}\n\n.mat-list-item[_ngcontent-%COMP%] {\n  height: 65px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZvbGxvdy11c2VyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksd0NBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxhQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7QUFDSiIsImZpbGUiOiJmb2xsb3ctdXNlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluIGhlYWRlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDM0LCA0MiwgNjksIDAuOTYpO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG5oZWFkZXIgaDQge1xyXG4gICAgcGFkZGluZzogMTVweDtcclxufVxyXG5cclxuLnVzZXItaW1nIHtcclxuICAgIG1hcmdpbi10b3A6IDRweDtcclxuICAgIHdpZHRoOiA0NXB4O1xyXG4gICAgaGVpZ2h0OiA0NXB4O1xyXG59XHJcblxyXG4ubWF0LWxpc3QtaXRlbSB7XHJcbiAgICBoZWlnaHQ6IDY1cHg7XHJcbn0iXX0= */"] });


/***/ }),

/***/ 43929:
/*!*********************************************!*\
  !*** ./src/app/views/forms/forms.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppFormsModule": () => (/* binding */ AppFormsModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/forms */ 90587);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/material/button */ 2955);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/card */ 4005);
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/material/checkbox */ 66665);
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material/core */ 2106);
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/datepicker */ 68197);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/material/icon */ 68159);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/input */ 23852);
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/list */ 42873);
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/progress-bar */ 58682);
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/radio */ 46825);
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/material/stepper */ 67675);
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/flex-layout */ 98115);
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ngx-quill */ 29432);
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! @swimlane/ngx-datatable */ 68501);
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ng2-file-upload */ 19403);
/* harmony import */ var _basic_form_basic_form_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./basic-form/basic-form.component */ 27553);
/* harmony import */ var _rich_text_editor_rich_text_editor_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./rich-text-editor/rich-text-editor.component */ 12521);
/* harmony import */ var _file_upload_file_upload_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./file-upload/file-upload.component */ 91814);
/* harmony import */ var _forms_routing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./forms.routing */ 47608);
/* harmony import */ var _wizard_wizard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./wizard/wizard.component */ 96389);
/* harmony import */ var _user_manage_user_manage_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user-manage/user-manage.component */ 98593);
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/material/select */ 5451);
/* harmony import */ var _update_user_update_user_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./update-user/update-user.component */ 45704);
/* harmony import */ var _add_plan_add_plan_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./add-plan/add-plan.component */ 41623);
/* harmony import */ var _viewprofile_viewprofile_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./viewprofile/viewprofile.component */ 85341);
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! @angular/material/grid-list */ 63357);
/* harmony import */ var _view_post_view_post_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./view-post/view-post.component */ 69429);
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @angular/material/tooltip */ 26844);
/* harmony import */ var _follow_user_follow_user_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./follow-user/follow-user.component */ 72148);
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @angular/material/menu */ 49399);
/* harmony import */ var _add_gift_add_gift_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./add-gift/add-gift.component */ 56501);
/* harmony import */ var _add_category_add_category_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./add-category/add-category.component */ 52426);
/* harmony import */ var _add_regions_add_regions_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./add-regions/add-regions.component */ 79394);
/* harmony import */ var _add_region_type_add_region_type_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./add-region-type/add-region-type.component */ 19916);
/* harmony import */ var _add_store_add_store_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./add-store/add-store.component */ 15263);
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! @agm/core */ 93333);
/* harmony import */ var _popup_popup_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./popup/popup.component */ 65101);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/core */ 3184);











































class AppFormsModule {
}
AppFormsModule.ɵfac = function AppFormsModule_Factory(t) { return new (t || AppFormsModule)(); };
AppFormsModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_17__["ɵɵdefineNgModule"]({ type: AppFormsModule });
AppFormsModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_17__["ɵɵdefineInjector"]({ imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_18__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_19__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_19__.ReactiveFormsModule,
            _angular_material_input__WEBPACK_IMPORTED_MODULE_20__.MatInputModule,
            _angular_material_list__WEBPACK_IMPORTED_MODULE_21__.MatListModule,
            _angular_material_card__WEBPACK_IMPORTED_MODULE_22__.MatCardModule,
            _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_23__.MatDatepickerModule,
            _angular_material_core__WEBPACK_IMPORTED_MODULE_24__.MatNativeDateModule,
            _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_25__.MatProgressBarModule,
            _angular_material_radio__WEBPACK_IMPORTED_MODULE_26__.MatRadioModule,
            _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_27__.MatCheckboxModule,
            _angular_material_select__WEBPACK_IMPORTED_MODULE_28__.MatSelectModule,
            _angular_material_button__WEBPACK_IMPORTED_MODULE_29__.MatButtonModule,
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_30__.MatIconModule,
            _angular_material_stepper__WEBPACK_IMPORTED_MODULE_31__.MatStepperModule,
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_32__.FlexLayoutModule,
            _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_33__.MatTooltipModule,
            _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_34__.MatGridListModule,
            _angular_material_menu__WEBPACK_IMPORTED_MODULE_35__.MatMenuModule,
            ngx_quill__WEBPACK_IMPORTED_MODULE_36__.QuillModule,
            _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_37__.NgxDatatableModule,
            ng2_file_upload__WEBPACK_IMPORTED_MODULE_38__.FileUploadModule,
            _angular_router__WEBPACK_IMPORTED_MODULE_39__.RouterModule.forChild(_forms_routing__WEBPACK_IMPORTED_MODULE_3__.FormsRoutes),
            _agm_core__WEBPACK_IMPORTED_MODULE_40__.AgmCoreModule.forRoot({
                apiKey: "AIzaSyBhVQKXquGT9zTznHXq1A6YJCcrrC5HWv8",
                libraries: ['places']
            }),
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_17__["ɵɵsetNgModuleScope"](AppFormsModule, { declarations: [_basic_form_basic_form_component__WEBPACK_IMPORTED_MODULE_0__.BasicFormComponent,
        _rich_text_editor_rich_text_editor_component__WEBPACK_IMPORTED_MODULE_1__.RichTextEditorComponent,
        _file_upload_file_upload_component__WEBPACK_IMPORTED_MODULE_2__.FileUploadComponent,
        _wizard_wizard_component__WEBPACK_IMPORTED_MODULE_4__.WizardComponent,
        _user_manage_user_manage_component__WEBPACK_IMPORTED_MODULE_5__.UserManageComponent,
        _update_user_update_user_component__WEBPACK_IMPORTED_MODULE_6__.UpdateUserComponent,
        _viewprofile_viewprofile_component__WEBPACK_IMPORTED_MODULE_8__.ViewprofileComponent,
        _view_post_view_post_component__WEBPACK_IMPORTED_MODULE_9__.ViewPostComponent,
        _follow_user_follow_user_component__WEBPACK_IMPORTED_MODULE_10__.FollowUserComponent,
        _add_plan_add_plan_component__WEBPACK_IMPORTED_MODULE_7__.AddPlanComponent,
        _add_gift_add_gift_component__WEBPACK_IMPORTED_MODULE_11__.AddGiftComponent,
        _add_category_add_category_component__WEBPACK_IMPORTED_MODULE_12__.AddCategoryComponent,
        _add_regions_add_regions_component__WEBPACK_IMPORTED_MODULE_13__.AddRegionsComponent,
        _add_region_type_add_region_type_component__WEBPACK_IMPORTED_MODULE_14__.AddRegionTypeComponent,
        _add_store_add_store_component__WEBPACK_IMPORTED_MODULE_15__.AddStoreComponent,
        _popup_popup_component__WEBPACK_IMPORTED_MODULE_16__.PopupComponent], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_18__.CommonModule,
        _angular_forms__WEBPACK_IMPORTED_MODULE_19__.FormsModule,
        _angular_forms__WEBPACK_IMPORTED_MODULE_19__.ReactiveFormsModule,
        _angular_material_input__WEBPACK_IMPORTED_MODULE_20__.MatInputModule,
        _angular_material_list__WEBPACK_IMPORTED_MODULE_21__.MatListModule,
        _angular_material_card__WEBPACK_IMPORTED_MODULE_22__.MatCardModule,
        _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_23__.MatDatepickerModule,
        _angular_material_core__WEBPACK_IMPORTED_MODULE_24__.MatNativeDateModule,
        _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_25__.MatProgressBarModule,
        _angular_material_radio__WEBPACK_IMPORTED_MODULE_26__.MatRadioModule,
        _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_27__.MatCheckboxModule,
        _angular_material_select__WEBPACK_IMPORTED_MODULE_28__.MatSelectModule,
        _angular_material_button__WEBPACK_IMPORTED_MODULE_29__.MatButtonModule,
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_30__.MatIconModule,
        _angular_material_stepper__WEBPACK_IMPORTED_MODULE_31__.MatStepperModule,
        _angular_flex_layout__WEBPACK_IMPORTED_MODULE_32__.FlexLayoutModule,
        _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_33__.MatTooltipModule,
        _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_34__.MatGridListModule,
        _angular_material_menu__WEBPACK_IMPORTED_MODULE_35__.MatMenuModule,
        ngx_quill__WEBPACK_IMPORTED_MODULE_36__.QuillModule,
        _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_37__.NgxDatatableModule,
        ng2_file_upload__WEBPACK_IMPORTED_MODULE_38__.FileUploadModule, _angular_router__WEBPACK_IMPORTED_MODULE_39__.RouterModule, _agm_core__WEBPACK_IMPORTED_MODULE_40__.AgmCoreModule] }); })();


/***/ }),

/***/ 47608:
/*!**********************************************!*\
  !*** ./src/app/views/forms/forms.routing.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FormsRoutes": () => (/* binding */ FormsRoutes)
/* harmony export */ });
/* harmony import */ var _basic_form_basic_form_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./basic-form/basic-form.component */ 27553);
/* harmony import */ var _rich_text_editor_rich_text_editor_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./rich-text-editor/rich-text-editor.component */ 12521);
/* harmony import */ var _file_upload_file_upload_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./file-upload/file-upload.component */ 91814);
/* harmony import */ var _wizard_wizard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./wizard/wizard.component */ 96389);
/* harmony import */ var _user_manage_user_manage_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user-manage/user-manage.component */ 98593);
/* harmony import */ var _update_user_update_user_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update-user/update-user.component */ 45704);
/* harmony import */ var _viewprofile_viewprofile_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./viewprofile/viewprofile.component */ 85341);
/* harmony import */ var _add_plan_add_plan_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./add-plan/add-plan.component */ 41623);
/* harmony import */ var _add_gift_add_gift_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./add-gift/add-gift.component */ 56501);
/* harmony import */ var _add_category_add_category_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./add-category/add-category.component */ 52426);
/* harmony import */ var _add_region_type_add_region_type_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./add-region-type/add-region-type.component */ 19916);
/* harmony import */ var _add_regions_add_regions_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./add-regions/add-regions.component */ 79394);
/* harmony import */ var _add_store_add_store_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./add-store/add-store.component */ 15263);













const FormsRoutes = [
    {
        path: '',
        children: [{
                path: 'basic',
                component: _basic_form_basic_form_component__WEBPACK_IMPORTED_MODULE_0__.BasicFormComponent,
                data: { title: 'Roles', breadcrumb: 'ROLES' }
            },
            {
                path: 'add-store',
                component: _add_store_add_store_component__WEBPACK_IMPORTED_MODULE_12__.AddStoreComponent,
                data: { title: 'Add-Store', breadcrumb: 'Add-Store' }
            },
            {
                path: 'editor',
                component: _rich_text_editor_rich_text_editor_component__WEBPACK_IMPORTED_MODULE_1__.RichTextEditorComponent,
                data: { title: 'Editor', breadcrumb: 'EDITOR' }
            }, {
                path: 'upload',
                component: _file_upload_file_upload_component__WEBPACK_IMPORTED_MODULE_2__.FileUploadComponent,
                data: { title: 'Upload', breadcrumb: 'UPLOAD' }
            }, {
                path: 'wizard',
                component: _wizard_wizard_component__WEBPACK_IMPORTED_MODULE_3__.WizardComponent,
                data: { title: 'Wizard', breadcrumb: 'WIZARD' }
            }, {
                path: 'user',
                component: _user_manage_user_manage_component__WEBPACK_IMPORTED_MODULE_4__.UserManageComponent,
                data: { title: 'User', breadcrumb: 'USER' }
            }, {
                path: 'view-profile/:id',
                component: _viewprofile_viewprofile_component__WEBPACK_IMPORTED_MODULE_6__.ViewprofileComponent,
                data: { title: 'view-profile', breadcrumb: 'view-profile' }
            },
            {
                path: 'add-plan',
                component: _add_plan_add_plan_component__WEBPACK_IMPORTED_MODULE_7__.AddPlanComponent,
                data: { title: 'add-plan', breadcrumb: 'add-plan' }
            },
            {
                path: 'add-gift',
                component: _add_gift_add_gift_component__WEBPACK_IMPORTED_MODULE_8__.AddGiftComponent,
                data: { title: 'add-gift', breadcrumb: 'add-gift' }
            },
            {
                path: 'add-category',
                component: _add_category_add_category_component__WEBPACK_IMPORTED_MODULE_9__.AddCategoryComponent,
                data: { title: 'add-category', breadcrumb: 'add-category' }
            },
            {
                path: 'add-regiontype',
                component: _add_region_type_add_region_type_component__WEBPACK_IMPORTED_MODULE_10__.AddRegionTypeComponent,
                data: { title: 'add-regiontype', breadcrumb: 'add-regiontype' }
            },
            {
                path: 'add-regions',
                component: _add_regions_add_regions_component__WEBPACK_IMPORTED_MODULE_11__.AddRegionsComponent,
                data: { title: 'add-regions', breadcrumb: 'add-regions' }
            },
            {
                path: 'update',
                component: _update_user_update_user_component__WEBPACK_IMPORTED_MODULE_5__.UpdateUserComponent,
                data: { title: 'User', breadcrumb: 'USER' }
            }]
    }
];


/***/ }),

/***/ 65101:
/*!******************************************************!*\
  !*** ./src/app/views/forms/popup/popup.component.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PopupComponent": () => (/* binding */ PopupComponent)
/* harmony export */ });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ 90587);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ 86290);
/* harmony import */ var app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/shared/services/dataservice.service */ 43396);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/button */ 2955);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/form-field */ 3403);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/input */ 23852);










class PopupComponent {
    constructor(dialogRef, dataservice, data) {
        this.dialogRef = dialogRef;
        this.dataservice = dataservice;
        this.data = data;
    }
    ngOnInit() {
        this.Competitores = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormGroup({
            companyName: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormControl(''),
            fulfilledCombinedDemand: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormControl(''),
            fulfilledBrandDemand: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormControl(''),
            fulfilledDemandforBrand: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormControl(''),
        });
    }
    close() {
        this.dialogRef.close();
    }
    add() {
        console.log(this.Competitores.value);
        this.dataservice.setOption(this.Competitores.value);
        this.close();
    }
}
PopupComponent.ɵfac = function PopupComponent_Factory(t) { return new (t || PopupComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__.MatDialogRef), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_0__.DataService), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__.MAT_DIALOG_DATA)); };
PopupComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({ type: PopupComponent, selectors: [["app-popup"]], decls: 23, vars: 1, consts: [[1, "container"], ["mat-dialog-actions", ""], ["mat-button", "", "mat-dialog-close", "", 1, "close", 3, "click"], ["id", "title"], [1, "box"], [3, "formGroup"], [1, "pb-1"], [1, "full-width"], ["matInput", "", "placeholder", "companyName", "formControlName", "companyName"], ["matInput", "", "placeholder", "fulfilledCombinedDemand", "formControlName", "fulfilledCombinedDemand"], ["matInput", "", "placeholder", "fulfilledBrandDemand", "formControlName", "fulfilledBrandDemand"], ["matInput", "", "placeholder", "fulfilledDemandforBrand", "formControlName", "fulfilledDemandforBrand"], ["id", "btnnn", "mat-raised-button", "", "color", "primary", 1, "submitBtn", 3, "click"]], template: function PopupComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function PopupComponent_Template_button_click_2_listener() { return ctx.close(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](3, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "h1", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](5, "Competitores");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](7, "form", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](8, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](9, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](10, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](11, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](12, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](13, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](14, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](15, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](16, "input", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](17, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](18, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](19, "input", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](20, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](21, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function PopupComponent_Template_button_click_21_listener() { return ctx.add(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](22, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("formGroup", ctx.Competitores);
    } }, directives: [_angular_material_button__WEBPACK_IMPORTED_MODULE_4__.MatButton, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormGroupDirective, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__.MatFormField, _angular_material_input__WEBPACK_IMPORTED_MODULE_6__.MatInput, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormControlName], styles: [".container[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.box[_ngcontent-%COMP%] {\n  width: 500px;\n  margin: auto;\n  padding-top: 20px;\n}\n\nh1#title[_ngcontent-%COMP%] {\n  margin-top: -20px;\n}\n\nbutton.mat-focus-indicator.close.mat-button.mat-button-base[_ngcontent-%COMP%] {\n  display: block;\n  margin-left: auto;\n}\n\nbutton#btnnn[_ngcontent-%COMP%] {\n  margin-left: auto;\n  margin-right: auto;\n  display: block;\n  margin-top: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBvcHVwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7QUFDSjs7QUFFQTtFQUNJLGNBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQUNKIiwiZmlsZSI6InBvcHVwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lciB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmJveCB7XHJcbiAgICB3aWR0aDogNTAwcHg7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcclxufVxyXG5cclxuaDEjdGl0bGUge1xyXG4gICAgbWFyZ2luLXRvcDogLTIwcHg7XHJcbn1cclxuXHJcbmJ1dHRvbi5tYXQtZm9jdXMtaW5kaWNhdG9yLmNsb3NlLm1hdC1idXR0b24ubWF0LWJ1dHRvbi1iYXNlIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbn1cclxuXHJcbmJ1dHRvbiNidG5ubiB7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbWFyZ2luLXRvcDogMTVweDtcclxufSJdfQ== */"] });


/***/ }),

/***/ 12521:
/*!****************************************************************************!*\
  !*** ./src/app/views/forms/rich-text-editor/rich-text-editor.component.ts ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RichTextEditorComponent": () => (/* binding */ RichTextEditorComponent)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/card */ 4005);
/* harmony import */ var ngx_quill__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-quill */ 29432);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ 90587);




class RichTextEditorComponent {
    constructor() {
        this.editorData = `<h1>Egret | Angular material admin</h1>
  <p><a href="http://devegret.com" target="_blank"><strong>DevEgret</strong></a></p>
  <p><br></p><p><strong >Lorem Ipsum</strong>
  <span>&nbsp;is simply dummy text of the printing and typesetting industry. 
  Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a 
  galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</span></p>`;
    }
    ngOnInit() {
    }
    onContentChanged() { }
    onSelectionChanged() { }
}
RichTextEditorComponent.ɵfac = function RichTextEditorComponent_Factory(t) { return new (t || RichTextEditorComponent)(); };
RichTextEditorComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: RichTextEditorComponent, selectors: [["app-rich-text-editor"]], decls: 3, vars: 1, consts: [[1, "p-0"], ["theme", "snow", "onSelectionChanged", "onSelectionChanged()", 3, "ngModel", "ngModelChange", "onContentChanged"]], template: function RichTextEditorComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card-content", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "quill-editor", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function RichTextEditorComponent_Template_quill_editor_ngModelChange_2_listener($event) { return ctx.editorData = $event; })("onContentChanged", function RichTextEditorComponent_Template_quill_editor_onContentChanged_2_listener() { return ctx.onContentChanged(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.editorData);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_1__.MatCard, _angular_material_card__WEBPACK_IMPORTED_MODULE_1__.MatCardContent, ngx_quill__WEBPACK_IMPORTED_MODULE_2__.QuillEditorComponent, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.NgModel], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyaWNoLXRleHQtZWRpdG9yLmNvbXBvbmVudC5jc3MifQ== */"] });


/***/ }),

/***/ 45704:
/*!******************************************************************!*\
  !*** ./src/app/views/forms/update-user/update-user.component.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateUserComponent": () => (/* binding */ UpdateUserComponent)
/* harmony export */ });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 90587);
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/snack-bar */ 89737);
/* harmony import */ var app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/shared/services/api.service */ 94761);
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ 77009);
/* harmony import */ var app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/dataservice.service */ 43396);
/* harmony import */ var app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/views/tables/tables.service */ 27582);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-ui-loader */ 21175);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ 7100);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/card */ 4005);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/divider */ 51996);
/* harmony import */ var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/flex-layout/flex */ 23081);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/form-field */ 3403);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/input */ 23852);
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/select */ 5451);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/button */ 2955);
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/core */ 2106);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/icon */ 68159);





























function UpdateUserComponent_mat_option_18_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-option", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const gender_r2 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("value", gender_r2.value);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate1"](" ", gender_r2.name, " ");
} }
function UpdateUserComponent_div_32_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-form-field", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "input", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UpdateUserComponent_div_32_Template_input_ngModelChange_2_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r3.user.password = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "mat-icon", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function UpdateUserComponent_div_32_Template_mat_icon_click_3_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r4); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r5.show = !ctx_r5.show; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("type", ctx_r1.show ? "password" : "text")("formControl", ctx_r1.userForm.controls["password"])("ngModel", ctx_r1.user.password);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](ctx_r1.show ? "visibility_off" : "visibility");
} }
class UpdateUserComponent {
    constructor(service, dataRoute, dataservice, apiservice, snack, ngxLoader, route, toast, loader) {
        this.service = service;
        this.dataRoute = dataRoute;
        this.dataservice = dataservice;
        this.apiservice = apiservice;
        this.snack = snack;
        this.ngxLoader = ngxLoader;
        this.route = route;
        this.toast = toast;
        this.loader = loader;
        this.formData = {};
        this.rows = [];
        this.columns = [];
        this.temp = [];
        this.user = {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            roleId: '5ff5a043c51e0042898fe15e',
            phoneNumber: '',
            sex: '',
        };
        // roles: any[] = [
        //   { name: 'SuperAdmin', value: 'SA' },
        //   { name: 'Admin', value: 'A' },
        //   { name: 'User', value: 'U' }
        // ];
        this.genders = [
            { name: 'Male', value: 'male' },
            { name: 'Female', value: 'female' },
            { name: 'Other', value: 'other' }
        ];
        this.rolesList = [];
        this.usersData = {};
        this.show = true;
        this.message = 'User Added Successfully!';
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.horizontalPosition = 'center';
        this.verticalPosition = 'bottom';
        let config = new _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__.MatSnackBarConfig();
        config.verticalPosition = this.verticalPosition;
        config.horizontalPosition = this.horizontalPosition;
        config.duration = this.setAutoHide ? this.autoHide : 0;
        let data = dataservice.getOption();
        if (Object.keys(data).length === 0 && data.constructor === Object) {
            this.formName = "Add user";
            this.user.email = null;
            this.user.password = null;
        }
        else {
            this.user = data;
            this.formName = "Edit User";
        }
    }
    back() {
        this.route.navigate(['tables/filter']);
    }
    // getRoles() {
    //   this.isLoading = true;
    //   this.loader.open();
    //   this.apiservice.getRoles().subscribe((res) => {
    //     this.rolesList = res
    //     this.loader.close();
    //     console.log('role list', this.rolesList);
    //     this.isLoading = false;
    //   });
    // }
    ngOnInit() {
        // this.getRoles();
        // this.user = this.dataservice.getOption();
        this.userForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormGroup({
            firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            phoneNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            sex: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            addressLine1: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            addressLine2: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            roleId: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            city: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            country: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            zipCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            // lat: new FormControl(''),
            // long: new FormControl(''),
            // stripeToken: new FormControl(''),
            // stripeKey: new FormControl(''),
            // ssn: new FormControl('',),
            // deviceToken: new FormControl(''),
        });
    }
    addUsers() {
        // let email = this.user.email.toLowerCase();
        // this.user.email = email;
        // this.apiservice.addUsers(this.user).subscribe((res: any) => {
        //   console.log('resssssss', res)
        // });
    }
    onSubmit() {
        this.submitted = true;
        if (!this.user.id) {
            return this.addUsers();
        }
    }
}
UpdateUserComponent.ɵfac = function UpdateUserComponent_Factory(t) { return new (t || UpdateUserComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_3__.TablesService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_7__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__.DataService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__.ApiService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__.MatSnackBar), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ngx_ui_loader__WEBPACK_IMPORTED_MODULE_8__.NgxUiLoaderService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_7__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_9__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_1__.AppLoaderService)); };
UpdateUserComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({ type: UpdateUserComponent, selectors: [["app-update-user"]], decls: 46, vars: 22, consts: [[1, "p-0"], [1, "title"], [1, "card-title-text"], [3, "formGroup", "ngSubmit"], ["fxLayout", "row wrap"], ["fxFlex", "100", "fxFlex.gt-xs", "50", 1, "pr-1"], [1, "pb-1"], [1, "full-width"], ["matInput", "", "name", "firstName", "placeholder", "Name", "formControlName", "firstName", 3, "ngModel", "ngModelChange"], ["matInput", "", "type", "email", "name", "email", "placeholder", "Email", "formControlName", "email", 3, "ngModel", "ngModelChange"], ["name", "sex", "placeholder", "Gender", "formControlName", "sex", 3, "ngModel", "ngModelChange"], [3, "value", 4, "ngFor", "ngForOf"], ["matInput", "", "name", "address_line1", "placeholder", "Address Line1", 3, "formControl", "ngModel", "ngModelChange"], ["matInput", "", "name", "country", "placeholder", "Country", 3, "formControl", "ngModel", "ngModelChange"], ["matInput", "", "name", "zip_code", "placeholder", "Zip Code", "maxlength", "6", 3, "formControl", "ngModel", "ngModelChange"], ["matInput", "", "name", "lastName", "placeholder", "Last name", 3, "formControl", "ngModel", "ngModelChange"], ["class", "pb-1", 4, "ngIf"], ["matInput", "", "name", "phone", "placeholder", "Phone number", "maxlength", "10", 3, "formControl", "ngModel", "ngModelChange"], ["matInput", "", "name", "address_line2", "placeholder", "Address Line2", 3, "formControl", "ngModel", "ngModelChange"], ["matInput", "", "name", "city", "placeholder", "City", 3, "formControl", "ngModel", "ngModelChange"], ["mat-raised-button", "", "color", "warn", 3, "click"], ["mat-raised-button", "", "color", "primary", 1, "submitBtn", 3, "disabled"], [3, "value"], ["matInput", "", "name", "password", "placeholder", "Password", 3, "type", "formControl", "ngModel", "ngModelChange"], ["matSuffix", "", 1, "cursor-pointer", 3, "click"]], template: function UpdateUserComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-card-title", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](4, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "mat-card-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](6, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngSubmit", function UpdateUserComponent_Template_form_ngSubmit_6_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](10, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](11, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UpdateUserComponent_Template_input_ngModelChange_11_listener($event) { return ctx.user.firstName = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](12, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](13, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](14, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UpdateUserComponent_Template_input_ngModelChange_14_listener($event) { return ctx.user.email = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](16, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](17, "mat-select", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UpdateUserComponent_Template_mat_select_ngModelChange_17_listener($event) { return ctx.user.sex = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](18, UpdateUserComponent_mat_option_18_Template, 2, 2, "mat-option", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](19, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](20, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](21, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UpdateUserComponent_Template_input_ngModelChange_21_listener($event) { return ctx.user.addressLine1 = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](22, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](23, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](24, "input", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UpdateUserComponent_Template_input_ngModelChange_24_listener($event) { return ctx.user.country = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](25, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](26, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](27, "input", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UpdateUserComponent_Template_input_ngModelChange_27_listener($event) { return ctx.user.zipCode = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](28, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](29, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](30, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](31, "input", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UpdateUserComponent_Template_input_ngModelChange_31_listener($event) { return ctx.user.lastName = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](32, UpdateUserComponent_div_32_Template, 5, 4, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](33, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](34, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](35, "input", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UpdateUserComponent_Template_input_ngModelChange_35_listener($event) { return ctx.user.phoneNumber = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](36, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](37, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](38, "input", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UpdateUserComponent_Template_input_ngModelChange_38_listener($event) { return ctx.user.addressLine2 = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](39, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](40, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](41, "input", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UpdateUserComponent_Template_input_ngModelChange_41_listener($event) { return ctx.user.city = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](42, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function UpdateUserComponent_Template_button_click_42_listener() { return ctx.back(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](43, "Back");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](44, "button", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](45, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](ctx.formName);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formGroup", ctx.userForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.user.firstName);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.user.email);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.user.sex);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngForOf", ctx.genders);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formControl", ctx.userForm.controls["addressLine1"])("ngModel", ctx.user.addressLine1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formControl", ctx.userForm.controls["country"])("ngModel", ctx.user.country);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formControl", ctx.userForm.controls["zipCode"])("ngModel", ctx.user.zipCode);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formControl", ctx.userForm.controls["lastName"])("ngModel", ctx.user.lastName);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", !ctx.user._id);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formControl", ctx.userForm.controls["phoneNumber"])("ngModel", ctx.user.phoneNumber);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formControl", ctx.userForm.controls["addressLine2"])("ngModel", ctx.user.addressLine2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formControl", ctx.userForm.controls["city"])("ngModel", ctx.user.city);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("disabled", ctx.userForm.invalid);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_10__.MatCard, _angular_material_card__WEBPACK_IMPORTED_MODULE_10__.MatCardTitle, _angular_material_divider__WEBPACK_IMPORTED_MODULE_11__.MatDivider, _angular_material_card__WEBPACK_IMPORTED_MODULE_10__.MatCardContent, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormGroupDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_12__.DefaultLayoutDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_12__.DefaultFlexDirective, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_13__.MatFormField, _angular_material_input__WEBPACK_IMPORTED_MODULE_14__.MatInput, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControlName, _angular_material_select__WEBPACK_IMPORTED_MODULE_15__.MatSelect, _angular_common__WEBPACK_IMPORTED_MODULE_16__.NgForOf, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControlDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.MaxLengthValidator, _angular_common__WEBPACK_IMPORTED_MODULE_16__.NgIf, _angular_material_button__WEBPACK_IMPORTED_MODULE_17__.MatButton, _angular_material_core__WEBPACK_IMPORTED_MODULE_18__.MatOption, _angular_material_icon__WEBPACK_IMPORTED_MODULE_19__.MatIcon, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_13__.MatSuffix], styles: [".mat-card[_ngcontent-%COMP%] {\n  box-shadow: none !important;\n}\n\n.title[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\n.submitBtn[_ngcontent-%COMP%] {\n  align-items: flex-end;\n  margin-left: 15px;\n}\n\nmat-icon[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVwZGF0ZS11c2VyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksMkJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQkFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0FBQ0oiLCJmaWxlIjoidXBkYXRlLXVzZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWF0LWNhcmQge1xyXG4gICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGl0bGUge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uc3VibWl0QnRuIHtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG59XHJcblxyXG5tYXQtaWNvbiB7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn0iXX0= */"] });


/***/ }),

/***/ 98593:
/*!******************************************************************!*\
  !*** ./src/app/views/forms/user-manage/user-manage.component.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserManageComponent": () => (/* binding */ UserManageComponent)
/* harmony export */ });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 90587);
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/snack-bar */ 89737);
/* harmony import */ var app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/views/tables/tables.service */ 27582);
/* harmony import */ var app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/services/dataservice.service */ 43396);
/* harmony import */ var app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/api.service */ 94761);
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ 77009);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-toastr */ 7100);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/card */ 4005);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/divider */ 51996);
/* harmony import */ var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/flex-layout/flex */ 23081);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/form-field */ 3403);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/input */ 23852);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/button */ 2955);
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/select */ 5451);
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/core */ 2106);


























function UserManageComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, "Add User");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function UserManageComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, "Edit User");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function UserManageComponent_div_9_mat_option_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-option", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const storeName_r6 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("value", storeName_r6._id);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate2"](" Name: ", storeName_r6.name, "\u00A0\u00A0 Store Id:", storeName_r6.storeID, " ");
} }
function UserManageComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-form-field", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "mat-select", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UserManageComponent_div_9_Template_mat_select_ngModelChange_2_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r8); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r7.admin.store._id = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](3, UserManageComponent_div_9_mat_option_3_Template, 2, 3, "mat-option", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx_r2.admin.store._id);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngForOf", ctx_r2.stores);
} }
function UserManageComponent_div_10_mat_option_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-option", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const storeName_r10 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("value", storeName_r10._id);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate2"](" Name: ", storeName_r10.name, "\u00A0\u00A0 Store Id:", storeName_r10.storeID, " ");
} }
function UserManageComponent_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-form-field", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "mat-select", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](3, UserManageComponent_div_10_mat_option_3_Template, 2, 3, "mat-option", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngForOf", ctx_r3.stores);
} }
function UserManageComponent_div_27_Template(rf, ctx) { if (rf & 1) {
    const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-form-field", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "input", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UserManageComponent_div_27_Template_input_ngModelChange_2_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r12); const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r11.admin.password = $event; });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx_r4.admin.password);
} }
class UserManageComponent {
    constructor(service, dataRoute, dataservice, apiservice, snack, toast, route, loader) {
        this.service = service;
        this.dataRoute = dataRoute;
        this.dataservice = dataservice;
        this.apiservice = apiservice;
        this.snack = snack;
        this.toast = toast;
        this.route = route;
        this.loader = loader;
        this.formData = {};
        this.admin = {
            storeID: '',
            username: '',
            firstName: '',
            lastName: '',
            email: '',
            phoneNumber: '',
            profilePictureUrl: '',
            password: '',
        };
        this.usersData = {};
        this.show = true;
        this.message = 'User Added Successfully!';
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 4000;
        this.horizontalPosition = 'center';
        this.verticalPosition = 'bottom';
        let config = new _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__.MatSnackBarConfig();
        config.verticalPosition = this.verticalPosition;
        config.horizontalPosition = this.horizontalPosition;
        config.duration = this.setAutoHide ? this.autoHide : 0;
        this.admin = this.dataservice.getOption();
        console.log("admin", this.admin);
    }
    back() {
        this.route.navigate(['tables/filter']);
    }
    ngOnInit() {
        this.adminForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormGroup({
            storeId: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            phoneNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl(''),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required),
        });
        this.getStore();
    }
    addUser() {
        console.log(this.adminForm.value);
        this.apiservice.addUser(this.adminForm.value).subscribe((res) => {
            console.log('add user res ==>', res);
            this.route.navigate(['tables/filter']);
        });
    }
    getCurrentUser(id) {
        this.apiservice.getUserByIdd(id).subscribe((res) => {
            console.log('res ===>', res);
        });
    }
    userUpdate() {
        let id = this.admin._id;
        if (id) {
            this.apiservice.userUpdate(id, this.admin).subscribe((res) => {
                console.log('update res', res);
                this.route.navigate(['tables/filter']);
            });
        }
    }
    onSubmit() {
        this.submitted = true;
        // this.addUser();
        if (!this.admin._id) {
            return this.addUser();
        }
        else {
            this.userUpdate();
        }
    }
    getStore() {
        console.log('add function calling');
        this.apiservice.getAllStore().subscribe((res) => {
            console.log('resssssss', res);
            this.stores = res.data;
            console.log('get users ===>>', this.stores);
        });
    }
}
UserManageComponent.ɵfac = function UserManageComponent_Factory(t) { return new (t || UserManageComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_views_tables_tables_service__WEBPACK_IMPORTED_MODULE_0__.TablesService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_7__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_1__.DataService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_2__.ApiService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__.MatSnackBar), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_8__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_7__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_3__.AppLoaderService)); };
UserManageComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({ type: UserManageComponent, selectors: [["app-user-manage"]], decls: 32, vars: 11, consts: [[1, "p-0"], [1, "title"], ["class", "card-title-text", 4, "ngIf"], [3, "formGroup", "ngSubmit"], ["fxLayout", "row wrap"], ["fxFlex", "100", "fxFlex.gt-xs", "50", 1, "pr-1"], ["class", "pb-1", 4, "ngIf"], [1, "pb-1"], [1, "full-width"], ["matInput", "", "name", "username", "placeholder", "username", "formControlName", "username", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "firstName", "placeholder", "firstName", "formControlName", "firstName", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "email", "placeholder", "email", "formControlName", "email", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "phoneNumber", "placeholder", "phoneNumber", "formControlName", "phoneNumber", 3, "ngModel", "ngModelChange"], ["matInput", "", "name", "lastName", "placeholder", "lastName", "formControlName", "lastName", 3, "ngModel", "ngModelChange"], ["mat-raised-button", "", "color", "warn", 3, "click"], ["mat-raised-button", "", "color", "primary", 1, "submitBtn", 3, "click"], [1, "card-title-text"], ["name", "selectStore", "placeholder", "Select Store", "formControlName", "storeId", 3, "ngModel", "ngModelChange"], [3, "value", 4, "ngFor", "ngForOf"], [3, "value"], ["name", "selectStore", "placeholder", "Select Store", "formControlName", "storeId"], ["matInput", "", "name", "password", "placeholder", "password", "formControlName", "password", 3, "ngModel", "ngModelChange"]], template: function UserManageComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "mat-card-title", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](2, UserManageComponent_div_2_Template, 2, 0, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](3, UserManageComponent_div_3_Template, 2, 0, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](4, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "mat-card-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](6, "form", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngSubmit", function UserManageComponent_Template_form_ngSubmit_6_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](9, UserManageComponent_div_9_Template, 4, 2, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](10, UserManageComponent_div_10_Template, 4, 1, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](11, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](12, "mat-form-field", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](13, "input", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UserManageComponent_Template_input_ngModelChange_13_listener($event) { return ctx.admin.username = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](14, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](15, "mat-form-field", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](16, "input", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UserManageComponent_Template_input_ngModelChange_16_listener($event) { return ctx.admin.firstName = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](17, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](18, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](19, "mat-form-field", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](20, "input", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UserManageComponent_Template_input_ngModelChange_20_listener($event) { return ctx.admin.email = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](21, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](22, "mat-form-field", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](23, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UserManageComponent_Template_input_ngModelChange_23_listener($event) { return ctx.admin.phoneNumber = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](24, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](25, "mat-form-field", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](26, "input", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function UserManageComponent_Template_input_ngModelChange_26_listener($event) { return ctx.admin.lastName = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](27, UserManageComponent_div_27_Template, 3, 1, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](28, "button", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function UserManageComponent_Template_button_click_28_listener() { return ctx.back(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](29, "Back");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](30, "button", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function UserManageComponent_Template_button_click_30_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](31, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", !ctx.admin._id);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.admin._id);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formGroup", ctx.adminForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.admin.store);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", !ctx.admin.store);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.admin.username);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.admin.firstName);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.admin.email);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.admin.phoneNumber);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngModel", ctx.admin.lastName);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", !ctx.admin._id);
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_9__.MatCard, _angular_material_card__WEBPACK_IMPORTED_MODULE_9__.MatCardTitle, _angular_common__WEBPACK_IMPORTED_MODULE_10__.NgIf, _angular_material_divider__WEBPACK_IMPORTED_MODULE_11__.MatDivider, _angular_material_card__WEBPACK_IMPORTED_MODULE_9__.MatCardContent, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormGroupDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_12__.DefaultLayoutDirective, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_12__.DefaultFlexDirective, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_13__.MatFormField, _angular_material_input__WEBPACK_IMPORTED_MODULE_14__.MatInput, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControlName, _angular_material_button__WEBPACK_IMPORTED_MODULE_15__.MatButton, _angular_material_select__WEBPACK_IMPORTED_MODULE_16__.MatSelect, _angular_common__WEBPACK_IMPORTED_MODULE_10__.NgForOf, _angular_material_core__WEBPACK_IMPORTED_MODULE_17__.MatOption], styles: [".mat-card[_ngcontent-%COMP%] {\n  box-shadow: none !important;\n}\n\n.title[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\n.submitBtn[_ngcontent-%COMP%] {\n  align-items: flex-end;\n  margin-left: 15px;\n}\n\nmat-icon[_ngcontent-%COMP%] {\n  cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVzZXItbWFuYWdlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksMkJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQkFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0FBQ0oiLCJmaWxlIjoidXNlci1tYW5hZ2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWF0LWNhcmQge1xyXG4gICAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGl0bGUge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4uc3VibWl0QnRuIHtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG59XHJcblxyXG5tYXQtaWNvbiB7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn0iXX0= */"] });


/***/ }),

/***/ 69429:
/*!**************************************************************!*\
  !*** ./src/app/views/forms/view-post/view-post.component.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ViewPostComponent": () => (/* binding */ ViewPostComponent)
/* harmony export */ });
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/dialog */ 86290);
/* harmony import */ var app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/shared/services/api.service */ 94761);
/* harmony import */ var app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/services/app-confirm/app-confirm.service */ 65403);
/* harmony import */ var app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/dataservice.service */ 43396);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ 7100);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button */ 2955);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/icon */ 68159);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/menu */ 49399);
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/list */ 42873);
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/tooltip */ 26844);



















function ViewPostComponent_p_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "p", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](ctx_r0.post.description);
} }
function ViewPostComponent_p_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "p", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "No Description!!!");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ViewPostComponent_div_16_button_1_span_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](ctx_r10.lastLike);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"](" and ", ctx_r10.totalLikes, " others like your post.");
} }
function ViewPostComponent_div_16_button_1_span_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](3, " liked your post.");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](ctx_r11.lastLike);
} }
function ViewPostComponent_div_16_button_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "button", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "mat-icon");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, "favorite");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](3, "\u00A0 ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](4, ViewPostComponent_div_16_button_1_span_4_Template, 4, 2, "span", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](5, ViewPostComponent_div_16_button_1_span_5_Template, 4, 1, "span", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵreference"](3);
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("matMenuTriggerFor", _r8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r7.likesList.length !== 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r7.likesList.length === 1);
} }
function ViewPostComponent_div_16_ul_4_img_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "img", 26);
} if (rf & 2) {
    const user_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("src", user_r12.userId.avatar, _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsanitizeUrl"]);
} }
function ViewPostComponent_div_16_ul_4_img_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "img", 27);
} }
function ViewPostComponent_div_16_ul_4_Template(rf, ctx) { if (rf & 1) {
    const _r17 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "ul", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "li", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ViewPostComponent_div_16_ul_4_Template_li_click_1_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r17); const user_r12 = restoredCtx.$implicit; const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2); return ctx_r16.selectUser(user_r12.userId); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, ViewPostComponent_div_16_ul_4_img_2_Template, 1, 1, "img", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, ViewPostComponent_div_16_ul_4_img_3_Template, 1, 0, "img", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "span", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const user_r12 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", user_r12.userId.avatar);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !user_r12.userId.avatar);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](user_r12.userId.userName);
} }
function ViewPostComponent_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ViewPostComponent_div_16_button_1_Template, 6, 3, "button", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "mat-menu", null, 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](4, ViewPostComponent_div_16_ul_4_Template, 6, 3, "ul", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r2.likesList.length);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngForOf", ctx_r2.likesList);
} }
function ViewPostComponent_button_18_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "button", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "No Likes!!!!");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ViewPostComponent_div_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"]("", ctx_r4.commentList.length, " Comments");
} }
function ViewPostComponent_div_21_div_2_img_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "img", 26);
} if (rf & 2) {
    const comment_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("src", comment_r19.userId.avatar, _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsanitizeUrl"]);
} }
function ViewPostComponent_div_21_div_2_img_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "img", 27);
} }
function ViewPostComponent_div_21_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "div", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "div", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, ViewPostComponent_div_21_div_2_img_3_Template, 1, 1, "img", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](4, ViewPostComponent_div_21_div_2_img_4_Template, 1, 0, "img", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "small", 36);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ViewPostComponent_div_21_div_2_Template_small_click_6_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r24); const comment_r19 = restoredCtx.$implicit; const ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2); return ctx_r23.selectUser(comment_r19.userId); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](8, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](9, "small", 37);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](11, "small");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](12, "mat-icon", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ViewPostComponent_div_21_div_2_Template_mat_icon_click_12_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r24); const comment_r19 = restoredCtx.$implicit; const ctx_r25 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2); return ctx_r25.deleteComment(comment_r19); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](13, "delete");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](14, "div", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](15, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](16, "i", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](17, "small", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](19, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](20, "small");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](21);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpipe"](22, "date");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const comment_r19 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", comment_r19.userId.avatar);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !comment_r19.userId.avatar);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](comment_r19.userId.userName);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](comment_r19.comment);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"]("", comment_r19.likes, " Likes");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpipeBind2"](22, 6, comment_r19.createdOn, "medium"));
} }
function ViewPostComponent_div_21_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "div", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, ViewPostComponent_div_21_div_2_Template, 23, 9, "div", 32);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngForOf", ctx_r5.commentList);
} }
function ViewPostComponent_div_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "h5");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, "No Comments !!!!");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
class ViewPostComponent {
    constructor(apiservice, toast, confirmService, route, dataservice, dialogRef, data) {
        this.apiservice = apiservice;
        this.toast = toast;
        this.confirmService = confirmService;
        this.route = route;
        this.dataservice = dataservice;
        this.dialogRef = dialogRef;
        this.data = data;
        this.userObj = [];
        this.post = this.data;
    }
    ngOnInit() {
        // this.getLikes();
        // this.getComments();
    }
    // deleteComment(comment) {
    //   this.confirmService.confirm({ message: `Delete Comment?` }).subscribe(res => {
    //     if (res) {
    //       this.apiservice.deleteComment(comment._id).subscribe((res: any) => {
    //         this.toast.success('Post Deleted Successfully');
    //         this.dialogRef.close(true);
    //       });
    //     }
    //   });
    // }
    // deletePost() {
    //   this.confirmService.confirm({ message: `Delete Post?` }).subscribe(res => {
    //     if (res) {
    //       this.apiservice.deletePost(this.post._id).subscribe((res: any) => {
    //         this.toast.success('Post Deleted Successfully');
    //         this.dialogRef.close(true);
    //       });
    //     }
    //   });
    // }
    // getComments() {
    //   this.apiservice.getComments(this.post._id).subscribe((res: any) => {
    //     this.commentList = res.reverse();
    //   });
    // }
    // getLikes() {
    //   this.apiservice.getLikes(this.post._id).subscribe((res: any) => {
    //     this.likesList = res.reverse();
    //     if (this.likesList && this.likesList.length) {
    //       for (let item of this.likesList) {
    //         this.lastLike = item.userId.userName;
    //         // this.userObj.push({userName: item.userId.userName, userAvatar:item.userId.avatar})
    //       }
    //       this.totalLikes = this.likesList.length - 1;
    //     }
    //   });
    // }
    selectUser(data) {
        this.dataservice.setOption(data);
        this.route.navigate(['forms/view-profile']);
        this.dialogRef.close(true);
    }
    back() {
        this.dialogRef.close();
    }
}
ViewPostComponent.ɵfac = function ViewPostComponent_Factory(t) { return new (t || ViewPostComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_0__.ApiService), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_4__.ToastrService), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_1__.AppConfirmService), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_2__.DataService), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__.MatDialogRef), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__.MAT_DIALOG_DATA)); };
ViewPostComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({ type: ViewPostComponent, selectors: [["app-view-post"]], decls: 23, vars: 8, consts: [[1, "row"], [1, "col-md-8", "col-sm-12"], ["controls", "", "preload", "auto", "data-config", "some-js-object-here", 1, "video"], ["type", "video/mp4", 3, "src"], ["mat-icon-button", "", 3, "click"], [1, "col-md-4", "col-sm-12"], [1, "container"], [1, "title"], [1, "header"], ["class", "text-break", 4, "ngIf"], [1, "content"], [4, "ngIf"], ["class", "likes", 4, "ngIf"], [1, "comments"], ["mat-subheader", "", 4, "ngIf"], ["class", "row d-flex justify-content-center commentPart", 4, "ngIf"], [1, "text-break"], ["class", "likes", 3, "matMenuTriggerFor", 4, "ngIf"], ["menu", "matMenu"], ["mat-menu-item", "", 4, "ngFor", "ngForOf"], [1, "likes", 3, "matMenuTriggerFor"], ["mat-menu-item", ""], [3, "click"], ["width", "30", "class", "user-img rounded-circle mr-2", 3, "src", 4, "ngIf"], ["src", "../../../../assets/images/no-user.jpg", "width", "30", "class", "user-img rounded-circle mr-2", 4, "ngIf"], [1, "text-capitalize"], ["width", "30", 1, "user-img", "rounded-circle", "mr-2", 3, "src"], ["src", "../../../../assets/images/no-user.jpg", "width", "30", 1, "user-img", "rounded-circle", "mr-2"], [1, "likes"], ["mat-subheader", ""], [1, "row", "d-flex", "justify-content-center", "commentPart"], [1, "col-md-12", "col-sm-12"], ["class", "card p-1", 4, "ngFor", "ngForOf"], [1, "card", "p-1"], [1, "d-flex", "justify-content-between", "align-items-center"], [1, "user", "d-flex", "flex-row", "align-items-center"], [1, "font-weight-bold", "text-primary", "text-capitalize", "cursor-pointer", 3, "click"], [1, "font-weight-bold"], ["matTooltip", "Delete Comment", 1, "delete", 3, "click"], [1, "action", "d-flex", "justify-content-between", "mt-2", "align-items-center"], [1, "icons", "align-items-center"], [1, "fa", "fa-heart"], [1, "m-1"], [1, "reply", "px-2"]], template: function ViewPostComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "video", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](3, "source", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ViewPostComponent_Template_button_click_4_listener() { return ctx.back(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](6, "close");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](7, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](8, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](10, "h5");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](11, "Liks and Comments");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](12, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](13, ViewPostComponent_p_13_Template, 2, 1, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](14, ViewPostComponent_p_14_Template, 2, 0, "p", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](15, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](16, ViewPostComponent_div_16_Template, 5, 2, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](17, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](18, ViewPostComponent_button_18_Template, 2, 0, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](19, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](20, ViewPostComponent_div_20_Template, 2, 1, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](21, ViewPostComponent_div_21_Template, 3, 1, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](22, ViewPostComponent_div_22_Template, 3, 0, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpropertyInterpolate"]("src", ctx.post.url, _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.post && ctx.post.description);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.post && !ctx.post.description);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.likesList);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.likesList && ctx.likesList.length === 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.commentList && ctx.commentList.length);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.commentList && ctx.commentList.length);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.commentList && !ctx.commentList.length);
    } }, directives: [_angular_material_button__WEBPACK_IMPORTED_MODULE_7__.MatButton, _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__.MatIcon, _angular_common__WEBPACK_IMPORTED_MODULE_9__.NgIf, _angular_material_menu__WEBPACK_IMPORTED_MODULE_10__.MatMenu, _angular_common__WEBPACK_IMPORTED_MODULE_9__.NgForOf, _angular_material_menu__WEBPACK_IMPORTED_MODULE_10__.MatMenuTrigger, _angular_material_menu__WEBPACK_IMPORTED_MODULE_10__.MatMenuItem, _angular_material_list__WEBPACK_IMPORTED_MODULE_11__.MatListSubheaderCssMatStyler, _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_12__.MatTooltip], pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_9__.DatePipe], styles: [".video[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.col-md-8[_ngcontent-%COMP%] {\n  background-color: #ccc;\n  height: 450px;\n  display: flex;\n  justify-content: center;\n  padding-right: 0;\n}\n\n.mat-icon-button[_ngcontent-%COMP%] {\n  right: 15px;\n  position: absolute;\n}\n\n.col-md-4[_ngcontent-%COMP%] {\n  padding-left: 0px;\n  padding-top: 20px;\n}\n\n.header[_ngcontent-%COMP%] {\n  padding-top: 2%;\n  text-align: center;\n}\n\n.text-break[_ngcontent-%COMP%] {\n  text-align: start;\n}\n\n.likes[_ngcontent-%COMP%] {\n  background-color: white;\n  color: #7a7a7a;\n  border: 0px solid;\n  display: flex;\n}\n\n.likes[_ngcontent-%COMP%]   mat-icon[_ngcontent-%COMP%] {\n  color: red;\n}\n\n.avatar[_ngcontent-%COMP%] {\n  vertical-align: middle;\n  width: 50px;\n  height: 50px;\n  border-radius: 50%;\n}\n\n.comments[_ngcontent-%COMP%]   body[_ngcontent-%COMP%] {\n  background-color: #f7f6f6;\n}\n\n.comments[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\n  border: none;\n}\n\n.comments[_ngcontent-%COMP%]   .dots[_ngcontent-%COMP%] {\n  height: 4px;\n  width: 4px;\n  margin-bottom: 2px;\n  background-color: #bbb;\n  border-radius: 50%;\n  display: inline-block;\n}\n\n.comments[_ngcontent-%COMP%]   .badge[_ngcontent-%COMP%] {\n  padding: 7px;\n  padding-right: 9px;\n  padding-left: 16px;\n  box-shadow: 5px 6px 6px 2px #e9ecef;\n}\n\n.comments[_ngcontent-%COMP%]   .user-img[_ngcontent-%COMP%] {\n  margin-top: 4px;\n  width: 30px;\n  height: 30px;\n}\n\n.comments[_ngcontent-%COMP%]   .check-icon[_ngcontent-%COMP%] {\n  font-size: 17px;\n  color: #c3bfbf;\n  top: 1px;\n  position: relative;\n  margin-left: 3px;\n}\n\n.comments[_ngcontent-%COMP%]   .form-check-input[_ngcontent-%COMP%] {\n  margin-top: 6px;\n  margin-left: -24px !important;\n  cursor: pointer;\n}\n\n.comments[_ngcontent-%COMP%]   .form-check-input[_ngcontent-%COMP%]:focus {\n  box-shadow: none;\n}\n\n.comments[_ngcontent-%COMP%]   .icons[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  margin-left: 8px;\n  color: red;\n}\n\n.comments[_ngcontent-%COMP%]   .icons[_ngcontent-%COMP%] {\n  margin-left: 30px;\n}\n\n.comments[_ngcontent-%COMP%]   .reply[_ngcontent-%COMP%] {\n  margin-left: 12px;\n}\n\n.comments[_ngcontent-%COMP%]   .reply[_ngcontent-%COMP%]   small[_ngcontent-%COMP%] {\n  color: #b7b4b4;\n}\n\n.comments[_ngcontent-%COMP%]   .commentPart[_ngcontent-%COMP%] {\n  max-height: 265px;\n  overflow: auto;\n}\n\n.delete[_ngcontent-%COMP%]:hover {\n  color: red;\n  cursor: pointer;\n}\n\n.title[_ngcontent-%COMP%]   h5[_ngcontent-%COMP%] {\n  width: 90%;\n  float: left;\n  text-align: center;\n}\n\nul[_ngcontent-%COMP%] {\n  list-style: none;\n  margin: 0;\n  width: 270px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZpZXctcG9zdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0VBQ0EsaUJBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7QUFDSjs7QUFFQTtFQUNJLHVCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtBQUNKOztBQUVBO0VBQ0ksVUFBQTtBQUNKOztBQUVBO0VBQ0ksc0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FBQ0o7O0FBR0k7RUFDSSx5QkFBQTtBQUFSOztBQUVJO0VBQ0ksWUFBQTtBQUFSOztBQUlJO0VBQ0ksV0FBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtBQUZSOztBQUlJO0VBQ0ksWUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQ0FBQTtBQUZSOztBQUlJO0VBQ0ksZUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBRlI7O0FBSUk7RUFDSSxlQUFBO0VBQ0EsY0FBQTtFQUNBLFFBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBRlI7O0FBSUk7RUFDSSxlQUFBO0VBQ0EsNkJBQUE7RUFDQSxlQUFBO0FBRlI7O0FBSUk7RUFDSSxnQkFBQTtBQUZSOztBQUlJO0VBQ0ksZ0JBQUE7RUFDQSxVQUFBO0FBRlI7O0FBSUk7RUFDSSxpQkFBQTtBQUZSOztBQUlJO0VBQ0ksaUJBQUE7QUFGUjs7QUFJSTtFQUNJLGNBQUE7QUFGUjs7QUFJSTtFQUNJLGlCQUFBO0VBQ0EsY0FBQTtBQUZSOztBQU1BO0VBQ0ksVUFBQTtFQUNBLGVBQUE7QUFISjs7QUFNQTtFQUNJLFVBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUFISjs7QUFNQTtFQUNJLGdCQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7QUFISiIsImZpbGUiOiJ2aWV3LXBvc3QuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudmlkZW8ge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5jb2wtbWQtOCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2NjO1xyXG4gICAgaGVpZ2h0OiA0NTBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDA7XHJcbn1cclxuXHJcbi5tYXQtaWNvbi1idXR0b24ge1xyXG4gICAgcmlnaHQ6IDE1cHg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbn1cclxuXHJcbi5jb2wtbWQtNCB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgIHBhZGRpbmctdG9wOiAyMHB4O1xyXG59XHJcblxyXG4uaGVhZGVyIHtcclxuICAgIHBhZGRpbmctdG9wOiAyJTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLnRleHQtYnJlYWsge1xyXG4gICAgdGV4dC1hbGlnbjogc3RhcnQ7XHJcbn1cclxuXHJcbi5saWtlcyB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgIGNvbG9yOiByZ2IoMTIyLCAxMjIsIDEyMik7XHJcbiAgICBib3JkZXI6IDBweCBzb2xpZDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbn1cclxuXHJcbi5saWtlcyBtYXQtaWNvbiB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG59XHJcblxyXG4uYXZhdGFyIHtcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxufVxyXG5cclxuLmNvbW1lbnRzIHtcclxuICAgIGJvZHkge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y2ZjZcclxuICAgIH1cclxuICAgIC5jYXJkIHtcclxuICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgLy8gYm94LXNoYWRvdzogNXB4IDZweCA2cHggMnB4ICNlOWVjZWY7XHJcbiAgICAgICAgLy8gYm9yZGVyLXJhZGl1czogNHB4XHJcbiAgICB9XHJcbiAgICAuZG90cyB7XHJcbiAgICAgICAgaGVpZ2h0OiA0cHg7XHJcbiAgICAgICAgd2lkdGg6IDRweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAycHg7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2JiYjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrXHJcbiAgICB9XHJcbiAgICAuYmFkZ2Uge1xyXG4gICAgICAgIHBhZGRpbmc6IDdweDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA5cHg7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNnB4O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDVweCA2cHggNnB4IDJweCAjZTllY2VmXHJcbiAgICB9XHJcbiAgICAudXNlci1pbWcge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDRweDtcclxuICAgICAgICB3aWR0aDogMzBweDtcclxuICAgICAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICB9XHJcbiAgICAuY2hlY2staWNvbiB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgICAgIGNvbG9yOiAjYzNiZmJmO1xyXG4gICAgICAgIHRvcDogMXB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtYXJnaW4tbGVmdDogM3B4XHJcbiAgICB9XHJcbiAgICAuZm9ybS1jaGVjay1pbnB1dCB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNnB4O1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAtMjRweCAhaW1wb3J0YW50O1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlclxyXG4gICAgfVxyXG4gICAgLmZvcm0tY2hlY2staW5wdXQ6Zm9jdXMge1xyXG4gICAgICAgIGJveC1zaGFkb3c6IG5vbmVcclxuICAgIH1cclxuICAgIC5pY29ucyBpIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogOHB4O1xyXG4gICAgICAgIGNvbG9yOiByZWQ7XHJcbiAgICB9XHJcbiAgICAuaWNvbnMge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xyXG4gICAgfVxyXG4gICAgLnJlcGx5IHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTJweFxyXG4gICAgfVxyXG4gICAgLnJlcGx5IHNtYWxsIHtcclxuICAgICAgICBjb2xvcjogI2I3YjRiNFxyXG4gICAgfVxyXG4gICAgLmNvbW1lbnRQYXJ0IHtcclxuICAgICAgICBtYXgtaGVpZ2h0OiAyNjVweDtcclxuICAgICAgICBvdmVyZmxvdzogYXV0bztcclxuICAgIH1cclxufVxyXG5cclxuLmRlbGV0ZTpob3ZlciB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyXHJcbn1cclxuXHJcbi50aXRsZSBoNSB7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbnVsIHtcclxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB3aWR0aDogMjcwcHg7XHJcbn0iXX0= */"] });


/***/ }),

/***/ 85341:
/*!******************************************************************!*\
  !*** ./src/app/views/forms/viewprofile/viewprofile.component.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ViewprofileComponent": () => (/* binding */ ViewprofileComponent)
/* harmony export */ });
/* harmony import */ var app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! app/shared/services/dataservice.service */ 43396);
/* harmony import */ var app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/services/api.service */ 94761);
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ 77009);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/dialog */ 86290);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 36362);












function ViewprofileComponent_div_2_img_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "img", 13);
} }
function ViewprofileComponent_div_2_img_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "img", 14);
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpropertyInterpolate"]("src", ctx_r4.userImg, _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsanitizeUrl"]);
} }
function ViewprofileComponent_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, ViewprofileComponent_div_2_img_2_Template, 1, 0, "img", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, ViewprofileComponent_div_2_img_3_Template, 1, 1, "img", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "h1", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](6, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](7, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](8, "p", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](9, "span", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", !ctx_r0.userImg);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.userImg);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](ctx_r0.user.name);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate1"](" ", ctx_r0.user.bio, "");
} }
function ViewprofileComponent_ng_container_6_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "video", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](2, "source", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "span", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, "Video");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](6, "i", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const post_r6 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpropertyInterpolate"]("src", post_r6.url, _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsanitizeUrl"]);
} }
function ViewprofileComponent_ng_container_6_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ViewprofileComponent_ng_container_6_div_1_Template, 7, 1, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementContainerEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngForOf", ctx_r1.postList);
} }
function ViewprofileComponent_ng_container_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "h1", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "b");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](3, "No Posts !!!");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementContainerEnd"]();
} }
class ViewprofileComponent {
    constructor(apiservice, loader, dialog, dataservice, activatedRoute) {
        this.apiservice = apiservice;
        this.loader = loader;
        this.dialog = dialog;
        this.dataservice = dataservice;
        this.activatedRoute = activatedRoute;
        this.userId = {
            userId: ''
        };
        this.activatedRoute.params.subscribe(params => {
            this.userId.userId = params['id'];
            // this.getMedia();
            // this.getCurrentUser()
        });
    }
    ngOnInit() {
        // this.currentUser = this.dataservice.getOption();
    }
}
ViewprofileComponent.ɵfac = function ViewprofileComponent_Factory(t) { return new (t || ViewprofileComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_1__.ApiService), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_2__.AppLoaderService), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_material_dialog__WEBPACK_IMPORTED_MODULE_4__.MatDialog), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](app_shared_services_dataservice_service__WEBPACK_IMPORTED_MODULE_0__.DataService), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__.ActivatedRoute)); };
ViewprofileComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({ type: ViewprofileComponent, selectors: [["app-viewprofile"]], decls: 8, vars: 3, consts: [[1, "container"], ["class", "profile", 4, "ngIf"], [1, "row"], [4, "ngIf"], [1, "profile"], [1, "profile-image", "d-block"], ["src", "../../../../assets/images/no-user.jpg", "alt", "", "class", "avatar", 4, "ngIf"], ["class", "avatar", "alt", "", 3, "src", 4, "ngIf"], [1, "profile-user-name", "text-capitalize"], [1, "profile-user-settings"], [1, "profile-bio"], [1, "bio"], [1, "profile-real-name"], ["src", "../../../../assets/images/no-user.jpg", "alt", "", 1, "avatar"], ["alt", "", 1, "avatar", 3, "src"], ["class", "gallery-item col-md-4 col-sm-12", 4, "ngFor", "ngForOf"], [1, "gallery-item", "col-md-4", "col-sm-12"], ["controls", "", "preload", "auto", "data-config", "some-js-object-here", 1, "library", "gallery-image"], ["type", "video/mp4", 3, "src"], [1, "gallery-item-type"], [1, "visually-hidden"], ["aria-hidden", "true", 1, "fas", "fa-video"], [1, "text-center", "noPost"]], template: function ViewprofileComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "header");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, ViewprofileComponent_div_2_Template, 11, 4, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "main");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](6, ViewprofileComponent_ng_container_6_Template, 2, 1, "ng-container", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](7, ViewprofileComponent_ng_container_7_Template, 4, 0, "ng-container", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.user);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.postList && ctx.postList.length);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.postList && !ctx.postList.length);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_6__.NgIf, _angular_common__WEBPACK_IMPORTED_MODULE_6__.NgForOf], styles: ["[_ngcontent-%COMP%]:root {\n  font-size: 10px;\n}\n*[_ngcontent-%COMP%], *[_ngcontent-%COMP%]::before, *[_ngcontent-%COMP%]::after {\n  box-sizing: border-box;\n}\nbody[_ngcontent-%COMP%] {\n  font-family: \"Open Sans\", Arial, sans-serif;\n  min-height: 100vh;\n  background-color: #fafafa;\n  color: #262626;\n  padding-bottom: 3rem;\n}\nimg[_ngcontent-%COMP%] {\n  display: block;\n}\n.container[_ngcontent-%COMP%] {\n  max-width: 93.5rem;\n  margin: 0 auto;\n  padding: 0 2rem;\n}\n.btn[_ngcontent-%COMP%] {\n  display: inline-block;\n  font: inherit;\n  background: none;\n  border: none;\n  color: inherit;\n  padding: 0;\n  cursor: pointer;\n}\n.btn[_ngcontent-%COMP%]:focus {\n  outline: 0.5rem auto #4d90fe;\n}\n.visually-hidden[_ngcontent-%COMP%] {\n  position: absolute !important;\n  height: 1px;\n  width: 1px;\n  overflow: hidden;\n  clip: rect(1px, 1px, 1px, 1px);\n}\n\n.profile[_ngcontent-%COMP%] {\n  padding: 5rem 0;\n}\n.profile[_ngcontent-%COMP%]::after {\n  content: \"\";\n  display: block;\n  clear: both;\n}\n.profile-image[_ngcontent-%COMP%] {\n  float: left;\n  width: calc(33.333% - 1rem);\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  margin-right: 3rem;\n}\n.profile-image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  border-radius: 50%;\n}\n.profile-user-settings[_ngcontent-%COMP%], .profile-stats[_ngcontent-%COMP%], .profile-bio[_ngcontent-%COMP%] {\n  float: left;\n  width: calc(66.666% - 2rem);\n}\n.profile-user-settings[_ngcontent-%COMP%] {\n  margin-top: 1.1rem;\n}\n.profile-user-name[_ngcontent-%COMP%] {\n  font-size: 3.2rem;\n  font-weight: 300;\n  text-align: center;\n}\n.profile-edit-btn[_ngcontent-%COMP%] {\n  font-size: 1.4rem;\n  line-height: 1.8;\n  border: 0.1rem solid #dbdbdb;\n  border-radius: 0.3rem;\n  padding: 0 2.4rem;\n  margin-left: 2rem;\n}\n.profile-settings-btn[_ngcontent-%COMP%] {\n  font-size: 2rem;\n  margin-left: 1rem;\n}\n.profile-stats[_ngcontent-%COMP%] {\n  margin-top: 2.3rem;\n}\n.profile-stats[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  display: inline-block;\n  font-size: 1.6rem;\n  line-height: 1.5;\n  margin-right: 4rem;\n  cursor: pointer;\n}\n.profile-stats[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]:last-of-type {\n  margin-right: 0;\n}\n.profile-bio[_ngcontent-%COMP%] {\n  font-size: 1.6rem;\n  font-weight: 400;\n  line-height: 1.5;\n  margin-top: 2.3rem;\n}\n.profile-real-name[_ngcontent-%COMP%], .profile-stat-count[_ngcontent-%COMP%], .profile-edit-btn[_ngcontent-%COMP%] {\n  font-weight: 600;\n}\n\n.gallery[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  margin: -1rem -1rem;\n  padding-bottom: 3rem;\n}\n.gallery-item[_ngcontent-%COMP%] {\n  position: relative;\n  flex: 1 0 22rem;\n  margin: 1rem;\n  color: #fff;\n  cursor: pointer;\n}\n.gallery-item[_ngcontent-%COMP%]:hover   .gallery-item-info[_ngcontent-%COMP%], .gallery-item[_ngcontent-%COMP%]:focus   .gallery-item-info[_ngcontent-%COMP%] {\n  width: 91%;\n  display: flex;\n  align-items: center;\n  position: absolute;\n  top: 0;\n  height: 500px;\n  background-color: rgba(0, 0, 0, 0.3);\n}\n.gallery-item-info[_ngcontent-%COMP%] {\n  display: none;\n}\n.gallery-item-info[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  display: inline-block;\n  font-size: 1.7rem;\n  font-weight: 600;\n}\n.gallery-item-likes[_ngcontent-%COMP%] {\n  margin-right: 25px;\n  margin-left: 55px;\n}\n.gallery-item-likes[_ngcontent-%COMP%]   .fa-heart[_ngcontent-%COMP%] {\n  color: red;\n}\n.gallery-item-type[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 1rem;\n  right: 2.5rem;\n  font-size: 2.5rem;\n  text-shadow: 0.2rem 0.2rem 0.2rem rgba(0, 0, 0, 0.1);\n}\n.fa-clone[_ngcontent-%COMP%], .fa-comment[_ngcontent-%COMP%] {\n  transform: rotateY(180deg);\n}\n.gallery-image[_ngcontent-%COMP%] {\n  width: 100%;\n  height: 500px;\n  object-fit: cover;\n  border: 1px solid #ccc;\n}\n\n.loader[_ngcontent-%COMP%] {\n  width: 5rem;\n  height: 5rem;\n  border: 0.6rem solid #999;\n  border-bottom-color: transparent;\n  border-radius: 50%;\n  margin: 0 auto;\n  animation: loader 500ms linear infinite;\n}\n\n@media screen and (max-width: 40rem) {\n  .profile[_ngcontent-%COMP%] {\n    display: flex;\n    flex-wrap: wrap;\n    padding: 4rem 0;\n  }\n\n  .profile[_ngcontent-%COMP%]::after {\n    display: none;\n  }\n\n  .profile-image[_ngcontent-%COMP%], .profile-user-settings[_ngcontent-%COMP%], .profile-bio[_ngcontent-%COMP%], .profile-stats[_ngcontent-%COMP%] {\n    float: none;\n    width: auto;\n  }\n\n  .profile-image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n    width: 7.7rem;\n  }\n\n  .profile-user-settings[_ngcontent-%COMP%] {\n    flex-basis: calc(100% - 10.7rem);\n    display: flex;\n    flex-wrap: wrap;\n    margin-top: 1rem;\n  }\n\n  .profile-user-name[_ngcontent-%COMP%] {\n    font-size: 2.2rem;\n  }\n\n  .profile-edit-btn[_ngcontent-%COMP%] {\n    order: 1;\n    padding: 0;\n    text-align: center;\n    margin-top: 1rem;\n  }\n\n  .profile-edit-btn[_ngcontent-%COMP%] {\n    margin-left: 0;\n  }\n\n  .profile-bio[_ngcontent-%COMP%] {\n    font-size: 1.4rem;\n    margin-top: 1.5rem;\n  }\n\n  .profile-edit-btn[_ngcontent-%COMP%], .profile-bio[_ngcontent-%COMP%], .profile-stats[_ngcontent-%COMP%] {\n    flex-basis: 100%;\n  }\n\n  .profile-stats[_ngcontent-%COMP%] {\n    order: 1;\n    margin-top: 1.5rem;\n  }\n\n  .profile-stats[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%] {\n    display: flex;\n    text-align: center;\n    padding: 1.2rem 0;\n    border-top: 0.1rem solid #dadada;\n    border-bottom: 0.1rem solid #dadada;\n  }\n\n  .profile-stats[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n    font-size: 1.4rem;\n    flex: 1;\n    margin: 0;\n  }\n\n  .profile-stat-count[_ngcontent-%COMP%] {\n    display: block;\n  }\n}\n\n@keyframes loader {\n  to {\n    transform: rotate(360deg);\n  }\n}\n\n@supports (display: grid) {\n  .profile[_ngcontent-%COMP%] {\n    display: grid;\n    grid-template-columns: 1fr 2fr;\n    grid-template-rows: repeat(3, auto);\n    grid-column-gap: 3rem;\n    align-items: center;\n  }\n\n  .profile-image[_ngcontent-%COMP%] {\n    grid-row: 1/-1;\n  }\n\n  .gallery[_ngcontent-%COMP%] {\n    display: grid;\n    grid-template-columns: repeat(auto-fit, minmax(22rem, 1fr));\n    grid-gap: 2rem;\n  }\n\n  .profile-image[_ngcontent-%COMP%], .profile-user-settings[_ngcontent-%COMP%], .profile-stats[_ngcontent-%COMP%], .profile-bio[_ngcontent-%COMP%], .gallery-item[_ngcontent-%COMP%], .gallery[_ngcontent-%COMP%] {\n    width: auto;\n    margin: 0;\n  }\n\n  @media (max-width: 40rem) {\n    .profile[_ngcontent-%COMP%] {\n      grid-template-columns: auto 1fr;\n      grid-row-gap: 1.5rem;\n    }\n\n    .profile-image[_ngcontent-%COMP%] {\n      grid-row: 1/2;\n    }\n\n    .profile-user-settings[_ngcontent-%COMP%] {\n      display: grid;\n      grid-template-columns: auto 1fr;\n      grid-gap: 1rem;\n    }\n\n    .profile-edit-btn[_ngcontent-%COMP%], .profile-stats[_ngcontent-%COMP%], .profile-bio[_ngcontent-%COMP%] {\n      grid-column: 1/-1;\n    }\n\n    .profile-user-settings[_ngcontent-%COMP%], .profile-edit-btn[_ngcontent-%COMP%], .profile-settings-btn[_ngcontent-%COMP%], .profile-bio[_ngcontent-%COMP%], .profile-stats[_ngcontent-%COMP%] {\n      margin: 0;\n    }\n  }\n}\n.avatar[_ngcontent-%COMP%] {\n  vertical-align: middle;\n  width: 200px;\n  height: 200px;\n  border-radius: 50%;\n  margin-left: auto;\n  margin-right: auto;\n}\n.bio[_ngcontent-%COMP%] {\n  margin-left: 10%;\n}\n.noPost[_ngcontent-%COMP%] {\n  margin: 10px auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZpZXdwcm9maWxlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7OztDQUFBO0FBV0EsZ0JBQUE7QUFFQTtFQUNJLGVBQUE7QUFGSjtBQUtBOzs7RUFHSSxzQkFBQTtBQUZKO0FBS0E7RUFDSSwyQ0FBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0Esb0JBQUE7QUFGSjtBQUtBO0VBQ0ksY0FBQTtBQUZKO0FBS0E7RUFDSSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FBRko7QUFLQTtFQUNJLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtBQUZKO0FBS0E7RUFDSSw0QkFBQTtBQUZKO0FBS0E7RUFDSSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSw4QkFBQTtBQUZKO0FBTUEsb0JBQUE7QUFFQTtFQUNJLGVBQUE7QUFKSjtBQU9BO0VBQ0ksV0FBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0FBSko7QUFPQTtFQUNJLFdBQUE7RUFDQSwyQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUFKSjtBQU9BO0VBQ0ksa0JBQUE7QUFKSjtBQU9BOzs7RUFHSSxXQUFBO0VBQ0EsMkJBQUE7QUFKSjtBQU9BO0VBQ0ksa0JBQUE7QUFKSjtBQU9BO0VBRUksaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBTEo7QUFRQTtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSw0QkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQUxKO0FBUUE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7QUFMSjtBQVFBO0VBQ0ksa0JBQUE7QUFMSjtBQVFBO0VBQ0kscUJBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FBTEo7QUFRQTtFQUNJLGVBQUE7QUFMSjtBQVFBO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFMSjtBQVFBOzs7RUFHSSxnQkFBQTtBQUxKO0FBU0Esb0JBQUE7QUFFQTtFQUNJLGFBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtBQVBKO0FBVUE7RUFDSSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFQSjtBQVVBOztFQU9JLFVBQUE7RUFHQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLE1BQUE7RUFFQSxhQUFBO0VBQ0Esb0NBQUE7QUFmSjtBQWtCQTtFQUNJLGFBQUE7QUFmSjtBQWtCQTtFQUNJLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQWZKO0FBa0JBO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtBQWZKO0FBa0JBO0VBQ0ksVUFBQTtBQWZKO0FBcUJBO0VBQ0ksa0JBQUE7RUFDQSxTQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0Esb0RBQUE7QUFsQko7QUFxQkE7O0VBRUksMEJBQUE7QUFsQko7QUFxQkE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7QUFsQko7QUFzQkEsV0FBQTtBQUVBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGdDQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsdUNBQUE7QUFwQko7QUF3QkEsZ0JBQUE7QUFFQTtFQUNJO0lBQ0ksYUFBQTtJQUNBLGVBQUE7SUFDQSxlQUFBO0VBdEJOOztFQXdCRTtJQUNJLGFBQUE7RUFyQk47O0VBdUJFOzs7O0lBSUksV0FBQTtJQUNBLFdBQUE7RUFwQk47O0VBc0JFO0lBQ0ksYUFBQTtFQW5CTjs7RUFxQkU7SUFDSSxnQ0FBQTtJQUNBLGFBQUE7SUFDQSxlQUFBO0lBQ0EsZ0JBQUE7RUFsQk47O0VBb0JFO0lBQ0ksaUJBQUE7RUFqQk47O0VBbUJFO0lBQ0ksUUFBQTtJQUNBLFVBQUE7SUFDQSxrQkFBQTtJQUNBLGdCQUFBO0VBaEJOOztFQWtCRTtJQUNJLGNBQUE7RUFmTjs7RUFpQkU7SUFDSSxpQkFBQTtJQUNBLGtCQUFBO0VBZE47O0VBZ0JFOzs7SUFHSSxnQkFBQTtFQWJOOztFQWVFO0lBQ0ksUUFBQTtJQUNBLGtCQUFBO0VBWk47O0VBY0U7SUFDSSxhQUFBO0lBQ0Esa0JBQUE7SUFDQSxpQkFBQTtJQUNBLGdDQUFBO0lBQ0EsbUNBQUE7RUFYTjs7RUFhRTtJQUNJLGlCQUFBO0lBQ0EsT0FBQTtJQUNBLFNBQUE7RUFWTjs7RUFZRTtJQUNJLGNBQUE7RUFUTjtBQUNGO0FBYUEsc0JBQUE7QUFFQTtFQUNJO0lBQ0kseUJBQUE7RUFaTjtBQUNGO0FBZ0JBOzs7Ozs7Q0FBQTtBQVFBO0VBQ0k7SUFDSSxhQUFBO0lBQ0EsOEJBQUE7SUFDQSxtQ0FBQTtJQUNBLHFCQUFBO0lBQ0EsbUJBQUE7RUFmTjs7RUFpQkU7SUFDSSxjQUFBO0VBZE47O0VBZ0JFO0lBQ0ksYUFBQTtJQUNBLDJEQUFBO0lBQ0EsY0FBQTtFQWJOOztFQWVFOzs7Ozs7SUFNSSxXQUFBO0lBQ0EsU0FBQTtFQVpOOztFQWNFO0lBQ0k7TUFDSSwrQkFBQTtNQUNBLG9CQUFBO0lBWFI7O0lBYUk7TUFDSSxhQUFBO0lBVlI7O0lBWUk7TUFDSSxhQUFBO01BQ0EsK0JBQUE7TUFDQSxjQUFBO0lBVFI7O0lBV0k7OztNQUdJLGlCQUFBO0lBUlI7O0lBVUk7Ozs7O01BS0ksU0FBQTtJQVBSO0VBQ0Y7QUFDRjtBQVVBO0VBQ0ksc0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQVJKO0FBV0E7RUFDSSxnQkFBQTtBQVJKO0FBV0E7RUFDSSxpQkFBQTtBQVJKIiwiZmlsZSI6InZpZXdwcm9maWxlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLypcclxuXHJcbkFsbCBncmlkIGNvZGUgaXMgcGxhY2VkIGluIGEgJ3N1cHBvcnRzJyBydWxlIChmZWF0dXJlIHF1ZXJ5KSBhdCB0aGUgYm90dG9tIG9mIHRoZSBDU1MgKExpbmUgMzEwKS4gXHJcbiAgICAgICAgXHJcblRoZSAnc3VwcG9ydHMnIHJ1bGUgd2lsbCBvbmx5IHJ1biBpZiB5b3VyIGJyb3dzZXIgc3VwcG9ydHMgQ1NTIGdyaWQuXHJcblxyXG5GbGV4Ym94IGFuZCBmbG9hdHMgYXJlIHVzZWQgYXMgYSBmYWxsYmFjayBzbyB0aGF0IGJyb3dzZXJzIHdoaWNoIGRvbid0IHN1cHBvcnQgZ3JpZCB3aWxsIHN0aWxsIHJlY2lldmUgYSBzaW1pbGFyIGxheW91dC5cclxuXHJcbiovXHJcblxyXG5cclxuLyogQmFzZSBTdHlsZXMgKi9cclxuXHJcbjpyb290IHtcclxuICAgIGZvbnQtc2l6ZTogMTBweDtcclxufVxyXG5cclxuKixcclxuKjo6YmVmb3JlLFxyXG4qOjphZnRlciB7XHJcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG59XHJcblxyXG5ib2R5IHtcclxuICAgIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBBcmlhbCwgc2Fucy1zZXJpZjtcclxuICAgIG1pbi1oZWlnaHQ6IDEwMHZoO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZhZmFmYTtcclxuICAgIGNvbG9yOiAjMjYyNjI2O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDNyZW07XHJcbn1cclxuXHJcbmltZyB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuLmNvbnRhaW5lciB7XHJcbiAgICBtYXgtd2lkdGg6IDkzLjVyZW07XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHBhZGRpbmc6IDAgMnJlbTtcclxufVxyXG5cclxuLmJ0biB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250OiBpbmhlcml0O1xyXG4gICAgYmFja2dyb3VuZDogbm9uZTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiBpbmhlcml0O1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuLmJ0bjpmb2N1cyB7XHJcbiAgICBvdXRsaW5lOiAwLjVyZW0gYXV0byAjNGQ5MGZlO1xyXG59XHJcblxyXG4udmlzdWFsbHktaGlkZGVuIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xyXG4gICAgaGVpZ2h0OiAxcHg7XHJcbiAgICB3aWR0aDogMXB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGNsaXA6IHJlY3QoMXB4LCAxcHgsIDFweCwgMXB4KTtcclxufVxyXG5cclxuXHJcbi8qIFByb2ZpbGUgU2VjdGlvbiAqL1xyXG5cclxuLnByb2ZpbGUge1xyXG4gICAgcGFkZGluZzogNXJlbSAwO1xyXG59XHJcblxyXG4ucHJvZmlsZTo6YWZ0ZXIge1xyXG4gICAgY29udGVudDogXCJcIjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgY2xlYXI6IGJvdGg7XHJcbn1cclxuXHJcbi5wcm9maWxlLWltYWdlIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgd2lkdGg6IGNhbGMoMzMuMzMzJSAtIDFyZW0pO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIG1hcmdpbi1yaWdodDogM3JlbTtcclxufVxyXG5cclxuLnByb2ZpbGUtaW1hZ2UgaW1nIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxufVxyXG5cclxuLnByb2ZpbGUtdXNlci1zZXR0aW5ncyxcclxuLnByb2ZpbGUtc3RhdHMsXHJcbi5wcm9maWxlLWJpbyB7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIHdpZHRoOiBjYWxjKDY2LjY2NiUgLSAycmVtKTtcclxufVxyXG5cclxuLnByb2ZpbGUtdXNlci1zZXR0aW5ncyB7XHJcbiAgICBtYXJnaW4tdG9wOiAxLjFyZW07XHJcbn1cclxuXHJcbi5wcm9maWxlLXVzZXItbmFtZSB7XHJcbiAgICAvLyBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDMuMnJlbTtcclxuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5wcm9maWxlLWVkaXQtYnRuIHtcclxuICAgIGZvbnQtc2l6ZTogMS40cmVtO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuODtcclxuICAgIGJvcmRlcjogMC4xcmVtIHNvbGlkICNkYmRiZGI7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwLjNyZW07XHJcbiAgICBwYWRkaW5nOiAwIDIuNHJlbTtcclxuICAgIG1hcmdpbi1sZWZ0OiAycmVtO1xyXG59XHJcblxyXG4ucHJvZmlsZS1zZXR0aW5ncy1idG4ge1xyXG4gICAgZm9udC1zaXplOiAycmVtO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDFyZW07XHJcbn1cclxuXHJcbi5wcm9maWxlLXN0YXRzIHtcclxuICAgIG1hcmdpbi10b3A6IDIuM3JlbTtcclxufVxyXG5cclxuLnByb2ZpbGUtc3RhdHMgbGkge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAxLjZyZW07XHJcbiAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA0cmVtO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4ucHJvZmlsZS1zdGF0cyBsaTpsYXN0LW9mLXR5cGUge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG59XHJcblxyXG4ucHJvZmlsZS1iaW8ge1xyXG4gICAgZm9udC1zaXplOiAxLjZyZW07XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgbGluZS1oZWlnaHQ6IDEuNTtcclxuICAgIG1hcmdpbi10b3A6IDIuM3JlbTtcclxufVxyXG5cclxuLnByb2ZpbGUtcmVhbC1uYW1lLFxyXG4ucHJvZmlsZS1zdGF0LWNvdW50LFxyXG4ucHJvZmlsZS1lZGl0LWJ0biB7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG59XHJcblxyXG5cclxuLyogR2FsbGVyeSBTZWN0aW9uICovXHJcblxyXG4uZ2FsbGVyeSB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAgbWFyZ2luOiAtMXJlbSAtMXJlbTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAzcmVtO1xyXG59XHJcblxyXG4uZ2FsbGVyeS1pdGVtIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGZsZXg6IDEgMCAyMnJlbTtcclxuICAgIG1hcmdpbjogMXJlbTtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4uZ2FsbGVyeS1pdGVtOmhvdmVyIC5nYWxsZXJ5LWl0ZW0taW5mbyxcclxuLmdhbGxlcnktaXRlbTpmb2N1cyAuZ2FsbGVyeS1pdGVtLWluZm8ge1xyXG4gICAgLy8gZGlzcGxheTogZmxleDtcclxuICAgIC8vIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgLy8gYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIC8vIHRvcDogMDtcclxuICAgIHdpZHRoOiA5MSU7XHJcbiAgICAvLyBoZWlnaHQ6IDEwMCU7XHJcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuMyk7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIC8vIHBhZGRpbmctcmlnaHQ6IDMwJTtcclxuICAgIGhlaWdodDogNTAwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuMyk7XHJcbn1cclxuXHJcbi5nYWxsZXJ5LWl0ZW0taW5mbyB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4uZ2FsbGVyeS1pdGVtLWluZm8gbGkge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAxLjdyZW07XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG59XHJcblxyXG4uZ2FsbGVyeS1pdGVtLWxpa2VzIHtcclxuICAgIG1hcmdpbi1yaWdodDogMjVweDtcclxuICAgIG1hcmdpbi1sZWZ0OiA1NXB4O1xyXG59XHJcblxyXG4uZ2FsbGVyeS1pdGVtLWxpa2VzIC5mYS1oZWFydCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG59XHJcblxyXG4vLyAuZ2FsbGVyeS1pdGVtLWNvbW1lbnRzIC5mYS1jb21tZW50IHtcclxuLy8gICAgIGNvbG9yOiBncmVlbjtcclxuLy8gfVxyXG4uZ2FsbGVyeS1pdGVtLXR5cGUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAxcmVtO1xyXG4gICAgcmlnaHQ6IDIuNXJlbTtcclxuICAgIGZvbnQtc2l6ZTogMi41cmVtO1xyXG4gICAgdGV4dC1zaGFkb3c6IDAuMnJlbSAwLjJyZW0gMC4ycmVtIHJnYmEoMCwgMCwgMCwgMC4xKTtcclxufVxyXG5cclxuLmZhLWNsb25lLFxyXG4uZmEtY29tbWVudCB7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZVkoMTgwZGVnKTtcclxufVxyXG5cclxuLmdhbGxlcnktaW1hZ2Uge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDUwMHB4O1xyXG4gICAgb2JqZWN0LWZpdDogY292ZXI7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xyXG59XHJcblxyXG5cclxuLyogTG9hZGVyICovXHJcblxyXG4ubG9hZGVyIHtcclxuICAgIHdpZHRoOiA1cmVtO1xyXG4gICAgaGVpZ2h0OiA1cmVtO1xyXG4gICAgYm9yZGVyOiAwLjZyZW0gc29saWQgIzk5OTtcclxuICAgIGJvcmRlci1ib3R0b20tY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBhbmltYXRpb246IGxvYWRlciA1MDBtcyBsaW5lYXIgaW5maW5pdGU7XHJcbn1cclxuXHJcblxyXG4vKiBNZWRpYSBRdWVyeSAqL1xyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDByZW0pIHtcclxuICAgIC5wcm9maWxlIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICBwYWRkaW5nOiA0cmVtIDA7XHJcbiAgICB9XHJcbiAgICAucHJvZmlsZTo6YWZ0ZXIge1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcbiAgICAucHJvZmlsZS1pbWFnZSxcclxuICAgIC5wcm9maWxlLXVzZXItc2V0dGluZ3MsXHJcbiAgICAucHJvZmlsZS1iaW8sXHJcbiAgICAucHJvZmlsZS1zdGF0cyB7XHJcbiAgICAgICAgZmxvYXQ6IG5vbmU7XHJcbiAgICAgICAgd2lkdGg6IGF1dG87XHJcbiAgICB9XHJcbiAgICAucHJvZmlsZS1pbWFnZSBpbWcge1xyXG4gICAgICAgIHdpZHRoOiA3LjdyZW07XHJcbiAgICB9XHJcbiAgICAucHJvZmlsZS11c2VyLXNldHRpbmdzIHtcclxuICAgICAgICBmbGV4LWJhc2lzOiBjYWxjKDEwMCUgLSAxMC43cmVtKTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gICAgfVxyXG4gICAgLnByb2ZpbGUtdXNlci1uYW1lIHtcclxuICAgICAgICBmb250LXNpemU6IDIuMnJlbTtcclxuICAgIH1cclxuICAgIC5wcm9maWxlLWVkaXQtYnRuIHtcclxuICAgICAgICBvcmRlcjogMTtcclxuICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG4gICAgfVxyXG4gICAgLnByb2ZpbGUtZWRpdC1idG4ge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xyXG4gICAgfVxyXG4gICAgLnByb2ZpbGUtYmlvIHtcclxuICAgICAgICBmb250LXNpemU6IDEuNHJlbTtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxLjVyZW07XHJcbiAgICB9XHJcbiAgICAucHJvZmlsZS1lZGl0LWJ0bixcclxuICAgIC5wcm9maWxlLWJpbyxcclxuICAgIC5wcm9maWxlLXN0YXRzIHtcclxuICAgICAgICBmbGV4LWJhc2lzOiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnByb2ZpbGUtc3RhdHMge1xyXG4gICAgICAgIG9yZGVyOiAxO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDEuNXJlbTtcclxuICAgIH1cclxuICAgIC5wcm9maWxlLXN0YXRzIHVsIHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBwYWRkaW5nOiAxLjJyZW0gMDtcclxuICAgICAgICBib3JkZXItdG9wOiAwLjFyZW0gc29saWQgI2RhZGFkYTtcclxuICAgICAgICBib3JkZXItYm90dG9tOiAwLjFyZW0gc29saWQgI2RhZGFkYTtcclxuICAgIH1cclxuICAgIC5wcm9maWxlLXN0YXRzIGxpIHtcclxuICAgICAgICBmb250LXNpemU6IDEuNHJlbTtcclxuICAgICAgICBmbGV4OiAxO1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgIH1cclxuICAgIC5wcm9maWxlLXN0YXQtY291bnQge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuLyogU3Bpbm5lciBBbmltYXRpb24gKi9cclxuXHJcbkBrZXlmcmFtZXMgbG9hZGVyIHtcclxuICAgIHRvIHtcclxuICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuLypcclxuXHJcblRoZSBmb2xsb3dpbmcgY29kZSB3aWxsIG9ubHkgcnVuIGlmIHlvdXIgYnJvd3NlciBzdXBwb3J0cyBDU1MgZ3JpZC5cclxuXHJcblJlbW92ZSBvciBjb21tZW50LW91dCB0aGUgY29kZSBibG9jayBiZWxvdyB0byBzZWUgaG93IHRoZSBicm93c2VyIHdpbGwgZmFsbC1iYWNrIHRvIGZsZXhib3ggJiBmbG9hdGVkIHN0eWxpbmcuIFxyXG5cclxuKi9cclxuXHJcbkBzdXBwb3J0cyAoZGlzcGxheTogZ3JpZCkge1xyXG4gICAgLnByb2ZpbGUge1xyXG4gICAgICAgIGRpc3BsYXk6IGdyaWQ7XHJcbiAgICAgICAgZ3JpZC10ZW1wbGF0ZS1jb2x1bW5zOiAxZnIgMmZyO1xyXG4gICAgICAgIGdyaWQtdGVtcGxhdGUtcm93czogcmVwZWF0KDMsIGF1dG8pO1xyXG4gICAgICAgIGdyaWQtY29sdW1uLWdhcDogM3JlbTtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG4gICAgLnByb2ZpbGUtaW1hZ2Uge1xyXG4gICAgICAgIGdyaWQtcm93OiAxIC8gLTE7XHJcbiAgICB9XHJcbiAgICAuZ2FsbGVyeSB7XHJcbiAgICAgICAgZGlzcGxheTogZ3JpZDtcclxuICAgICAgICBncmlkLXRlbXBsYXRlLWNvbHVtbnM6IHJlcGVhdChhdXRvLWZpdCwgbWlubWF4KDIycmVtLCAxZnIpKTtcclxuICAgICAgICBncmlkLWdhcDogMnJlbTtcclxuICAgIH1cclxuICAgIC5wcm9maWxlLWltYWdlLFxyXG4gICAgLnByb2ZpbGUtdXNlci1zZXR0aW5ncyxcclxuICAgIC5wcm9maWxlLXN0YXRzLFxyXG4gICAgLnByb2ZpbGUtYmlvLFxyXG4gICAgLmdhbGxlcnktaXRlbSxcclxuICAgIC5nYWxsZXJ5IHtcclxuICAgICAgICB3aWR0aDogYXV0bztcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICB9XHJcbiAgICBAbWVkaWEgKG1heC13aWR0aDogNDByZW0pIHtcclxuICAgICAgICAucHJvZmlsZSB7XHJcbiAgICAgICAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogYXV0byAxZnI7XHJcbiAgICAgICAgICAgIGdyaWQtcm93LWdhcDogMS41cmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICAucHJvZmlsZS1pbWFnZSB7XHJcbiAgICAgICAgICAgIGdyaWQtcm93OiAxIC8gMjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnByb2ZpbGUtdXNlci1zZXR0aW5ncyB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGdyaWQ7XHJcbiAgICAgICAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogYXV0byAxZnI7XHJcbiAgICAgICAgICAgIGdyaWQtZ2FwOiAxcmVtO1xyXG4gICAgICAgIH1cclxuICAgICAgICAucHJvZmlsZS1lZGl0LWJ0bixcclxuICAgICAgICAucHJvZmlsZS1zdGF0cyxcclxuICAgICAgICAucHJvZmlsZS1iaW8ge1xyXG4gICAgICAgICAgICBncmlkLWNvbHVtbjogMSAvIC0xO1xyXG4gICAgICAgIH1cclxuICAgICAgICAucHJvZmlsZS11c2VyLXNldHRpbmdzLFxyXG4gICAgICAgIC5wcm9maWxlLWVkaXQtYnRuLFxyXG4gICAgICAgIC5wcm9maWxlLXNldHRpbmdzLWJ0bixcclxuICAgICAgICAucHJvZmlsZS1iaW8sXHJcbiAgICAgICAgLnByb2ZpbGUtc3RhdHMge1xyXG4gICAgICAgICAgICBtYXJnaW46IDA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG4uYXZhdGFyIHtcclxuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgICB3aWR0aDogMjAwcHg7XHJcbiAgICBoZWlnaHQ6IDIwMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbn1cclxuXHJcbi5iaW8ge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwJTtcclxufVxyXG5cclxuLm5vUG9zdCB7XHJcbiAgICBtYXJnaW46IDEwcHggYXV0bztcclxufSJdfQ== */"] });


/***/ }),

/***/ 96389:
/*!********************************************************!*\
  !*** ./src/app/views/forms/wizard/wizard.component.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "WizardComponent": () => (/* binding */ WizardComponent)
/* harmony export */ });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ 90587);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/card */ 4005);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/divider */ 51996);
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/stepper */ 67675);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/form-field */ 3403);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/input */ 23852);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button */ 2955);
/* harmony import */ var _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/flex-layout/flex */ 23081);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/icon */ 68159);











function WizardComponent_ng_template_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0, "Fill out your name");
} }
function WizardComponent_ng_template_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0, "Fill out your address");
} }
function WizardComponent_ng_template_27_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0, "Done");
} }
function WizardComponent_ng_template_48_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0, "Fill out your name");
} }
function WizardComponent_ng_template_56_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0, "Fill out your address");
} }
function WizardComponent_ng_template_66_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0, "Done");
} }
class WizardComponent {
    constructor(fb) {
        this.fb = fb;
    }
    ngOnInit() {
        this.firstFormGroup = this.fb.group({
            firstCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__.Validators.required]
        });
        this.secondFormGroup = this.fb.group({
            secondCtrl: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__.Validators.required]
        });
    }
    submit() {
        console.log(this.firstFormGroup.value);
        console.log(this.secondFormGroup.value);
    }
}
WizardComponent.ɵfac = function WizardComponent_Factory(t) { return new (t || WizardComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormBuilder)); };
WizardComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: WizardComponent, selectors: [["app-wizard"]], decls: 75, vars: 14, consts: [[1, "p-0"], [1, ""], [1, "card-title-text"], [3, "linear"], [3, "stepControl"], [3, "formGroup"], ["matStepLabel", ""], [1, "pt-1", "pb-1"], ["matInput", "", "placeholder", "Last name, First name", "formControlName", "firstCtrl", "required", ""], ["mat-raised-button", "", "color", "primary", "matStepperNext", ""], ["matInput", "", "placeholder", "Address", "formControlName", "secondCtrl", "required", ""], ["fxLayout", "row"], ["mat-raised-button", "", "color", "accent", "matStepperPrevious", ""], ["fxFlex", "8px"], [1, "pt-1"], [1, "pb-1", "mb-1"], ["mat-raised-button", "", "color", "primary", 3, "click"]], template: function WizardComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-card-title", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Horizontal");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "mat-card-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-horizontal-stepper", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-step", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "form", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, WizardComponent_ng_template_9_Template, 1, 0, "ng-template", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Next");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "mat-step", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "form", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, WizardComponent_ng_template_17_Template, 1, 0, "ng-template", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "mat-form-field", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "input", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Back");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Next");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "mat-step");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, WizardComponent_ng_template_27_Template, 1, 0, "ng-template", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "mat-icon", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "check");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, " You Are Done.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Back");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "button", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function WizardComponent_Template_button_click_37_listener() { return ctx.submit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Submit");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "mat-card", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "mat-card-title", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Verticle");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "mat-divider");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "mat-card-content");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "mat-vertical-stepper", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "mat-step", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "form", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](48, WizardComponent_ng_template_48_Template, 1, 0, "ng-template", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "mat-form-field");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Next");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "mat-step", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "form", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](56, WizardComponent_ng_template_56_Template, 1, 0, "ng-template", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "mat-form-field");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](58, "input", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Back");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "Next");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "mat-step");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](66, WizardComponent_ng_template_66_Template, 1, 0, "ng-template", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "mat-icon", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "check");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, " You Are Done.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](74, "Back");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("linear", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("stepControl", ctx.firstFormGroup);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.firstFormGroup);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("stepControl", ctx.secondFormGroup);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.secondFormGroup);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("font-size", "36px");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("linear", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("stepControl", ctx.firstFormGroup);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.firstFormGroup);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("stepControl", ctx.secondFormGroup);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.secondFormGroup);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("font-size", "36px");
    } }, directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_2__.MatCard, _angular_material_card__WEBPACK_IMPORTED_MODULE_2__.MatCardTitle, _angular_material_divider__WEBPACK_IMPORTED_MODULE_3__.MatDivider, _angular_material_card__WEBPACK_IMPORTED_MODULE_2__.MatCardContent, _angular_material_stepper__WEBPACK_IMPORTED_MODULE_4__.MatStepper, _angular_material_stepper__WEBPACK_IMPORTED_MODULE_4__.MatStep, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormGroupDirective, _angular_material_stepper__WEBPACK_IMPORTED_MODULE_4__.MatStepLabel, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__.MatFormField, _angular_material_input__WEBPACK_IMPORTED_MODULE_6__.MatInput, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormControlName, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.RequiredValidator, _angular_material_button__WEBPACK_IMPORTED_MODULE_7__.MatButton, _angular_material_stepper__WEBPACK_IMPORTED_MODULE_4__.MatStepperNext, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_8__.DefaultLayoutDirective, _angular_material_stepper__WEBPACK_IMPORTED_MODULE_4__.MatStepperPrevious, _angular_flex_layout_flex__WEBPACK_IMPORTED_MODULE_8__.DefaultFlexDirective, _angular_material_icon__WEBPACK_IMPORTED_MODULE_9__.MatIcon], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ3aXphcmQuY29tcG9uZW50LmNzcyJ9 */"] });


/***/ }),

/***/ 29432:
/*!******************************************************!*\
  !*** ./node_modules/ngx-quill/fesm2015/ngx-quill.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "QUILL_CONFIG_TOKEN": () => (/* binding */ QUILL_CONFIG_TOKEN),
/* harmony export */   "QuillEditorBase": () => (/* binding */ QuillEditorBase),
/* harmony export */   "QuillEditorComponent": () => (/* binding */ QuillEditorComponent),
/* harmony export */   "QuillModule": () => (/* binding */ QuillModule),
/* harmony export */   "QuillService": () => (/* binding */ QuillService),
/* harmony export */   "QuillViewComponent": () => (/* binding */ QuillViewComponent),
/* harmony export */   "QuillViewHTMLComponent": () => (/* binding */ QuillViewHTMLComponent),
/* harmony export */   "defaultModules": () => (/* binding */ defaultModules)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 34929);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ 50318);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 90587);










const _c0 = [[["", "quill-editor-toolbar", ""]]];
const _c1 = ["[quill-editor-toolbar]"];
const defaultModules = {
    toolbar: [
        ['bold', 'italic', 'underline', 'strike'],
        ['blockquote', 'code-block'],
        [{ header: 1 }, { header: 2 }],
        [{ list: 'ordered' }, { list: 'bullet' }],
        [{ script: 'sub' }, { script: 'super' }],
        [{ indent: '-1' }, { indent: '+1' }],
        [{ direction: 'rtl' }],
        [{ size: ['small', false, 'large', 'huge'] }],
        [{ header: [1, 2, 3, 4, 5, 6, false] }],
        [
            { color: [] },
            { background: [] }
        ],
        [{ font: [] }],
        [{ align: [] }],
        ['clean'],
        ['link', 'image', 'video'] // link and image, video
    ]
};

const getFormat = (format, configFormat) => {
    const passedFormat = format || configFormat;
    return passedFormat || 'html';
};

const QUILL_CONFIG_TOKEN = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.InjectionToken('config');

class QuillService {
    constructor(config) {
        this.config = config;
        this.count = 0;
        if (!this.config) {
            this.config = { modules: defaultModules };
        }
    }
    getQuill() {
        this.count++;
        if (!this.Quill && this.count === 1) {
            this.$importPromise = new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
                var _a, _b;
                const quillImport = yield __webpack_require__.e(/*! import() */ "node_modules_quill_dist_quill_js").then(__webpack_require__.t.bind(__webpack_require__, /*! quill */ 63786, 23));
                this.Quill = (quillImport.default ? quillImport.default : quillImport);
                // Only register custom options and modules once
                (_a = this.config.customOptions) === null || _a === void 0 ? void 0 : _a.forEach((customOption) => {
                    const newCustomOption = this.Quill.import(customOption.import);
                    newCustomOption.whitelist = customOption.whitelist;
                    this.Quill.register(newCustomOption, true, this.config.suppressGlobalRegisterWarning);
                });
                (_b = this.config.customModules) === null || _b === void 0 ? void 0 : _b.forEach(({ implementation, path }) => {
                    this.Quill.register(path, implementation, this.config.suppressGlobalRegisterWarning);
                });
                resolve(this.Quill);
            }));
        }
        return this.$importPromise;
    }
}
QuillService.ɵfac = function QuillService_Factory(t) { return new (t || QuillService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](QUILL_CONFIG_TOKEN)); };
QuillService.ɵprov = (0,_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"])({ factory: function QuillService_Factory() { return new QuillService((0,_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"])(QUILL_CONFIG_TOKEN)); }, token: QuillService, providedIn: "root" });
QuillService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [QUILL_CONFIG_TOKEN,] }] }
];
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](QuillService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [QUILL_CONFIG_TOKEN]
            }] }]; }, null); })();

// tslint:disable-next-line:directive-class-suffix
class QuillEditorBase {
    constructor(elementRef, domSanitizer, doc, platformId, renderer, zone, service) {
        this.elementRef = elementRef;
        this.domSanitizer = domSanitizer;
        this.doc = doc;
        this.platformId = platformId;
        this.renderer = renderer;
        this.zone = zone;
        this.service = service;
        this.required = false;
        this.customToolbarPosition = 'top';
        this.sanitize = false;
        this.styles = null;
        this.strict = true;
        this.customOptions = [];
        this.customModules = [];
        this.preserveWhitespace = false;
        this.trimOnValidation = false;
        this.onEditorCreated = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
        this.onEditorChanged = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
        this.onContentChanged = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
        this.onSelectionChanged = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
        this.onFocus = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
        this.onBlur = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
        this.disabled = false; // used to store initial value before ViewInit
        this.valueGetter = (quillEditor, editorElement) => {
            let html = editorElement.querySelector('.ql-editor').innerHTML;
            if (html === '<p><br></p>' || html === '<div><br></div>') {
                html = null;
            }
            let modelValue = html;
            const format = getFormat(this.format, this.service.config.format);
            if (format === 'text') {
                modelValue = quillEditor.getText();
            }
            else if (format === 'object') {
                modelValue = quillEditor.getContents();
            }
            else if (format === 'json') {
                try {
                    modelValue = JSON.stringify(quillEditor.getContents());
                }
                catch (e) {
                    modelValue = quillEditor.getText();
                }
            }
            return modelValue;
        };
        this.valueSetter = (quillEditor, value) => {
            const format = getFormat(this.format, this.service.config.format);
            if (format === 'html') {
                if (this.sanitize) {
                    value = this.domSanitizer.sanitize(_angular_core__WEBPACK_IMPORTED_MODULE_0__.SecurityContext.HTML, value);
                }
                return quillEditor.clipboard.convert(value);
            }
            else if (format === 'json') {
                try {
                    return JSON.parse(value);
                }
                catch (e) {
                    return [{ insert: value }];
                }
            }
            return value;
        };
        this.selectionChangeHandler = (range, oldRange, source) => {
            const shouldTriggerOnModelTouched = !range && !!this.onModelTouched;
            // only emit changes when there's any listener
            if (!this.onBlur.observers.length &&
                !this.onFocus.observers.length &&
                !this.onSelectionChanged.observers.length &&
                !shouldTriggerOnModelTouched) {
                return;
            }
            this.zone.run(() => {
                if (range === null) {
                    this.onBlur.emit({
                        editor: this.quillEditor,
                        source
                    });
                }
                else if (oldRange === null) {
                    this.onFocus.emit({
                        editor: this.quillEditor,
                        source
                    });
                }
                this.onSelectionChanged.emit({
                    editor: this.quillEditor,
                    oldRange,
                    range,
                    source
                });
                if (shouldTriggerOnModelTouched) {
                    this.onModelTouched();
                }
            });
        };
        this.textChangeHandler = (delta, oldDelta, source) => {
            // only emit changes emitted by user interactions
            const text = this.quillEditor.getText();
            const content = this.quillEditor.getContents();
            let html = this.editorElem.querySelector('.ql-editor').innerHTML;
            if (html === '<p><br></p>' || html === '<div><br></div>') {
                html = null;
            }
            const trackChanges = this.trackChanges || this.service.config.trackChanges;
            const shouldTriggerOnModelChange = (source === 'user' || trackChanges && trackChanges === 'all') && !!this.onModelChange;
            // only emit changes when there's any listener
            if (!this.onContentChanged.observers.length && !shouldTriggerOnModelChange) {
                return;
            }
            this.zone.run(() => {
                if (shouldTriggerOnModelChange) {
                    this.onModelChange(this.valueGetter(this.quillEditor, this.editorElem));
                }
                this.onContentChanged.emit({
                    content,
                    delta,
                    editor: this.quillEditor,
                    html,
                    oldDelta,
                    source,
                    text
                });
            });
        };
        // tslint:disable-next-line:max-line-length
        this.editorChangeHandler = (event, current, old, source) => {
            // only emit changes when there's any listener
            if (!this.onEditorChanged.observers.length) {
                return;
            }
            // only emit changes emitted by user interactions
            if (event === 'text-change') {
                const text = this.quillEditor.getText();
                const content = this.quillEditor.getContents();
                let html = this.editorElem.querySelector('.ql-editor').innerHTML;
                if (html === '<p><br></p>' || html === '<div><br></div>') {
                    html = null;
                }
                this.zone.run(() => {
                    this.onEditorChanged.emit({
                        content,
                        delta: current,
                        editor: this.quillEditor,
                        event,
                        html,
                        oldDelta: old,
                        source,
                        text
                    });
                });
            }
            else {
                this.onEditorChanged.emit({
                    editor: this.quillEditor,
                    event,
                    oldRange: old,
                    range: current,
                    source
                });
            }
        };
    }
    static normalizeClassNames(classes) {
        const classList = classes.trim().split(' ');
        return classList.reduce((prev, cur) => {
            const trimmed = cur.trim();
            if (trimmed) {
                prev.push(trimmed);
            }
            return prev;
        }, []);
    }
    ngAfterViewInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            if ((0,_angular_common__WEBPACK_IMPORTED_MODULE_2__.isPlatformServer)(this.platformId)) {
                return;
            }
            const Quill = yield this.service.getQuill();
            this.elementRef.nativeElement.insertAdjacentHTML(this.customToolbarPosition === 'top' ? 'beforeend' : 'afterbegin', this.preserveWhitespace ? '<pre quill-editor-element></pre>' : '<div quill-editor-element></div>');
            this.editorElem = this.elementRef.nativeElement.querySelector('[quill-editor-element]');
            const toolbarElem = this.elementRef.nativeElement.querySelector('[quill-editor-toolbar]');
            const modules = Object.assign({}, this.modules || this.service.config.modules);
            if (toolbarElem) {
                modules.toolbar = toolbarElem;
            }
            else if (modules.toolbar === undefined) {
                modules.toolbar = defaultModules.toolbar;
            }
            let placeholder = this.placeholder !== undefined ? this.placeholder : this.service.config.placeholder;
            if (placeholder === undefined) {
                placeholder = 'Insert text here ...';
            }
            if (this.styles) {
                Object.keys(this.styles).forEach((key) => {
                    this.renderer.setStyle(this.editorElem, key, this.styles[key]);
                });
            }
            if (this.classes) {
                this.addClasses(this.classes);
            }
            this.customOptions.forEach((customOption) => {
                const newCustomOption = Quill.import(customOption.import);
                newCustomOption.whitelist = customOption.whitelist;
                Quill.register(newCustomOption, true);
            });
            this.customModules.forEach(({ implementation, path }) => {
                Quill.register(path, implementation);
            });
            let bounds = this.bounds && this.bounds === 'self' ? this.editorElem : this.bounds;
            if (!bounds) {
                bounds = this.service.config.bounds ? this.service.config.bounds : this.doc.body;
            }
            let debug = this.debug;
            if (!debug && debug !== false && this.service.config.debug) {
                debug = this.service.config.debug;
            }
            let readOnly = this.readOnly;
            if (!readOnly && this.readOnly !== false) {
                readOnly = this.service.config.readOnly !== undefined ? this.service.config.readOnly : false;
            }
            let scrollingContainer = this.scrollingContainer;
            if (!scrollingContainer && this.scrollingContainer !== null) {
                scrollingContainer =
                    this.service.config.scrollingContainer === null
                        || this.service.config.scrollingContainer ? this.service.config.scrollingContainer : null;
            }
            let formats = this.formats;
            if (!formats && formats === undefined) {
                formats = this.service.config.formats ? [...this.service.config.formats] : (this.service.config.formats === null ? null : undefined);
            }
            this.zone.runOutsideAngular(() => {
                this.quillEditor = new Quill(this.editorElem, {
                    bounds,
                    debug: debug,
                    formats: formats,
                    modules,
                    placeholder,
                    readOnly,
                    scrollingContainer: scrollingContainer,
                    strict: this.strict,
                    theme: this.theme || (this.service.config.theme ? this.service.config.theme : 'snow')
                });
            });
            if (this.content) {
                const format = getFormat(this.format, this.service.config.format);
                if (format === 'object') {
                    this.quillEditor.setContents(this.content, 'silent');
                }
                else if (format === 'text') {
                    this.quillEditor.setText(this.content, 'silent');
                }
                else if (format === 'json') {
                    try {
                        this.quillEditor.setContents(JSON.parse(this.content), 'silent');
                    }
                    catch (e) {
                        this.quillEditor.setText(this.content, 'silent');
                    }
                }
                else {
                    if (this.sanitize) {
                        this.content = this.domSanitizer.sanitize(_angular_core__WEBPACK_IMPORTED_MODULE_0__.SecurityContext.HTML, this.content);
                    }
                    const contents = this.quillEditor.clipboard.convert(this.content);
                    this.quillEditor.setContents(contents, 'silent');
                }
                this.quillEditor.getModule('history').clear();
            }
            // initialize disabled status based on this.disabled as default value
            this.setDisabledState();
            // triggered if selection or text changed
            this.quillEditor.on('editor-change', this.editorChangeHandler);
            // mark model as touched if editor lost focus
            this.quillEditor.on('selection-change', this.selectionChangeHandler);
            // update model if text changes
            this.quillEditor.on('text-change', this.textChangeHandler);
            // trigger created in a timeout to avoid changed models after checked
            // if you are using the editor api in created output to change the editor content
            setTimeout(() => {
                if (this.onValidatorChanged) {
                    this.onValidatorChanged();
                }
                this.onEditorCreated.emit(this.quillEditor);
            });
        });
    }
    ngOnDestroy() {
        if (this.quillEditor) {
            this.quillEditor.off('selection-change', this.selectionChangeHandler);
            this.quillEditor.off('text-change', this.textChangeHandler);
            this.quillEditor.off('editor-change', this.editorChangeHandler);
        }
    }
    ngOnChanges(changes) {
        if (!this.quillEditor) {
            return;
        }
        // tslint:disable:no-string-literal
        if (changes['readOnly']) {
            this.quillEditor.enable(!changes['readOnly'].currentValue);
        }
        if (changes['placeholder']) {
            this.quillEditor.root.dataset.placeholder =
                changes['placeholder'].currentValue;
        }
        if (changes['styles']) {
            const currentStyling = changes['styles'].currentValue;
            const previousStyling = changes['styles'].previousValue;
            if (previousStyling) {
                Object.keys(previousStyling).forEach((key) => {
                    this.renderer.removeStyle(this.editorElem, key);
                });
            }
            if (currentStyling) {
                Object.keys(currentStyling).forEach((key) => {
                    this.renderer.setStyle(this.editorElem, key, this.styles[key]);
                });
            }
        }
        if (changes['classes']) {
            const currentClasses = changes['classes'].currentValue;
            const previousClasses = changes['classes'].previousValue;
            if (previousClasses) {
                this.removeClasses(previousClasses);
            }
            if (currentClasses) {
                this.addClasses(currentClasses);
            }
        }
        // tslint:enable:no-string-literal
    }
    addClasses(classList) {
        QuillEditorBase.normalizeClassNames(classList).forEach((c) => {
            this.renderer.addClass(this.editorElem, c);
        });
    }
    removeClasses(classList) {
        QuillEditorBase.normalizeClassNames(classList).forEach((c) => {
            this.renderer.removeClass(this.editorElem, c);
        });
    }
    writeValue(currentValue) {
        this.content = currentValue;
        const format = getFormat(this.format, this.service.config.format);
        if (this.quillEditor) {
            if (currentValue) {
                if (format === 'text') {
                    this.quillEditor.setText(currentValue);
                }
                else {
                    this.quillEditor.setContents(this.valueSetter(this.quillEditor, this.content));
                }
                return;
            }
            this.quillEditor.setText('');
        }
    }
    setDisabledState(isDisabled = this.disabled) {
        // store initial value to set appropriate disabled status after ViewInit
        this.disabled = isDisabled;
        if (this.quillEditor) {
            if (isDisabled) {
                this.quillEditor.disable();
                this.renderer.setAttribute(this.elementRef.nativeElement, 'disabled', 'disabled');
            }
            else {
                if (!this.readOnly) {
                    this.quillEditor.enable();
                }
                this.renderer.removeAttribute(this.elementRef.nativeElement, 'disabled');
            }
        }
    }
    registerOnChange(fn) {
        this.onModelChange = fn;
    }
    registerOnTouched(fn) {
        this.onModelTouched = fn;
    }
    registerOnValidatorChange(fn) {
        this.onValidatorChanged = fn;
    }
    validate() {
        if (!this.quillEditor) {
            return null;
        }
        const err = {};
        let valid = true;
        const text = this.quillEditor.getText();
        // trim text if wanted + handle special case that an empty editor contains a new line
        const textLength = this.trimOnValidation ? text.trim().length : (text.length === 1 && text.trim().length === 0 ? 0 : text.length - 1);
        if (this.minLength && textLength && textLength < this.minLength) {
            err.minLengthError = {
                given: textLength,
                minLength: this.minLength
            };
            valid = false;
        }
        if (this.maxLength && textLength > this.maxLength) {
            err.maxLengthError = {
                given: textLength,
                maxLength: this.maxLength
            };
            valid = false;
        }
        if (this.required && !textLength) {
            err.requiredError = {
                empty: true
            };
            valid = false;
        }
        return valid ? null : err;
    }
}
QuillEditorBase.ɵfac = function QuillEditorBase_Factory(t) { return new (t || QuillEditorBase)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_2__.DOCUMENT), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.PLATFORM_ID), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.NgZone), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](QuillService)); };
QuillEditorBase.ɵdir = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({ type: QuillEditorBase, inputs: { required: "required", customToolbarPosition: "customToolbarPosition", sanitize: "sanitize", styles: "styles", strict: "strict", customOptions: "customOptions", customModules: "customModules", preserveWhitespace: "preserveWhitespace", trimOnValidation: "trimOnValidation", valueGetter: "valueGetter", valueSetter: "valueSetter", format: "format", theme: "theme", modules: "modules", debug: "debug", readOnly: "readOnly", placeholder: "placeholder", maxLength: "maxLength", minLength: "minLength", formats: "formats", scrollingContainer: "scrollingContainer", bounds: "bounds", trackChanges: "trackChanges", classes: "classes" }, outputs: { onEditorCreated: "onEditorCreated", onEditorChanged: "onEditorChanged", onContentChanged: "onContentChanged", onSelectionChanged: "onSelectionChanged", onFocus: "onFocus", onBlur: "onBlur" }, features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]] });
QuillEditorBase.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.DOCUMENT,] }] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__.PLATFORM_ID,] }] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2 },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgZone },
    { type: QuillService }
];
QuillEditorBase.propDecorators = {
    format: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    theme: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    modules: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    debug: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    readOnly: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    placeholder: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    maxLength: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    minLength: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    required: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    formats: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    customToolbarPosition: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    sanitize: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    styles: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    strict: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    scrollingContainer: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    bounds: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    customOptions: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    customModules: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    trackChanges: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    preserveWhitespace: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    classes: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    trimOnValidation: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    onEditorCreated: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    onEditorChanged: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    onContentChanged: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    onSelectionChanged: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    onFocus: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    onBlur: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    valueGetter: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    valueSetter: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }]
};
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](QuillEditorBase, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Directive
    }], function () { return [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer }, { type: undefined, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.DOCUMENT]
            }] }, { type: undefined, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__.PLATFORM_ID]
            }] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2 }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgZone }, { type: QuillService }]; }, { required: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], customToolbarPosition: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], sanitize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], styles: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], strict: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], customOptions: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], customModules: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], preserveWhitespace: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], trimOnValidation: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], onEditorCreated: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output
        }], onEditorChanged: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output
        }], onContentChanged: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output
        }], onSelectionChanged: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output
        }], onFocus: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output
        }], onBlur: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output
        }], valueGetter: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], valueSetter: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], format: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], theme: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], modules: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], debug: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], readOnly: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], placeholder: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], maxLength: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], minLength: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], formats: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], scrollingContainer: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], bounds: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], trackChanges: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], classes: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }] }); })();
class QuillEditorComponent extends QuillEditorBase {
    constructor(elementRef, domSanitizer, doc, platformId, renderer, zone, service) {
        super(elementRef, domSanitizer, doc, platformId, renderer, zone, service);
    }
}
QuillEditorComponent.ɵfac = function QuillEditorComponent_Factory(t) { return new (t || QuillEditorComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_2__.DOCUMENT), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.PLATFORM_ID), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.NgZone), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](QuillService)); };
QuillEditorComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: QuillEditorComponent, selectors: [["quill-editor"]], features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([
            {
                multi: true,
                provide: _angular_forms__WEBPACK_IMPORTED_MODULE_4__.NG_VALUE_ACCESSOR,
                // eslint-disable-next-line @typescript-eslint/no-use-before-define
                useExisting: (0,_angular_core__WEBPACK_IMPORTED_MODULE_0__.forwardRef)(() => QuillEditorComponent)
            },
            {
                multi: true,
                provide: _angular_forms__WEBPACK_IMPORTED_MODULE_4__.NG_VALIDATORS,
                // eslint-disable-next-line @typescript-eslint/no-use-before-define
                useExisting: (0,_angular_core__WEBPACK_IMPORTED_MODULE_0__.forwardRef)(() => QuillEditorComponent)
            }
        ]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵInheritDefinitionFeature"]], ngContentSelectors: _c1, decls: 1, vars: 0, template: function QuillEditorComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojectionDef"](_c0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojection"](0);
    } }, encapsulation: 2 });
QuillEditorComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef,] }] },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer,] }] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.DOCUMENT,] }] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__.PLATFORM_ID,] }] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2,] }] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgZone, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__.NgZone,] }] },
    { type: QuillService, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [QuillService,] }] }
];
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](QuillEditorComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Component,
        args: [{
                encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ViewEncapsulation.None,
                providers: [
                    {
                        multi: true,
                        provide: _angular_forms__WEBPACK_IMPORTED_MODULE_4__.NG_VALUE_ACCESSOR,
                        // eslint-disable-next-line @typescript-eslint/no-use-before-define
                        useExisting: (0,_angular_core__WEBPACK_IMPORTED_MODULE_0__.forwardRef)(() => QuillEditorComponent)
                    },
                    {
                        multi: true,
                        provide: _angular_forms__WEBPACK_IMPORTED_MODULE_4__.NG_VALIDATORS,
                        // eslint-disable-next-line @typescript-eslint/no-use-before-define
                        useExisting: (0,_angular_core__WEBPACK_IMPORTED_MODULE_0__.forwardRef)(() => QuillEditorComponent)
                    }
                ],
                selector: 'quill-editor',
                template: `
  <ng-content select="[quill-editor-toolbar]"></ng-content>
`
            }]
    }], function () { return [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef]
            }] }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer]
            }] }, { type: undefined, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.DOCUMENT]
            }] }, { type: undefined, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__.PLATFORM_ID]
            }] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2]
            }] }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgZone, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__.NgZone]
            }] }, { type: QuillService, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [QuillService]
            }] }]; }, null); })();

class QuillViewHTMLComponent {
    constructor(sanitizer, service) {
        this.sanitizer = sanitizer;
        this.service = service;
        this.innerHTML = '';
        this.themeClass = 'ql-snow';
        this.content = '';
    }
    ngOnChanges(changes) {
        if (changes.theme) {
            const theme = changes.theme.currentValue || (this.service.config.theme ? this.service.config.theme : 'snow');
            this.themeClass = `ql-${theme} ngx-quill-view-html`;
        }
        else if (!this.theme) {
            const theme = this.service.config.theme ? this.service.config.theme : 'snow';
            this.themeClass = `ql-${theme} ngx-quill-view-html`;
        }
        if (changes.content) {
            this.innerHTML = this.sanitizer.bypassSecurityTrustHtml(changes.content.currentValue);
        }
    }
}
QuillViewHTMLComponent.ɵfac = function QuillViewHTMLComponent_Factory(t) { return new (t || QuillViewHTMLComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](QuillService)); };
QuillViewHTMLComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: QuillViewHTMLComponent, selectors: [["quill-view-html"]], inputs: { content: "content", theme: "theme" }, features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]], decls: 2, vars: 2, consts: [[1, "ql-container", 3, "ngClass"], [1, "ql-editor", 3, "innerHTML"]], template: function QuillViewHTMLComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx.themeClass);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("innerHTML", ctx.innerHTML, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeHtml"]);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.NgClass], styles: ["\n.ql-container.ngx-quill-view-html {\n  border: 0;\n}\n"], encapsulation: 2 });
QuillViewHTMLComponent.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer,] }] },
    { type: QuillService }
];
QuillViewHTMLComponent.propDecorators = {
    content: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    theme: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }]
};
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](QuillViewHTMLComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Component,
        args: [{
                encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ViewEncapsulation.None,
                selector: 'quill-view-html',
                template: `
  <div class="ql-container" [ngClass]="themeClass">
    <div class="ql-editor" [innerHTML]="innerHTML">
    </div>
  </div>
`,
                styles: [`
.ql-container.ngx-quill-view-html {
  border: 0;
}
`]
            }]
    }], function () { return [{ type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer]
            }] }, { type: QuillService }]; }, { content: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], theme: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }] }); })();

class QuillViewComponent {
    constructor(elementRef, renderer, zone, service, domSanitizer, platformId) {
        this.elementRef = elementRef;
        this.renderer = renderer;
        this.zone = zone;
        this.service = service;
        this.domSanitizer = domSanitizer;
        this.platformId = platformId;
        this.sanitize = false;
        this.strict = true;
        this.customModules = [];
        this.customOptions = [];
        this.preserveWhitespace = false;
        this.valueSetter = (quillEditor, value) => {
            const format = getFormat(this.format, this.service.config.format);
            let content = value;
            if (format === 'text') {
                quillEditor.setText(content);
            }
            else {
                if (format === 'html') {
                    if (this.sanitize) {
                        value = this.domSanitizer.sanitize(_angular_core__WEBPACK_IMPORTED_MODULE_0__.SecurityContext.HTML, value);
                    }
                    content = quillEditor.clipboard.convert(value);
                }
                else if (format === 'json') {
                    try {
                        content = JSON.parse(value);
                    }
                    catch (e) {
                        content = [{ insert: value }];
                    }
                }
                quillEditor.setContents(content);
            }
        };
    }
    ngOnChanges(changes) {
        if (!this.quillEditor) {
            return;
        }
        if (changes.content) {
            this.valueSetter(this.quillEditor, changes.content.currentValue);
        }
    }
    ngAfterViewInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            if ((0,_angular_common__WEBPACK_IMPORTED_MODULE_2__.isPlatformServer)(this.platformId)) {
                return;
            }
            const Quill = yield this.service.getQuill();
            const modules = Object.assign({}, this.modules || this.service.config.modules);
            modules.toolbar = false;
            this.customOptions.forEach((customOption) => {
                const newCustomOption = Quill.import(customOption.import);
                newCustomOption.whitelist = customOption.whitelist;
                Quill.register(newCustomOption, true);
            });
            this.customModules.forEach(({ implementation, path }) => {
                Quill.register(path, implementation);
            });
            let debug = this.debug;
            if (!debug && debug !== false && this.service.config.debug) {
                debug = this.service.config.debug;
            }
            let formats = this.formats;
            if (!formats && formats === undefined) {
                formats = this.service.config.formats ?
                    Object.assign({}, this.service.config.formats) : (this.service.config.formats === null ? null : undefined);
            }
            const theme = this.theme || (this.service.config.theme ? this.service.config.theme : 'snow');
            this.elementRef.nativeElement.insertAdjacentHTML('afterbegin', this.preserveWhitespace ? '<pre quill-view-element></pre>' : '<div quill-view-element></div>');
            this.editorElem = this.elementRef.nativeElement.querySelector('[quill-view-element]');
            this.zone.runOutsideAngular(() => {
                this.quillEditor = new Quill(this.editorElem, {
                    debug: debug,
                    formats: formats,
                    modules,
                    readOnly: true,
                    strict: this.strict,
                    theme
                });
            });
            this.renderer.addClass(this.editorElem, 'ngx-quill-view');
            if (this.content) {
                this.valueSetter(this.quillEditor, this.content);
            }
        });
    }
}
QuillViewComponent.ɵfac = function QuillViewComponent_Factory(t) { return new (t || QuillViewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.NgZone), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](QuillService), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.PLATFORM_ID)); };
QuillViewComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: QuillViewComponent, selectors: [["quill-view"]], inputs: { sanitize: "sanitize", strict: "strict", customModules: "customModules", customOptions: "customOptions", preserveWhitespace: "preserveWhitespace", format: "format", theme: "theme", modules: "modules", debug: "debug", formats: "formats", content: "content" }, features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]], decls: 0, vars: 0, template: function QuillViewComponent_Template(rf, ctx) { }, styles: ["\n.ql-container.ngx-quill-view {\n  border: 0;\n}\n"], encapsulation: 2 });
QuillViewComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2 },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgZone },
    { type: QuillService },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__.PLATFORM_ID,] }] }
];
QuillViewComponent.propDecorators = {
    format: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    theme: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    modules: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    debug: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    formats: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    sanitize: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    strict: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    content: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    customModules: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    customOptions: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    preserveWhitespace: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }]
};
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](QuillViewComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Component,
        args: [{
                encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ViewEncapsulation.None,
                selector: 'quill-view',
                template: `
`,
                styles: [`
.ql-container.ngx-quill-view {
  border: 0;
}
`]
            }]
    }], function () { return [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2 }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgZone }, { type: QuillService }, { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer }, { type: undefined, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [_angular_core__WEBPACK_IMPORTED_MODULE_0__.PLATFORM_ID]
            }] }]; }, { sanitize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], strict: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], customModules: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], customOptions: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], preserveWhitespace: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], format: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], theme: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], modules: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], debug: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], formats: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], content: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }] }); })();

class QuillModule {
    static forRoot(config) {
        return {
            ngModule: QuillModule,
            providers: [
                {
                    provide: QUILL_CONFIG_TOKEN,
                    useValue: config
                }
            ]
        };
    }
}
QuillModule.ɵfac = function QuillModule_Factory(t) { return new (t || QuillModule)(); };
QuillModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: QuillModule });
QuillModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ providers: [QuillService], imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule]] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](QuillModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgModule,
        args: [{
                declarations: [
                    QuillEditorComponent,
                    QuillViewComponent,
                    QuillViewHTMLComponent
                ],
                exports: [QuillEditorComponent, QuillViewComponent, QuillViewHTMLComponent],
                imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule],
                providers: [QuillService]
            }]
    }], null, null); })();
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](QuillModule, { declarations: function () { return [QuillEditorComponent, QuillViewComponent, QuillViewHTMLComponent]; }, imports: function () { return [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule]; }, exports: function () { return [QuillEditorComponent, QuillViewComponent, QuillViewHTMLComponent]; } }); })();

/*
 * Public API Surface of ngx-quill
 */

/**
 * Generated bundle index. Do not edit.
 */





/***/ }),

/***/ 21175:
/*!**************************************************************!*\
  !*** ./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NgxUiLoaderHttpModule": () => (/* binding */ NgxUiLoaderHttpModule),
/* harmony export */   "NgxUiLoaderModule": () => (/* binding */ NgxUiLoaderModule),
/* harmony export */   "NgxUiLoaderRouterModule": () => (/* binding */ NgxUiLoaderRouterModule),
/* harmony export */   "NgxUiLoaderService": () => (/* binding */ NgxUiLoaderService),
/* harmony export */   "PB_DIRECTION": () => (/* binding */ PB_DIRECTION),
/* harmony export */   "POSITION": () => (/* binding */ POSITION),
/* harmony export */   "SPINNER": () => (/* binding */ SPINNER),
/* harmony export */   "ɵa": () => (/* binding */ NGX_UI_LOADER_CONFIG_TOKEN),
/* harmony export */   "ɵb": () => (/* binding */ NgxUiLoaderComponent),
/* harmony export */   "ɵc": () => (/* binding */ NgxUiLoaderBlurredDirective),
/* harmony export */   "ɵd": () => (/* binding */ NGX_UI_LOADER_ROUTER_CONFIG_TOKEN),
/* harmony export */   "ɵe": () => (/* binding */ NgxUiLoaderHttpInterceptor),
/* harmony export */   "ɵf": () => (/* binding */ NGX_UI_LOADER_HTTP_CONFIG_TOKEN)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ 84505);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 36362);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ 50318);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 59151);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 44661);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 52816);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ 28784);








/**
 * Available spinner types
 */





function NgxUiLoaderComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 9);
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("height", ctx_r0.pbThickness, "px")("color", ctx_r0.pbColor);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("ngx-position-absolute", ctx_r0.loaderId !== ctx_r0.defaultConfig.masterLoaderId)("loading-foreground", ctx_r0.showForeground)("foreground-closing", ctx_r0.foregroundClosing)("fast-closing", ctx_r0.foregroundClosing && ctx_r0.fastFadeOut);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", "ngx-progress-bar-" + ctx_r0.pbDirection);
} }
function NgxUiLoaderComponent_img_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "img", 10);
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("width", ctx_r1.logoSize, "px")("height", ctx_r1.logoSize, "px")("top", ctx_r1.logoTop);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx_r1.logoPosition)("src", ctx_r1.trustedLogoUrl, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
function NgxUiLoaderComponent_div_4_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div");
} }
function NgxUiLoaderComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, NgxUiLoaderComponent_div_4_div_1_Template, 1, 0, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx_r2.fgSpinnerClass);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r2.fgDivs);
} }
function NgxUiLoaderComponent_ng_template_5_ng_container_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
} }
function NgxUiLoaderComponent_ng_template_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, NgxUiLoaderComponent_ng_template_5_ng_container_0_Template, 1, 0, "ng-container", 12);
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", ctx_r4.fgsTemplate);
} }
function NgxUiLoaderComponent_div_10_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div");
} }
function NgxUiLoaderComponent_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, NgxUiLoaderComponent_div_10_div_1_Template, 1, 0, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx_r5.bgSpinnerClass);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r5.bgDivs);
} }
function NgxUiLoaderComponent_ng_template_11_ng_container_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
} }
function NgxUiLoaderComponent_ng_template_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, NgxUiLoaderComponent_ng_template_11_ng_container_0_Template, 1, 0, "ng-container", 12);
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", ctx_r7.bgsTemplate);
} }
var SPINNER;
(function (SPINNER) {
    SPINNER["ballScaleMultiple"] = "ball-scale-multiple";
    SPINNER["ballSpin"] = "ball-spin";
    SPINNER["ballSpinClockwise"] = "ball-spin-clockwise";
    SPINNER["ballSpinClockwiseFadeRotating"] = "ball-spin-clockwise-fade-rotating";
    SPINNER["ballSpinFadeRotating"] = "ball-spin-fade-rotating";
    SPINNER["chasingDots"] = "chasing-dots";
    SPINNER["circle"] = "circle";
    SPINNER["cubeGrid"] = "cube-grid";
    SPINNER["doubleBounce"] = "double-bounce";
    SPINNER["fadingCircle"] = "fading-circle";
    SPINNER["foldingCube"] = "folding-cube";
    SPINNER["pulse"] = "pulse";
    SPINNER["rectangleBounce"] = "rectangle-bounce";
    SPINNER["rectangleBounceParty"] = "rectangle-bounce-party";
    SPINNER["rectangleBouncePulseOut"] = "rectangle-bounce-pulse-out";
    SPINNER["rectangleBouncePulseOutRapid"] = "rectangle-bounce-pulse-out-rapid";
    SPINNER["rotatingPlane"] = "rotating-plane";
    SPINNER["squareJellyBox"] = "square-jelly-box";
    SPINNER["squareLoader"] = "square-loader";
    SPINNER["threeBounce"] = "three-bounce";
    SPINNER["threeStrings"] = "three-strings";
    SPINNER["wanderingCubes"] = "wandering-cubes";
})(SPINNER || (SPINNER = {}));
/**
 * Available postions
 */
var POSITION;
(function (POSITION) {
    POSITION["bottomCenter"] = "bottom-center";
    POSITION["bottomLeft"] = "bottom-left";
    POSITION["bottomRight"] = "bottom-right";
    POSITION["centerCenter"] = "center-center";
    POSITION["centerLeft"] = "center-left";
    POSITION["centerRight"] = "center-right";
    POSITION["topCenter"] = "top-center";
    POSITION["topLeft"] = "top-left";
    POSITION["topRight"] = "top-right";
})(POSITION || (POSITION = {}));
/**
 * Progress bar directions
 */
var PB_DIRECTION;
(function (PB_DIRECTION) {
    PB_DIRECTION["leftToRight"] = "ltr";
    PB_DIRECTION["rightToLeft"] = "rtl";
})(PB_DIRECTION || (PB_DIRECTION = {}));

/**
 * The default value of foreground task id
 */
const DEFAULT_FG_TASK_ID = 'fg-default';
/**
 * The default value of background task id
 */
const DEFAULT_BG_TASK_ID = 'bg-default';
/**
 * The default value of loader id
 */
const DEFAULT_MASTER_LOADER_ID = 'master';
const DEFAULT_TIME = {};
const MIN_DELAY = 0;
const MIN_TIME = 0;
const CLOSING_TIME = 1001;
const FAST_CLOSING_TIME = 601;
const BACKGROUND = false;
const FOREGROUND = true;
const OVERLAY_DISAPPEAR_TIME = 500;
const FAST_OVERLAY_DISAPPEAR_TIME = 300;
/**
 * Http loader taskId
 */
const HTTP_LOADER_TASK_ID = '$_http-loader';
/**
 * Router loader taskId
 */
const ROUTER_LOADER_TASK_ID = '$_router_loader';
/**
 * The configuration of spinners
 */
const SPINNER_CONFIG = {
    'ball-scale-multiple': {
        divs: 3,
        class: 'sk-ball-scale-multiple'
    },
    'ball-spin': {
        divs: 8,
        class: 'sk-ball-spin'
    },
    'ball-spin-clockwise': {
        divs: 8,
        class: 'sk-ball-spin-clockwise'
    },
    'ball-spin-clockwise-fade-rotating': {
        divs: 8,
        class: 'sk-ball-spin-clockwise-fade-rotating'
    },
    'ball-spin-fade-rotating': {
        divs: 8,
        class: 'sk-ball-spin-fade-rotating'
    },
    'chasing-dots': {
        divs: 2,
        class: 'sk-chasing-dots'
    },
    circle: {
        divs: 12,
        class: 'sk-circle'
    },
    'cube-grid': {
        divs: 9,
        class: 'sk-cube-grid'
    },
    'double-bounce': {
        divs: 2,
        class: 'sk-double-bounce'
    },
    'fading-circle': {
        divs: 12,
        class: 'sk-fading-circle'
    },
    'folding-cube': {
        divs: 4,
        class: 'sk-folding-cube'
    },
    pulse: {
        divs: 1,
        class: 'sk-pulse'
    },
    'rectangle-bounce': {
        divs: 5,
        class: 'sk-rectangle-bounce'
    },
    'rectangle-bounce-party': {
        divs: 5,
        class: 'sk-rectangle-bounce-party'
    },
    'rectangle-bounce-pulse-out': {
        divs: 5,
        class: 'sk-rectangle-bounce-pulse-out'
    },
    'rectangle-bounce-pulse-out-rapid': {
        divs: 5,
        class: 'sk-rectangle-bounce-pulse-out-rapid'
    },
    'rotating-plane': {
        divs: 1,
        class: 'sk-rotating-plane'
    },
    'square-jelly-box': {
        divs: 2,
        class: 'sk-square-jelly-box'
    },
    'square-loader': {
        divs: 1,
        class: 'sk-square-loader'
    },
    'three-bounce': {
        divs: 3,
        class: 'sk-three-bounce'
    },
    'three-strings': {
        divs: 3,
        class: 'sk-three-strings'
    },
    'wandering-cubes': {
        divs: 2,
        class: 'sk-wandering-cubes'
    }
};
/**
 * The default configuration of ngx-ui-loader
 */
const DEFAULT_CONFIG = {
    bgsColor: '#00ACC1',
    bgsOpacity: 0.5,
    bgsPosition: POSITION.bottomRight,
    bgsSize: 60,
    bgsType: SPINNER.ballSpinClockwise,
    blur: 5,
    delay: 0,
    fastFadeOut: false,
    fgsColor: '#00ACC1',
    fgsPosition: POSITION.centerCenter,
    fgsSize: 60,
    fgsType: SPINNER.ballSpinClockwise,
    gap: 24,
    logoPosition: POSITION.centerCenter,
    logoSize: 120,
    logoUrl: '',
    masterLoaderId: DEFAULT_MASTER_LOADER_ID,
    overlayBorderRadius: '0',
    overlayColor: 'rgba(40, 40, 40, 0.8)',
    pbColor: '#00ACC1',
    pbDirection: PB_DIRECTION.leftToRight,
    pbThickness: 3,
    hasProgressBar: true,
    text: '',
    textColor: '#FFFFFF',
    textPosition: POSITION.centerCenter,
    maxTime: -1,
    minTime: 300
};

/**
 * Injection token for ngx-ui-loader configuration
 */
const NGX_UI_LOADER_CONFIG_TOKEN = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.InjectionToken('ngxUiLoaderCustom.config');

class NgxUiLoaderService {
    /**
     * Constructor
     */
    constructor(config) {
        this.config = config;
        this.defaultConfig = Object.assign({}, DEFAULT_CONFIG);
        if (this.config) {
            if (this.config.minTime && this.config.minTime < MIN_TIME) {
                this.config.minTime = MIN_TIME;
            }
            this.defaultConfig = Object.assign(Object.assign({}, this.defaultConfig), this.config);
        }
        this.loaders = {};
        this.showForeground = new rxjs__WEBPACK_IMPORTED_MODULE_1__.BehaviorSubject({ loaderId: '', isShow: false });
        this.showForeground$ = this.showForeground.asObservable();
        this.showBackground = new rxjs__WEBPACK_IMPORTED_MODULE_1__.BehaviorSubject({ loaderId: '', isShow: false });
        this.showBackground$ = this.showBackground.asObservable();
        this.fgClosing = new rxjs__WEBPACK_IMPORTED_MODULE_1__.BehaviorSubject({ loaderId: '', isShow: false });
        this.foregroundClosing$ = this.fgClosing.asObservable();
        this.bgClosing = new rxjs__WEBPACK_IMPORTED_MODULE_1__.BehaviorSubject({ loaderId: '', isShow: false });
        this.backgroundClosing$ = this.bgClosing.asObservable();
    }
    /**
     * For internal use only.
     * @docs-private
     */
    bindLoaderData(loaderId) {
        const isMaster = loaderId === this.defaultConfig.masterLoaderId;
        if (this.loaders[loaderId]) {
            if (this.loaders[loaderId].isBound) {
                throw new Error(`[ngx-ui-loader] - loaderId "${loaderId}" is duplicated.`);
            }
            this.loaders[loaderId].isBound = true;
            this.loaders[loaderId].isMaster = isMaster;
            // emit showEvent after data loader is bound
            if (this.hasRunningTask(FOREGROUND, loaderId)) {
                this.showForeground.next({ loaderId, isShow: true });
            }
            else {
                if (this.hasRunningTask(BACKGROUND, loaderId)) {
                    this.showBackground.next({ loaderId, isShow: true });
                }
            }
        }
        else {
            this.createLoaderData(loaderId, isMaster, true);
        }
    }
    /**
     * For internal use only.
     * @docs-private
     */
    destroyLoaderData(loaderId) {
        this.stopAllLoader(loaderId);
        delete this.loaders[loaderId];
    }
    /**
     * Get default loader configuration
     * @returns default configuration object
     */
    getDefaultConfig() {
        return Object.assign({}, this.defaultConfig);
    }
    /**
     * Get all the loaders
     */
    getLoaders() {
        return JSON.parse(JSON.stringify(this.loaders));
    }
    /**
     * Get data of a specified loader. If loaderId is not provided, it will return data of
     * master loader(if existed). Otherwise null is returned.
     */
    getLoader(loaderId) {
        loaderId = loaderId ? loaderId : this.defaultConfig.masterLoaderId;
        if (this.loaders[loaderId]) {
            return JSON.parse(JSON.stringify(this.loaders[loaderId]));
        }
        return null;
    }
    /**
     * Start the foreground loading of loader having `loaderId` with a specified `taskId`.
     * The loading is only closed off when all taskIds of that loader are called with stopLoader() method.
     * @param loaderId the loader Id
     * @param taskId the optional task Id of the loading. taskId is set to 'fd-default' by default.
     */
    startLoader(loaderId, taskId = DEFAULT_FG_TASK_ID, time) {
        if (!this.readyToStart(loaderId, taskId, true, time)) {
            return;
        }
        if (!this.loaders[loaderId].tasks[taskId].isOtherRunning) {
            // no other foreground task running
            if (this.hasRunningTask(BACKGROUND, loaderId)) {
                this.backgroundCloseout(loaderId);
                this.showBackground.next({ loaderId, isShow: false });
            }
            this.showForeground.next({ loaderId, isShow: true });
        }
    }
    /**
     * Start the foreground loading of master loader with a specified `taskId`.
     * The loading is only closed off when all taskIds of that loader are called with stop() method.
     * NOTE: Really this function just wraps startLoader() function
     * @param taskId the optional task Id of the loading. taskId is set to 'fd-default' by default.
     */
    start(taskId = DEFAULT_FG_TASK_ID, time) {
        this.startLoader(this.defaultConfig.masterLoaderId, taskId, time);
    }
    /**
     * Start the background loading of loader having `loaderId` with a specified `taskId`.
     * The loading is only closed off when all taskIds of that loader are called with stopLoaderBackground() method.
     * @param loaderId the loader Id
     * @param taskId the optional task Id of the loading. taskId is set to 'bg-default' by default.
     */
    startBackgroundLoader(loaderId, taskId = DEFAULT_BG_TASK_ID, time) {
        if (!this.readyToStart(loaderId, taskId, false, time)) {
            return;
        }
        if (!this.hasRunningTask(FOREGROUND, loaderId) && !this.loaders[loaderId].tasks[taskId].isOtherRunning) {
            this.showBackground.next({ loaderId, isShow: true });
        }
    }
    /**
     * Start the background loading of master loader with a specified `taskId`.
     * The loading is only closed off when all taskIds of that loader are called with stopBackground() method.
     * NOTE: Really this function just wraps startBackgroundLoader() function
     * @param taskId the optional task Id of the loading. taskId is set to 'bg-default' by default.
     */
    startBackground(taskId = DEFAULT_BG_TASK_ID, time) {
        this.startBackgroundLoader(this.defaultConfig.masterLoaderId, taskId, time);
    }
    /**
     * Stop a foreground loading of loader having `loaderId` with specific `taskId`
     * @param loaderId the loader Id
     * @param taskId the optional task Id to stop. If not provided, 'fg-default' is used.
     * @returns Object
     */
    stopLoader(loaderId, taskId = DEFAULT_FG_TASK_ID) {
        if (!this.readyToStop(loaderId, taskId)) {
            return;
        }
        if (!this.hasRunningTask(FOREGROUND, loaderId)) {
            this.foregroundCloseout(loaderId);
            this.showForeground.next({ loaderId, isShow: false });
            if (this.hasRunningTask(BACKGROUND, loaderId)) {
                setTimeout(() => {
                    if (this.hasRunningTask(BACKGROUND, loaderId)) {
                        // still have background tasks
                        this.showBackground.next({ loaderId, isShow: true });
                    }
                }, this.defaultConfig.fastFadeOut ? FAST_OVERLAY_DISAPPEAR_TIME : OVERLAY_DISAPPEAR_TIME);
            }
        }
    }
    /**
     * Stop a foreground loading of master loader with specific `taskId`
     * @param taskId the optional task Id to stop. If not provided, 'fg-default' is used.
     * @returns Object
     */
    stop(taskId = DEFAULT_FG_TASK_ID) {
        this.stopLoader(this.defaultConfig.masterLoaderId, taskId);
    }
    /**
     * Stop a background loading of loader having `loaderId` with specific `taskId`
     * @param loaderId the loader Id
     * @param taskId the optional task Id to stop. If not provided, 'bg-default' is used.
     * @returns Object
     */
    stopBackgroundLoader(loaderId, taskId = DEFAULT_BG_TASK_ID) {
        if (!this.readyToStop(loaderId, taskId)) {
            return;
        }
        if (!this.hasRunningTask(FOREGROUND, loaderId) && !this.hasRunningTask(BACKGROUND, loaderId)) {
            this.backgroundCloseout(loaderId);
            this.showBackground.next({ loaderId, isShow: false });
        }
    }
    /**
     * Stop a background loading of master loader with specific taskId
     * @param taskId the optional task Id to stop. If not provided, 'bg-default' is used.
     * @returns Object
     */
    stopBackground(taskId = DEFAULT_BG_TASK_ID) {
        this.stopBackgroundLoader(this.defaultConfig.masterLoaderId, taskId);
    }
    /**
     * Stop all the background and foreground loadings of loader having `loaderId`
     * @param loaderId the loader Id
     */
    stopAllLoader(loaderId) {
        if (!this.loaders[loaderId]) {
            console.warn(`[ngx-ui-loader] - loaderId "${loaderId}" does not exist.`);
            return;
        }
        if (this.hasRunningTask(FOREGROUND, loaderId)) {
            this.foregroundCloseout(loaderId);
            this.showForeground.next({ loaderId, isShow: false });
        }
        else if (this.hasRunningTask(BACKGROUND, loaderId)) {
            this.backgroundCloseout(loaderId);
            this.showBackground.next({ loaderId, isShow: false });
        }
        this.clearAllTimers(this.loaders[loaderId].tasks);
        this.loaders[loaderId].tasks = {};
    }
    /**
     * Stop all the background and foreground loadings of master loader
     */
    stopAll() {
        this.stopAllLoader(this.defaultConfig.masterLoaderId);
    }
    /**
     * Create loader data if it does not exist
     * @docs-private
     */
    createLoaderData(loaderId, isMaster, isBound) {
        if (!this.loaders[loaderId]) {
            this.loaders[loaderId] = {
                loaderId,
                tasks: {},
                isMaster,
                isBound
            };
        }
    }
    /**
     * Manage to close foreground loading
     * @docs-private
     * @param loaderId the loader id
     */
    foregroundCloseout(loaderId) {
        this.fgClosing.next({ loaderId, isShow: true });
        setTimeout(() => {
            this.fgClosing.next({ loaderId, isShow: false });
        }, this.defaultConfig.fastFadeOut ? FAST_CLOSING_TIME : CLOSING_TIME);
    }
    /**
     * Manage to close background loading
     * @docs-private
     * @param loaderId the loader id
     */
    backgroundCloseout(loaderId) {
        this.bgClosing.next({ loaderId, isShow: true });
        setTimeout(() => {
            this.bgClosing.next({ loaderId, isShow: false });
        }, this.defaultConfig.fastFadeOut ? FAST_CLOSING_TIME : CLOSING_TIME);
    }
    /**
     * Clear all timers of the given task
     * @docs-private
     */
    clearTimers(task) {
        clearTimeout(task.delayTimer);
        clearTimeout(task.maxTimer);
        clearTimeout(task.minTimer);
    }
    /**
     * Clear all timers of the given tasks
     * @docs-private
     */
    clearAllTimers(tasks) {
        Object.keys(tasks).map(id => {
            this.clearTimers(tasks[id]);
        });
    }
    /**
     * Check whether the specified loader has a running task with the given `taskId`.
     * If no `taskId` specified, it will check whether the loader has any running tasks.
     * For internal use only.
     * @docs-private
     * @param isForeground foreground task or background task
     * @param loaderId the loader Id
     * @param taskId the optional task Id
     * @returns boolean
     */
    hasRunningTask(isForeground, loaderId, taskId) {
        if (this.loaders[loaderId]) {
            const tasks = this.loaders[loaderId].tasks;
            if (taskId) {
                return tasks[taskId] ? (tasks[taskId].startAt ? true : false) : false;
            }
            return Object.keys(tasks).some(id => !!tasks[id].startAt && tasks[id].isForeground === isForeground);
        }
        return false;
    }
    /**
     * @docs-private
     */
    readyToStart(loaderId, taskId, isForeground, time = DEFAULT_TIME) {
        this.createLoaderData(loaderId, undefined, false);
        const isOtherRunning = this.hasRunningTask(isForeground, loaderId);
        if (!this.loaders[loaderId].tasks[taskId]) {
            this.loaders[loaderId].tasks[taskId] = {
                taskId,
                isForeground,
                minTime: time.minTime >= MIN_TIME ? time.minTime : this.defaultConfig.minTime,
                maxTime: time.maxTime ? time.maxTime : this.defaultConfig.maxTime,
                delay: time.delay >= MIN_DELAY ? time.delay : this.defaultConfig.delay
            };
        }
        else {
            if (this.loaders[loaderId].tasks[taskId].isForeground !== isForeground) {
                throw new Error(`[ngx-ui-loader] - taskId "${taskId}" is duplicated.`);
            }
        }
        if (this.setDelayTimer(this.loaders[loaderId].tasks[taskId], loaderId)) {
            return false;
        }
        this.loaders[loaderId].tasks[taskId] = Object.assign(Object.assign({}, this.loaders[loaderId].tasks[taskId]), { isOtherRunning, startAt: Date.now() });
        this.setMaxTimer(this.loaders[loaderId].tasks[taskId], loaderId);
        if (!this.loaders[loaderId].isBound) {
            return false;
        }
        return true;
    }
    /**
     * @docs-private
     */
    readyToStop(loaderId, taskId) {
        if (!this.loaders[loaderId]) {
            console.warn(`[ngx-ui-loader] - loaderId "${loaderId}" does not exist.`);
            return false;
        }
        const task = this.loaders[loaderId].tasks[taskId];
        if (!task) {
            return false;
        }
        if (task.isDelayed) {
            this.clearTimers(task);
            delete this.loaders[loaderId].tasks[taskId];
            return false;
        }
        if (this.setMinTimer(task, loaderId)) {
            return false;
        }
        this.clearTimers(task);
        delete this.loaders[loaderId].tasks[taskId];
        return true;
    }
    /**
     * Set delay timer, if `delay` > 0
     * @docs-private
     * @returns boolean
     */
    setDelayTimer(task, loaderId) {
        if (task.delay > MIN_DELAY) {
            if (task.isDelayed) {
                return true;
            }
            if (!task.delayTimer) {
                task.isDelayed = true;
                task.delayTimer = setTimeout(() => {
                    task.isDelayed = false;
                    if (task.isForeground) {
                        this.startLoader(loaderId, task.taskId);
                    }
                    else {
                        this.startBackgroundLoader(loaderId, task.taskId);
                    }
                }, task.delay);
                return true;
            }
        }
        return false;
    }
    /**
     * Set maxTimer if `maxTime` > `minTime`
     * @docs-private
     * @returns boolean
     */
    setMaxTimer(task, loaderId) {
        if (task.maxTime > task.minTime) {
            // restart the task, reset maxTimer
            clearTimeout(task.maxTimer);
            task.maxTimer = setTimeout(() => {
                if (task.isForeground) {
                    this.stopLoader(loaderId, task.taskId);
                }
                else {
                    this.stopBackgroundLoader(loaderId, task.taskId);
                }
            }, task.maxTime);
        }
    }
    /**
     * Set minTimer if `startAt` + `minTime` > `Date.now()`
     * @docs-private
     * @returns boolean
     */
    setMinTimer(task, loaderId) {
        const now = Date.now();
        if (task.startAt) {
            if (task.startAt + task.minTime > now) {
                task.minTimer = setTimeout(() => {
                    if (task.isForeground) {
                        this.stopLoader(loaderId, task.taskId);
                    }
                    else {
                        this.stopBackgroundLoader(loaderId, task.taskId);
                    }
                }, task.startAt + task.minTime - now);
                return true;
            }
        }
        return false;
    }
}
NgxUiLoaderService.ɵfac = function NgxUiLoaderService_Factory(t) { return new (t || NgxUiLoaderService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](NGX_UI_LOADER_CONFIG_TOKEN, 8)); };
NgxUiLoaderService.ɵprov = (0,_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"])({ factory: function NgxUiLoaderService_Factory() { return new NgxUiLoaderService((0,_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"])(NGX_UI_LOADER_CONFIG_TOKEN, 8)); }, token: NgxUiLoaderService, providedIn: "root" });
NgxUiLoaderService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Optional }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [NGX_UI_LOADER_CONFIG_TOKEN,] }] }
];
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxUiLoaderService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: undefined, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Optional
            }, {
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [NGX_UI_LOADER_CONFIG_TOKEN]
            }] }]; }, null); })();

class NgxUiLoaderComponent {
    /**
     * Constructor
     */
    constructor(domSanitizer, changeDetectorRef, ngxService) {
        this.domSanitizer = domSanitizer;
        this.changeDetectorRef = changeDetectorRef;
        this.ngxService = ngxService;
        this.initialized = false;
        this.defaultConfig = this.ngxService.getDefaultConfig();
        this.bgsColor = this.defaultConfig.bgsColor;
        this.bgsOpacity = this.defaultConfig.bgsOpacity;
        this.bgsPosition = this.defaultConfig.bgsPosition;
        this.bgsSize = this.defaultConfig.bgsSize;
        this.bgsType = this.defaultConfig.bgsType;
        this.fastFadeOut = this.defaultConfig.fastFadeOut;
        this.fgsColor = this.defaultConfig.fgsColor;
        this.fgsPosition = this.defaultConfig.fgsPosition;
        this.fgsSize = this.defaultConfig.fgsSize;
        this.fgsType = this.defaultConfig.fgsType;
        this.gap = this.defaultConfig.gap;
        this.loaderId = this.defaultConfig.masterLoaderId;
        this.logoPosition = this.defaultConfig.logoPosition;
        this.logoSize = this.defaultConfig.logoSize;
        this.logoUrl = this.defaultConfig.logoUrl;
        this.overlayBorderRadius = this.defaultConfig.overlayBorderRadius;
        this.overlayColor = this.defaultConfig.overlayColor;
        this.pbColor = this.defaultConfig.pbColor;
        this.pbDirection = this.defaultConfig.pbDirection;
        this.pbThickness = this.defaultConfig.pbThickness;
        this.hasProgressBar = this.defaultConfig.hasProgressBar;
        this.text = this.defaultConfig.text;
        this.textColor = this.defaultConfig.textColor;
        this.textPosition = this.defaultConfig.textPosition;
    }
    /**
     * On init event
     */
    ngOnInit() {
        this.initializeSpinners();
        this.ngxService.bindLoaderData(this.loaderId);
        this.determinePositions();
        this.trustedLogoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.logoUrl);
        this.showForegroundWatcher = this.ngxService.showForeground$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.filter)((showEvent) => this.loaderId === showEvent.loaderId))
            .subscribe(data => {
            this.showForeground = data.isShow;
            this.changeDetectorRef.markForCheck();
        });
        this.showBackgroundWatcher = this.ngxService.showBackground$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.filter)((showEvent) => this.loaderId === showEvent.loaderId))
            .subscribe(data => {
            this.showBackground = data.isShow;
            this.changeDetectorRef.markForCheck();
        });
        this.foregroundClosingWatcher = this.ngxService.foregroundClosing$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.filter)((showEvent) => this.loaderId === showEvent.loaderId))
            .subscribe(data => {
            this.foregroundClosing = data.isShow;
            this.changeDetectorRef.markForCheck();
        });
        this.backgroundClosingWatcher = this.ngxService.backgroundClosing$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.filter)((showEvent) => this.loaderId === showEvent.loaderId))
            .subscribe(data => {
            this.backgroundClosing = data.isShow;
            this.changeDetectorRef.markForCheck();
        });
        this.initialized = true;
    }
    /**
     * On changes event
     */
    ngOnChanges(changes) {
        if (!this.initialized) {
            return;
        }
        const bgsTypeChange = changes.bgsType;
        const fgsTypeChange = changes.fgsType;
        const logoUrlChange = changes.logoUrl;
        if (fgsTypeChange || bgsTypeChange) {
            this.initializeSpinners();
        }
        this.determinePositions();
        if (logoUrlChange) {
            this.trustedLogoUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(this.logoUrl);
        }
    }
    /**
     * Initialize spinners
     */
    initializeSpinners() {
        this.fgDivs = Array(SPINNER_CONFIG[this.fgsType].divs).fill(1);
        this.fgSpinnerClass = SPINNER_CONFIG[this.fgsType].class;
        this.bgDivs = Array(SPINNER_CONFIG[this.bgsType].divs).fill(1);
        this.bgSpinnerClass = SPINNER_CONFIG[this.bgsType].class;
    }
    /**
     * Determine the positions of spinner, logo and text
     */
    determinePositions() {
        this.logoTop = 'initial';
        this.spinnerTop = 'initial';
        this.textTop = 'initial';
        const textSize = 24;
        if (this.logoPosition.startsWith('center')) {
            this.logoTop = '50%';
        }
        else if (this.logoPosition.startsWith('top')) {
            this.logoTop = '30px';
        }
        if (this.fgsPosition.startsWith('center')) {
            this.spinnerTop = '50%';
        }
        else if (this.fgsPosition.startsWith('top')) {
            this.spinnerTop = '30px';
        }
        if (this.textPosition.startsWith('center')) {
            this.textTop = '50%';
        }
        else if (this.textPosition.startsWith('top')) {
            this.textTop = '30px';
        }
        if (this.fgsPosition === POSITION.centerCenter) {
            if (this.logoUrl && this.logoPosition === POSITION.centerCenter) {
                if (this.text && this.textPosition === POSITION.centerCenter) {
                    // logo, spinner and text
                    this.logoTop = this.domSanitizer.bypassSecurityTrustStyle(`calc(50% - ${this.fgsSize / 2}px - ${textSize / 2}px - ${this.gap}px)`);
                    this.spinnerTop = this.domSanitizer.bypassSecurityTrustStyle(`calc(50% + ${this.logoSize / 2}px - ${textSize / 2}px)`);
                    this.textTop = this.domSanitizer.bypassSecurityTrustStyle(`calc(50% + ${this.logoSize / 2}px + ${this.gap}px + ${this.fgsSize / 2}px)`);
                }
                else {
                    // logo and spinner
                    this.logoTop = this.domSanitizer.bypassSecurityTrustStyle(`calc(50% - ${this.fgsSize / 2}px - ${this.gap / 2}px)`);
                    this.spinnerTop = this.domSanitizer.bypassSecurityTrustStyle(`calc(50% + ${this.logoSize / 2}px + ${this.gap / 2}px)`);
                }
            }
            else {
                if (this.text && this.textPosition === POSITION.centerCenter) {
                    // spinner and text
                    this.spinnerTop = this.domSanitizer.bypassSecurityTrustStyle(`calc(50% - ${textSize / 2}px - ${this.gap / 2}px)`);
                    this.textTop = this.domSanitizer.bypassSecurityTrustStyle(`calc(50% + ${this.fgsSize / 2}px + ${this.gap / 2}px)`);
                }
            }
        }
        else {
            if (this.logoUrl && this.logoPosition === POSITION.centerCenter && this.text && this.textPosition === POSITION.centerCenter) {
                // logo and text
                this.logoTop = this.domSanitizer.bypassSecurityTrustStyle(`calc(50% - ${textSize / 2}px - ${this.gap / 2}px)`);
                this.textTop = this.domSanitizer.bypassSecurityTrustStyle(`calc(50% + ${this.logoSize / 2}px + ${this.gap / 2}px)`);
            }
        }
    }
    /**
     * On destroy event
     */
    ngOnDestroy() {
        this.ngxService.destroyLoaderData(this.loaderId);
        if (this.showForegroundWatcher) {
            this.showForegroundWatcher.unsubscribe();
        }
        if (this.showBackgroundWatcher) {
            this.showBackgroundWatcher.unsubscribe();
        }
        if (this.foregroundClosingWatcher) {
            this.foregroundClosingWatcher.unsubscribe();
        }
        if (this.backgroundClosingWatcher) {
            this.backgroundClosingWatcher.unsubscribe();
        }
    }
}
NgxUiLoaderComponent.ɵfac = function NgxUiLoaderComponent_Factory(t) { return new (t || NgxUiLoaderComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.ChangeDetectorRef), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](NgxUiLoaderService)); };
NgxUiLoaderComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NgxUiLoaderComponent, selectors: [["ngx-ui-loader"]], inputs: { bgsColor: "bgsColor", bgsOpacity: "bgsOpacity", bgsPosition: "bgsPosition", bgsSize: "bgsSize", bgsType: "bgsType", fgsColor: "fgsColor", fgsPosition: "fgsPosition", fgsSize: "fgsSize", fgsType: "fgsType", gap: "gap", loaderId: "loaderId", logoPosition: "logoPosition", logoSize: "logoSize", logoUrl: "logoUrl", overlayBorderRadius: "overlayBorderRadius", overlayColor: "overlayColor", pbColor: "pbColor", pbDirection: "pbDirection", pbThickness: "pbThickness", hasProgressBar: "hasProgressBar", text: "text", textColor: "textColor", textPosition: "textPosition", bgsTemplate: "bgsTemplate", fgsTemplate: "fgsTemplate" }, features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]], decls: 13, vars: 50, consts: [["class", "ngx-progress-bar", 3, "ngx-position-absolute", "ngClass", "height", "color", "loading-foreground", "foreground-closing", "fast-closing", 4, "ngIf"], [1, "ngx-overlay"], ["class", "ngx-loading-logo", 3, "ngClass", "src", "width", "height", "top", 4, "ngIf"], [1, "ngx-foreground-spinner", 3, "ngClass"], [3, "class", 4, "ngIf", "ngIfElse"], ["foregroundTemplate", ""], [1, "ngx-loading-text", 3, "ngClass"], [1, "ngx-background-spinner", 3, "ngClass"], ["backgroundTemplate", ""], [1, "ngx-progress-bar", 3, "ngClass"], [1, "ngx-loading-logo", 3, "ngClass", "src"], [4, "ngFor", "ngForOf"], [4, "ngTemplateOutlet"]], template: function NgxUiLoaderComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, NgxUiLoaderComponent_div_0_Template, 1, 13, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, NgxUiLoaderComponent_img_2_Template, 1, 8, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, NgxUiLoaderComponent_div_4_Template, 2, 3, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, NgxUiLoaderComponent_ng_template_5_Template, 1, 1, "ng-template", null, 5, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, NgxUiLoaderComponent_div_10_Template, 2, 3, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, NgxUiLoaderComponent_ng_template_11_Template, 1, 1, "ng-template", null, 8, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](6);
        const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.hasProgressBar);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("background-color", ctx.overlayColor)("border-radius", ctx.overlayBorderRadius);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("ngx-position-absolute", ctx.loaderId !== ctx.defaultConfig.masterLoaderId)("loading-foreground", ctx.showForeground)("foreground-closing", ctx.foregroundClosing)("fast-closing", ctx.foregroundClosing && ctx.fastFadeOut);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.logoUrl);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("color", ctx.fgsColor)("width", ctx.fgsSize, "px")("height", ctx.fgsSize, "px")("top", ctx.spinnerTop);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx.fgsPosition);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.fgsTemplate)("ngIfElse", _r3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("top", ctx.textTop)("color", ctx.textColor);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx.textPosition);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.text);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("width", ctx.bgsSize, "px")("height", ctx.bgsSize, "px")("color", ctx.bgsColor)("opacity", ctx.bgsOpacity);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("ngx-position-absolute", ctx.loaderId !== ctx.defaultConfig.masterLoaderId)("loading-background", ctx.showBackground)("background-closing", ctx.backgroundClosing)("fast-closing", ctx.backgroundClosing && ctx.fastFadeOut);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx.bgsPosition);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.bgsTemplate)("ngIfElse", _r6);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.NgIf, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgClass, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgForOf, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgTemplateOutlet], styles: [".ngx-progress-bar[_ngcontent-%COMP%]{color:#00acc1;display:none;height:3px;left:0;overflow:hidden;position:fixed;top:0;width:100%;z-index:99999!important}.ngx-progress-bar.foreground-closing[_ngcontent-%COMP%], .ngx-progress-bar.loading-foreground[_ngcontent-%COMP%]{display:block}.ngx-progress-bar.foreground-closing[_ngcontent-%COMP%]{opacity:0!important;transition:opacity .5s ease-out .5s}.ngx-progress-bar.fast-closing[_ngcontent-%COMP%]{transition:opacity .3s ease-out .3s!important}.ngx-progress-bar[_ngcontent-%COMP%]:after, .ngx-progress-bar[_ngcontent-%COMP%]:before{background-color:currentColor;content:\"\";display:block;height:100%;position:absolute;top:0;width:100%}.ngx-progress-bar-ltr[_ngcontent-%COMP%]:before{transform:translate3d(-100%,0,0)}.ngx-progress-bar-ltr[_ngcontent-%COMP%]:after{-webkit-animation:progressBar-slide-ltr 12s ease-out 0s 1 normal;animation:progressBar-slide-ltr 12s ease-out 0s 1 normal;transform:translate3d(-5%,0,0)}.ngx-progress-bar-rtl[_ngcontent-%COMP%]:before{transform:translate3d(100%,0,0)}.ngx-progress-bar-rtl[_ngcontent-%COMP%]:after{-webkit-animation:progressBar-slide-rtl 12s ease-out 0s 1 normal;animation:progressBar-slide-rtl 12s ease-out 0s 1 normal;transform:translate3d(5%,0,0)}.foreground-closing.ngx-progress-bar-ltr[_ngcontent-%COMP%]:before{-webkit-animation:progressBar-slide-complete-ltr 1s ease-out 0s 1;animation:progressBar-slide-complete-ltr 1s ease-out 0s 1;transform:translateZ(0)}.fast-closing.ngx-progress-bar-ltr[_ngcontent-%COMP%]:before{-webkit-animation:progressBar-slide-complete-ltr .6s ease-out 0s 1!important;animation:progressBar-slide-complete-ltr .6s ease-out 0s 1!important}.foreground-closing.ngx-progress-bar-rtl[_ngcontent-%COMP%]:before{-webkit-animation:progressBar-slide-complete-rtl 1s ease-out 0s 1;animation:progressBar-slide-complete-rtl 1s ease-out 0s 1;transform:translateZ(0)}.fast-closing.ngx-progress-bar-rtl[_ngcontent-%COMP%]:before{-webkit-animation:progressBar-slide-complete-rtl .6s ease-out 0s 1!important;animation:progressBar-slide-complete-rtl .6s ease-out 0s 1!important}@-webkit-keyframes progressBar-slide-ltr{0%{transform:translate3d(-100%,0,0)}to{transform:translate3d(-5%,0,0)}}@keyframes progressBar-slide-ltr{0%{transform:translate3d(-100%,0,0)}to{transform:translate3d(-5%,0,0)}}@-webkit-keyframes progressBar-slide-rtl{0%{transform:translate3d(100%,0,0)}to{transform:translate3d(5%,0,0)}}@keyframes progressBar-slide-rtl{0%{transform:translate3d(100%,0,0)}to{transform:translate3d(5%,0,0)}}@-webkit-keyframes progressBar-slide-complete-ltr{0%{transform:translate3d(-75%,0,0)}50%{transform:translateZ(0)}}@keyframes progressBar-slide-complete-ltr{0%{transform:translate3d(-75%,0,0)}50%{transform:translateZ(0)}}@-webkit-keyframes progressBar-slide-complete-rtl{0%{transform:translate3d(75%,0,0)}50%{transform:translateZ(0)}}@keyframes progressBar-slide-complete-rtl{0%{transform:translate3d(75%,0,0)}50%{transform:translateZ(0)}}.ngx-overlay[_ngcontent-%COMP%]{background-color:rgba(40,40,40,.8);cursor:progress;display:none;height:100%;left:0;position:fixed;top:0;width:100%;z-index:99998!important}.ngx-overlay.foreground-closing[_ngcontent-%COMP%], .ngx-overlay.loading-foreground[_ngcontent-%COMP%]{display:block}.ngx-overlay.foreground-closing[_ngcontent-%COMP%]{opacity:0!important;transition:opacity .5s ease-out .5s}.ngx-overlay.fast-closing[_ngcontent-%COMP%]{transition:opacity .3s ease-out .3s!important}.ngx-overlay[_ngcontent-%COMP%] > .ngx-foreground-spinner[_ngcontent-%COMP%]{color:#00acc1;height:60px;margin:0;position:fixed;width:60px}.ngx-overlay[_ngcontent-%COMP%] > .ngx-loading-logo[_ngcontent-%COMP%]{height:120px;margin:0;position:fixed;width:120px}.ngx-overlay[_ngcontent-%COMP%] > .ngx-loading-text[_ngcontent-%COMP%]{color:#fff;font-family:sans-serif;font-size:1.2em;font-weight:400;margin:0;position:fixed}.ngx-background-spinner[_ngcontent-%COMP%]{color:#00acc1;display:none;height:60px;margin:0;opacity:.6;position:fixed;width:60px;z-index:99997!important}.ngx-background-spinner.background-closing[_ngcontent-%COMP%], .ngx-background-spinner.loading-background[_ngcontent-%COMP%]{display:block}.ngx-background-spinner.background-closing[_ngcontent-%COMP%]{opacity:0!important;transition:opacity .7s ease-out}.ngx-background-spinner.fast-closing[_ngcontent-%COMP%]{transition:opacity .4s ease-out!important}.ngx-position-absolute[_ngcontent-%COMP%], .ngx-position-absolute[_ngcontent-%COMP%] > .ngx-foreground-spinner[_ngcontent-%COMP%], .ngx-position-absolute[_ngcontent-%COMP%] > .ngx-loading-logo[_ngcontent-%COMP%], .ngx-position-absolute[_ngcontent-%COMP%] > .ngx-loading-text[_ngcontent-%COMP%]{position:absolute!important}.ngx-position-absolute.ngx-progress-bar[_ngcontent-%COMP%]{z-index:99996!important}.ngx-position-absolute.ngx-overlay[_ngcontent-%COMP%]{z-index:99995!important}.ngx-position-absolute.ngx-background-spinner[_ngcontent-%COMP%], .ngx-position-absolute[_ngcontent-%COMP%]   .sk-square-jelly-box[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:first-child{z-index:99994!important}.top-left[_ngcontent-%COMP%]{left:30px;top:30px}.top-center[_ngcontent-%COMP%]{left:50%;top:30px;transform:translateX(-50%)}.top-right[_ngcontent-%COMP%]{right:30px;top:30px}.center-left[_ngcontent-%COMP%]{left:30px;top:50%;transform:translateY(-50%)}.center-center[_ngcontent-%COMP%]{left:50%;top:50%;transform:translate(-50%,-50%)}.center-right[_ngcontent-%COMP%]{right:30px;top:50%;transform:translateY(-50%)}.bottom-left[_ngcontent-%COMP%]{bottom:30px;left:30px}.bottom-center[_ngcontent-%COMP%]{bottom:30px;left:50%;transform:translateX(-50%)}.bottom-right[_ngcontent-%COMP%]{bottom:30px;right:30px}.sk-ball-scale-multiple[_ngcontent-%COMP%], .sk-ball-scale-multiple[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{box-sizing:border-box;position:relative}.sk-ball-scale-multiple[_ngcontent-%COMP%]{font-size:0;height:100%;width:100%}.sk-ball-scale-multiple[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:ball-scale-multiple 1s linear 0s infinite;animation:ball-scale-multiple 1s linear 0s infinite;background-color:currentColor;border:0 solid;border-radius:100%;display:inline-block;float:none;height:100%;left:0;opacity:0;position:absolute;top:0;width:100%}.sk-ball-scale-multiple[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation-delay:.2s;animation-delay:.2s}.sk-ball-scale-multiple[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){-webkit-animation-delay:.4s;animation-delay:.4s}@-webkit-keyframes ball-scale-multiple{0%{opacity:0;transform:scale(0)}5%{opacity:.75}to{opacity:0;transform:scale(1)}}@keyframes ball-scale-multiple{0%{opacity:0;transform:scale(0)}5%{opacity:.75}to{opacity:0;transform:scale(1)}}.sk-ball-spin[_ngcontent-%COMP%], .sk-ball-spin[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{box-sizing:border-box;position:relative}.sk-ball-spin[_ngcontent-%COMP%]{font-size:0;height:100%;width:100%}.sk-ball-spin[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:ball-spin-clockwise 1s ease-in-out infinite;animation:ball-spin-clockwise 1s ease-in-out infinite;background-color:currentColor;border:0 solid;border-radius:100%;display:inline-block;float:none;height:25%;left:50%;margin-left:-12.5%;margin-top:-12.5%;position:absolute;top:50%;width:25%}.sk-ball-spin[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:first-child{-webkit-animation-delay:-1.125s;animation-delay:-1.125s;left:50%;top:5%}.sk-ball-spin[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation-delay:-1.25s;animation-delay:-1.25s;left:81.8198051534%;top:18.1801948466%}.sk-ball-spin[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){-webkit-animation-delay:-1.375s;animation-delay:-1.375s;left:95%;top:50%}.sk-ball-spin[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4){-webkit-animation-delay:-1.5s;animation-delay:-1.5s;left:81.8198051534%;top:81.8198051534%}.sk-ball-spin[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(5){-webkit-animation-delay:-1.625s;animation-delay:-1.625s;left:50.0000000005%;top:94.9999999966%}.sk-ball-spin[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(6){-webkit-animation-delay:-1.75s;animation-delay:-1.75s;left:18.1801949248%;top:81.8198046966%}.sk-ball-spin[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(7){-webkit-animation-delay:-1.875s;animation-delay:-1.875s;left:5.0000051215%;top:49.9999750815%}.sk-ball-spin[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(8){-webkit-animation-delay:-2s;animation-delay:-2s;left:18.1803700518%;top:18.179464974%}@-webkit-keyframes ball-spin{0%,to{opacity:1;transform:scale(1)}20%{opacity:1}80%{opacity:0;transform:scale(0)}}@keyframes ball-spin{0%,to{opacity:1;transform:scale(1)}20%{opacity:1}80%{opacity:0;transform:scale(0)}}.sk-ball-spin-clockwise[_ngcontent-%COMP%], .sk-ball-spin-clockwise[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{box-sizing:border-box;position:relative}.sk-ball-spin-clockwise[_ngcontent-%COMP%]{font-size:0;height:100%;width:100%}.sk-ball-spin-clockwise[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:ball-spin-clockwise 1s ease-in-out infinite;animation:ball-spin-clockwise 1s ease-in-out infinite;background-color:currentColor;border:0 solid;border-radius:100%;display:inline-block;float:none;height:25%;left:50%;margin-left:-12.5%;margin-top:-12.5%;position:absolute;top:50%;width:25%}.sk-ball-spin-clockwise[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:first-child{-webkit-animation-delay:-.875s;animation-delay:-.875s;left:50%;top:5%}.sk-ball-spin-clockwise[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation-delay:-.75s;animation-delay:-.75s;left:81.8198051534%;top:18.1801948466%}.sk-ball-spin-clockwise[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){-webkit-animation-delay:-.625s;animation-delay:-.625s;left:95%;top:50%}.sk-ball-spin-clockwise[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4){-webkit-animation-delay:-.5s;animation-delay:-.5s;left:81.8198051534%;top:81.8198051534%}.sk-ball-spin-clockwise[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(5){-webkit-animation-delay:-.375s;animation-delay:-.375s;left:50.0000000005%;top:94.9999999966%}.sk-ball-spin-clockwise[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(6){-webkit-animation-delay:-.25s;animation-delay:-.25s;left:18.1801949248%;top:81.8198046966%}.sk-ball-spin-clockwise[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(7){-webkit-animation-delay:-.125s;animation-delay:-.125s;left:5.0000051215%;top:49.9999750815%}.sk-ball-spin-clockwise[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(8){-webkit-animation-delay:0s;animation-delay:0s;left:18.1803700518%;top:18.179464974%}@-webkit-keyframes ball-spin-clockwise{0%,to{opacity:1;transform:scale(1)}20%{opacity:1}80%{opacity:0;transform:scale(0)}}@keyframes ball-spin-clockwise{0%,to{opacity:1;transform:scale(1)}20%{opacity:1}80%{opacity:0;transform:scale(0)}}.sk-ball-spin-clockwise-fade-rotating[_ngcontent-%COMP%], .sk-ball-spin-clockwise-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{box-sizing:border-box;position:relative}.sk-ball-spin-clockwise-fade-rotating[_ngcontent-%COMP%]{-webkit-animation:ball-spin-clockwise-fade-rotating-rotate 6s linear infinite;animation:ball-spin-clockwise-fade-rotating-rotate 6s linear infinite;font-size:0;height:100%;width:100%}.sk-ball-spin-clockwise-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:ball-spin-clockwise-fade-rotating 1s linear infinite;animation:ball-spin-clockwise-fade-rotating 1s linear infinite;background-color:currentColor;border:0 solid;border-radius:100%;display:inline-block;float:none;height:25%;left:50%;margin-left:-12.5%;margin-top:-12.5%;position:absolute;top:50%;width:25%}.sk-ball-spin-clockwise-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:first-child{-webkit-animation-delay:-.875s;animation-delay:-.875s;left:50%;top:5%}.sk-ball-spin-clockwise-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation-delay:-.75s;animation-delay:-.75s;left:81.8198051534%;top:18.1801948466%}.sk-ball-spin-clockwise-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){-webkit-animation-delay:-.625s;animation-delay:-.625s;left:95%;top:50%}.sk-ball-spin-clockwise-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4){-webkit-animation-delay:-.5s;animation-delay:-.5s;left:81.8198051534%;top:81.8198051534%}.sk-ball-spin-clockwise-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(5){-webkit-animation-delay:-.375s;animation-delay:-.375s;left:50.0000000005%;top:94.9999999966%}.sk-ball-spin-clockwise-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(6){-webkit-animation-delay:-.25s;animation-delay:-.25s;left:18.1801949248%;top:81.8198046966%}.sk-ball-spin-clockwise-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(7){-webkit-animation-delay:-.125s;animation-delay:-.125s;left:5.0000051215%;top:49.9999750815%}.sk-ball-spin-clockwise-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(8){-webkit-animation-delay:0s;animation-delay:0s;left:18.1803700518%;top:18.179464974%}@-webkit-keyframes ball-spin-clockwise-fade-rotating-rotate{to{transform:rotate(-1turn)}}@keyframes ball-spin-clockwise-fade-rotating-rotate{to{transform:rotate(-1turn)}}@-webkit-keyframes ball-spin-clockwise-fade-rotating{50%{opacity:.25;transform:scale(.5)}to{opacity:1;transform:scale(1)}}@keyframes ball-spin-clockwise-fade-rotating{50%{opacity:.25;transform:scale(.5)}to{opacity:1;transform:scale(1)}}.sk-ball-spin-fade-rotating[_ngcontent-%COMP%], .sk-ball-spin-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{box-sizing:border-box;position:relative}.sk-ball-spin-fade-rotating[_ngcontent-%COMP%]{-webkit-animation:ball-spin-fade-rotate 6s linear infinite;animation:ball-spin-fade-rotate 6s linear infinite;font-size:0;height:100%;width:100%}.sk-ball-spin-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:ball-spin-fade 1s linear infinite;animation:ball-spin-fade 1s linear infinite;background-color:currentColor;border:0 solid;border-radius:100%;display:inline-block;float:none;height:25%;left:50%;margin-left:-12.5%;margin-top:-12.5%;position:absolute;top:50%;width:25%}.sk-ball-spin-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:first-child{-webkit-animation-delay:-1.125s;animation-delay:-1.125s;left:50%;top:5%}.sk-ball-spin-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation-delay:-1.25s;animation-delay:-1.25s;left:81.8198051534%;top:18.1801948466%}.sk-ball-spin-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){-webkit-animation-delay:-1.375s;animation-delay:-1.375s;left:95%;top:50%}.sk-ball-spin-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4){-webkit-animation-delay:-1.5s;animation-delay:-1.5s;left:81.8198051534%;top:81.8198051534%}.sk-ball-spin-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(5){-webkit-animation-delay:-1.625s;animation-delay:-1.625s;left:50.0000000005%;top:94.9999999966%}.sk-ball-spin-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(6){-webkit-animation-delay:-1.75s;animation-delay:-1.75s;left:18.1801949248%;top:81.8198046966%}.sk-ball-spin-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(7){-webkit-animation-delay:-1.875s;animation-delay:-1.875s;left:5.0000051215%;top:49.9999750815%}.sk-ball-spin-fade-rotating[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(8){-webkit-animation-delay:-2s;animation-delay:-2s;left:18.1803700518%;top:18.179464974%}@-webkit-keyframes ball-spin-fade-rotate{to{transform:rotate(1turn)}}@keyframes ball-spin-fade-rotate{to{transform:rotate(1turn)}}@-webkit-keyframes ball-spin-fade{0%,to{opacity:1;transform:scale(1)}50%{opacity:.25;transform:scale(.5)}}@keyframes ball-spin-fade{0%,to{opacity:1;transform:scale(1)}50%{opacity:.25;transform:scale(.5)}}.sk-chasing-dots[_ngcontent-%COMP%]{-webkit-animation:sk-chasingDots-rotate 2s linear infinite;animation:sk-chasingDots-rotate 2s linear infinite;height:100%;margin:auto;position:absolute;text-align:center;width:100%}.sk-chasing-dots[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:sk-chasingDots-bounce 2s ease-in-out infinite;animation:sk-chasingDots-bounce 2s ease-in-out infinite;background-color:currentColor;border-radius:100%;display:inline-block;height:60%;position:absolute;top:0;width:60%}.sk-chasing-dots[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation-delay:-1s;animation-delay:-1s;bottom:0;top:auto}@-webkit-keyframes sk-chasingDots-rotate{to{transform:rotate(1turn)}}@keyframes sk-chasingDots-rotate{to{transform:rotate(1turn)}}@-webkit-keyframes sk-chasingDots-bounce{0%,to{transform:scale(0)}50%{transform:scale(1)}}@keyframes sk-chasingDots-bounce{0%,to{transform:scale(0)}50%{transform:scale(1)}}.sk-circle[_ngcontent-%COMP%]{height:100%;margin:auto;position:relative;width:100%}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{height:100%;left:0;position:absolute;top:0;width:100%}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:before{-webkit-animation:sk-circle-bounceDelay 1.2s ease-in-out infinite both;animation:sk-circle-bounceDelay 1.2s ease-in-out infinite both;background-color:currentColor;border-radius:100%;content:\"\";display:block;height:15%;margin:0 auto;width:15%}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){transform:rotate(30deg)}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){transform:rotate(60deg)}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4){transform:rotate(90deg)}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(5){transform:rotate(120deg)}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(6){transform:rotate(150deg)}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(7){transform:rotate(180deg)}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(8){transform:rotate(210deg)}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(9){transform:rotate(240deg)}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(10){transform:rotate(270deg)}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(11){transform:rotate(300deg)}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(12){transform:rotate(330deg)}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2):before{-webkit-animation-delay:-1.1s;animation-delay:-1.1s}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3):before{-webkit-animation-delay:-1s;animation-delay:-1s}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4):before{-webkit-animation-delay:-.9s;animation-delay:-.9s}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(5):before{-webkit-animation-delay:-.8s;animation-delay:-.8s}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(6):before{-webkit-animation-delay:-.7s;animation-delay:-.7s}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(7):before{-webkit-animation-delay:-.6s;animation-delay:-.6s}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(8):before{-webkit-animation-delay:-.5s;animation-delay:-.5s}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(9):before{-webkit-animation-delay:-.4s;animation-delay:-.4s}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(10):before{-webkit-animation-delay:-.3s;animation-delay:-.3s}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(11):before{-webkit-animation-delay:-.2s;animation-delay:-.2s}.sk-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(12):before{-webkit-animation-delay:-.1s;animation-delay:-.1s}@-webkit-keyframes sk-circle-bounceDelay{0%,80%,to{transform:scale(0)}40%{transform:scale(1)}}@keyframes sk-circle-bounceDelay{0%,80%,to{transform:scale(0)}40%{transform:scale(1)}}.sk-cube-grid[_ngcontent-%COMP%]{height:100%;margin:auto;width:100%}.sk-cube-grid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:sk-cubeGrid-scaleDelay 1.3s ease-in-out infinite;animation:sk-cubeGrid-scaleDelay 1.3s ease-in-out infinite;background-color:currentColor;float:left;height:33%;width:33%}.sk-cube-grid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:first-child{-webkit-animation-delay:.2s;animation-delay:.2s}.sk-cube-grid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation-delay:.3s;animation-delay:.3s}.sk-cube-grid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){-webkit-animation-delay:.4s;animation-delay:.4s}.sk-cube-grid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4){-webkit-animation-delay:.1s;animation-delay:.1s}.sk-cube-grid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(5){-webkit-animation-delay:.2s;animation-delay:.2s}.sk-cube-grid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(6){-webkit-animation-delay:.3s;animation-delay:.3s}.sk-cube-grid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(7){-webkit-animation-delay:0s;animation-delay:0s}.sk-cube-grid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(8){-webkit-animation-delay:.1s;animation-delay:.1s}.sk-cube-grid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(9){-webkit-animation-delay:.2s;animation-delay:.2s}@-webkit-keyframes sk-cubeGrid-scaleDelay{0%,70%,to{transform:scaleX(1)}35%{transform:scale3D(0,0,1)}}@keyframes sk-cubeGrid-scaleDelay{0%,70%,to{transform:scaleX(1)}35%{transform:scale3D(0,0,1)}}.sk-double-bounce[_ngcontent-%COMP%]{height:100%;margin:auto;position:relative;width:100%}.sk-double-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:sk-doubleBounce-bounce 2s ease-in-out infinite;animation:sk-doubleBounce-bounce 2s ease-in-out infinite;background-color:currentColor;border-radius:50%;height:100%;left:0;opacity:.6;position:absolute;top:0;width:100%}.sk-double-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation-delay:-1s;animation-delay:-1s}@-webkit-keyframes sk-doubleBounce-bounce{0%,to{transform:scale(0)}50%{transform:scale(1)}}@keyframes sk-doubleBounce-bounce{0%,to{transform:scale(0)}50%{transform:scale(1)}}.sk-fading-circle[_ngcontent-%COMP%]{height:100%;margin:auto;position:relative;width:100%}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{height:100%;left:0;position:absolute;top:0;width:100%}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:before{-webkit-animation:sk-fadingCircle-FadeDelay 1.2s ease-in-out infinite both;animation:sk-fadingCircle-FadeDelay 1.2s ease-in-out infinite both;background-color:currentColor;border-radius:100%;content:\"\";display:block;height:15%;margin:0 auto;width:15%}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){transform:rotate(30deg)}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){transform:rotate(60deg)}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4){transform:rotate(90deg)}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(5){transform:rotate(120deg)}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(6){transform:rotate(150deg)}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(7){transform:rotate(180deg)}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(8){transform:rotate(210deg)}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(9){transform:rotate(240deg)}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(10){transform:rotate(270deg)}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(11){transform:rotate(300deg)}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(12){transform:rotate(330deg)}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2):before{-webkit-animation-delay:-1.1s;animation-delay:-1.1s}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3):before{-webkit-animation-delay:-1s;animation-delay:-1s}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4):before{-webkit-animation-delay:-.9s;animation-delay:-.9s}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(5):before{-webkit-animation-delay:-.8s;animation-delay:-.8s}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(6):before{-webkit-animation-delay:-.7s;animation-delay:-.7s}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(7):before{-webkit-animation-delay:-.6s;animation-delay:-.6s}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(8):before{-webkit-animation-delay:-.5s;animation-delay:-.5s}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(9):before{-webkit-animation-delay:-.4s;animation-delay:-.4s}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(10):before{-webkit-animation-delay:-.3s;animation-delay:-.3s}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(11):before{-webkit-animation-delay:-.2s;animation-delay:-.2s}.sk-fading-circle[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(12):before{-webkit-animation-delay:-.1s;animation-delay:-.1s}@-webkit-keyframes sk-fadingCircle-FadeDelay{0%,39%,to{opacity:0}40%{opacity:1}}@keyframes sk-fadingCircle-FadeDelay{0%,39%,to{opacity:0}40%{opacity:1}}.sk-folding-cube[_ngcontent-%COMP%]{height:100%;margin:auto;position:relative;transform:rotate(45deg);width:100%}.sk-folding-cube[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{float:left;height:50%;position:relative;transform:scale(1.1);width:50%}.sk-folding-cube[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:before{-webkit-animation:sk-foldingCube-angle 2.4s linear infinite both;animation:sk-foldingCube-angle 2.4s linear infinite both;background-color:currentColor;content:\"\";height:100%;left:0;position:absolute;top:0;transform-origin:100% 100%;width:100%}.sk-folding-cube[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){transform:scale(1.1) rotate(90deg)}.sk-folding-cube[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){transform:scale(1.1) rotate(270deg)}.sk-folding-cube[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4){transform:scale(1.1) rotate(180deg)}.sk-folding-cube[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2):before{-webkit-animation-delay:.3s;animation-delay:.3s}.sk-folding-cube[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3):before{-webkit-animation-delay:.9s;animation-delay:.9s}.sk-folding-cube[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4):before{-webkit-animation-delay:.6s;animation-delay:.6s}@-webkit-keyframes sk-foldingCube-angle{0%,10%{opacity:0;transform:perspective(140px) rotateX(-180deg)}25%,75%{opacity:1;transform:perspective(140px) rotateX(0deg)}90%,to{opacity:0;transform:perspective(140px) rotateY(180deg)}}@keyframes sk-foldingCube-angle{0%,10%{opacity:0;transform:perspective(140px) rotateX(-180deg)}25%,75%{opacity:1;transform:perspective(140px) rotateX(0deg)}90%,to{opacity:0;transform:perspective(140px) rotateY(180deg)}}.sk-pulse[_ngcontent-%COMP%]{height:100%;margin:auto;width:100%}.sk-pulse[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:sk-pulse-scaleOut 1s ease-in-out infinite;animation:sk-pulse-scaleOut 1s ease-in-out infinite;background-color:currentColor;border-radius:100%;height:100%;width:100%}@-webkit-keyframes sk-pulse-scaleOut{0%{transform:scale(0)}to{opacity:0;transform:scale(1)}}@keyframes sk-pulse-scaleOut{0%{transform:scale(0)}to{opacity:0;transform:scale(1)}}.sk-rectangle-bounce[_ngcontent-%COMP%]{font-size:0;height:100%;margin:auto;text-align:center;width:100%}.sk-rectangle-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:sk-rectangleBounce-stretchDelay 1.2s ease-in-out infinite;animation:sk-rectangleBounce-stretchDelay 1.2s ease-in-out infinite;background-color:currentColor;display:inline-block;height:100%;margin:0 5%;width:10%}.sk-rectangle-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation-delay:-1.1s;animation-delay:-1.1s}.sk-rectangle-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){-webkit-animation-delay:-1s;animation-delay:-1s}.sk-rectangle-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4){-webkit-animation-delay:-.9s;animation-delay:-.9s}.sk-rectangle-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(5){-webkit-animation-delay:-.8s;animation-delay:-.8s}@-webkit-keyframes sk-rectangleBounce-stretchDelay{0%,40%,to{transform:scaleY(.4)}20%{transform:scaleY(1)}}@keyframes sk-rectangleBounce-stretchDelay{0%,40%,to{transform:scaleY(.4)}20%{transform:scaleY(1)}}.sk-rectangle-bounce-party[_ngcontent-%COMP%], .sk-rectangle-bounce-party[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{box-sizing:border-box;position:relative}.sk-rectangle-bounce-party[_ngcontent-%COMP%]{font-size:0;height:100%;margin:auto;text-align:center;width:100%}.sk-rectangle-bounce-party[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation-iteration-count:infinite;-webkit-animation-name:rectangle-bounce-party;animation-iteration-count:infinite;animation-name:rectangle-bounce-party;background-color:currentColor;border:0 solid;border-radius:0;display:inline-block;float:none;height:100%;margin:0 5%;width:10%}.sk-rectangle-bounce-party[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:first-child{-webkit-animation-delay:-.23s;-webkit-animation-duration:.43s;animation-delay:-.23s;animation-duration:.43s}.sk-rectangle-bounce-party[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation-delay:-.32s;-webkit-animation-duration:.62s;animation-delay:-.32s;animation-duration:.62s}.sk-rectangle-bounce-party[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){-webkit-animation-delay:-.44s;-webkit-animation-duration:.43s;animation-delay:-.44s;animation-duration:.43s}.sk-rectangle-bounce-party[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4){-webkit-animation-delay:-.31s;-webkit-animation-duration:.8s;animation-delay:-.31s;animation-duration:.8s}.sk-rectangle-bounce-party[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(5){-webkit-animation-delay:-.24s;-webkit-animation-duration:.74s;animation-delay:-.24s;animation-duration:.74s}@-webkit-keyframes rectangle-bounce-party{0%{transform:scaleY(1)}50%{transform:scaleY(.4)}to{transform:scaleY(1)}}@keyframes rectangle-bounce-party{0%{transform:scaleY(1)}50%{transform:scaleY(.4)}to{transform:scaleY(1)}}.sk-rectangle-bounce-pulse-out[_ngcontent-%COMP%], .sk-rectangle-bounce-pulse-out[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{box-sizing:border-box;position:relative}.sk-rectangle-bounce-pulse-out[_ngcontent-%COMP%]{font-size:0;height:100%;margin:auto;text-align:center;width:100%}.sk-rectangle-bounce-pulse-out[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:rectangle-bounce-pulse-out .9s cubic-bezier(.85,.25,.37,.85) infinite;animation:rectangle-bounce-pulse-out .9s cubic-bezier(.85,.25,.37,.85) infinite;background-color:currentColor;border:0 solid;border-radius:0;display:inline-block;float:none;height:100%;margin:0 5%;width:10%}.sk-rectangle-bounce-pulse-out[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){-webkit-animation-delay:-.9s;animation-delay:-.9s}.sk-rectangle-bounce-pulse-out[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2), .sk-rectangle-bounce-pulse-out[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4){-webkit-animation-delay:-.7s;animation-delay:-.7s}.sk-rectangle-bounce-pulse-out[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:first-child, .sk-rectangle-bounce-pulse-out[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(5){-webkit-animation-delay:-.5s;animation-delay:-.5s}@-webkit-keyframes rectangle-bounce-pulse-out{0%{transform:scaley(1)}50%{transform:scaley(.4)}to{transform:scaley(1)}}@keyframes rectangle-bounce-pulse-out{0%{transform:scaley(1)}50%{transform:scaley(.4)}to{transform:scaley(1)}}.sk-rectangle-bounce-pulse-out-rapid[_ngcontent-%COMP%], .sk-rectangle-bounce-pulse-out-rapid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{box-sizing:border-box;position:relative}.sk-rectangle-bounce-pulse-out-rapid[_ngcontent-%COMP%]{font-size:0;height:100%;margin:auto;text-align:center;width:100%}.sk-rectangle-bounce-pulse-out-rapid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:rectangle-bounce-pulse-out-rapid .9s cubic-bezier(.11,.49,.38,.78) infinite;animation:rectangle-bounce-pulse-out-rapid .9s cubic-bezier(.11,.49,.38,.78) infinite;background-color:currentColor;border:0 solid;border-radius:0;display:inline-block;float:none;height:100%;margin:0 5%;width:10%}.sk-rectangle-bounce-pulse-out-rapid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){-webkit-animation-delay:-.9s;animation-delay:-.9s}.sk-rectangle-bounce-pulse-out-rapid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2), .sk-rectangle-bounce-pulse-out-rapid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(4){-webkit-animation-delay:-.65s;animation-delay:-.65s}.sk-rectangle-bounce-pulse-out-rapid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:first-child, .sk-rectangle-bounce-pulse-out-rapid[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(5){-webkit-animation-delay:-.4s;animation-delay:-.4s}@-webkit-keyframes rectangle-bounce-pulse-out-rapid{0%{transform:scaley(1)}80%{transform:scaley(.4)}90%{transform:scaley(1)}}@keyframes rectangle-bounce-pulse-out-rapid{0%{transform:scaley(1)}80%{transform:scaley(.4)}90%{transform:scaley(1)}}.sk-rotating-plane[_ngcontent-%COMP%]{height:100%;margin:auto;text-align:center;width:100%}.sk-rotating-plane[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:sk-rotatePlane 1.2s ease-in-out infinite;animation:sk-rotatePlane 1.2s ease-in-out infinite;background-color:currentColor;height:100%;width:100%}@-webkit-keyframes sk-rotatePlane{0%{transform:perspective(120px) rotateX(0deg) rotateY(0deg)}50%{transform:perspective(120px) rotateX(-180.1deg) rotateY(0deg)}to{transform:perspective(120px) rotateX(-180deg) rotateY(-179.9deg)}}@keyframes sk-rotatePlane{0%{transform:perspective(120px) rotateX(0deg) rotateY(0deg)}50%{transform:perspective(120px) rotateX(-180.1deg) rotateY(0deg)}to{transform:perspective(120px) rotateX(-180deg) rotateY(-179.9deg)}}.sk-square-jelly-box[_ngcontent-%COMP%], .sk-square-jelly-box[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{box-sizing:border-box;position:relative}.sk-square-jelly-box[_ngcontent-%COMP%]{font-size:0;height:100%;width:100%}.sk-square-jelly-box[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{background-color:currentColor;border:0 solid;display:inline-block;float:none}.sk-square-jelly-box[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:first-child, .sk-square-jelly-box[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){left:0;position:absolute;width:100%}.sk-square-jelly-box[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:first-child{-webkit-animation:square-jelly-box-animate .6s linear -.1s infinite;animation:square-jelly-box-animate .6s linear -.1s infinite;border-radius:10%;height:100%;top:-25%;z-index:99997}.sk-square-jelly-box[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation:square-jelly-box-shadow .6s linear -.1s infinite;animation:square-jelly-box-shadow .6s linear -.1s infinite;background:#000;border-radius:50%;bottom:-9%;height:10%;opacity:.2}@-webkit-keyframes square-jelly-box-animate{17%{border-bottom-right-radius:10%}25%{transform:translateY(25%) rotate(22.5deg)}50%{border-bottom-right-radius:100%;transform:translateY(50%) scaleY(.9) rotate(45deg)}75%{transform:translateY(25%) rotate(67.5deg)}to{transform:translateY(0) rotate(90deg)}}@keyframes square-jelly-box-animate{17%{border-bottom-right-radius:10%}25%{transform:translateY(25%) rotate(22.5deg)}50%{border-bottom-right-radius:100%;transform:translateY(50%) scaleY(.9) rotate(45deg)}75%{transform:translateY(25%) rotate(67.5deg)}to{transform:translateY(0) rotate(90deg)}}@-webkit-keyframes square-jelly-box-shadow{50%{transform:scaleX(1.25)}}@keyframes square-jelly-box-shadow{50%{transform:scaleX(1.25)}}.sk-square-loader[_ngcontent-%COMP%], .sk-square-loader[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{box-sizing:border-box;position:relative}.sk-square-loader[_ngcontent-%COMP%]{font-size:0;height:100%;width:100%}.sk-square-loader[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:square-loader 2s ease infinite;animation:square-loader 2s ease infinite;background:transparent;background-color:currentColor;border:0 solid;border-radius:0;border-width:3px;display:inline-block;float:none;height:100%;width:100%}.sk-square-loader[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:after{-webkit-animation:square-loader-inner 2s ease-in infinite;animation:square-loader-inner 2s ease-in infinite;background-color:currentColor;content:\"\";display:inline-block;vertical-align:top;width:100%}@-webkit-keyframes square-loader{0%{transform:rotate(0deg)}25%{transform:rotate(180deg)}50%{transform:rotate(180deg)}75%{transform:rotate(1turn)}to{transform:rotate(1turn)}}@keyframes square-loader{0%{transform:rotate(0deg)}25%{transform:rotate(180deg)}50%{transform:rotate(180deg)}75%{transform:rotate(1turn)}to{transform:rotate(1turn)}}@-webkit-keyframes square-loader-inner{0%{height:0}25%{height:0}50%{height:100%}75%{height:100%}to{height:0}}@keyframes square-loader-inner{0%{height:0}25%{height:0}50%{height:100%}75%{height:100%}to{height:0}}.sk-three-bounce[_ngcontent-%COMP%]{height:100%;margin:auto;text-align:center;width:100%}.sk-three-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:sk-threeBounce-bounceDelay 1.4s ease-in-out infinite both;animation:sk-threeBounce-bounceDelay 1.4s ease-in-out infinite both;background-color:currentColor;border-radius:100%;display:inline-block;height:30%;margin-top:35%;width:30%}.bottom-center[_ngcontent-%COMP%] > .sk-three-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%], .bottom-left[_ngcontent-%COMP%] > .sk-three-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%], .bottom-right[_ngcontent-%COMP%] > .sk-three-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{margin-top:70%!important}.top-center[_ngcontent-%COMP%] > .sk-three-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%], .top-left[_ngcontent-%COMP%] > .sk-three-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%], .top-right[_ngcontent-%COMP%] > .sk-three-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{margin-top:0!important}.sk-three-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:first-child{-webkit-animation-delay:-.32s;animation-delay:-.32s}.sk-three-bounce[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes sk-threeBounce-bounceDelay{0%,80%,to{transform:scale(0)}40%{transform:scale(1)}}@keyframes sk-threeBounce-bounceDelay{0%,80%,to{transform:scale(0)}40%{transform:scale(1)}}.sk-three-strings[_ngcontent-%COMP%], .sk-three-strings[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{height:100%;width:100%}.sk-three-strings[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{border-radius:50%;box-sizing:border-box;position:absolute}.sk-three-strings[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:first-child{-webkit-animation:sk-threeStrings-rotateOne 1s linear infinite;animation:sk-threeStrings-rotateOne 1s linear infinite;border-bottom:3px solid;left:0;top:0}.sk-three-strings[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation:sk-threeStrings-rotateTwo 1s linear infinite;animation:sk-threeStrings-rotateTwo 1s linear infinite;border-right:3px solid;right:0;top:0}.sk-three-strings[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(3){-webkit-animation:sk-threeStrings-rotateThree 1s linear infinite;animation:sk-threeStrings-rotateThree 1s linear infinite;border-top:3px solid;bottom:0;right:0}@-webkit-keyframes sk-threeStrings-rotateOne{0%{transform:rotateX(35deg) rotateY(-45deg) rotate(0deg)}to{transform:rotateX(35deg) rotateY(-45deg) rotate(1turn)}}@keyframes sk-threeStrings-rotateOne{0%{transform:rotateX(35deg) rotateY(-45deg) rotate(0deg)}to{transform:rotateX(35deg) rotateY(-45deg) rotate(1turn)}}@-webkit-keyframes sk-threeStrings-rotateTwo{0%{transform:rotateX(50deg) rotateY(10deg) rotate(0deg)}to{transform:rotateX(50deg) rotateY(10deg) rotate(1turn)}}@keyframes sk-threeStrings-rotateTwo{0%{transform:rotateX(50deg) rotateY(10deg) rotate(0deg)}to{transform:rotateX(50deg) rotateY(10deg) rotate(1turn)}}@-webkit-keyframes sk-threeStrings-rotateThree{0%{transform:rotateX(35deg) rotateY(55deg) rotate(0deg)}to{transform:rotateX(35deg) rotateY(55deg) rotate(1turn)}}@keyframes sk-threeStrings-rotateThree{0%{transform:rotateX(35deg) rotateY(55deg) rotate(0deg)}to{transform:rotateX(35deg) rotateY(55deg) rotate(1turn)}}.sk-wandering-cubes[_ngcontent-%COMP%]{height:100%;margin:auto;position:relative;text-align:center;width:100%}.sk-wandering-cubes[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]{-webkit-animation:sk-wanderingCubes-cubeMove 1.8s ease-in-out infinite;animation:sk-wanderingCubes-cubeMove 1.8s ease-in-out infinite;background-color:currentColor;height:25%;left:0;position:absolute;top:0;width:25%}.sk-wandering-cubes[_ngcontent-%COMP%] > div[_ngcontent-%COMP%]:nth-child(2){-webkit-animation-delay:-.9s;animation-delay:-.9s}@-webkit-keyframes sk-wanderingCubes-cubeMove{25%{transform:translateX(290%) rotate(-90deg) scale(.5)}50%{transform:translateX(290%) translateY(290%) rotate(-179deg)}50.1%{transform:translateX(290%) translateY(290%) rotate(-180deg)}75%{transform:translateX(0) translateY(290%) rotate(-270deg) scale(.5)}to{transform:rotate(-1turn)}}@keyframes sk-wanderingCubes-cubeMove{25%{transform:translateX(290%) rotate(-90deg) scale(.5)}50%{transform:translateX(290%) translateY(290%) rotate(-179deg)}50.1%{transform:translateX(290%) translateY(290%) rotate(-180deg)}75%{transform:translateX(0) translateY(290%) rotate(-270deg) scale(.5)}to{transform:rotate(-1turn)}}"], changeDetection: 0 });
NgxUiLoaderComponent.ctorParameters = () => [
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ChangeDetectorRef },
    { type: NgxUiLoaderService }
];
NgxUiLoaderComponent.propDecorators = {
    bgsColor: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    bgsOpacity: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    bgsPosition: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    bgsSize: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    bgsTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    bgsType: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    fgsColor: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    fgsPosition: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    fgsSize: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    fgsTemplate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    fgsType: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    gap: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    loaderId: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    logoPosition: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    logoSize: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    logoUrl: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    overlayBorderRadius: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    overlayColor: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    pbColor: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    pbDirection: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    pbThickness: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    hasProgressBar: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    text: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    textColor: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    textPosition: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }]
};
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxUiLoaderComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Component,
        args: [{
                selector: 'ngx-ui-loader',
                template: "<!-- Progress bar {{{ -->\n<div\n  *ngIf=\"hasProgressBar\"\n  class=\"ngx-progress-bar\"\n  [class.ngx-position-absolute]=\"loaderId !== defaultConfig.masterLoaderId\"\n  [ngClass]=\"'ngx-progress-bar-' + pbDirection\"\n  [style.height.px]=\"pbThickness\"\n  [style.color]=\"pbColor\"\n  [class.loading-foreground]=\"showForeground\"\n  [class.foreground-closing]=\"foregroundClosing\"\n  [class.fast-closing]=\"foregroundClosing && fastFadeOut\"\n></div>\n<!-- Progress bar }}} -->\n\n<!-- Foreground container {{{ -->\n<div\n  class=\"ngx-overlay\"\n  [class.ngx-position-absolute]=\"loaderId !== defaultConfig.masterLoaderId\"\n  [style.background-color]=\"overlayColor\"\n  [style.border-radius]=\"overlayBorderRadius\"\n  [class.loading-foreground]=\"showForeground\"\n  [class.foreground-closing]=\"foregroundClosing\"\n  [class.fast-closing]=\"foregroundClosing && fastFadeOut\"\n>\n  <!-- Logo {{{ -->\n  <img\n    *ngIf=\"logoUrl\"\n    class=\"ngx-loading-logo\"\n    [ngClass]=\"logoPosition\"\n    [src]=\"trustedLogoUrl\"\n    [style.width.px]=\"logoSize\"\n    [style.height.px]=\"logoSize\"\n    [style.top]=\"logoTop\"\n  />\n  <!-- Logo }}} -->\n\n  <!-- Foreground spinner {{{ -->\n  <div\n    class=\"ngx-foreground-spinner\"\n    [ngClass]=\"fgsPosition\"\n    [style.color]=\"fgsColor\"\n    [style.width.px]=\"fgsSize\"\n    [style.height.px]=\"fgsSize\"\n    [style.top]=\"spinnerTop\"\n  >\n    <div *ngIf=\"!fgsTemplate; else foregroundTemplate\" [class]=\"fgSpinnerClass\">\n      <div *ngFor=\"let div of fgDivs\"></div>\n    </div>\n    <ng-template #foregroundTemplate>\n      <ng-container *ngTemplateOutlet=\"fgsTemplate\"></ng-container>\n    </ng-template>\n  </div>\n  <!-- Foreground spinner }}} -->\n\n  <!-- Loading text {{{ -->\n  <div class=\"ngx-loading-text\" [ngClass]=\"textPosition\" [style.top]=\"textTop\" [style.color]=\"textColor\">{{ text }}</div>\n  <!-- Loading text }}} -->\n</div>\n<!-- Foreground container }}} -->\n\n<!-- Background spinner {{{ -->\n<div\n  class=\"ngx-background-spinner\"\n  [class.ngx-position-absolute]=\"loaderId !== defaultConfig.masterLoaderId\"\n  [ngClass]=\"bgsPosition\"\n  [class.loading-background]=\"showBackground\"\n  [class.background-closing]=\"backgroundClosing\"\n  [class.fast-closing]=\"backgroundClosing && fastFadeOut\"\n  [style.width.px]=\"bgsSize\"\n  [style.height.px]=\"bgsSize\"\n  [style.color]=\"bgsColor\"\n  [style.opacity]=\"bgsOpacity\"\n>\n  <div *ngIf=\"!bgsTemplate; else backgroundTemplate\" [class]=\"bgSpinnerClass\">\n    <div *ngFor=\"let div of bgDivs\"></div>\n  </div>\n  <ng-template #backgroundTemplate>\n    <ng-container *ngTemplateOutlet=\"bgsTemplate\"></ng-container>\n  </ng-template>\n</div>\n<!-- Background spinner }}} -->\n",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ChangeDetectionStrategy.OnPush,
                styles: [".ngx-progress-bar{color:#00acc1;display:none;height:3px;left:0;overflow:hidden;position:fixed;top:0;width:100%;z-index:99999!important}.ngx-progress-bar.foreground-closing,.ngx-progress-bar.loading-foreground{display:block}.ngx-progress-bar.foreground-closing{opacity:0!important;transition:opacity .5s ease-out .5s}.ngx-progress-bar.fast-closing{transition:opacity .3s ease-out .3s!important}.ngx-progress-bar:after,.ngx-progress-bar:before{background-color:currentColor;content:\"\";display:block;height:100%;position:absolute;top:0;width:100%}.ngx-progress-bar-ltr:before{transform:translate3d(-100%,0,0)}.ngx-progress-bar-ltr:after{-webkit-animation:progressBar-slide-ltr 12s ease-out 0s 1 normal;animation:progressBar-slide-ltr 12s ease-out 0s 1 normal;transform:translate3d(-5%,0,0)}.ngx-progress-bar-rtl:before{transform:translate3d(100%,0,0)}.ngx-progress-bar-rtl:after{-webkit-animation:progressBar-slide-rtl 12s ease-out 0s 1 normal;animation:progressBar-slide-rtl 12s ease-out 0s 1 normal;transform:translate3d(5%,0,0)}.foreground-closing.ngx-progress-bar-ltr:before{-webkit-animation:progressBar-slide-complete-ltr 1s ease-out 0s 1;animation:progressBar-slide-complete-ltr 1s ease-out 0s 1;transform:translateZ(0)}.fast-closing.ngx-progress-bar-ltr:before{-webkit-animation:progressBar-slide-complete-ltr .6s ease-out 0s 1!important;animation:progressBar-slide-complete-ltr .6s ease-out 0s 1!important}.foreground-closing.ngx-progress-bar-rtl:before{-webkit-animation:progressBar-slide-complete-rtl 1s ease-out 0s 1;animation:progressBar-slide-complete-rtl 1s ease-out 0s 1;transform:translateZ(0)}.fast-closing.ngx-progress-bar-rtl:before{-webkit-animation:progressBar-slide-complete-rtl .6s ease-out 0s 1!important;animation:progressBar-slide-complete-rtl .6s ease-out 0s 1!important}@-webkit-keyframes progressBar-slide-ltr{0%{transform:translate3d(-100%,0,0)}to{transform:translate3d(-5%,0,0)}}@keyframes progressBar-slide-ltr{0%{transform:translate3d(-100%,0,0)}to{transform:translate3d(-5%,0,0)}}@-webkit-keyframes progressBar-slide-rtl{0%{transform:translate3d(100%,0,0)}to{transform:translate3d(5%,0,0)}}@keyframes progressBar-slide-rtl{0%{transform:translate3d(100%,0,0)}to{transform:translate3d(5%,0,0)}}@-webkit-keyframes progressBar-slide-complete-ltr{0%{transform:translate3d(-75%,0,0)}50%{transform:translateZ(0)}}@keyframes progressBar-slide-complete-ltr{0%{transform:translate3d(-75%,0,0)}50%{transform:translateZ(0)}}@-webkit-keyframes progressBar-slide-complete-rtl{0%{transform:translate3d(75%,0,0)}50%{transform:translateZ(0)}}@keyframes progressBar-slide-complete-rtl{0%{transform:translate3d(75%,0,0)}50%{transform:translateZ(0)}}.ngx-overlay{background-color:rgba(40,40,40,.8);cursor:progress;display:none;height:100%;left:0;position:fixed;top:0;width:100%;z-index:99998!important}.ngx-overlay.foreground-closing,.ngx-overlay.loading-foreground{display:block}.ngx-overlay.foreground-closing{opacity:0!important;transition:opacity .5s ease-out .5s}.ngx-overlay.fast-closing{transition:opacity .3s ease-out .3s!important}.ngx-overlay>.ngx-foreground-spinner{color:#00acc1;height:60px;margin:0;position:fixed;width:60px}.ngx-overlay>.ngx-loading-logo{height:120px;margin:0;position:fixed;width:120px}.ngx-overlay>.ngx-loading-text{color:#fff;font-family:sans-serif;font-size:1.2em;font-weight:400;margin:0;position:fixed}.ngx-background-spinner{color:#00acc1;display:none;height:60px;margin:0;opacity:.6;position:fixed;width:60px;z-index:99997!important}.ngx-background-spinner.background-closing,.ngx-background-spinner.loading-background{display:block}.ngx-background-spinner.background-closing{opacity:0!important;transition:opacity .7s ease-out}.ngx-background-spinner.fast-closing{transition:opacity .4s ease-out!important}.ngx-position-absolute,.ngx-position-absolute>.ngx-foreground-spinner,.ngx-position-absolute>.ngx-loading-logo,.ngx-position-absolute>.ngx-loading-text{position:absolute!important}.ngx-position-absolute.ngx-progress-bar{z-index:99996!important}.ngx-position-absolute.ngx-overlay{z-index:99995!important}.ngx-position-absolute.ngx-background-spinner,.ngx-position-absolute .sk-square-jelly-box>div:first-child{z-index:99994!important}.top-left{left:30px;top:30px}.top-center{left:50%;top:30px;transform:translateX(-50%)}.top-right{right:30px;top:30px}.center-left{left:30px;top:50%;transform:translateY(-50%)}.center-center{left:50%;top:50%;transform:translate(-50%,-50%)}.center-right{right:30px;top:50%;transform:translateY(-50%)}.bottom-left{bottom:30px;left:30px}.bottom-center{bottom:30px;left:50%;transform:translateX(-50%)}.bottom-right{bottom:30px;right:30px}.sk-ball-scale-multiple,.sk-ball-scale-multiple>div{box-sizing:border-box;position:relative}.sk-ball-scale-multiple{font-size:0;height:100%;width:100%}.sk-ball-scale-multiple>div{-webkit-animation:ball-scale-multiple 1s linear 0s infinite;animation:ball-scale-multiple 1s linear 0s infinite;background-color:currentColor;border:0 solid;border-radius:100%;display:inline-block;float:none;height:100%;left:0;opacity:0;position:absolute;top:0;width:100%}.sk-ball-scale-multiple>div:nth-child(2){-webkit-animation-delay:.2s;animation-delay:.2s}.sk-ball-scale-multiple>div:nth-child(3){-webkit-animation-delay:.4s;animation-delay:.4s}@-webkit-keyframes ball-scale-multiple{0%{opacity:0;transform:scale(0)}5%{opacity:.75}to{opacity:0;transform:scale(1)}}@keyframes ball-scale-multiple{0%{opacity:0;transform:scale(0)}5%{opacity:.75}to{opacity:0;transform:scale(1)}}.sk-ball-spin,.sk-ball-spin>div{box-sizing:border-box;position:relative}.sk-ball-spin{font-size:0;height:100%;width:100%}.sk-ball-spin>div{-webkit-animation:ball-spin-clockwise 1s ease-in-out infinite;animation:ball-spin-clockwise 1s ease-in-out infinite;background-color:currentColor;border:0 solid;border-radius:100%;display:inline-block;float:none;height:25%;left:50%;margin-left:-12.5%;margin-top:-12.5%;position:absolute;top:50%;width:25%}.sk-ball-spin>div:first-child{-webkit-animation-delay:-1.125s;animation-delay:-1.125s;left:50%;top:5%}.sk-ball-spin>div:nth-child(2){-webkit-animation-delay:-1.25s;animation-delay:-1.25s;left:81.8198051534%;top:18.1801948466%}.sk-ball-spin>div:nth-child(3){-webkit-animation-delay:-1.375s;animation-delay:-1.375s;left:95%;top:50%}.sk-ball-spin>div:nth-child(4){-webkit-animation-delay:-1.5s;animation-delay:-1.5s;left:81.8198051534%;top:81.8198051534%}.sk-ball-spin>div:nth-child(5){-webkit-animation-delay:-1.625s;animation-delay:-1.625s;left:50.0000000005%;top:94.9999999966%}.sk-ball-spin>div:nth-child(6){-webkit-animation-delay:-1.75s;animation-delay:-1.75s;left:18.1801949248%;top:81.8198046966%}.sk-ball-spin>div:nth-child(7){-webkit-animation-delay:-1.875s;animation-delay:-1.875s;left:5.0000051215%;top:49.9999750815%}.sk-ball-spin>div:nth-child(8){-webkit-animation-delay:-2s;animation-delay:-2s;left:18.1803700518%;top:18.179464974%}@-webkit-keyframes ball-spin{0%,to{opacity:1;transform:scale(1)}20%{opacity:1}80%{opacity:0;transform:scale(0)}}@keyframes ball-spin{0%,to{opacity:1;transform:scale(1)}20%{opacity:1}80%{opacity:0;transform:scale(0)}}.sk-ball-spin-clockwise,.sk-ball-spin-clockwise>div{box-sizing:border-box;position:relative}.sk-ball-spin-clockwise{font-size:0;height:100%;width:100%}.sk-ball-spin-clockwise>div{-webkit-animation:ball-spin-clockwise 1s ease-in-out infinite;animation:ball-spin-clockwise 1s ease-in-out infinite;background-color:currentColor;border:0 solid;border-radius:100%;display:inline-block;float:none;height:25%;left:50%;margin-left:-12.5%;margin-top:-12.5%;position:absolute;top:50%;width:25%}.sk-ball-spin-clockwise>div:first-child{-webkit-animation-delay:-.875s;animation-delay:-.875s;left:50%;top:5%}.sk-ball-spin-clockwise>div:nth-child(2){-webkit-animation-delay:-.75s;animation-delay:-.75s;left:81.8198051534%;top:18.1801948466%}.sk-ball-spin-clockwise>div:nth-child(3){-webkit-animation-delay:-.625s;animation-delay:-.625s;left:95%;top:50%}.sk-ball-spin-clockwise>div:nth-child(4){-webkit-animation-delay:-.5s;animation-delay:-.5s;left:81.8198051534%;top:81.8198051534%}.sk-ball-spin-clockwise>div:nth-child(5){-webkit-animation-delay:-.375s;animation-delay:-.375s;left:50.0000000005%;top:94.9999999966%}.sk-ball-spin-clockwise>div:nth-child(6){-webkit-animation-delay:-.25s;animation-delay:-.25s;left:18.1801949248%;top:81.8198046966%}.sk-ball-spin-clockwise>div:nth-child(7){-webkit-animation-delay:-.125s;animation-delay:-.125s;left:5.0000051215%;top:49.9999750815%}.sk-ball-spin-clockwise>div:nth-child(8){-webkit-animation-delay:0s;animation-delay:0s;left:18.1803700518%;top:18.179464974%}@-webkit-keyframes ball-spin-clockwise{0%,to{opacity:1;transform:scale(1)}20%{opacity:1}80%{opacity:0;transform:scale(0)}}@keyframes ball-spin-clockwise{0%,to{opacity:1;transform:scale(1)}20%{opacity:1}80%{opacity:0;transform:scale(0)}}.sk-ball-spin-clockwise-fade-rotating,.sk-ball-spin-clockwise-fade-rotating>div{box-sizing:border-box;position:relative}.sk-ball-spin-clockwise-fade-rotating{-webkit-animation:ball-spin-clockwise-fade-rotating-rotate 6s linear infinite;animation:ball-spin-clockwise-fade-rotating-rotate 6s linear infinite;font-size:0;height:100%;width:100%}.sk-ball-spin-clockwise-fade-rotating>div{-webkit-animation:ball-spin-clockwise-fade-rotating 1s linear infinite;animation:ball-spin-clockwise-fade-rotating 1s linear infinite;background-color:currentColor;border:0 solid;border-radius:100%;display:inline-block;float:none;height:25%;left:50%;margin-left:-12.5%;margin-top:-12.5%;position:absolute;top:50%;width:25%}.sk-ball-spin-clockwise-fade-rotating>div:first-child{-webkit-animation-delay:-.875s;animation-delay:-.875s;left:50%;top:5%}.sk-ball-spin-clockwise-fade-rotating>div:nth-child(2){-webkit-animation-delay:-.75s;animation-delay:-.75s;left:81.8198051534%;top:18.1801948466%}.sk-ball-spin-clockwise-fade-rotating>div:nth-child(3){-webkit-animation-delay:-.625s;animation-delay:-.625s;left:95%;top:50%}.sk-ball-spin-clockwise-fade-rotating>div:nth-child(4){-webkit-animation-delay:-.5s;animation-delay:-.5s;left:81.8198051534%;top:81.8198051534%}.sk-ball-spin-clockwise-fade-rotating>div:nth-child(5){-webkit-animation-delay:-.375s;animation-delay:-.375s;left:50.0000000005%;top:94.9999999966%}.sk-ball-spin-clockwise-fade-rotating>div:nth-child(6){-webkit-animation-delay:-.25s;animation-delay:-.25s;left:18.1801949248%;top:81.8198046966%}.sk-ball-spin-clockwise-fade-rotating>div:nth-child(7){-webkit-animation-delay:-.125s;animation-delay:-.125s;left:5.0000051215%;top:49.9999750815%}.sk-ball-spin-clockwise-fade-rotating>div:nth-child(8){-webkit-animation-delay:0s;animation-delay:0s;left:18.1803700518%;top:18.179464974%}@-webkit-keyframes ball-spin-clockwise-fade-rotating-rotate{to{transform:rotate(-1turn)}}@keyframes ball-spin-clockwise-fade-rotating-rotate{to{transform:rotate(-1turn)}}@-webkit-keyframes ball-spin-clockwise-fade-rotating{50%{opacity:.25;transform:scale(.5)}to{opacity:1;transform:scale(1)}}@keyframes ball-spin-clockwise-fade-rotating{50%{opacity:.25;transform:scale(.5)}to{opacity:1;transform:scale(1)}}.sk-ball-spin-fade-rotating,.sk-ball-spin-fade-rotating>div{box-sizing:border-box;position:relative}.sk-ball-spin-fade-rotating{-webkit-animation:ball-spin-fade-rotate 6s linear infinite;animation:ball-spin-fade-rotate 6s linear infinite;font-size:0;height:100%;width:100%}.sk-ball-spin-fade-rotating>div{-webkit-animation:ball-spin-fade 1s linear infinite;animation:ball-spin-fade 1s linear infinite;background-color:currentColor;border:0 solid;border-radius:100%;display:inline-block;float:none;height:25%;left:50%;margin-left:-12.5%;margin-top:-12.5%;position:absolute;top:50%;width:25%}.sk-ball-spin-fade-rotating>div:first-child{-webkit-animation-delay:-1.125s;animation-delay:-1.125s;left:50%;top:5%}.sk-ball-spin-fade-rotating>div:nth-child(2){-webkit-animation-delay:-1.25s;animation-delay:-1.25s;left:81.8198051534%;top:18.1801948466%}.sk-ball-spin-fade-rotating>div:nth-child(3){-webkit-animation-delay:-1.375s;animation-delay:-1.375s;left:95%;top:50%}.sk-ball-spin-fade-rotating>div:nth-child(4){-webkit-animation-delay:-1.5s;animation-delay:-1.5s;left:81.8198051534%;top:81.8198051534%}.sk-ball-spin-fade-rotating>div:nth-child(5){-webkit-animation-delay:-1.625s;animation-delay:-1.625s;left:50.0000000005%;top:94.9999999966%}.sk-ball-spin-fade-rotating>div:nth-child(6){-webkit-animation-delay:-1.75s;animation-delay:-1.75s;left:18.1801949248%;top:81.8198046966%}.sk-ball-spin-fade-rotating>div:nth-child(7){-webkit-animation-delay:-1.875s;animation-delay:-1.875s;left:5.0000051215%;top:49.9999750815%}.sk-ball-spin-fade-rotating>div:nth-child(8){-webkit-animation-delay:-2s;animation-delay:-2s;left:18.1803700518%;top:18.179464974%}@-webkit-keyframes ball-spin-fade-rotate{to{transform:rotate(1turn)}}@keyframes ball-spin-fade-rotate{to{transform:rotate(1turn)}}@-webkit-keyframes ball-spin-fade{0%,to{opacity:1;transform:scale(1)}50%{opacity:.25;transform:scale(.5)}}@keyframes ball-spin-fade{0%,to{opacity:1;transform:scale(1)}50%{opacity:.25;transform:scale(.5)}}.sk-chasing-dots{-webkit-animation:sk-chasingDots-rotate 2s linear infinite;animation:sk-chasingDots-rotate 2s linear infinite;height:100%;margin:auto;position:absolute;text-align:center;width:100%}.sk-chasing-dots>div{-webkit-animation:sk-chasingDots-bounce 2s ease-in-out infinite;animation:sk-chasingDots-bounce 2s ease-in-out infinite;background-color:currentColor;border-radius:100%;display:inline-block;height:60%;position:absolute;top:0;width:60%}.sk-chasing-dots>div:nth-child(2){-webkit-animation-delay:-1s;animation-delay:-1s;bottom:0;top:auto}@-webkit-keyframes sk-chasingDots-rotate{to{transform:rotate(1turn)}}@keyframes sk-chasingDots-rotate{to{transform:rotate(1turn)}}@-webkit-keyframes sk-chasingDots-bounce{0%,to{transform:scale(0)}50%{transform:scale(1)}}@keyframes sk-chasingDots-bounce{0%,to{transform:scale(0)}50%{transform:scale(1)}}.sk-circle{height:100%;margin:auto;position:relative;width:100%}.sk-circle>div{height:100%;left:0;position:absolute;top:0;width:100%}.sk-circle>div:before{-webkit-animation:sk-circle-bounceDelay 1.2s ease-in-out infinite both;animation:sk-circle-bounceDelay 1.2s ease-in-out infinite both;background-color:currentColor;border-radius:100%;content:\"\";display:block;height:15%;margin:0 auto;width:15%}.sk-circle>div:nth-child(2){transform:rotate(30deg)}.sk-circle>div:nth-child(3){transform:rotate(60deg)}.sk-circle>div:nth-child(4){transform:rotate(90deg)}.sk-circle>div:nth-child(5){transform:rotate(120deg)}.sk-circle>div:nth-child(6){transform:rotate(150deg)}.sk-circle>div:nth-child(7){transform:rotate(180deg)}.sk-circle>div:nth-child(8){transform:rotate(210deg)}.sk-circle>div:nth-child(9){transform:rotate(240deg)}.sk-circle>div:nth-child(10){transform:rotate(270deg)}.sk-circle>div:nth-child(11){transform:rotate(300deg)}.sk-circle>div:nth-child(12){transform:rotate(330deg)}.sk-circle>div:nth-child(2):before{-webkit-animation-delay:-1.1s;animation-delay:-1.1s}.sk-circle>div:nth-child(3):before{-webkit-animation-delay:-1s;animation-delay:-1s}.sk-circle>div:nth-child(4):before{-webkit-animation-delay:-.9s;animation-delay:-.9s}.sk-circle>div:nth-child(5):before{-webkit-animation-delay:-.8s;animation-delay:-.8s}.sk-circle>div:nth-child(6):before{-webkit-animation-delay:-.7s;animation-delay:-.7s}.sk-circle>div:nth-child(7):before{-webkit-animation-delay:-.6s;animation-delay:-.6s}.sk-circle>div:nth-child(8):before{-webkit-animation-delay:-.5s;animation-delay:-.5s}.sk-circle>div:nth-child(9):before{-webkit-animation-delay:-.4s;animation-delay:-.4s}.sk-circle>div:nth-child(10):before{-webkit-animation-delay:-.3s;animation-delay:-.3s}.sk-circle>div:nth-child(11):before{-webkit-animation-delay:-.2s;animation-delay:-.2s}.sk-circle>div:nth-child(12):before{-webkit-animation-delay:-.1s;animation-delay:-.1s}@-webkit-keyframes sk-circle-bounceDelay{0%,80%,to{transform:scale(0)}40%{transform:scale(1)}}@keyframes sk-circle-bounceDelay{0%,80%,to{transform:scale(0)}40%{transform:scale(1)}}.sk-cube-grid{height:100%;margin:auto;width:100%}.sk-cube-grid>div{-webkit-animation:sk-cubeGrid-scaleDelay 1.3s ease-in-out infinite;animation:sk-cubeGrid-scaleDelay 1.3s ease-in-out infinite;background-color:currentColor;float:left;height:33%;width:33%}.sk-cube-grid>div:first-child{-webkit-animation-delay:.2s;animation-delay:.2s}.sk-cube-grid>div:nth-child(2){-webkit-animation-delay:.3s;animation-delay:.3s}.sk-cube-grid>div:nth-child(3){-webkit-animation-delay:.4s;animation-delay:.4s}.sk-cube-grid>div:nth-child(4){-webkit-animation-delay:.1s;animation-delay:.1s}.sk-cube-grid>div:nth-child(5){-webkit-animation-delay:.2s;animation-delay:.2s}.sk-cube-grid>div:nth-child(6){-webkit-animation-delay:.3s;animation-delay:.3s}.sk-cube-grid>div:nth-child(7){-webkit-animation-delay:0s;animation-delay:0s}.sk-cube-grid>div:nth-child(8){-webkit-animation-delay:.1s;animation-delay:.1s}.sk-cube-grid>div:nth-child(9){-webkit-animation-delay:.2s;animation-delay:.2s}@-webkit-keyframes sk-cubeGrid-scaleDelay{0%,70%,to{transform:scaleX(1)}35%{transform:scale3D(0,0,1)}}@keyframes sk-cubeGrid-scaleDelay{0%,70%,to{transform:scaleX(1)}35%{transform:scale3D(0,0,1)}}.sk-double-bounce{height:100%;margin:auto;position:relative;width:100%}.sk-double-bounce>div{-webkit-animation:sk-doubleBounce-bounce 2s ease-in-out infinite;animation:sk-doubleBounce-bounce 2s ease-in-out infinite;background-color:currentColor;border-radius:50%;height:100%;left:0;opacity:.6;position:absolute;top:0;width:100%}.sk-double-bounce>div:nth-child(2){-webkit-animation-delay:-1s;animation-delay:-1s}@-webkit-keyframes sk-doubleBounce-bounce{0%,to{transform:scale(0)}50%{transform:scale(1)}}@keyframes sk-doubleBounce-bounce{0%,to{transform:scale(0)}50%{transform:scale(1)}}.sk-fading-circle{height:100%;margin:auto;position:relative;width:100%}.sk-fading-circle>div{height:100%;left:0;position:absolute;top:0;width:100%}.sk-fading-circle>div:before{-webkit-animation:sk-fadingCircle-FadeDelay 1.2s ease-in-out infinite both;animation:sk-fadingCircle-FadeDelay 1.2s ease-in-out infinite both;background-color:currentColor;border-radius:100%;content:\"\";display:block;height:15%;margin:0 auto;width:15%}.sk-fading-circle>div:nth-child(2){transform:rotate(30deg)}.sk-fading-circle>div:nth-child(3){transform:rotate(60deg)}.sk-fading-circle>div:nth-child(4){transform:rotate(90deg)}.sk-fading-circle>div:nth-child(5){transform:rotate(120deg)}.sk-fading-circle>div:nth-child(6){transform:rotate(150deg)}.sk-fading-circle>div:nth-child(7){transform:rotate(180deg)}.sk-fading-circle>div:nth-child(8){transform:rotate(210deg)}.sk-fading-circle>div:nth-child(9){transform:rotate(240deg)}.sk-fading-circle>div:nth-child(10){transform:rotate(270deg)}.sk-fading-circle>div:nth-child(11){transform:rotate(300deg)}.sk-fading-circle>div:nth-child(12){transform:rotate(330deg)}.sk-fading-circle>div:nth-child(2):before{-webkit-animation-delay:-1.1s;animation-delay:-1.1s}.sk-fading-circle>div:nth-child(3):before{-webkit-animation-delay:-1s;animation-delay:-1s}.sk-fading-circle>div:nth-child(4):before{-webkit-animation-delay:-.9s;animation-delay:-.9s}.sk-fading-circle>div:nth-child(5):before{-webkit-animation-delay:-.8s;animation-delay:-.8s}.sk-fading-circle>div:nth-child(6):before{-webkit-animation-delay:-.7s;animation-delay:-.7s}.sk-fading-circle>div:nth-child(7):before{-webkit-animation-delay:-.6s;animation-delay:-.6s}.sk-fading-circle>div:nth-child(8):before{-webkit-animation-delay:-.5s;animation-delay:-.5s}.sk-fading-circle>div:nth-child(9):before{-webkit-animation-delay:-.4s;animation-delay:-.4s}.sk-fading-circle>div:nth-child(10):before{-webkit-animation-delay:-.3s;animation-delay:-.3s}.sk-fading-circle>div:nth-child(11):before{-webkit-animation-delay:-.2s;animation-delay:-.2s}.sk-fading-circle>div:nth-child(12):before{-webkit-animation-delay:-.1s;animation-delay:-.1s}@-webkit-keyframes sk-fadingCircle-FadeDelay{0%,39%,to{opacity:0}40%{opacity:1}}@keyframes sk-fadingCircle-FadeDelay{0%,39%,to{opacity:0}40%{opacity:1}}.sk-folding-cube{height:100%;margin:auto;position:relative;transform:rotate(45deg);width:100%}.sk-folding-cube>div{float:left;height:50%;position:relative;transform:scale(1.1);width:50%}.sk-folding-cube>div:before{-webkit-animation:sk-foldingCube-angle 2.4s linear infinite both;animation:sk-foldingCube-angle 2.4s linear infinite both;background-color:currentColor;content:\"\";height:100%;left:0;position:absolute;top:0;transform-origin:100% 100%;width:100%}.sk-folding-cube>div:nth-child(2){transform:scale(1.1) rotate(90deg)}.sk-folding-cube>div:nth-child(3){transform:scale(1.1) rotate(270deg)}.sk-folding-cube>div:nth-child(4){transform:scale(1.1) rotate(180deg)}.sk-folding-cube>div:nth-child(2):before{-webkit-animation-delay:.3s;animation-delay:.3s}.sk-folding-cube>div:nth-child(3):before{-webkit-animation-delay:.9s;animation-delay:.9s}.sk-folding-cube>div:nth-child(4):before{-webkit-animation-delay:.6s;animation-delay:.6s}@-webkit-keyframes sk-foldingCube-angle{0%,10%{opacity:0;transform:perspective(140px) rotateX(-180deg)}25%,75%{opacity:1;transform:perspective(140px) rotateX(0deg)}90%,to{opacity:0;transform:perspective(140px) rotateY(180deg)}}@keyframes sk-foldingCube-angle{0%,10%{opacity:0;transform:perspective(140px) rotateX(-180deg)}25%,75%{opacity:1;transform:perspective(140px) rotateX(0deg)}90%,to{opacity:0;transform:perspective(140px) rotateY(180deg)}}.sk-pulse{height:100%;margin:auto;width:100%}.sk-pulse>div{-webkit-animation:sk-pulse-scaleOut 1s ease-in-out infinite;animation:sk-pulse-scaleOut 1s ease-in-out infinite;background-color:currentColor;border-radius:100%;height:100%;width:100%}@-webkit-keyframes sk-pulse-scaleOut{0%{transform:scale(0)}to{opacity:0;transform:scale(1)}}@keyframes sk-pulse-scaleOut{0%{transform:scale(0)}to{opacity:0;transform:scale(1)}}.sk-rectangle-bounce{font-size:0;height:100%;margin:auto;text-align:center;width:100%}.sk-rectangle-bounce>div{-webkit-animation:sk-rectangleBounce-stretchDelay 1.2s ease-in-out infinite;animation:sk-rectangleBounce-stretchDelay 1.2s ease-in-out infinite;background-color:currentColor;display:inline-block;height:100%;margin:0 5%;width:10%}.sk-rectangle-bounce>div:nth-child(2){-webkit-animation-delay:-1.1s;animation-delay:-1.1s}.sk-rectangle-bounce>div:nth-child(3){-webkit-animation-delay:-1s;animation-delay:-1s}.sk-rectangle-bounce>div:nth-child(4){-webkit-animation-delay:-.9s;animation-delay:-.9s}.sk-rectangle-bounce>div:nth-child(5){-webkit-animation-delay:-.8s;animation-delay:-.8s}@-webkit-keyframes sk-rectangleBounce-stretchDelay{0%,40%,to{transform:scaleY(.4)}20%{transform:scaleY(1)}}@keyframes sk-rectangleBounce-stretchDelay{0%,40%,to{transform:scaleY(.4)}20%{transform:scaleY(1)}}.sk-rectangle-bounce-party,.sk-rectangle-bounce-party>div{box-sizing:border-box;position:relative}.sk-rectangle-bounce-party{font-size:0;height:100%;margin:auto;text-align:center;width:100%}.sk-rectangle-bounce-party>div{-webkit-animation-iteration-count:infinite;-webkit-animation-name:rectangle-bounce-party;animation-iteration-count:infinite;animation-name:rectangle-bounce-party;background-color:currentColor;border:0 solid;border-radius:0;display:inline-block;float:none;height:100%;margin:0 5%;width:10%}.sk-rectangle-bounce-party>div:first-child{-webkit-animation-delay:-.23s;-webkit-animation-duration:.43s;animation-delay:-.23s;animation-duration:.43s}.sk-rectangle-bounce-party>div:nth-child(2){-webkit-animation-delay:-.32s;-webkit-animation-duration:.62s;animation-delay:-.32s;animation-duration:.62s}.sk-rectangle-bounce-party>div:nth-child(3){-webkit-animation-delay:-.44s;-webkit-animation-duration:.43s;animation-delay:-.44s;animation-duration:.43s}.sk-rectangle-bounce-party>div:nth-child(4){-webkit-animation-delay:-.31s;-webkit-animation-duration:.8s;animation-delay:-.31s;animation-duration:.8s}.sk-rectangle-bounce-party>div:nth-child(5){-webkit-animation-delay:-.24s;-webkit-animation-duration:.74s;animation-delay:-.24s;animation-duration:.74s}@-webkit-keyframes rectangle-bounce-party{0%{transform:scaleY(1)}50%{transform:scaleY(.4)}to{transform:scaleY(1)}}@keyframes rectangle-bounce-party{0%{transform:scaleY(1)}50%{transform:scaleY(.4)}to{transform:scaleY(1)}}.sk-rectangle-bounce-pulse-out,.sk-rectangle-bounce-pulse-out>div{box-sizing:border-box;position:relative}.sk-rectangle-bounce-pulse-out{font-size:0;height:100%;margin:auto;text-align:center;width:100%}.sk-rectangle-bounce-pulse-out>div{-webkit-animation:rectangle-bounce-pulse-out .9s cubic-bezier(.85,.25,.37,.85) infinite;animation:rectangle-bounce-pulse-out .9s cubic-bezier(.85,.25,.37,.85) infinite;background-color:currentColor;border:0 solid;border-radius:0;display:inline-block;float:none;height:100%;margin:0 5%;width:10%}.sk-rectangle-bounce-pulse-out>div:nth-child(3){-webkit-animation-delay:-.9s;animation-delay:-.9s}.sk-rectangle-bounce-pulse-out>div:nth-child(2),.sk-rectangle-bounce-pulse-out>div:nth-child(4){-webkit-animation-delay:-.7s;animation-delay:-.7s}.sk-rectangle-bounce-pulse-out>div:first-child,.sk-rectangle-bounce-pulse-out>div:nth-child(5){-webkit-animation-delay:-.5s;animation-delay:-.5s}@-webkit-keyframes rectangle-bounce-pulse-out{0%{transform:scaley(1)}50%{transform:scaley(.4)}to{transform:scaley(1)}}@keyframes rectangle-bounce-pulse-out{0%{transform:scaley(1)}50%{transform:scaley(.4)}to{transform:scaley(1)}}.sk-rectangle-bounce-pulse-out-rapid,.sk-rectangle-bounce-pulse-out-rapid>div{box-sizing:border-box;position:relative}.sk-rectangle-bounce-pulse-out-rapid{font-size:0;height:100%;margin:auto;text-align:center;width:100%}.sk-rectangle-bounce-pulse-out-rapid>div{-webkit-animation:rectangle-bounce-pulse-out-rapid .9s cubic-bezier(.11,.49,.38,.78) infinite;animation:rectangle-bounce-pulse-out-rapid .9s cubic-bezier(.11,.49,.38,.78) infinite;background-color:currentColor;border:0 solid;border-radius:0;display:inline-block;float:none;height:100%;margin:0 5%;width:10%}.sk-rectangle-bounce-pulse-out-rapid>div:nth-child(3){-webkit-animation-delay:-.9s;animation-delay:-.9s}.sk-rectangle-bounce-pulse-out-rapid>div:nth-child(2),.sk-rectangle-bounce-pulse-out-rapid>div:nth-child(4){-webkit-animation-delay:-.65s;animation-delay:-.65s}.sk-rectangle-bounce-pulse-out-rapid>div:first-child,.sk-rectangle-bounce-pulse-out-rapid>div:nth-child(5){-webkit-animation-delay:-.4s;animation-delay:-.4s}@-webkit-keyframes rectangle-bounce-pulse-out-rapid{0%{transform:scaley(1)}80%{transform:scaley(.4)}90%{transform:scaley(1)}}@keyframes rectangle-bounce-pulse-out-rapid{0%{transform:scaley(1)}80%{transform:scaley(.4)}90%{transform:scaley(1)}}.sk-rotating-plane{height:100%;margin:auto;text-align:center;width:100%}.sk-rotating-plane>div{-webkit-animation:sk-rotatePlane 1.2s ease-in-out infinite;animation:sk-rotatePlane 1.2s ease-in-out infinite;background-color:currentColor;height:100%;width:100%}@-webkit-keyframes sk-rotatePlane{0%{transform:perspective(120px) rotateX(0deg) rotateY(0deg)}50%{transform:perspective(120px) rotateX(-180.1deg) rotateY(0deg)}to{transform:perspective(120px) rotateX(-180deg) rotateY(-179.9deg)}}@keyframes sk-rotatePlane{0%{transform:perspective(120px) rotateX(0deg) rotateY(0deg)}50%{transform:perspective(120px) rotateX(-180.1deg) rotateY(0deg)}to{transform:perspective(120px) rotateX(-180deg) rotateY(-179.9deg)}}.sk-square-jelly-box,.sk-square-jelly-box>div{box-sizing:border-box;position:relative}.sk-square-jelly-box{font-size:0;height:100%;width:100%}.sk-square-jelly-box>div{background-color:currentColor;border:0 solid;display:inline-block;float:none}.sk-square-jelly-box>div:first-child,.sk-square-jelly-box>div:nth-child(2){left:0;position:absolute;width:100%}.sk-square-jelly-box>div:first-child{-webkit-animation:square-jelly-box-animate .6s linear -.1s infinite;animation:square-jelly-box-animate .6s linear -.1s infinite;border-radius:10%;height:100%;top:-25%;z-index:99997}.sk-square-jelly-box>div:nth-child(2){-webkit-animation:square-jelly-box-shadow .6s linear -.1s infinite;animation:square-jelly-box-shadow .6s linear -.1s infinite;background:#000;border-radius:50%;bottom:-9%;height:10%;opacity:.2}@-webkit-keyframes square-jelly-box-animate{17%{border-bottom-right-radius:10%}25%{transform:translateY(25%) rotate(22.5deg)}50%{border-bottom-right-radius:100%;transform:translateY(50%) scaleY(.9) rotate(45deg)}75%{transform:translateY(25%) rotate(67.5deg)}to{transform:translateY(0) rotate(90deg)}}@keyframes square-jelly-box-animate{17%{border-bottom-right-radius:10%}25%{transform:translateY(25%) rotate(22.5deg)}50%{border-bottom-right-radius:100%;transform:translateY(50%) scaleY(.9) rotate(45deg)}75%{transform:translateY(25%) rotate(67.5deg)}to{transform:translateY(0) rotate(90deg)}}@-webkit-keyframes square-jelly-box-shadow{50%{transform:scaleX(1.25)}}@keyframes square-jelly-box-shadow{50%{transform:scaleX(1.25)}}.sk-square-loader,.sk-square-loader>div{box-sizing:border-box;position:relative}.sk-square-loader{font-size:0;height:100%;width:100%}.sk-square-loader>div{-webkit-animation:square-loader 2s ease infinite;animation:square-loader 2s ease infinite;background:transparent;background-color:currentColor;border:0 solid;border-radius:0;border-width:3px;display:inline-block;float:none;height:100%;width:100%}.sk-square-loader>div:after{-webkit-animation:square-loader-inner 2s ease-in infinite;animation:square-loader-inner 2s ease-in infinite;background-color:currentColor;content:\"\";display:inline-block;vertical-align:top;width:100%}@-webkit-keyframes square-loader{0%{transform:rotate(0deg)}25%{transform:rotate(180deg)}50%{transform:rotate(180deg)}75%{transform:rotate(1turn)}to{transform:rotate(1turn)}}@keyframes square-loader{0%{transform:rotate(0deg)}25%{transform:rotate(180deg)}50%{transform:rotate(180deg)}75%{transform:rotate(1turn)}to{transform:rotate(1turn)}}@-webkit-keyframes square-loader-inner{0%{height:0}25%{height:0}50%{height:100%}75%{height:100%}to{height:0}}@keyframes square-loader-inner{0%{height:0}25%{height:0}50%{height:100%}75%{height:100%}to{height:0}}.sk-three-bounce{height:100%;margin:auto;text-align:center;width:100%}.sk-three-bounce>div{-webkit-animation:sk-threeBounce-bounceDelay 1.4s ease-in-out infinite both;animation:sk-threeBounce-bounceDelay 1.4s ease-in-out infinite both;background-color:currentColor;border-radius:100%;display:inline-block;height:30%;margin-top:35%;width:30%}.bottom-center>.sk-three-bounce>div,.bottom-left>.sk-three-bounce>div,.bottom-right>.sk-three-bounce>div{margin-top:70%!important}.top-center>.sk-three-bounce>div,.top-left>.sk-three-bounce>div,.top-right>.sk-three-bounce>div{margin-top:0!important}.sk-three-bounce>div:first-child{-webkit-animation-delay:-.32s;animation-delay:-.32s}.sk-three-bounce>div:nth-child(2){-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes sk-threeBounce-bounceDelay{0%,80%,to{transform:scale(0)}40%{transform:scale(1)}}@keyframes sk-threeBounce-bounceDelay{0%,80%,to{transform:scale(0)}40%{transform:scale(1)}}.sk-three-strings,.sk-three-strings>div{height:100%;width:100%}.sk-three-strings>div{border-radius:50%;box-sizing:border-box;position:absolute}.sk-three-strings>div:first-child{-webkit-animation:sk-threeStrings-rotateOne 1s linear infinite;animation:sk-threeStrings-rotateOne 1s linear infinite;border-bottom:3px solid;left:0;top:0}.sk-three-strings>div:nth-child(2){-webkit-animation:sk-threeStrings-rotateTwo 1s linear infinite;animation:sk-threeStrings-rotateTwo 1s linear infinite;border-right:3px solid;right:0;top:0}.sk-three-strings>div:nth-child(3){-webkit-animation:sk-threeStrings-rotateThree 1s linear infinite;animation:sk-threeStrings-rotateThree 1s linear infinite;border-top:3px solid;bottom:0;right:0}@-webkit-keyframes sk-threeStrings-rotateOne{0%{transform:rotateX(35deg) rotateY(-45deg) rotate(0deg)}to{transform:rotateX(35deg) rotateY(-45deg) rotate(1turn)}}@keyframes sk-threeStrings-rotateOne{0%{transform:rotateX(35deg) rotateY(-45deg) rotate(0deg)}to{transform:rotateX(35deg) rotateY(-45deg) rotate(1turn)}}@-webkit-keyframes sk-threeStrings-rotateTwo{0%{transform:rotateX(50deg) rotateY(10deg) rotate(0deg)}to{transform:rotateX(50deg) rotateY(10deg) rotate(1turn)}}@keyframes sk-threeStrings-rotateTwo{0%{transform:rotateX(50deg) rotateY(10deg) rotate(0deg)}to{transform:rotateX(50deg) rotateY(10deg) rotate(1turn)}}@-webkit-keyframes sk-threeStrings-rotateThree{0%{transform:rotateX(35deg) rotateY(55deg) rotate(0deg)}to{transform:rotateX(35deg) rotateY(55deg) rotate(1turn)}}@keyframes sk-threeStrings-rotateThree{0%{transform:rotateX(35deg) rotateY(55deg) rotate(0deg)}to{transform:rotateX(35deg) rotateY(55deg) rotate(1turn)}}.sk-wandering-cubes{height:100%;margin:auto;position:relative;text-align:center;width:100%}.sk-wandering-cubes>div{-webkit-animation:sk-wanderingCubes-cubeMove 1.8s ease-in-out infinite;animation:sk-wanderingCubes-cubeMove 1.8s ease-in-out infinite;background-color:currentColor;height:25%;left:0;position:absolute;top:0;width:25%}.sk-wandering-cubes>div:nth-child(2){-webkit-animation-delay:-.9s;animation-delay:-.9s}@-webkit-keyframes sk-wanderingCubes-cubeMove{25%{transform:translateX(290%) rotate(-90deg) scale(.5)}50%{transform:translateX(290%) translateY(290%) rotate(-179deg)}50.1%{transform:translateX(290%) translateY(290%) rotate(-180deg)}75%{transform:translateX(0) translateY(290%) rotate(-270deg) scale(.5)}to{transform:rotate(-1turn)}}@keyframes sk-wanderingCubes-cubeMove{25%{transform:translateX(290%) rotate(-90deg) scale(.5)}50%{transform:translateX(290%) translateY(290%) rotate(-179deg)}50.1%{transform:translateX(290%) translateY(290%) rotate(-180deg)}75%{transform:translateX(0) translateY(290%) rotate(-270deg) scale(.5)}to{transform:rotate(-1turn)}}"]
            }]
    }], function () { return [{ type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.DomSanitizer }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ChangeDetectorRef }, { type: NgxUiLoaderService }]; }, { bgsColor: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], bgsOpacity: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], bgsPosition: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], bgsSize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], bgsType: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], fgsColor: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], fgsPosition: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], fgsSize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], fgsType: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], gap: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], loaderId: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], logoPosition: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], logoSize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], logoUrl: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], overlayBorderRadius: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], overlayColor: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], pbColor: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], pbDirection: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], pbThickness: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], hasProgressBar: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], text: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], textColor: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], textPosition: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], bgsTemplate: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], fgsTemplate: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }] }); })();

class NgxUiLoaderBlurredDirective {
    constructor(elementRef, renderer, loader) {
        this.elementRef = elementRef;
        this.renderer = renderer;
        this.loader = loader;
        this.blur = this.loader.getDefaultConfig().blur;
        this.loaderId = this.loader.getDefaultConfig().masterLoaderId;
        this.fastFadeOut = this.loader.getDefaultConfig().fastFadeOut;
    }
    /**
     * On Init event
     */
    ngOnInit() {
        this.showForegroundWatcher = this.loader.showForeground$
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.filter)((showEvent) => this.loaderId === showEvent.loaderId))
            .subscribe(data => {
            if (data.isShow) {
                const filterValue = `blur(${this.blur}px)`;
                this.renderer.setStyle(this.elementRef.nativeElement, '-webkit-filter', filterValue);
                this.renderer.setStyle(this.elementRef.nativeElement, 'filter', filterValue);
            }
            else {
                setTimeout(() => {
                    if (!this.loader.hasRunningTask(FOREGROUND, data.loaderId)) {
                        this.renderer.setStyle(this.elementRef.nativeElement, '-webkit-filter', 'none');
                        this.renderer.setStyle(this.elementRef.nativeElement, 'filter', 'none');
                    }
                }, this.fastFadeOut ? FAST_OVERLAY_DISAPPEAR_TIME : OVERLAY_DISAPPEAR_TIME);
            }
        });
    }
    /**
     * On destroy event
     */
    ngOnDestroy() {
        if (this.showForegroundWatcher) {
            this.showForegroundWatcher.unsubscribe();
        }
    }
}
NgxUiLoaderBlurredDirective.ɵfac = function NgxUiLoaderBlurredDirective_Factory(t) { return new (t || NgxUiLoaderBlurredDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](NgxUiLoaderService)); };
NgxUiLoaderBlurredDirective.ɵdir = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineDirective"]({ type: NgxUiLoaderBlurredDirective, selectors: [["", "ngxUiLoaderBlurred", ""]], inputs: { blur: "blur", loaderId: "loaderId" } });
NgxUiLoaderBlurredDirective.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2 },
    { type: NgxUiLoaderService }
];
NgxUiLoaderBlurredDirective.propDecorators = {
    blur: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    loaderId: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }]
};
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxUiLoaderBlurredDirective, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Directive,
        args: [{ selector: '[ngxUiLoaderBlurred]' }]
    }], function () { return [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2 }, { type: NgxUiLoaderService }]; }, { blur: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], loaderId: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }] }); })();

class NgxUiLoaderModule {
    /**
     * forRoot
     * @returns A module with its provider dependencies
     */
    static forRoot(ngxUiLoaderConfig) {
        return {
            ngModule: NgxUiLoaderModule,
            providers: [
                {
                    provide: NGX_UI_LOADER_CONFIG_TOKEN,
                    useValue: ngxUiLoaderConfig
                }
            ]
        };
    }
}
NgxUiLoaderModule.ɵfac = function NgxUiLoaderModule_Factory(t) { return new (t || NgxUiLoaderModule)(); };
NgxUiLoaderModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: NgxUiLoaderModule });
NgxUiLoaderModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule]] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxUiLoaderModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgModule,
        args: [{
                imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule],
                declarations: [NgxUiLoaderComponent, NgxUiLoaderBlurredDirective],
                exports: [NgxUiLoaderComponent, NgxUiLoaderBlurredDirective]
            }]
    }], null, null); })();
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](NgxUiLoaderModule, { declarations: function () { return [NgxUiLoaderComponent, NgxUiLoaderBlurredDirective]; }, imports: function () { return [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule]; }, exports: function () { return [NgxUiLoaderComponent, NgxUiLoaderBlurredDirective]; } }); })();

/**
 * Injection token for ngx-ui-loader-router configuration
 */
const NGX_UI_LOADER_ROUTER_CONFIG_TOKEN = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.InjectionToken('ngxUiLoaderRouterCustom.config');

function getExcludeObj(config) {
    let strs;
    let regExps;
    if (config) {
        if (config.exclude) {
            strs = config.exclude.map(url => url.toLowerCase());
        }
        if (config.excludeRegexp) {
            regExps = config.excludeRegexp.map(regexp => new RegExp(regexp, 'i'));
        }
    }
    return { strs, regExps };
}
function isIgnored(url, excludeStrings, excludeRegexps) {
    if (excludeStrings) {
        // do not show the loader for urls in the `exclude` list
        if (excludeStrings.findIndex(str => url.toLowerCase().startsWith(str)) !== -1) {
            return true;
        }
    }
    if (excludeRegexps) {
        // do not show the loader for urls which matches regexps in the `excludeRegexp` list
        if (excludeRegexps.findIndex(regexp => regexp.test(url)) !== -1) {
            return true;
        }
    }
    return false;
}

class NgxUiLoaderRouterModule {
    /**
     * Constructor
     */
    constructor(parentModule, customConfig, router, loader) {
        if (parentModule) {
            throw new Error('[ngx-ui-loader] - NgxUiLoaderRouterModule is already loaded. It should be imported in the root `AppModule` only!');
        }
        let config = {
            loaderId: loader.getDefaultConfig().masterLoaderId,
            showForeground: true
        };
        this.exclude = getExcludeObj(customConfig);
        if (customConfig) {
            config = Object.assign(Object.assign({}, config), customConfig);
        }
        router.events.subscribe((event) => {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__.NavigationStart) {
                if (!isIgnored(event.url, this.exclude.strs, this.exclude.regExps)) {
                    if (config.showForeground) {
                        loader.startLoader(config.loaderId, ROUTER_LOADER_TASK_ID);
                    }
                    else {
                        loader.startBackgroundLoader(config.loaderId, ROUTER_LOADER_TASK_ID);
                    }
                }
            }
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__.NavigationEnd || event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__.NavigationCancel || event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_5__.NavigationError) {
                if (!isIgnored(event.url, this.exclude.strs, this.exclude.regExps)) {
                    if (config.showForeground) {
                        loader.stopLoader(config.loaderId, ROUTER_LOADER_TASK_ID);
                    }
                    else {
                        loader.stopBackgroundLoader(config.loaderId, ROUTER_LOADER_TASK_ID);
                    }
                }
            }
        });
    }
    /**
     * forRoot
     * @returns A module with its provider dependencies
     */
    static forRoot(routerConfig) {
        return {
            ngModule: NgxUiLoaderRouterModule,
            providers: [
                {
                    provide: NGX_UI_LOADER_ROUTER_CONFIG_TOKEN,
                    useValue: routerConfig
                }
            ]
        };
    }
}
NgxUiLoaderRouterModule.ɵfac = function NgxUiLoaderRouterModule_Factory(t) { return new (t || NgxUiLoaderRouterModule)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](NgxUiLoaderRouterModule, 12), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](NGX_UI_LOADER_ROUTER_CONFIG_TOKEN, 8), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__.Router), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](NgxUiLoaderService)); };
NgxUiLoaderRouterModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: NgxUiLoaderRouterModule });
NgxUiLoaderRouterModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({});
NgxUiLoaderRouterModule.ctorParameters = () => [
    { type: NgxUiLoaderRouterModule, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Optional }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.SkipSelf }] },
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Optional }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [NGX_UI_LOADER_ROUTER_CONFIG_TOKEN,] }] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: NgxUiLoaderService }
];
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxUiLoaderRouterModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgModule,
        args: [{}]
    }], function () { return [{ type: NgxUiLoaderRouterModule, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Optional
            }, {
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.SkipSelf
            }] }, { type: undefined, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Optional
            }, {
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [NGX_UI_LOADER_ROUTER_CONFIG_TOKEN]
            }] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router }, { type: NgxUiLoaderService }]; }, null); })();

/**
 * Injection token for ngx-ui-loader-http configuration
 */
const NGX_UI_LOADER_HTTP_CONFIG_TOKEN = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.InjectionToken('ngxUiLoaderHttpCustom.config');

class NgxUiLoaderHttpInterceptor {
    /**
     * Constructor
     */
    constructor(customConfig, loader) {
        this.loader = loader;
        this.count = 0;
        this.config = {
            loaderId: this.loader.getDefaultConfig().masterLoaderId,
            showForeground: false
        };
        this.exclude = getExcludeObj(customConfig);
        if (customConfig) {
            this.config = Object.assign(Object.assign({}, this.config), customConfig);
        }
    }
    intercept(req, next) {
        if (isIgnored(req.url, this.exclude.strs, this.exclude.regExps)) {
            return next.handle(req);
        }
        this.count++;
        if (this.config.showForeground) {
            this.loader.startLoader(this.config.loaderId, HTTP_LOADER_TASK_ID, this.config);
        }
        else {
            this.loader.startBackgroundLoader(this.config.loaderId, HTTP_LOADER_TASK_ID, this.config);
        }
        return next.handle(req).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.finalize)(() => {
            this.count--;
            if (this.count === 0) {
                if (this.config.showForeground) {
                    this.loader.stopLoader(this.config.loaderId, HTTP_LOADER_TASK_ID);
                }
                else {
                    this.loader.stopBackgroundLoader(this.config.loaderId, HTTP_LOADER_TASK_ID);
                }
            }
        }));
    }
}
NgxUiLoaderHttpInterceptor.ɵfac = function NgxUiLoaderHttpInterceptor_Factory(t) { return new (t || NgxUiLoaderHttpInterceptor)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](NGX_UI_LOADER_HTTP_CONFIG_TOKEN, 8), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](NgxUiLoaderService)); };
NgxUiLoaderHttpInterceptor.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: NgxUiLoaderHttpInterceptor, factory: NgxUiLoaderHttpInterceptor.ɵfac });
NgxUiLoaderHttpInterceptor.ctorParameters = () => [
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Optional }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject, args: [NGX_UI_LOADER_HTTP_CONFIG_TOKEN,] }] },
    { type: NgxUiLoaderService }
];
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxUiLoaderHttpInterceptor, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Injectable
    }], function () { return [{ type: undefined, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Optional
            }, {
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
                args: [NGX_UI_LOADER_HTTP_CONFIG_TOKEN]
            }] }, { type: NgxUiLoaderService }]; }, null); })();

class NgxUiLoaderHttpModule {
    /**
     * Constructor
     */
    constructor(parentModule) {
        if (parentModule) {
            throw new Error('[ngx-ui-loader] - NgxUiLoaderHttpModule is already loaded. It should be imported in the root `AppModule` only!');
        }
    }
    /**
     * forRoot
     * @returns A module with its provider dependencies
     */
    static forRoot(httpConfig) {
        return {
            ngModule: NgxUiLoaderHttpModule,
            providers: [
                {
                    provide: NGX_UI_LOADER_HTTP_CONFIG_TOKEN,
                    useValue: httpConfig
                }
            ]
        };
    }
}
NgxUiLoaderHttpModule.ɵfac = function NgxUiLoaderHttpModule_Factory(t) { return new (t || NgxUiLoaderHttpModule)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](NgxUiLoaderHttpModule, 12)); };
NgxUiLoaderHttpModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: NgxUiLoaderHttpModule });
NgxUiLoaderHttpModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ providers: [
        {
            provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__.HTTP_INTERCEPTORS,
            useClass: NgxUiLoaderHttpInterceptor,
            multi: true
        }
    ] });
NgxUiLoaderHttpModule.ctorParameters = () => [
    { type: NgxUiLoaderHttpModule, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Optional }, { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.SkipSelf }] }
];
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgxUiLoaderHttpModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgModule,
        args: [{
                providers: [
                    {
                        provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_7__.HTTP_INTERCEPTORS,
                        useClass: NgxUiLoaderHttpInterceptor,
                        multi: true
                    }
                ]
            }]
    }], function () { return [{ type: NgxUiLoaderHttpModule, decorators: [{
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Optional
            }, {
                type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.SkipSelf
            }] }]; }, null); })();

/*
 * Public API Surface of ngx-ui-loader
 */

/**
 * Generated bundle index. Do not edit.
 */





/***/ })

}]);
//# sourceMappingURL=src_app_views_forms_forms_module_ts.js.map