"use strict";
(self["webpackChunkegret"] = self["webpackChunkegret"] || []).push([["common"],{

/***/ 80997:
/*!*******************************************************!*\
  !*** ./src/app/shared/animations/egret-animations.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "egretAnimations": () => (/* binding */ egretAnimations)
/* harmony export */ });
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/animations */ 31631);

const reusable = (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.animation)([
    (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.style)({
        opacity: '{{opacity}}',
        transform: 'scale({{scale}}) translate3d({{x}}, {{y}}, {{z}})',
    }),
    (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.animate)('{{duration}} {{delay}} cubic-bezier(0.0, 0.0, 0.2, 1)', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.style)('*')),
], {
    params: {
        duration: '200ms',
        delay: '0ms',
        opacity: '0',
        scale: '1',
        x: '0',
        y: '0',
        z: '0',
    },
});
const egretAnimations = [
    (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.trigger)('animate', [(0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.transition)('void => *', [(0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.useAnimation)(reusable)])]),
    (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.trigger)('fadeInOut', [
        (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.state)('0', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.style)({
            opacity: 0,
            display: 'none',
        })),
        (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.state)('1', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.style)({
            opacity: 1,
            display: 'block',
        })),
        (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.transition)('0 => 1', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.animate)('300ms')),
        (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.transition)('1 => 0', (0,_angular_animations__WEBPACK_IMPORTED_MODULE_0__.animate)('300ms')),
    ]),
];


/***/ })

}]);
//# sourceMappingURL=common.js.map